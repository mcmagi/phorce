# Phorce #

Phorce is a multi-tiered web development framework for PHP. Consists of a Front-Controller MVC design, Extended PHP presentation templates, IoC container, and an XML/DB persistence layer.

### How do I get set up? ###

TBD

### Who do I talk to? ###

* [Michael C. Maggio](mailto:mcmaggio@mcmagi.com)