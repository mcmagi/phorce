@echo off

php fusionGenerate.php classes ^
 -s ../config/schema/phorce-config.xsd ^
 -b ../lib/phorce/common/config/phorce-config-binding.xml ^
 -n phorce\common\config ^
 -o ../lib ^
 -d PhorceConfigDAO
