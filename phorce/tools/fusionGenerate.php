<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

/**
 * Command-line wrapper to the FusionGenerator class.  See
 * <code>php fusionGenerate -h</code> for usage details.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsg.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */

require_once "../init.php";

use phorce\fusion\generator\FusionGenerator;

$gen = new FusionGenerator();

for ($i = 1; $i < $argc && empty($error); $i++)
{
    if ($i == 1)
    {
        $gen->mode = $argv[$i];
        if ($gen->mode != FusionGenerator::MODE_BINDING &&
            $gen->mode != FusionGenerator::MODE_CLASSES)
        {
            $error = "Invalid input mode '{$gen->mode}'.";
            $help = true;
        }
        continue;
    }

    switch ($argv[$i])
    {
    // schema
    case "-s":
        $gen->schema = $argv[++$i];
        break;

    // namespace
    case "-n":
        $ns = $argv[++$i];
        $eq = strpos($ns, "=");

        if ($eq === false)
            $gen->namespace = $ns;
        else
        {
            list($xmlns, $phpns) = explode("=", $ns);
            $gen->addImportNamespace($xmlns, $phpns);
        }

        break;

    // binding
    case "-b":
        $gen->binding = $argv[++$i];
        break;

    // output directory
    case "-o":
        $gen->outputDir = $argv[++$i];
        break;

    // output directory
    case "-d":
        $gen->daoClass = $argv[++$i];
        break;

    // help mode
    case "-h":
        $help = true;
        break;

    // catch-all
    default:
        $error = "Unrecognized parameter '{$argv[$i]}'";
        $help = true;
    }
}

if (! isset($gen->mode))
{
    $error = "Please specify an output mode.";
    $help = true;
}

if (isset($help) && $help)
{
    if (isset($error))
        echo "Error: $error\n";

    // output help message
    echo <<<EOT

    php fusionGenerator <mode> [-s <schema>] [-b <binding>] [-o <outdir>]
                               [-n <phpns>] [-n <phpns>=<xmlns> ...] [-d <dao>]

    mode            One of the following:
                      classes => generates bindings and classes from the schema
                                 or classes from the binding file
                      binding => generates the binding file from the schema
    -s file         The schema file from which bindings and classes
                    will be generated
    -n phpns        The PHP namespace where classes for the target XML
                    namespace will be output
    -n xmlns=phpns  The PHP namespace where classes for an imported XML
                    namespace will be output.  There may be more than one
                    of these options.
    -b file         If -s is given, the binding file to generate
                    Othwerwise, the binding file to be read as input
    -o dir          The directory where output files will be written
    -d dao          The DAO class name to generate

EOT;
}
else
{
    // run the generator
    $gen->generate();
}

?>
