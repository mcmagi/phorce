@echo off

echo Generating classes...

php fusionGenerate.php classes ^
 -s ../config/schema/fusion-binding.xsd ^
 -b ../lib/phorce/fusion/binding/fusion-binding.xml ^
 -n phorce\fusion\binding ^
 -o ../lib

php fusionGenerate.php classes ^
 -s ../config/schema/fusion-binding-xml.xsd ^
 -b ../lib/phorce/fusion/binding/xml/fusion-binding-xml.xml ^
 -n phorce\fusion\binding\xml ^
 -o ../lib

php fusionGenerate.php classes ^
 -s ../config/schema/fusion-config.xsd ^
 -b ../lib/phorce/fusion/config/fusion-config.xml ^
 -n phorce\fusion\config ^
 -o ../lib

echo Updating license information...

php updateLicense.php ../lib/phorce/fusion/binding
php updateLicense.php ../lib/phorce/fusion/config

