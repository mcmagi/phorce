@echo off

php fusionGenerate.php classes ^
 -s ../config/schema/reaction-config.xsd ^
 -b ../lib/phorce/reaction/config/reaction-config-binding.xml ^
 -n phorce\reaction\config ^
 -o ../lib ^
 -d ReactionConfigDAO

php fusionGenerate.php classes ^
 -s ../config/schema/tiles-config.xsd ^
 -b ../lib/phorce/reaction/tiles/config/tiles-config-binding.xml ^
 -n phorce\reaction\tiles\config ^
 -o ../lib ^
 -d TilesConfigDAO
