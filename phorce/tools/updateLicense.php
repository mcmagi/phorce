<?php

main($argc, $argv);

function getFileIterator($path)
{
    $filelist = array();
    return new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($path),
        RecursiveIteratorIterator::SELF_FIRST);
}

function updateAuthorship($from)
{
    /* $to = preg_replace('/@author.*$/',
        '@author Michael C. Maggio <mcmaggio@mcmagi.com>', $from); */
    $to = $from;
    $to .= " * @copyright Copyright (c) 2008-2009, Michael C. Maggio\r\n";
    $to .= " * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3\r\n";
    return $to;
}

function updateLicense($from)
{
    $to = $from;
    $to .= '/*' . PHP_EOL;
    $to .= ' * The contents of this file are subject to the terms of the GNU Lesser' . PHP_EOL;
    $to .= ' * General Public License Version 3 ("LGPL").  You may not use this' . PHP_EOL;
    $to .= ' * file except in compliance with the License.  When distributing the' . PHP_EOL;
    $to .= ' * software, include this License Header Notice in each file and' . PHP_EOL;
    $to .= ' * include the License file at phorce/LICENSE.txt.' . PHP_EOL;
    $to .= ' */' . PHP_EOL;
    return $to;
}

function updatePhpFiles($dir)
{
    $i = 0;
    foreach (getFileIterator($dir) as $name => $file)
    {
        if (preg_match('/\.php$/', $name))
        {
            echo "$name\n";

            $fp = $file->openFile('r+');
            $output = "";

            while (! $fp->eof())
            {
                $line = $fp->fgets();
                if ($fp->eof())
                    break;

                if (preg_match('/^ *\* @author /', $line))
                    $line = updateAuthorship($line);

                if (preg_match('/^<\?php/', $line))
                    $line = updateLicense($line);

                $output .= $line;

                //$line = chop($line) . PHP_EOL;
            }

            //echo $output;

            $fp->seek(0);
            $fp->ftruncate(0);
            $fp->fwrite($output);

            // just do first 1
            //if ($i++ >= 1)
                //break;
        }
    }
}

function main($argc, $argv)
{
    for ($i = 1; $i < $argc; $i++)
    {
        $dir = $argv[$i];
        if (! file_exists($dir))
            echo "File/directory '{$dir}' does not exist.", PHP_EOL;
        else
            updatePhpFiles($dir);
    }
}

?>
