@echo off

php fusionGenerate.php classes ^
 -s ../config/schema/inertia-config.xsd ^
 -b ../lib/phorce/inertia/config/inertia-config-binding.xml ^
 -n phorce\inertia\config ^
 -o ../lib ^
 -d InertiaConfigDAO

php fusionGenerate.php classes ^
 -s ../config/schema/inertia-tld.xsd ^
 -b ../lib/phorce/inertia/tld/inertia-tld-binding.xml ^
 -n phorce\inertia\tld ^
 -o ../lib ^
 -d TaglibDescriptorDAO

