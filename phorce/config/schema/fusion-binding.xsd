<?xml version="1.0"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:tns="http://www.phorce.org/schema/fusion/binding"
    targetNamespace="http://www.phorce.org/schema/fusion/binding"
    elementFormDefault="qualified"
    attributeFormDefault="unqualified">
    <xsd:annotation>
        <xsd:documentation>
            The contents of this file are subject to the terms of the GNU Lesser
            General Public License Version 3 ("LGPL").  You may not use this
            file except in compliance with the License.  When distributing the
            software, include this License Header Notice in each file and
            include the License file at phorce/LICENSE.txt.
        </xsd:documentation>
        <xsd:documentation>
            This schema defines the structure of a binding document for the
            Fusion XML Binding Framework.  A binding document represents the
            association between a data object and a data resource.  While
            Fusion's primary data resource is XML, the binding schema was
            designed to be extensible such that other data resources may be
            added and therefore not limited soley to XML.  Gravity, for example,
            provides relational database bindings by extending this schema.

            The binding schema deconstructs a data resource into two key
            concepts: data structures and data elements.  A data structure, as
            its name implies, is a structured component of a data resource.
            In XML, for example a complex type (consisting of attributes and
            elements) is considered a data structure.  For a database, a data
            structure would correspond to a table.  The role of the binding
            schema in this case is to correlate these data structures to classes
            in application code.

            A data element is a sub-component of a data structure.  For example,
            in XML, the elements and attributes of a complex type are the data
            elements.  For a database, the data elements are the columns of the
            table.  The binding schema maps the data elements of a data
            structure to properties of a class.

            An additional concept employed by the binding schema is a data
            adapter.  A data adapter is used for special cases to adapt a data
            element's value to a property value, and vice-versa.  This may be
            used to transform raw data values to a particular class or type as
            needed by the data object's class.
        </xsd:documentation>
    </xsd:annotation>
    <xsd:element name="fusion-binding" type="tns:FusionBinding" nillable="false">
        <xsd:annotation>
            <xsd:documentation>
                This is the root element of the schema.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:key name="ClassKey">
            <xsd:selector xpath=".//tns:class" />
            <xsd:field xpath="@name" />
        </xsd:key>
    </xsd:element>
    <xsd:complexType name="FusionBinding">
        <xsd:annotation>
            <xsd:documentation>
                The FusionBinding type is the root type of a binding definition
                XML file.  It may contain one or more classes that are
                represented in the binding.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="0">
            <xsd:element name="class" type="tns:ClassBinding"
                minOccurs="0" maxOccurs="unbounded">
                <xsd:key name="PropertyKey">
                    <xsd:selector xpath=".//tns:property" />
                    <xsd:field xpath="@name" />
                </xsd:key>
            </xsd:element>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:complexType name="ClassBinding">
        <xsd:annotation>
            <xsd:documentation>
                The ClassBinding type contains all the binding information for
                a particular class definition.  It will contain a description
                of the resource we are binding to (via DataStructureBinding)
                and a list of all the properties on the class that will be
                bound to that resource (via PropertyBinding).
            </xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="0">
            <xsd:any namespace="##any" minOccurs="1" maxOccurs="unbounded">
                <xsd:annotation>
                    <xsd:documentation>
                        This will contain the data resource binding information
                        for the class.  Despite being represented by an xsd:any
                        type, the actual type used MUST extend
                        DataStructureBinding.  There must also be at least one
                        specified.
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:any>
            <xsd:element name="key" type="tns:KeyBinding"
                minOccurs="0" maxOccurs="1">
                <xsd:annotation>
                    <xsd:documentation>
                        Represents a property which acts as a unique identifier
                        for the class.  The key is optional.
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="property" type="tns:PropertyBinding"
                minOccurs="0" maxOccurs="unbounded">
                <xsd:annotation>
                    <xsd:documentation>
                        Represents all non-key properties on the class.
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
        </xsd:sequence>
        <xsd:attribute name="name" type="xsd:string" />
        <xsd:attribute name="type" type="xsd:string" />
    </xsd:complexType>
    <xsd:complexType name="DataStructureBinding" abstract="true">
        <xsd:annotation>
            <xsd:documentation>
                The DataStructureBinding type describes how a class will be
                bound to a data resource.  A data resource would include an XML
                data type, a database table, or an LDAP object class.  It is
                defined as abstract with no properties since all information
                pertaining to that data resource will depend on the type of
                implementation.
            </xsd:documentation>
        </xsd:annotation>
    </xsd:complexType>
    <xsd:complexType name="KeyBinding">
        <xsd:annotation>
            <xsd:documentation>
                The KeyBinding type contains the binding information for a key
                property of a particular class.  A key may be a single data
                element, an adapted data element, or a set of other properties.
                The latter is true only for compound keys.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:choice>
            <xsd:any namespace="##any" minOccurs="1" maxOccurs="1">
                <xsd:annotation>
                    <xsd:documentation>
                        This will describe the data element the key property is
                        bound to.  Despite being represented by an xsd:any type,
                        the actual type used MUST extend either
                        DataElementBinding or DataAdapterBinding (if the data
                        is to be adapted to the key property).  This is only
                        populated as such if the key is single-valued.
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:any>
            <xsd:element name="property" type="tns:PropertyBinding"
                minOccurs="1" maxOccurs="unbounded">
                <xsd:annotation>
                    <xsd:documentation>
                        The properties will be populated if this key is a
                        compound key.
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
        </xsd:choice>
        <xsd:attributeGroup ref="tns:PropertyAttrGroup" />
    </xsd:complexType>
    <xsd:complexType name="PropertyBinding">
        <xsd:annotation>
            <xsd:documentation>
                The PropertyBinding type contains the binding information for
                a particular class property.  This includes the name and type
                of the property and the data element it is bound to.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="0">
            <xsd:any namespace="##any" minOccurs="1" maxOccurs="1">
                <xsd:annotation>
                    <xsd:documentation>
                        This will describe the data element the property is
                        bound to.  Despite being represented by an xsd:any type,
                        the actual type used MUST extend either
                        DataElementBinding or DataAdapterBinding (if the data
                        is to be adapted to the property).
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:any>
        </xsd:sequence>
        <xsd:attributeGroup ref="tns:PropertyAttrGroup" />
        <xsd:attribute name="many" type="xsd:boolean" use="optional"
                default="false">
            <xsd:annotation>
                <xsd:documentation>
                    True if this property will contain more than one of the
                    represented data element.  This will typically manifest
                    itself in corresponding code as an array of values.
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="readonly" type="xsd:boolean" use="optional"
                default="false">
            <xsd:annotation>
                <xsd:documentation>
                    True if this property is read-only.
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:complexType>
    <xsd:complexType name="DataElementBinding" abstract="true">
        <xsd:annotation>
            <xsd:documentation>
                The DataElementBinding type describes how a property will be
                bound to a data element on the data resource.  It is defined as
                abstract with no properties since all information pertaining to
                the data resource will be implementation-dependant.
            </xsd:documentation>
        </xsd:annotation>
    </xsd:complexType>
    <xsd:complexType name="DataAdapterBinding" abstract="true">
        <xsd:annotation>
            <xsd:documentation>
                The DataAdapterBinding type describes what (if any)
                conversions need to take place on the content of the data
                element in order to be bound to the property.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:sequence>
            <xsd:any namespace="##any" minOccurs="1" maxOccurs="1">
                <xsd:annotation>
                    <xsd:documentation>
                        This will describe the data element it is bound to.
                        Despite being represented by an xsd:any type, the actual
                        type used MUST extend either DataElementBinding or
                        DataAdapterBinding (if the data is to be adapted to the
                        property).
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:any>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:element name="adapter">
        <xsd:annotation>
            <xsd:documentation>
                The 'adapter' element is a general-purpose adapter that
                developers may use without having to provide their own XML
                schema definition.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:complexType>
            <xsd:complexContent>
                <xsd:extension base="tns:DataAdapterBinding">
                    <xsd:attribute name="class" type="xsd:string">
                        <xsd:annotation>
                            <xsd:documentation>
                                The class name which will act as the adapter.
                            </xsd:documentation>
                        </xsd:annotation>
                    </xsd:attribute>
                </xsd:extension>
            </xsd:complexContent>
        </xsd:complexType>
    </xsd:element>
    <xsd:simpleType name="PropertyType">
        <xsd:annotation>
            <xsd:documentation>
                Contains the enumeration of valid values for the "type"
                field of the Property type.
            </xsd:documentation>
        </xsd:annotation>
        <xsd:restriction base="xsd:string">
            <xsd:enumeration value="boolean" />
            <xsd:enumeration value="integer" />
            <xsd:enumeration value="float" />
            <xsd:enumeration value="string" />
            <xsd:enumeration value="object" />
            <xsd:enumeration value="mixed" />
        </xsd:restriction>
    </xsd:simpleType>
    <xsd:attributeGroup name="PropertyAttrGroup">
        <xsd:attribute name="name" type="xsd:string">
            <xsd:annotation>
                <xsd:documentation>
                    A unique name for this property.
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="type" type="tns:PropertyType">
            <xsd:annotation>
                <xsd:documentation>
                    The type of data this property will contain.
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="class" type="xsd:string">
            <xsd:annotation>
                <xsd:documentation>
                    The namespace-qualified name of the class this property will
                    contain.  Only applies to properties where type="object".
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:attributeGroup>
</xsd:schema>
