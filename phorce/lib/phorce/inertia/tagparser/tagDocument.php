<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */
/**
 * Generates tag documentation as an html file, derived from the TLD file.  The
 * taglib name must be supplied on the command line.  It expects to find the TLD
 * file in the config directory.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagParser
 */

if (file_exists("config"))
{
    require_once "config/siteconfig.php";
}
else
{
    require_once "../../../../config/siteconfig.php";
}
require_once "phorce/inertia/tagparser/TagDocumentor.php";
require_once "phorce/support/xml/XMLElementWriter.php";


if (count($argv) < 2)
{
    echo "supply a taglib name\n";
    exit;
}

$taglib = $argv[1];

$documentor = new TagDocumentor($taglib);
$html = $documentor->getDocumentation();

/* $fh = fopen("$name-taglib.html", "w")
fwrite($fh, XMLElementWriter::writeElement( */

XMLElementWriter::writeFile("$taglib-taglib.html", $html);

?>
