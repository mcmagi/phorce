<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

$sQuotedStrRegExp = "'([^']*)'"; // no single quotes w/i single quotes
$dQuotedStrRegExp = '"(([^"]|\\\\")*)"'; // double quotes allowed within single quotes if escaped
$phpCodeRegExp = "<\?($sQuotedStrRegExp|$dQuotedStrRegExp|[^?])*\?>"; // php code ends with >, but can have it in quotes
$tagRegExp = "<($phpCodeRegExp|$sQuotedStrRegExp|$dQuotedStrRegExp|[^>])*>";

// define regular expression constants
define("REGEXP_SINGLE_QUOTED_STR", $sQuotedStrRegExp);
define("REGEXP_DOUBLE_QUOTED_STR", $dQuotedStrRegExp);
define("REGEXP_PHP_CODE", $phpCodeRegExp);
define("REGEXP_TAG", $tagRegExp);

define("TAGINFO_START_TAG_TYPE", "start");
define("TAGINFO_END_TAG_TYPE", "end");
define("TAGINFO_EMPTY_TAG_TYPE", "empty");

?>
