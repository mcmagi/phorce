<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\tagparser;

use phorce\common\property\PropertyObject;

/**
 * Represents an custom tag Element in the source document.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagParser
 */
class Element extends PropertyObject
{
    /**
     * Tag data pertaining to start of element.
     * @access private
     * @var object TagData
     */
    private $_startTagData;

    /**
     * Tag data pertaining to end of element.
     * @access private
     * @var object TagData
     */
    private $_endTagData;

    /**
     * HTML code contained within element.
     * @access private
     * @var string
     */
    private $_innerHtml;

    /**
     * Parent element.
     * @access private
     * @var object Element
     */
    private $_parent;

    /**
     * Any elements nested within this element.
     * @access private
     * @var array
     */
    private $_nestedElements = array();

    /**
     * Offset of this element within the parent element's innerHtml.
     * @access private
     * @var integer
     */
    private $_offset;

    /**
     * A unique identifier for this object for debugging purposes.
     * @access private
     * @var integer
     */
    private $_id = -1;


    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->getId();
    }

    /**
     * Returns true if this Element has no parent object.
     * @return bool Boolean
     */
    public function isRoot()
    {
        return is_null($this->_parent);
    }

    /**
     * Returns true if this Element has null content.
     * @return bool Boolean
     */
    public function isEmpty()
    {
        return is_null($this->_innerHtml);
    }

    /**
     * Returns the name of the start tag.
     * @return string Element name
     */
    public function getElementName()
    {
        return is_null($this->_startTagData) ? null : $this->_startTagData->getTagName();
    }

    /**
     * Returns the TagData associated with the start tag.  For empty elements,
     * this returns an TagData of type TAGINFO_EMPTY_TAG_TYPE.
     * @return object TagData
     */
    public function getStartTagData()
    {
        return $this->_startTagData;
    }

    /**
     * Sets the TagData associated with the start tag.
     * @param object TagData $tag
     */
    public function setStartTagData(TagData $tag)
    {
        $this->_startTagData = $tag;
    }

    /**
     * Returns the TagData associated with the end tag.  For empty elements,
     * this returns null.
     * @return object TagData
     */
    public function getEndTagData()
    {
        return $this->_endTagData;
    }

    /**
     * Sets the TagData associated with the end tag.
     * @param object TagData $tag
     */
    public function setEndTagData(TagData $tag)
    {
        $this->_endTagData = &$tag;
    }

    /**
     * Returns the untranslated HTML sourcecode between the start and end tags
     * of this element block.  For empty elements, this returns an empty string.
     * @return string HTML text
     */
    public function &getInnerHtml()
    {
        return $this->_innerHtml;
    }

    /**
     * Sets the untranslated HTML sourcecode between the start and end tags
     * of this element block.
     * @param string $html HTML text
     */
    public function setInnerHtml($html)
    {
        $this->_innerHtml =& $html;
    }

    /**
     * Returns the parent Element object.
     * @return object Element Parent Element
     */
    public function getParent()
    {
        return $this->_parent;
    }

    /**
     * Sets the parent Element object.
     * @param object $parent Parent Element
     */
    public function setParent(Element $parent)
    {
        $this->_parent = $parent;
    }

    /**
     * Returns an array of all sub-elements that call this element its parent.
     * @return array Array of Elements
     */
    public function &getNestedElements()
    {
        return $this->_nestedElements;
    }

    /**
     * Defines the array of all sub-elements that call this element its parent.
     * @param array $elements Array of Elements
     */
    public function setNestedElements(array &$elements)
    {
        $this->_nestedElements =& $elements;
    }

    /**
     * Adds a sub-element that calls this element its parent.
     * @param object Element $element An Element
     */
    public function addNestedElement(Element $element)
    {
        $element->setParent($this);
        $this->_nestedElements[] = $element;
    }

    /**
     * Returns the offset of this Element in its parent Element.
     * It should have no value on root Elements.
     * @return integer Integer
     */
    public function getOffset()
    {
        return $this->_offset;
    }

    /**
     * Sets the offset of this Element in its parent Element.
     * @param integer $offset Integer
     */
    public function setOffset($offset)
    {
        $this->_offset = $offset;
    }

    /**
     * Returns the size of the element block, including the start tag, end tag,
     * and body (inner html).
     * @return integer Integer
     */
    public function getLength()
    {
        $length = strlen($this->_innerHtml);

        if (! is_null($this->_startTagData))
            $length += $this->_startTagData->getTagLength();

        if (! is_null($this->_endTagData))
            $length += $this->_endTagData->getTagLength();

        return $length;
    }

    /**
     * Returns a unique id for this Element instance.
     * @return integer Unique integer
     * @staticvar integer $id Next unique integer
     */
    public function getId()
    {
        static $id = 0;

        if ($this->_id == -1)
            $this->_id = $id++;

        return $this->_id;
    }

    /**
     * Debugging.
     */
    private function _debug()
    {
        echo "<b>".$this->getElementName()." (".$this->_id.") innerHtml</b>: " . htmlentities($this->_innerHtml) . "<br>";
    }
}

?>
