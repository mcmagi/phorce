<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

require_once "phorce/inertia/tagparser/TagLoader.php";
require_once "phorce/support/xml/XMLElement.php";

/**
 * Generates documentation from Tag Library Definition (TLD) files.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagParser
 */
class TagDocumentor
{
    /**
     * Contains the name of the tag library.
     * @var string
     * @access private
     */
    private $_taglibName;


    /**
     * Creates a Tag Documentor.
     * @param string $taglibName Name of the tag library
     */
    public function __construct($taglibName)
    {
        $this->_taglibName = $taglibName;
    }

    /**
     * Returns the tag documentation as an XML tree of HTML tags.
     * @return object XMLElement
     */
    public function getDocumentation()
    {
        $taglib = TagLoader::_loadTagLibrary($this->_taglibName);

        $shortname = $taglib->getChildByName("short-name");

        // create html and page body
        $html = new XMLElement("html");
        $body = new XMLElement("body");
        $html->addChild($body);

        // header
        $h1 = new XMLElement("h1");
        $body->addChild($h1);
        $h1->setData($shortname->getData() . " taglib");

        // tag library documentation
        $libdoc = $taglib->getChildByName("description");
        $p1 = new XMLElement("p");
        $body->addChild($p1);
        if (! is_null($libdoc))
            $p1->setData($libdoc->getData());

        // this will contain a brief list of all tags in library
        $ul = new XMLElement("ul");
        $body->addChild($ul);

        // loop through tags
        foreach ($taglib->getChildrenByName("tag") as $tag)
        {
            // get name of tag
            $name = $tag->getChildByName("name");
            $tagname = $shortname->getData() . ":" . $name->getData();

            // get description of tag
            $tagdoc = $tag->getChildByName("description");

            // build anchor
            $a = new XMLElement("a");
            $a->setAttribute("name", $name->getData());
            $body->addChild($a);

            // add link to anchor to list of tags
            $li = new XMLElement("li");
            $ul->addChild($li);
            $link = new XMLElement("a");
            $link->setAttribute("href", "#" . $name->getData());
            $link->setData("<$tagname>");
            $li->addChild($link);
            $span = new XMLElement("span");
            if (! is_null($tagdoc))
            {
                // set a description with first sentance
                $arr = explode(".", $tagdoc->getData());
                if (count($arr) > 0)
                    $span->setData("- ".$arr[0].".");
            }
            $li->addChild($span);

            // build header with tag name
            $h2 = new XMLElement("h2");
            $h2->setData("<$tagname>");
            $body->addChild($h2);

            // build tag description paragraph
            $p = new XMLElement("p");
            if (! is_null($tagdoc))
                $p->setData($tagdoc->getData());
            $body->addChild($p);

            // create attribute table
            $table = new XMLElement("table");
            $table->setAttribute("border", "1");
            $body->addChild($table);
            $tbody = new XMLElement("tbody");
            $table->addChild($tbody);

            // loop through attributes
            foreach ($tag->getChildrenByName("attribute") as $attr)
            {
                // build table row
                $tr = new XMLElement("tr");
                $tbody->addChild($tr);

                // get required/rtexprvalue fields
                $required = $attr->getChildByName("required");
                $rtexprvalue = $attr->getChildByName("rtexprvalue");

                // build name cell
                $name = $attr->getChildByName("name");
                $td1 = new XMLElement("td");
                $attrname = $name->getData();
                if ($required->getData() === "true")
                    $attrname = "* $attrname";
                $td1->setData($attrname);
                $tr->addChild($td1);

                // build description cell
                $attrdoc = $attr->getChildByName("description");
                $td2 = new XMLElement("td");
                $description = "";
                if (! is_null($attrdoc))
                    $description = $attrdoc->getData();
                if (! is_null($required) && $required->getData() === "true")
                    $description .= " [required]";
                if (! is_null($rtexprvalue) && $rtexprvalue->getData() === "true")
                    $description .= " [rtexprvalue]";
                $td2->setData($description);
                $tr->addChild($td2);
            }
        }

        return $html;
    }
}
?>
