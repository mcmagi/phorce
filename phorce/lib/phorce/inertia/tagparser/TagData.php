<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\tagparser;

use phorce\common\property\PropertyObject;

require_once "phorce/inertia/tagparser/constants.php";

/**
 * This class is used by the CustomTagParser to derive information about an
 * XML tag given its string representation.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagParser
 */
class TagData extends PropertyObject
{
    /**
     * String representation of this tag.
     * @access private
     * @var string
     */
    private $_tagStr;

    /**
     * The name of this tag.
     * @access private
     * @var string
     */
    private $_tagName;

    /**
     * The index of this tag in some block of HTML.
     * @access private
     * @var integer
     */
    private $_index;

    /**
     * A unique identifier for this object for debugging purposes.
     * @access private
     * @var integer
     */
    private $_id = -1;


    /**
     * Constructor.  Creates a TagData object based on the tag text.
     * @param string $tagStr A string representation of a tag (e.g. <tag attr="value">)
     */
    public function __construct($tagStr)
    {
        // save string representation of tag
        $this->_tagStr =& $tagStr;

        // derive tag name
        $this->_tagName = preg_replace("/^<\/?([^ \t\n]+).*>$/s","\${1}", $tagStr);

        // for debugging
        $this->getTagId();
    }

    /**
     * Returns the length of the tag text.
     * @return integer Integer
     */
    public function getTagLength()
    {
        return strlen($this->_tagStr);
    }

    /**
     * Returns the tag text passed to the constructor.
     * @return string Tag text
     */
    public function getTagContent()
    {
        return $this->_tagStr;
    }

    /**
     * Returns the name of this tag.
     * @return string Tag name
     */
    public function getTagName()
    {
        //echo "in TagData::getTagName(), instance=" . $this->_id . "<br>";
        return $this->_tagName;
    }

    /**
     * Returns whether the specified tag name is a prefixed tag.  The tag
     * name must be supplied in prefix:tag format.  Tags are considered prefixed
     * tags if they conform to this format.
     * @return boolean
     */
    public function isPrefixedTag()
    {
        $arr = explode(":", $this->_tagName);
        return count($arr) > 1;
    }

    /**
     * Returns the prefix of a prefixed tag or null if it is not prefixed.
     * @return string Prefix
     */
    public function getTagPrefix()
    {
        list($prefix, $suffix) = explode(":", $this->_tagName);
        return is_null($suffix) ? null : $prefix;
    }

    /**
     * Returns the suffix of a prefixed tag or the tag name if it is not
     * prefixed.
     * @return string Suffix
     */
    public function getTagSuffix()
    {
        list($prefix, $suffix) = explode(":", $this->_tagName);
        return is_null($suffix) ? $prefix : $suffix;
    }

    /**
     * Returns the type of tag, start, end or empty.
     * @return string "start", "end", or "empty"
     */
    public function getTagType()
    {
        $type = null;
        if ($this->isStartTag())
            $type = TAGINFO_START_TAG_TYPE;
        else if ($this->isEndTag())
            $type = TAGINFO_END_TAG_TYPE;
        else if ($this->isEmptyTag())
            $type = TAGINFO_EMPTY_TAG_TYPE;
        return $type;
    }

    /**
     * Returns true if this tag is a start tag.  (e.g. <tag>)  It does not
     * count empty tags as start tags.
     * @return bool Boolean
     */
    public function isStartTag()
    {
        /*return preg_match("/^<[^\/>]?.+[^\/>]?>$/", $this->_tagStr);*/
        return preg_match("/^<[^\/>].*[^\/>]>$/s", $this->_tagStr);
    }

    /**
     * Returns true if this tag is an end tag.  (e.g. </tag>)
     * @return bool Boolean
     */
    public function isEndTag()
    {
        return preg_match("/^<\/.+[^\/>]?>$/s", $this->_tagStr);
    }

    /**
     * Returns true if this tag is an empty tag.  (e.g. <tag/>)
     * @return bool Boolean
     */
    public function isEmptyTag()
    {
        return preg_match("/^<[^\/>]?.+\/>$/s", $this->_tagStr);
    }

    /**
     * Returns a hash of all attributes in this tag as name=value pairs.
     * @return array Hash of attributes
     */
    public function &getAttributeMap()
    {
        global $phpCodeRegExp, $dQuotedStrRegExp, $sQuotedStrRegExp;

        $tagName = $this->_tagName;

        // strip brackets and tagname from tag (just get attributes)
        $attrStr = preg_replace("/^<\/?".$tagName."[ \t\n]+([^\/]+)[ \t\n]*\/?>$/s", "\${1}", $this->_tagStr);

        // split attributes into array of name=value pairs
        $attrArr = array();
        preg_match_all("/([^ \t\n=]+)=(".REGEXP_PHP_CODE."|".REGEXP_DOUBLE_QUOTED_STR."|".REGEXP_SINGLE_QUOTED_STR."|[^ \t\n]+)/s", $attrStr, $attrArr);

        // build attribute map
        $attrMap = array();
        for ($i = 0; $i < count($attrArr[0]); $i++)
        {
            // split into key/value
            //echo "parsing attribute: ".htmlentities($attrArr[0][$i])."<br>\n";

            $key =& $attrArr[1][$i];
            $value =& $attrArr[2][$i];

            // remove quotes from around value
            if (preg_match("/".REGEXP_DOUBLE_QUOTED_STR."/s", $value))
                $value = preg_replace("/".REGEXP_DOUBLE_QUOTED_STR."/s", "\${1}", $value);
            else if (preg_match("/".REGEXP_SINGLE_QUOTED_STR."/s", $value))
                $value = preg_replace("/".REGEXP_SINGLE_QUOTED_STR."/s", "\${1}", $value);

            //echo "parsed attribute: $key=".htmlentities($value)."<br>\n";
            $attrMap[$key] = $value;
        }

        return $attrMap;
    }

    /**
     * Returns the index of this tag in the input buffer.
     * @return integer Index
     */
    public function getTagIndex()
    {
        return $this->_index;
    }

    /**
     * Used by the CustomTagParser to mark the index of this tag in the
     * input buffer.
     * @param integer $i Index
     */
    public function setTagIndex($i)
    {
        $this->_index = $i;
    }

    /**
     * Returns a unique identifier for this instance.
     * @return integer Unique integer
     */
    public function getTagId()
    {
        static $id = 0;

        if ($this->_id == -1)
            $this->_id = $id++;

        return $this->_id;
    }
}

?>
