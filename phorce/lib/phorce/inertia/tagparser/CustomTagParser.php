<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\tagparser;

use \SplStack;
use phorce\common\php\PhpOutput;

require_once "phorce/inertia/tagparser/constants.php";

/**
 * This class is used to parse a PHPX file and translate it to a PHP file.
 * There are three steps in the translation process:
 * 1) Load and parse the document, identifying all custom tag elements, their
 * data, and their nested tags.  The document and elements that are returned
 * from this process are structured as a tree of Element objects.
 * 2) Evaluate custom tag elements and replace them with corresponding PHP code.
 * This step is handled by the TagTranslator class.
 * 3) Write the translated output to a file.
 *
 * TODO: The tag parser should use a an HTML SAX parser, rather than my own
 * home-grown quick-and-dirty thing, which does a lot of regular expression
 * stuff.  There is one such PEAR package for this purpose.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagParser
 */
class CustomTagParser
{
    /**
     * Location of PHPX file to parse.
     * @access private
     * @var string
     */
    private $_phptagFile;

    /**
     * Location of PHP file to output.
     * @access private
     * @var string
     */
    private $_phpFile;

    /**
     * Contents of PHPX file.
     * @access private
     * @var string
     */
    private $_inputContent;

    /**
     * Constructs a tag parser for the specified input and output files.
     * @param string $phptagFile Input PHPX file
     * @param string $phpFile Output PHP file
     */
    public function __construct($phptagFile, $phpFile)
    {
        $this->_phptagFile = $phptagFile;
        $this->_phpFile = $phpFile;
    }

    /**
     * Executes the PHPX translation process.
     */
    public function translateDocument()
    {
        // step 1 - load and parse document, identify all custom tags
        $this->_inputContent = file_get_contents($this->_phptagFile);
        $rootElement = $this->parseTags($this->_inputContent);
        //echo self::_displayElement($rootElement);

        // TODO: get list of includes
        // $includes = $rootElement->getIncludes();
        $includes = array();

        // step 2 - evaluate custom tags and replace with php code
        $phpOutput = new PhpOutput();
        $translator = new TagTranslator($phpOutput);
        $translator->addHeader($includes);
        $translator->translateElement($rootElement);
        //echo htmlentities($outputContent);

        // step 3 - write output to file
        file_put_contents($this->_phpFile, $phpOutput->getOutputString());
    }

    /**
     * Parses the PHPX document data, identifying all tags.  This method
     * returns an Element object representing the entire document.  Nested
     * Element objects represent the PHPX tags and their contents.
     * @param string $content The PHPX document data
     * @return object Element object for the entire document
     */
    public function parseTags(&$content)
    {
        $element = new Element();
        $tagStack = new SplStack();

        // get size of document
        $docsize = strlen($content);

        // save entirety of page content to root element
        $element->setInnerHtml($content);

        // loop through content
        for ($idx = 0; $idx < $docsize; $idx = $endIdx)
        {
            // find start of next tag
            $startIdx = strpos($content, "<", $idx);

            if ($startIdx === false)
                break;

            // search for tag at this index
            // NOTE: tags cannot be more than 10000 characters long
            $subText = substr($content, $startIdx, 10000);
            if (substr($subText, 0, 4) == "<!--")
            {
                // we found a comment, find its end
                $matches = array();
                preg_match("/--\>/", $subText, $matches, PREG_OFFSET_CAPTURE);
                $offset = $matches[0][1];

                //print "found comment at index $startIdx\n";
                //print_r($matches);

                // offset end idx to end of comment and continue searching
                $endIdx = $startIdx + $offset + 3;
                continue;
            }
            //elseif (substr($subText, 0, 1) != "<")
            elseif (! preg_match("/^(".REGEXP_TAG.").*/s", $subText))
            {
                // otherwise, try next character
                $endIdx = $startIdx + 1;
                //echo 'skipping...';
                continue;
            }

            // if we got here, we found a real tag

            // get entire tag string
            $tagText = preg_replace("/^(".REGEXP_TAG.").*/s", "\${1}", $subText);

            //echo "<b>$startIdx</b> " . htmlentities($tagText) . "<br>\n";
            //echo "$startIdx - $tagText\n";

            // advance end index to end of tag
            $endIdx = $startIdx + strlen($tagText);

            // get tag info
            $tagInfo = new TagData($tagText);

            // check if this is a recognizable custom-defined tag
            if ($tagInfo->isPrefixedTag())
            {
                //echo "<i>". $tagInfo->getTagType() ."</i> <b>". $tagInfo->getTagName() ."</b>: ". htmlentities($tagInfo->getTagContent()) ."<br>\n";
                //echo $tagInfo->getTagType() ." - ". $tagInfo->getTagName() .": ". $tagInfo->getTagContent() ."\n";

                // set index of tag in master content
                $tagInfo->setTagIndex($startIdx);

                if ($tagInfo->isStartTag() || $tagInfo->isEmptyTag())
                {
                    // create new element for this tag
                    $subElement = new Element();
                    $subElement->setStartTagData($tagInfo);

                    // calculate nested element's offset in the parent's body
                    $pTagData = $element->getStartTagData();
                    $pStart = is_null($pTagData) ? 0 : $pTagData->getTagIndex();
                    $pLength = is_null($pTagData) ? 0 : $pTagData->getTagLength();
                    $relOffset = $tagInfo->getTagIndex() - $pStart - $pLength;
                    $subElement->setOffset($relOffset);

                    if ($tagInfo->isStartTag())
                    {
                        // push parent on stack: this tag is now the current element
                        $tagStack->push($element);
                        unset($element);
                        $element = $subElement;
                    }
                    else if ($tagInfo->isEmptyTag())
                    {
                        // nest this element under its parent
                        $element->addNestedElement($subElement);
                    }
                }
                else if ($tagInfo->isEndTag())
                {
                    if ($element->getElementName() !== $tagInfo->getTagName())
                    {
                        // tag nesting error
                        // TODO: get line number
                        trigger_error("Tag Nesting Error: found " .
                            $tagInfo->getTagName() . " end tag but expected " .
                            $element->getElementName() . " end tag",
                            E_USER_ERROR);
                    }

                    // save end tag to element
                    $element->setEndTagData($tagInfo);

                    // determine the start of the data in the element
                    $startTag = $element->getStartTagData();
                    $dataStartIdx = $startTag->getTagIndex() + $startTag->getTagLength();

                    // get length of data (start of end tag is end of data)
                    $dataLength = $tagInfo->getTagIndex() - $dataStartIdx;

                    // get the html between the start & end tags
                    $innerHtml = substr($content, $dataStartIdx, $dataLength);
                    $element->setInnerHtml($innerHtml);

                    // nest this element under its parent
                    $parentElement = $tagStack->pop();
                    $parentElement->addNestedElement($element);

                    // drop back to parent element
                    unset($element);
                    $element = $parentElement;
                }
            }
        }

        return $element;
    }

    /**
     * For debugging.
     * @param object Element $element Element to display
     * @param integer $level Nesting level
     * @return string String to display
     * @access private
     * @static
     */
    private static function _displayElement(Element $element, $level = 0)
    {
        // build prefix
        $prefix = "";
        for ($i = 0; $i < $level; $i++)
            $prefix .= "     ";

        $str  = $prefix."element ".$element->getElementName()."\n";
        $str .= $prefix."innerHtml length ".strlen($element->getInnerHtml())."\n";
        $str .= $prefix."nested elements=\n";

        $nestedElements = $element->getNestedElements();
        foreach ($nestedElements as $key => $e)
        {
            $e = $nestedElements[$key];
            $str .= $prefix."  [$key] => \n";
            $str .= $prefix . self::_displayElement($e, $level+1);
        }

        return $str;
    }

    /**
     * Returns whether the specified tag name is a custom tag.  The tag
     * name must be supplied in taglib:tag format.  Tags are considered custom
     * tags if they conform to this format.
     * @param string $fullTagName
     * @return boolean
     */
    private static function _isCustomTag($fullTagName)
    {
        $arr = explode(":", $fullTagName);
        return count($arr) > 1 && self::_getTagElement($arr[0], $arr[1]) != null;
    }
}
?>
