<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\tagparser;

use phorce\common\php\PhpOutput;
use phorce\inertia\phpx\TaglibContext;
use phorce\inertia\tagext\TagExtraInfo;
use phorce\inertia\tagext\VariableInfo;

define('TAG_TRANSLATOR_RE_EL', '\$\{(([^\}]|'.REGEXP_SINGLE_QUOTED_STR.'|'.REGEXP_DOUBLE_QUOTED_STR.')+)\}');

/**
 * This class handles the translation of custom tags into PHP code.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagParser
 */
class TagTranslator
{
    // TODO: fix this so it captures more than one ${}
    const RE_EL = TAG_TRANSLATOR_RE_EL;
    const RE_PHP_OUT = '<\?=(.+)\?>';

    const PHPX_PREFIX = 'phpx';
    const PHPX_URI = 'http://www.phorce.org/taglib/inertia/phpx';

    /**
     * The output of the translation process as PHP code.
     * @var object PhpOutput
     * @access private
     */
    private $_php;

    /**
     * Taglib context used to access tag libraries.
     * @var array
     * @access private
     */
    private $_taglibContext;


    /**
     * Creates a new tag translator with the specified PHP output stream.
     * @param object PhpOutput $phpOutput PHP Output stream
     */
    public function __construct(PhpOutput $phpOutput)
    {
        $this->_php = $phpOutput;

        // PHP taglib context is hard-coded
        $this->_taglibContext = new TaglibContext();
        $this->_taglibContext->registerTagLibrary(self::PHPX_PREFIX, self::PHPX_URI);
    }

    /**
     * Returns the PHP output stream associated with this translator.
     * @return object PhpOutput PHP Output stream
     */
    public function getPhpOutput()
    {
        return $this->_php;
    }

    /**
     * Adds header information to the PHP output.
     * @param array $includes Array of includes
     */
    public function addHeader(array $includes)
    {
        $this->_php->startPhpCode();
        $this->_php->incIndent();
        $this->_php->addStatement("require_once '" . \phorce\makepath(PHORCE_HOME, "init.php") . "';");
        foreach ($includes as $include)
            $this->_php->addStatement('require_once "'.$include.'";');
        $this->_php->addStatement('use phorce\common\property\PropertyUtils;');
        $this->_php->addStatement('use phorce\inertia\phpx\InertiaContext;');
        $this->_php->addStatement('use phorce\inertia\phpx\PageContext;');
        $this->_php->addStatement('use phorce\inertia\tagext\TagHandler;');
        $this->_php->addStatement('$_inertiaContext = new InertiaContext(substr(__FILE__, strlen(PHORCE_WORK)));');
        $this->_php->addStatement('$pageContext = $_inertiaContext->getPageContext();');
        $this->_php->addStatement('$_inertiaContext->getTaglibContext()->registerTagLibrary(\'' . self::PHPX_PREFIX . '\',\'' . self::PHPX_URI . '\');');
        $this->_php->decIndent();
        $this->_php->endPhpCode();
    }

    /**
     * Evaluates the Custom Tag Element with replacement text stored in the
     * PHP output stream.
     * @param object Element $e Reference to Element to evaluate
     */
    public function translateElement(Element $e)
    {
        $inputContent =& $e->getInnerHtml();
        $startTagData = $e->getStartTagData();
        $tei = null;

        // if we have a start tag, evaluate it
        if (! is_null($startTagData))
        {
            //echo "{$startTagData->getTagPrefix()}:{$startTagData->getTagSuffix()}<br>\n";
            if ($this->_taglibContext->hasTagLibrary($startTagData->getTagPrefix()))
            {
                $taglib = $this->_taglibContext->getTagLibrary($startTagData->getTagPrefix());

                // get extra info for this tag
                $tei = $taglib->getTagExtraInfoInstance(
                    $startTagData->getTagSuffix());

                // evaluate (replace) the start tag
                $this->translateStartTag($e, $tei);
            }
            else
            {
                // if prefixed tag not a declared custom tag prefix
                $this->_php->addText($startTagData->getTagContent());
            }
        }

        // initialize string index to start of input content
        $idx = 0;

        // loop through nested elements in content
        $elements =& $e->getNestedElements();
        foreach (array_keys($elements) as $key)
        {
            $element = $elements[$key];

            // append html before next subelement to output
            $text = substr($inputContent, $idx, $element->getOffset()-$idx);
            $text = preg_replace("/" . self::RE_EL . "/", '<?php echo $_inertiaContext->getElEvaluator()->evaluate("${1}"); ?>', $text);
            $this->_php->addText($text);

            // evaluate nested element
            $this->translateElement($element);

            // set index to end of element
            $idx = $element->getOffset() + $element->getLength();
        }

        // append html after last subelement to output
        $text = substr($inputContent, $idx, strlen($inputContent));
        $text = preg_replace("/" . self::RE_EL . "/", '<?php echo $_inertiaContext->getElEvaluator()->evaluate("${1}"); ?>', $text);
        $this->_php->addText($text);

        // evaluate end tag if we evaluated its start tag
        if (! is_null($startTagData))
        {
            if ($this->_taglibContext->hasTagLibrary($startTagData->getTagPrefix()))
            {
                $this->translateEndTag($e, $tei);
            }
            elseif (! is_null($e->getEndTagData()))
            {
                // if prefixed tag not a declared custom tag prefix
                $this->_php->addText($e->getEndTagData()->getTagContent());
            }
        }
    }

    /**
     * Outputs tag handler code for a start tag of the given element
     * to the PHP output script.
     * @param object Element $e Element
     * @param object TagExtraInfo $tei TagExtraInfo
     */
    public function translateStartTag(Element $e, TagExtraInfo $tei = null)
    {
        $tagData = $e->getStartTagData();
        $parent = $e->getParent();

        $this->_php->startPhpCode();
        $this->_php->incIndent();

        if (! is_null($tei))
        {
            foreach ($tei->getVariableInfo($tagData) as $vi)
            {
                if ($vi->isAtBegin() || $vi->isPassToTag())
                    $this->translateVariableInfo($vi);
            }
        }

        // TODO - can we handle this (and other php directive tags) some other generic way???
        if ($tagData->getTagName() == 'phpx:taglib')
        {
            // TODO - check that these attributes actually exist
            //  (will be done by TagExtraInfo for ALL tags)
            $attrMap =& $tagData->getAttributeMap();
            $prefix = $attrMap["prefix"];
            $uri = $attrMap["uri"];

            $this->_taglibContext->registerTagLibrary($prefix, $uri);
            $this->_php->addStatement('$_inertiaContext->getTaglibContext()->registerTagLibrary(\'' . $prefix . '\',\'' . $uri . '\');');
        }

        // get tag library
        $taglib = $this->_taglibContext->getTagLibrary($tagData->getTagPrefix());

        $this->_php->addStatement('$_inertiaContext->pushTag(new '.$taglib->getTagClass($tagData->getTagSuffix()).'());');
        //TODO
        //$this->_php->addStatement('$_currentTei = new '.$taglib->getTagExtraInfoClass($tagData->getTagSuffix()).'();');
        foreach ($tagData->getAttributeMap() as $key => $value)
        {
            // parse runtime expression or string value
            if (preg_match("/^" . self::RE_PHP_OUT . "$/", $value))
            {
                // replace <?= tags
                $valueStr = preg_replace("/^" . self::RE_PHP_OUT . "$/","\${1}", $value);
            }
            elseif (preg_match("/" . self::RE_EL . "/", $value))
            {
                // make a quoted string
                $valueStr = "'" . preg_replace("/'/", "\\'", $value) . "'";
                // replace ${} el
                $valueStr = preg_replace("/" . self::RE_EL . "/", '\' . $_inertiaContext->getElEvaluator()->evaluate(\'${1}\') . \'', $valueStr);
            }
            else
                // make a quoted string
                $valueStr = "'" . preg_replace("/'/", "\\'", $value) . "'";

            /* TODO
                $valueAtr = preg_replace("/<?=/", "<?php echo ", $value);
                PropertyUtils::setProperty("name", $value);

                // must handle these cases:
                <html:link name="test <?= 'out' ?> test" />
                <html:link name="test <?php print 'out' ?> test" />
                <html:link name="test ${'out'} test" />
            */

            $this->_php->addStatement('PropertyUtils::setProperty($_inertiaContext->getCurrentTag(), \''.$key.'\', '.$valueStr.');');
        }

        if ($e->isEmpty())
        {
            $this->_php->addStatement('$_inertiaContext->getCurrentTag()->doStartTag();');
        }
        else
        {
            $this->_php->addStatement('switch ($_inertiaContext->getCurrentTag()->doStartTag()) {');
            $this->_php->incIndent();
            $this->_php->addStatement('case TagHandler::EVAL_BODY_BUFFERED:');
            $this->_php->incIndent();
            $this->_php->addStatement('$_inertiaContext->getTagStack()->startTagBuffer();');
            $this->_php->addStatement('$_inertiaContext->getCurrentTag()->doInitBody();');
            $this->_php->addStatement('// pass through to next case');
            $this->_php->decIndent();
            $this->_php->addStatement('case TagHandler::EVAL_BODY_INCLUDE:');
            $this->_php->incIndent();
            $this->_php->addStatement('do {');
            $this->_php->incIndent();

            if (! is_null($tei))
            {
                foreach ($tei->getVariableInfo($tagData) as $vi)
                {
                    if ($vi->isNested())
                        $this->translateVariableInfo($vi);
                }
            }

            $this->_php->endPhpCode();
        }
    }

    /**
     * Outputs tag handler code for an end tag of the given element
     * to the PHP output script.
     * @param object Element $e Element
     * @param object TagExtraInfo $tei TagExtraInfo
     */
    public function translateEndTag(Element $e, TagExtraInfo $tei = null)
    {
        $tagData = $e->getStartTagData();

        if (! $e->isEmpty())
        {
            $this->_php->startPhpCode();

            $this->_php->addStatement('$_inertiaContext->getTagStack()->saveTagBuffer();');
            $this->_php->decIndent();
            $this->_php->addStatement('} while ($_inertiaContext->getCurrentTag()->doAfterBody() == TagHandler::EVAL_BODY_AGAIN); // do');
            $this->_php->addStatement('$_inertiaContext->getTagStack()->endTagBuffer();');
            $this->_php->decIndent();
            $this->_php->addStatement('break; // case TagHandler::EVAL_BODY_INCLUDE');
            $this->_php->decIndent();
            $this->_php->addStatement('} // switch');
        }

        $this->_php->addStatement('if ($_inertiaContext->getCurrentTag()->doEndTag() == TagHandler::SKIP_PAGE) {');
        $this->_php->incIndent();
        $this->_php->addStatement('return;');
        $this->_php->decIndent();
        $this->_php->addStatement('} // if TagHandler::SKIP_PAGE');

        // pop current tag off stack and get parent tag
        $this->_php->addStatement('$_inertiaContext->popTag();');

        if (! is_null($tei))
        {
            foreach ($tei->getVariableInfo($tagData) as $vi)
            {
                if ($vi->isAtEnd())
                    $this->translateVariableInfo($vi);
            }
        }

        $this->_php->decIndent();
        $this->_php->endPhpCode();
    }

    /**
     * Outputs variable handling code for a given variable info object.
     * @param object VariableInfo $vi VariableInfo
     */
    public function translateVariableInfo(VariableInfo $vi)
    {
        if ($vi->isPassToTag())
            $this->_php->addStatement('$pageContext->setAttribute("'.$vi->getVarName().'", $'.$vi->getVarName().', PageContext::SCOPE_PAGE);');
        else
            $this->_php->addStatement('$'.$vi->getVarName().' =& $pageContext->getAttribute("'.$vi->getVarName().'", PageContext::SCOPE_PAGE);');
    }
}

?>
