<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */
/**
 * Parses the specified php file containing custom tags, expanding the custom
 * tags to PHP code.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagParser
 */

    $config = findSiteConfig(".");

    if (is_null($config))
    {
        echo "Unable to find siteconfig.php for execution.\n";
        exit;
    }

    echo "Using '$config'\n";

    // site config is required for CustomTagParser
    require_once $config;
    require_once "phorce/inertia/tagparser/CustomTagParser.php";

    if (count($argv) < 2)
    {
        echo "Must specify at least one phpx file or directory\n";
        exit;
    }

    for ($i = 1; $i < count($argv); $i++)
    {
        $file = $argv[$i];

        if (! file_exists($file))
            echo "$file does not exist\n";
        elseif (is_file($file) && preg_match("/.*\.(phpx|psp|phptag)$/", $file))
            parseFile($file);
        elseif (is_dir($file))
            parseDir($file);
        else
            echo "$file is not a phpx file or directory\n";
    }

    function parseFile($file)
    {
        // get name of php file
        $dir = dirname($file);
        $base = basename($file, ".psp");
        $base = basename($base, ".phpx");
        $base = basename($base, ".phptag");
        $outfile = "$dir/$base.php";

        $siteConfig = findSiteConfig($dir);

        if (is_null($siteConfig))
        {
            echo "Unable to find siteconfig.php for '$file'.\n";
            exit;
        }

        echo "using site config '$siteConfig' for file '$file'\n";

        // translate file
        $parser = new CustomTagParser($file, $outfile, $siteConfig);
        $parser->translateDocument();
    }

    function parseDir($dir)
    {
        $dh = @opendir($dir);
        while (($file = readdir($dh)) !== false)
        {
            $fullpath = "$dir/$file";

            if (preg_match("/^\./", $file))
                ; // skip dot-files
            elseif (is_file($fullpath) && preg_match("/.*\.(phpx|psp|phptag)/", $file))
                parseFile($fullpath);
            elseif (is_dir($fullpath))
                parseDir($fullpath);
        }
        closedir($dh);
    }

    /**
     * Finds the location of the siteconfig.php file relative to the currently
     * specified directory.
     * @param string $dir Directory
     * @return string Relative path to siteconfig.php
     */
    function findSiteConfig($dir)
    {
        $configpath = "phorce/config/siteconfig.php";

        // get full filesystem path to directory
        $abspath = realpath($dir);
        $relpath = "";

        // look for config/siteconfig.php
        while (! file_exists("$abspath/$configpath") && $abspath != "/")
        {
            // move up a directory
            $abspath = dirname($abspath);
            $relpath .= "../";
        }

        // return relative directory to siteconfig
        return $abspath == "/" ? null : "$abspath/$configpath";
    }
?>
