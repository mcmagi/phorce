<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\phpx;

use phorce\inertia\InertiaException;
use phorce\inertia\config\InertiaConfig;
use phorce\inertia\config\InertiaConfigDAO;
use phorce\inertia\tld\TaglibDescriptorDAO;

/**
 * Provides a means to retrieve Tag Library Definition (TLD) files.  This object
 * is maintained as a singleton for performance.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage PHPX
 */
class TagLoader
{
    const CONFIG_FILE = "inertia-config.xml";

    /**
     * Contains the singleton instance of the TagLoader.
     * @var object TagLoader
     * @access private
     */
    private static $_instance;

    /**
     * Inertia configuration.
     * @var object InertiaConfig
     * @access private
     */
    private $_inertiaCfg;

    /**
     * Array of URIs to tag libraries.
     * @var array
     * @access private
     */
    private $_taglibMap = array();

    /**
     * Returns the singleton instance of the TagLoader.
     * @return object TagLoader
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance))
        {
            // parse Inertia configuration
            // TODO - shouldn't we do this elsewhere???
            $dao = new InertiaConfigDAO(
                PHORCE_CONFIG . DIRECTORY_SEPARATOR . self::CONFIG_FILE);
            $inertiaCfg = $dao->load();

            // create default instance
            self::$_instance = new TagLoader($inertiaCfg);
        }
        return self::$_instance;
    }

    /**
     * Constructs an Inertia TagLoader object.
     * @param object InertiaConfig Inertia configuration
     */
    private function __construct(InertiaConfig $inertiaCfg)
    {
        $this->_inertiaCfg = $inertiaCfg;
    }

    /**
     * Returns the TagLibrary object for the specified URI.  If no tag library
     * for the URI is configured, this method will throw an exception.
     * @param string $uri URI of tag library
     * @return object TaglibDescriptor Tag library
     */
    public function loadTagLibrary($uri)
    {

        if (! array_key_exists($uri, $this->_taglibMap))
        {
            // lookup uri in inertia.ini
            $taglibCfg = $this->findTaglibConfig($uri);

            if (is_null($taglibCfg))
                throw new InertiaException("No tag library found for URI '$uri' in " . self::CONFIG_FILE);

            $fulltld = PHORCE_CONFIG . DIRECTORY_SEPARATOR . $taglibCfg->descriptor;
            $dao = new TaglibDescriptorDAO($fulltld);
            $this->_taglibMap[$uri] = new TagLibrary($dao->load());
        }

        return $this->_taglibMap[$uri];
    }

    /**
     * Returns the TaglibConfig from the configuration with the specified URI.
     * @param string $uri URI of taglib
     * @return object TaglibConfig Taglib configuration
     */
    private function findTaglibConfig($uri)
    {
        foreach ($this->_inertiaCfg->taglibs as $taglib)
        {
            if ($taglib->uri == $uri)
                return $taglib;
        }
        return null;
    }
}

?>
