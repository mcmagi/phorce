<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\phpx;

use phorce\common\property\PropertyObject;
use phorce\inertia\el\ElEvaluator;
use phorce\inertia\tagext\TagHandler;

/**
 * This class abstracts access to all classes needed by PHPX files.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage PHPX
 */
class InertiaContext extends PropertyObject
{
    /**
     * The page context.
     * @var object PageContext
     * @access private
     */
    private $_pageContext;

    /**
     * The taglib context.
     * @var object TaglibContext
     * @access private
     */
    private $_taglibContext;

    /**
     * The tag stack.
     * @var object TagStack
     * @access private
     */
    private $_tagStack;

    /**
     * The EL evaluator.
     * @var object ElEvaluator
     * @access private
     */
    private $_elEvaluator;

    /**
     * Creates a new InertiaContext.
     * @param string $currentPage Relative path to currently executing PHPX page
     */
    public function __construct($currentPage)
    {
        $this->_pageContext = new PageContext($currentPage);
        $this->_taglibContext = new TaglibContext();
        $this->_tagStack = new TagStack();
        $this->_elEvaluator = new ElEvaluator(
            $this->_pageContext, $this->_taglibContext);
    }

    /**
     * Returns the page context.
     * @return object PageContext
     */
    public function getPageContext()
    {
        return $this->_pageContext;
    }

    /**
     * Returns the taglib context.
     * @return object TaglibContext
     */
    public function getTaglibContext()
    {
        return $this->_taglibContext;
    }

    /**
     * Returns the tag stack.
     * @return object TagStack
     */
    public function getTagStack()
    {
        return $this->_tagStack;
    }

    /**
     * Returns the most recent tag from the TagStack.
     * @return object TagHandler Current tag
     */
    public function getCurrentTag()
    {
        return $this->_tagStack->top();
    }

    /**
     * Pushes the tag onto the tag stack.  Also associates the current page
     * context with the tag.
     * @param object TagHandler $tag
     */
    public function pushTag(TagHandler $tag)
    {
        $tag->setPageContext($this->_pageContext);
        $this->_tagStack->push($tag);
    }

    /**
     * Pops the current tag off the tag stack.
     * @return object TagHandler
     */
    public function popTag()
    {
        return $this->_tagStack->pop();
    }

    /**
     * Returns the EL evaluator.
     * @return object ElEvaluator
     */
    public function getElEvaluator()
    {
        return $this->_elEvaluator;
    }
}

?>
