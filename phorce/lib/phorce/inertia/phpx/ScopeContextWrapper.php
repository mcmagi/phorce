<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\phpx;

use phorce\common\web\AttributeContainer;

/**
 * This class acts as a wrapper around associative arrays, treating elements as
 * attributes.  This is primarily useful for the PageContext class, to access
 * attributes of different scopes in a uniform way.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage PHPX
 */
class ScopeContextWrapper implements AttributeContainer
{
    /**
     * The wrapped associative array.
     * @var array
     * @access private
     */
    private $_array = array();

    /**
     * Constructs a ScopeContextWrapper around the specified array.
     * @param array Reference to array
     */
    public function __construct(&$array)
    {
        if (! is_null($array))
            $this->_array =& $array;
    }

    /**
     * Returns the value of the attribute with the specified key.
     * @param string $key Key
     * @return mixed Reference to value
     */
    public function &getAttribute($key)
    {
        $value = null;
        if (array_key_exists($key, $this->_array))
            $value =& $this->_array[$key];
        return $value;
    }

    /**
     * Returns an array of all attribute keys.
     * @return array Array of attribute keys
     */
    public function getAttributeNames()
    {
        return array_keys($this->_array);
    }

    /**
     * Sets the value of the attribute with the specified key.
     * @param string $key Key
     * @param mixed $value Reference to value
     */
    public function setAttribute($key, &$value)
    {
        $this->_array[$key] =& $value;
    }

    /**
     * Removes the attribute with the specified key.
     * @param string $key Key
     */
    public function removeAttribute($key)
    {
        unset($this->_array[$key]);
    }
}

?>
