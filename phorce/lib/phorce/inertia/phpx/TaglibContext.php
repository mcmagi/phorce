<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\phpx;

use phorce\inertia\InertiaException;

/**
 * This class contains tag libraries which have been associated with a prefix
 * within a PHPX file.  When a prefix association is to be made,
 * <code>registerTagLibrary()</code> must be called.  However, the tag library
 * will not get loaded until it is actually requested to improve performance.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage PHPX
 */
class TaglibContext
{
    /**
     * Associative array of registered prefixes for tag libaries to their
     * corresponding URIs.  Taglib prefixes stored in this array represent
     * registered tag libaries.
     * @var array
     * @access private
     */
    private $_registeredTaglibMap = array();

    /**
     * Associative array of loaded prefixes for tag libraries to their
     * corresponding tag libraries.
     * Array of prefixes to tag libraries.
     * @var array
     * @access private
     */
    private $_loadedTaglibMap = array();


    /**
     * Returns whether a tag library with the specified prefix is registered in
     * the tag loader.
     * @param string $prefix
     * @return boolean True if registered
     */
    public function hasTagLibrary($prefix)
    {
        return array_key_exists($prefix, $this->_registeredTaglibMap);
    }

    /**
     * Registers the tag library prefix with the specified uri.  An exception
     * will be thrown if the prefix was already registered with another tag
     * library.
     * @param string $prefix
     * @param string $uri
     */
    public function registerTagLibrary($prefix, $uri)
    {
        if ($this->hasTagLibrary($prefix))
            throw new InertiaException("Prefix '$prefix' is already registered with URI '$uri'");

        //echo "register taglib $prefix=$uri<br>\n";
        $this->_registeredTaglibMap[$prefix] = $uri;
    }

    /**
     * Unregisters the tag library associated with the specified prefix.  It
     * will not complain if the prefix was never registered.
     * @param string $prefix
     */
    public function unregisterTagLibrary($prefix)
    {
        if ($this->hasTagLibrary($prefix))
        {
            unset($this->_registeredTaglibMap[$prefix]);
            unset($this->_loadedTaglibMap[$prefix]);
        }
    }

    /**
     * Returns the tag library with the specified prefix.  If the requested
     * prefix has not been registered, an exception will be thrown.  An
     * exception will also be thrown if the tag library with the registered URI
     * could not be located.
     * @param string $prefix
     * @return object TagLibrary
     */
    public function getTagLibrary($prefix)
    {
        if (! $this->hasTagLibrary($prefix))
            throw new InertiaException("No tag library associated with prefix '$prefix'");

        $uri = $this->_registeredTaglibMap[$prefix];

        // load the tag library only as needed
        if (! array_key_exists($prefix, $this->_loadedTaglibMap))
            $this->_loadedTaglibMap[$prefix] = self::_loadTagLibrary($uri);

        return $this->_loadedTaglibMap[$prefix];
    }

    /**
     * Returns the TLD for the specified library as an XML document.  This is
     * simply a wrapper to <code>TagLoader->loadTagLibrary()</code>.
     * @param string $uri URI of tag library
     * @return object XMLElement taglib element
     * @static
     * @access private
     */
    private static function _loadTagLibrary($uri)
    {
        return TagLoader::getInstance()->loadTagLibrary($uri);
    }
}

?>
