<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\phpx;

use phorce\common\property\PropertyObject;
use phorce\inertia\InertiaException;
use phorce\inertia\tld\TaglibDescriptor;

/**
 * Serves as an interface to the Tag Library Definition (TLD) file.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage PHPX
 */
class TagLibrary extends PropertyObject
{
    /**
     * Tag library descriptor object.
     * @var object TaglibDescriptor
     * @access private
     */
    private $_descriptor;

    /**
     * Constructs a tag library object for the specified TLD file.  An exception
     * will be thrown if the TLD file cannot be located.
     * @param string $tld Path to Tag Library Descriptor
     */
    public function __construct(TaglibDescriptor $tld)
    {
        $this->_descriptor = $tld;
    }

    /**
     * Returns the taglib descriptor for this TagLibrary abstraction.
     * @return object TaglibDescriptor
     */
    public function getDescriptor()
    {
        return $this->_descriptor;
    }

    /**
     * Creates an instance of the TagHandler for the specified tag.
     * @param string $tagName Name of the tag
     * @return object TagHandler
     */
    public function getTagInstance($tagName)
    {
        $tagClass = $this->getTagClass($tagName);
        return is_null($tagClass) ? null : new $tagClass();
    }

    /**
     * Returns the TagHandler class defined for the specified tag.
     * @param string $tagName Name of the tag
     * @return string Class name
     */
    public function getTagClass($tagName)
    {
        $tag = $this->_getTagDescriptor($tagName);
        return is_null($tag) ? null : $tag->tagClass;
    }

    /**
     * Creates an instance of the TagExtraInfo for the specified tag.
     * @param string $tagName Name of the tag
     * @return object TagExtraInfo
     */
    public function getTagExtraInfoInstance($tagName)
    {
        $teiClass = $this->getTagExtraInfoClass($tagName);
        return is_null($teiClass) ? null : new $teiClass();
    }

    /**
     * Returns the TagExtraInfo class defined for the specified tag.
     * @param string $tagName Name of the tag
     * @return string Class name
     */
    public function getTagExtraInfoClass($tagName)
    {
        $tag = $this->_getTagDescriptor($tagName);
        return is_null($tag) ? null : $tag->teiClass;
    }

    /**
     * Creates an instance of the function library for the specified tag
     * @param string $funcName Name of the function
     * @return mixed Function object
     */
    public function getFunctionInstance($funcName)
    {
        $fcnClass = $this->getFunctionClass($funcName);
        return is_null($fcnClass) ? null : new $fcnClass();
    }

    /**
     * Returns the class name of the function library for the specified
     * function.
     * @param string $funcName Name of the function
     * @return string Class name
     */
    public function getFunctionClass($funcName)
    {
        $fcn = $this->_getFunctionDescriptor($funcName);
        return is_null($fcn) ? null : $fcn->functionClass;
    }

    /**
     * Returns the TagDescriptor from the TLD which defines the specified tag.
     * @param string $tagname Tag name within library
     * @return object TagDescritor Tag element
     * @access private
     */
    private function _getTagDescriptor($tagname)
    {
        // find tag element with specified name attribute
        foreach ($this->_descriptor->tags as $tag)
        {
            $name = $tag->name;
            if ($tag->name == $tagname)
                return $tag;
        }

        return null;
    }

    /**
     * Returns the FunctionDescriptor from the TLD which defines the specified
     * function.
     * @param string $tagname Function name within library
     * @return object FunctionDescriptor Function element
     * @access private
     */
    private function _getFunctionDescriptor($fcnname)
    {
        // find function element with specified name attribute
        foreach ($this->_descriptor->functions as $fcn)
        {
            if ($fcn->name == $fcnname)
                return $fcn;
        }

        return null;
    }
}

?>
