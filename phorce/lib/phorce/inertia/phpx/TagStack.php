<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\phpx;

use \SplStack;
use phorce\inertia\tagext\TagHandler;

/**
 * This class is a stack used to hold nested TagHandlers when executing a
 * compiled PHPX page.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage PHPX
 */
class TagStack extends SplStack
{
    /**
     * An associative array of nesting levels with output buffering enabled.
     * Note that the values in this array are not important; only the keys.
     * If a key is in this array, it is the index+1 of a tag within the stack
     * that has output buffering enabled.
     * @var array
     * @access private
     */
    private $_obMap = array();


    /**
     * Constructor.
     */
    public function __construct()
    {
        //parent::__construct();
    }

    /**
     * Pushes a TagHandler onto the stack.  Also sets the TagHandler's parent.
     * @param object TagHandler $tag
     */
    public function push(TagHandler $tag)
    {
        if (! $this->isEmpty())
            $tag->setParent($this->top());
        parent::push($tag);
    }

    /**
     * Starts output buffering for the 'current' tag.  The current tag is the
     * topmost tag in the stack.  Body content will also be initialized to an
     * empty string.
     */
    public function startTagBuffer()
    {
        // initialize body content of tag
        $this->top()->setBodyContent("");

        // save this tag id within buffer map
        $this->_obMap[$this->count()] = true;

        // start output buffering
        ob_start();
    }

    /**
     * Saves the contents of the output buffer to the 'current' tag's body
     * content.  The current tag is the topmost tag in the stack.  Note that if
     * output buffering has not been started for this tag, this method will do
     * nothing.
     */
    public function saveTagBuffer()
    {
        if ($this->isTagBuffered())
        {
            // get contents of output buffer
            $this->top()->setBodyContent(ob_get_contents());
        }
    }

    /**
     * Ends and flushes the output buffer for the 'current' tag.  The current
     * tag is the topmost tag in the stack.  Note that if output buffering has
     * not been started for this tag, this method will do nothing.
     */
    public function endTagBuffer()
    {
        if ($this->isTagBuffered())
        {
            // clear it
            ob_end_clean();

            // remove from buffer map
            unset($this->_obMap[$this->count()]);
        }
    }

    /**
     * Returns true if output buffering has been enabled for the 'current' tag.
     * The current tag is the topmost tag in the stack.
     * @return boolean
     */
    public function isTagBuffered()
    {
        return array_key_exists($this->count(), $this->_obMap);
    }
}

?>
