<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\phpx;

use phorce\common\property\PropertyObject;
use phorce\common\web\ApplicationContext;
use phorce\inertia\InertiaException;

/**
 * Used to access all variables in several scopes accessable from the current
 * page.  The PageContext instance resides on the TagHandler.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage PHPX
 */
class PageContext extends PropertyObject
{
    /**
     * Application scope constant.  Variables in application scope are stored
     * as attributes on the ApplicationContext object.
     */
    const SCOPE_APPLICATION = "application";

    /**
     * Session scope constant.  Variables in session scope reside in the
     * $_SESSION superglobal.
     */
    const SCOPE_SESSION = "session";

    /**
     * Request scope constant.  Variables in request scope reside in the
     * $_REQUEST superglobal.
     */
    const SCOPE_REQUEST = "request";

    /**
     * Page scope constant.  Variables in page scope reside within this object.
     */
    const SCOPE_PAGE = "page";


    /**
     * The relative path path to the currently executing PHPX page.
     * @var object
     * @access private
     */
    private $pRO_currentPage;

    /**
     * Contains page-level attributes, keyed by identifier name.
     * @var object ScopeContextWrapper
     * @access private
     */
    private $_page;

    /**
     * Contains request-level attributes, keyed by identifier name.
     * @var object ScopeContextWrapper
     * @access private
     */
    private $_request;

    /**
     * Contains session-level attributes, keyed by identifier name.
     * @var object ScopeContextWrapper
     * @access private
     */
    private $_session;

    /**
     * Contains application-level attributes, keyed by identifier name.
     * @var object ApplicationContext
     * @access private
     */
    private $_application;


    /**
     * Creates a new PageContext.
     * @param string $currentPage Relative path to currently executing PHPX page
     */
    public function __construct($currentPage)
    {
        $this->pRO_currentPage = $currentPage;

        $pageCtxArr = array();
        $this->_page = new ScopeContextWrapper($pageCtxArr);
        $this->_request = new ScopeContextWrapper($_REQUEST);
        $this->_session = new ScopeContextWrapper($_SESSION);
        $this->_application = ApplicationContext::getInstance();
    }

    /**
     * Returns the application context.
     * @return object ApplicationContext Application context
     */
    public function getApplicationContext()
    {
        return ApplicationContext::getInstance();
    }

	/**
	 * Returns the $_SESSION superglobal.
	 * @return object ScopeContextWrapper Session array
	 */
	public function getSession()
	{
		return $this->_session;
	}

	/**
	 * Returns the $_REQUEST superglobal.
	 * @return object ScopeContextWrapper Request array
	 */
	public function getRequest()
	{
		return $this->_request;
	}

	/**
	 * Searches for the attribute in the specified scope context.  Looks in page
	 * scope if no scope specified.
	 * @param string $name Attribute name
	 * @param string $scope One of page, request, session, or application (optional)
	 * @return mixed Requested value or null if not found
	 */
	public function &getAttribute($name, $scope = null)
	{
		// get scope array
		$scopeCtx = $this->_getScopeContextArray($scope);

		// look up object in scope array
		$obj = null;
        if ($this->_existsInScope($name, $scope))
			$obj =& $scopeCtx->getAttribute($name);

		return $obj;
	}

	/**
	 * Replaces the attribute keyed by the specified name in the specified
	 * scope context.  The operation takes place in page scope if no scope is
	 * specified.
	 * @param string $name Attribute name
	 * @param mixed $obj Value to set
	 * @param string $scope One of page, request, session, or application (optional)
	 */
	public function setAttribute($name, &$value, $scope = null)
	{
		// get scope array
		$scopeCtx = $this->_getScopeContextArray($scope);

		// add value to scope array
		$scopeCtx->setAttribute($name, $value);
	}

	/**
	 * Removes the attribute keyed by the specified name in the specified
	 * scope context.  The attribute is removed from all scopes if no scope is
	 * specified.
	 * @param string $name Attribute name
	 * @param string $scope One of page, request, session, or application (optional)
	 */
	public function removeAttribute($name, $scope = null)
	{
		// get scope array
		if (! is_null($scope))
		{
			// get scope array
			$scopeCtx = $this->_getScopeContextArray($scope);

			// remove object from scope array
			$scopeCtx->removeAttribute($name);
		}
		else
		{
			// see if this object exists in any scope
			$scope = $this->getAttributesScope($name);

			// if it does, remove it
			while (! is_null($scope))
			{
				// get scope array
				$scopeCtx = $this->_getScopeContextArray($scope);

				// remove object from this scope array
				$scopeCtx->removeAttribute($name);

				// see if this object exists in any other scope
				$scope = $this->getAttributesScope();
			}
		}
	}

	/**
	 * Searches for the attribute in all scope contexts.
	 * @param string $name Attribute name
	 * @return object Requested object or null if not found
	 */
	public function &findAttribute($name)
	{
		$scope = $this->getAttributesScope($name);

		$obj = null;
		if (! is_null($scope))
		{
		    //echo "getting '$name' found in scope '$scope'<br>\n";
			$scopeCtx = $this->_getScopeContextArray($scope);
			$obj =& $scopeCtx->getAttribute($name);
		}

		//echo "returning $name as: " . gettype($obj) . " <br>\n";

		return $obj;
	}

	/**
	 * Returns the first scope in which the specified attribute name is found.
	 * Search order is page, request, session, then application scopes.
	 * @param string $name Attribute name
	 * @return string Scope
	 */
	public function getAttributesScope($name)
	{
		$scope = self::SCOPE_PAGE;

		if (! $this->_existsInScope($name, $scope))
		{
			$scope = self::SCOPE_REQUEST;

			if (! $this->_existsInScope($name, $scope))
			{
				$scope = self::SCOPE_SESSION;

				if (! $this->_existsInScope($name, $scope))
				{
					$scope = self::SCOPE_APPLICATION;

					if (! $this->_existsInScope($name, $scope))
						$scope = null;
				}
			}
		}

		return $scope;
	}

	/**
	 * Returns an array of all attribute names in the specified scope.
	 * @param string $scope One of page, request, session, or application
	 * @return array Array of attribute names
	 */
	public function getAttributeNamesInScope($scope)
	{
		// get scope array
		$scopeCtx = $this->_getScopeContextArray($scope);
		return $scopeCtx->getAttributeNames();
	}

	/**
	 * Returns an array for a given scope.  Defaults to page scope if no scope
	 * is specified.
	 * @access private
	 * @param string $scope One of page, request, session, or application (optional)
	 * @return object ScopeContextArray|object ApplicationContext Scope context array
	 */
	private function _getScopeContextArray($scope = null)
	{
		// default to page scope if none specified
		if (is_null($scope))
			$scope = self::SCOPE_PAGE;

		if ($scope === self::SCOPE_PAGE)
			$scopeCtx = $this->_page;
		elseif ($scope === self::SCOPE_REQUEST)
			$scopeCtx = $this->_request;
		elseif ($scope === self::SCOPE_SESSION)
			$scopeCtx = $this->_session;
		elseif ($scope === self::SCOPE_APPLICATION)
			$scopeCtx = $this->_application;
        else
            throw new InertiaException("Invalid scope '$scope'.");

		return $scopeCtx;
	}

	/**
	 * Returns true if the specified attribute key exists in the specified
	 * scope.  Defaults to page scope if none specified.
	 * @access private
	 * @param string $name Attribute name
	 * @param string $scope One of page, request, session, or application (optional)
	 * @return boolean True if exists
	 */
	private function _existsInScope($name, $scope = null)
	{
        $scopeCtx = $this->_getScopeContextArray($scope);
        return array_search($name, $scopeCtx->getAttributeNames()) !== false;
	}
}

?>
