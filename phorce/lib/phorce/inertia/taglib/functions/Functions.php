<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\functions;

use \Countable;

/**
 * This tag library contains basic functions for use in PHP scripts.  This is
 * a port of the JSTL Functions tag library in JSP 1.2.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage Functions-TagLib
 */
class Functions
{
    /**
     * Returns the number of elements in an array or the length of a string.
     * Objects which implement the Countable interface are treated as arrays.
     * @param mixed $item
     * @return int Length
     */
    public static function length($item)
    {
        return is_array($item) || $item instanceof Countable
            ? count($item)          // treat as array
            : strlen($item);        // treat as string
    }

    /**
     * Removes whitespace from the beginning and end of a string.
     * @param string $string
     * @return string
     */
    public static function trim($string)
    {
        return trim($string);
    }

    /**
     * Converts all characters of a string to upper-case.
     * @param string $string
     * @return string
     */
    public static function toUpperCase($string)
    {
        return strtoupper($string);
    }

    /**
     * Converts all characters of a string to lower-case.
     * @param string $string
     * @return string
     */
    public static function toLowerCase($string)
    {
        return strtolower($string);
    }

    /**
     * Joins an array of strings by a delimiter.
     * @param array $strings Array of strings
     * @param string $delim Delimiter
     * @return string Joined string
     */
    public static function join(array $strings, $delim)
    {
        return implode($delim, $strings);
    }

    /**
     * Splits a string into an array by a delimiter.
     * @param string $string String to split
     * @param string $delim Delimiter
     * @return array Array of strings
     */
    public static function split($string, $delim)
    {
        return explode($delim, $string);
    }

    /**
     * Returns the index of the search string in the subject string.
     * @param string $string Subject string
     * @param string $search Search string
     * @return int Index
     */
    public static function indexOf($string, $search)
    {
        return strpos($string, $search);
    }

    /**
     * Replaces all occurrances of the search string with the replacement string
     * in the subject string.
     * @param string $string Subject string
     * @param string $search Search string
     * @param string $replacement Replacement string
     * @return string Result
     */
    public static function replace($string, $search, $replacement)
    {
        return str_replace($search, $relacement, $string);
    }

    /**
     * Returns whether the search string exists in the subject string.
     * @param string $string Subject string
     * @param string $search Search string
     * @return bool
     */
    public static function contains($string, $search)
    {
        return strpos($string, $search) !== false;
    }

    /**
     * Returns whether the search string exists in the subject string,
     * regardless of case sensitivity.
     * @param string $string Subject string
     * @param string $search Search string
     * @return bool
     */
    public static function containsIgnoreCase($string, $search)
    {
        return stripos($string, $search) !== false;
    }

    /**
     * Returns whether the search string is at the end of the subject string.
     * @param string $string Subject string
     * @param string $search Search string
     * @return bool
     */
    public static function endsWith($string, $search)
    {
        return strpos($string, $search) + strlen($search) == strlen($string);
    }

    /**
     * Returns whether the search string is at the start of the subject string.
     * @param string $string Subject string
     * @param string $search Search string
     * @return bool
     */
    public static function startsWith($string, $search)
    {
        return strpos($string, $search) === 0;
    }

    /**
     * Returns a subset of a string from a starting index to an ending index.
     * @param string $string Subject string
     * @param int $startIdx Starting index
     * @param int $endIdx Ending index
     * @return string
     */
    public static function substring($string, $startIdx, $endIdx)
    {
        return substr($string, $startIdx, $endIdx - $startIdx);
    }

    /**
     * Returns a subset of a string after the specified search string.  If the
     * search string is not contained within the subject string, returns the
     * empty string.
     * @param string $string Subject string
     * @param string $search Search string
     * @return string
     */
    public static function substringAfter($string, $search)
    {
        $idx = strpos($search);
        return $idx === false ? "" :
            substr($string, $idx + strlen($search));
    }

    /**
     * Returns a subset of a string before the specified search string.  If the
     * search string is not contained within the subject string, returns the
     * empty string.
     * @param string $string Subject string
     * @param string $search Search string
     * @return string
     */
    public static function substringBefore($string, $search)
    {
        $idx = strpos($search);
        return $idx === false ? "" : substr($string, 0, $idx);
    }

    /**
     * Escapes any special XML entities (&lt;, &gt;, &amp;, &apos, &quot;)
     * in the specified string.
     * @param string $string Subject string
     * @return string XML escaped string
     */
    public static function escapeXml($string)
    {
        return str_replace(
            array ('&', '"', "'", '<', '>'),
            array ('&amp;', '&quot;', '&apos;', '&lt;', '&gt;'),
            $string );
    }
}

?>
