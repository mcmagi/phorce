<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use \ReflectionClass;
use phorce\common\property\PropertyAccessException;
use phorce\common\property\PropertySupport;

/**
 * A wrapper around a class that is used to resolve constants.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class ConstantResolver implements PropertySupport
{
    /**
     * @var object ReflectionClass
     * @access private
     */
    private $_class;

    /**
     * Constructs a constant resolver for the specified class.
     * @param string $class Class name
     */
    public function __construct($class)
    {
        $this->_class = new ReflectionClass($class);
    }

    /**
     * Returns the value of the named constant.  Throws an exception if the
     * property does not exist.
     * @param string $name Constant name
     * @return mixed Constant value
     */
    public function &__get($name)
    {
        if (! $this->_class->hasConstant($name))
            throw new PropertyAccessException($this, $name, "No such constant");
        $temp = $this->_class->getConstant($name);
        return $temp;
    }

    /**
     * Cannot set a constant - throws an exception.
     * @param string $name Constant name
     * @param mixed $value Value
     */
    public function __set($name, $value)
    {
        throw new PropertyAccessException($this, $name, "Cannot set a constant");
    }

    /**
     * Returns true if the constant is present on the class.
     * @param string $name Constant name
     * @return boolean True if present
     */
    public function __isset($name)
    {
        return $this->_class->hasConstant($name);
    }

    /**
     * Cannot unset a constant - throws an exception.
     * @param string $name Constant name
     */
    public function __unset($name)
    {
        throw new PropertyAccessException($this, $name, "Cannot unset a constant");
    }
}

?>
