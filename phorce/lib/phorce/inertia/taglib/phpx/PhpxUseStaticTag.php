<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;

/**
 * Exposes a variable in page scope that may be used to resolve static members.
 * If the class specified does not exist, an exception will be thrown.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxUseStaticTag extends TagHandler
{
    /**
     * The name of the page scope variable to store the resolver.
     * this name.
     * @var string
     * @access private
     */
    private $p_id;

    /**
     * This specified the class name where the constants are located.
     * @var string
     * @access private
     */
    private $p_class;


    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // expose as variable in page scope
        $resolver = new StaticResolver($this->class);
        $this->_setPropertyValue($resolver, $this->id, null, PageContext::SCOPE_PAGE);

        return TagHandler::SKIP_BODY;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }
}

?>
