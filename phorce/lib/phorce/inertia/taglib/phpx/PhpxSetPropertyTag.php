<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;

/**
 * <p>Used to set a property on some object.  Use the <code>name</code>
 * attribute to select an object in page scope, and the <code>property</code>
 * attribute to specify the property to set.  This tag can be used in two modes,
 * controlled by the property tag:</p>
 *
 * <ul>
 *   <li><strong>single-property mode</strong> - Sets a single property with a
 *   specified value (via the <code>value</code> attribute) or from a request
 *   parameter.  The request parameter may be specified via the
 *   <code>param</code> attribute.  If not specified, a request parameter with
 *   the same name as the property will be used.</li>
 *   <li><strong>multi-property mode</strong> - By setting the
 *   <code>property</code> attribute to the value "*", then all request
 *   parameters will be set as properties on the named object.  No
 *   <code>param</code> or <code>value</code> attributes should be
 *   specified.</li>
 * </ul>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxSetPropertyTag extends TagHandler
{
    /**
     * Name of the object in page scope.  This is the object which will have its
     * properties set.
     * @var string
     * @access private
     */
    private $_name;

    /**
     * Property to set on the named object, or "*" to set all properties from
     * the request parameters.
     * properties set.
     * @var string
     * @access private
     */
    private $_property;

    /**
     * Request parameter to set the property from in single-property mode.  If
     * not specified, assumes the parameter and property have the same name.
     * @var string
     * @access private
     */
    private $_param;

    /**
     * Value to set the request parameter in single-property mode.
     * @var string
     * @access private
     */
    private $_value;


    /**
     * Returns the name of the object in page scope.  This is the object which
     * will have its properties set.
     * @return string Name of object
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Sets the name of the object in page scope.  This is the object which
     * will have its properties set.
     * @param string $v Name of object
     */
    public function setName($v)
    {
        $this->_name = $v;
    }

    /**
     * Returns the property to set on the named object, or "*" to set all
     * properties from the request parameters.
     * @return string Property
     */
    public function getProperty()
    {
        return $this->_property;
    }

    /**
     * Sets the property to set on the named object, or "*" to set all
     * properties from the request parameters.
     * @param string $v Property
     */
    public function setProperty($v)
    {
        $this->_property = $v;
    }

    /**
     * Returns the request parameter to set the property from in single-property
     * mode.  If not specified, assumes the parameter and property have the same
     * name.
     * @return string Parameter name
     */
    public function getParam()
    {
        return $this->_param;
    }

    /**
     * Sets the request parameter to set the property from in single-property
     * mode.  If not specified, assumes the parameter and property have the same
     * name.
     * @param string $v Parameter name
     */
    public function setParam($v)
    {
        $this->_param = $v;
    }

    /**
     * Returns the value to set the request parameter in single-property mode.
     * @return string Value
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Sets the value to set the request parameter in single-property mode.
     * @param string $v Value
     */
    public function setValue($v)
    {
        $this->_value = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        if ($this->_property == "*")
        {
            // star-mode: set all request parameters on this object
            foreach ($_REQUEST as $param => $value)
            {
                $this->_setPropertyValue($value, $this->_name, $param,
                    PageContext::SCOPE_PAGE);
            }
        }
        else
        {
            // if no value is supplied, look up request parameter
            if (is_null($this->_value))
            {
                // if param is null, use property as param name
                $param = is_null($this->_param) ? $this->_property :
                    $this->_param;

                $value = $this->_getPropertyValue($param, null,
                    PageContext::SCOPE_REQUEST);
            }
            else
                $value = $this->_value;

            // set single property value
            $this->_setPropertyValue($value, $this->_name, $this->_property,
                PageContext::SCOPE_PAGE);
        }

        return TagHandler::SKIP_BODY;
    }
}

?>
