<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;

/**
 * Locates or instantiates a named object in the specified scope, exposing it in
 * page scope.  If the object does not exist in the specified scope it will be
 * created with the class specified by the <code>class</code> attribute.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxUseObjectTag extends TagHandler
{
    /**
     * Name of the object in some scope.  A page variable will be created with
     * this name.
     * @var string
     * @access private
     */
    private $_id;

    /**
     * If the variable must be created, this specified the class name of the
     * variable to create.
     * @var string
     * @access private
     */
    private $_class;

    /**
     * Scope in which to look for (or create) the named object.
     * @var string
     * @access private
     */
    private $_scope;


    /**
     * Returns the name of the object in some scope.  A page variable will be
     * created with this name.
     * @return string Identifer name
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Sets the name of the object in some scope.  A page variable will be
     * created with this name.
     * @param string $v Identifer name
     */
    public function setId($v)
    {
        $this->_id = $v;
    }

    /**
     * Returns the class name of the variable to create if the object must be
     * instantiated.
     * @return string Class name
     */
    public function getClass()
    {
        return $this->_class;
    }

    /**
     * Sets the class name of the variable to create if the object must be
     * instantiated.
     * @param string $v Class name
     */
    public function setClass($v)
    {
        $this->_class = $v;
    }

    /**
     * Returns the scope in which to look for (or create) the named object.
     * @return string Scope
     */
    public function getScope()
    {
        return $this->_scope;
    }

    /**
     * Sets the scope in which to look for (or create) the named object.
     * @param string $v Scope
     */
    public function setScope($v)
    {
        $this->_scope = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // default is to skip tag content
        $newobj = false;

        // what scope do we look in?
        if (is_null($this->_scope))
            $scope = $this->_pageContext->getAttributesScope($this->_id);
        else
            $scope = $this->_scope;

        // locate object (optionally, in specified scope)
        $obj =& $this->_getPropertyValue($this->_id, null, $this->_scope);

        // if object is null and could not be located in any scope
        if (is_null($obj))
        {
            // create new instance if its a class
            if (! is_null($this->_class) && class_exists($this->_class))
            {
                $obj = new $this->_class();

                // if it's new, evaluate tag content
                $newobj = true;
            }

            // save variable in specified scope
            $this->_setPropertyValue($obj, $this->_id, null, $this->_scope);
        }

        // expose as variable in page scope
        $this->_setPropertyValue($obj, $this->_id, null,
            PageContext::SCOPE_PAGE);

        return $newobj ? TagHandler::EVAL_BODY_BUFFERED : TagHandler::SKIP_BODY;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }
}

?>
