<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\inertia\tagext\TagHandler;

/**
 * <p>Used to register a tag library for use within a PHP page.  The tag library
 * is defined with a prefix as it will appear in the page.  For example, a
 * prefix of "foo" will allow you to access tags as "&lt;foo:mytag&gt;").  The
 * URI provides a means to locate the tag library.  The tag library with the
 * specified URI is looked up in the inertia.ini so that the Tag Library
 * Descriptor (TLD) for the tag library can be loaded.</p>
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxTaglibTag extends TagHandler
{
    /**
     * The prefix for the tag library.
     * @var string
     * @access private
     */
    private $_prefix;

    /**
     * The URI for the tag library.
     * @var string
     * @access private
     */
    private $_uri;

    /**
     * Returns the prefix for the tag library.
     * @return string Prefix
     */
    public function getPrefix()
    {
        return $this->_prefix;
    }

    /**
     * Sets the prefix for the tag library.
     * @param string $v Prefix
     */
    public function setPrefix($v)
    {
        $this->_prefix = $v;
    }

    /**
     * Returns the URI for the tag library.
     * @return string URI
     */
    public function getUri()
    {
        return $this->_uri;
    }

    /**
     * Sets the URI for the tag library.
     * @param string $v URI
     */
    public function setUri($v)
    {
        $this->_uri = $v;
    }
}

?>
