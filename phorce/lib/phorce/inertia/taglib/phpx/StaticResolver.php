<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use \ReflectionClass;
use phorce\common\property\PropertyAccessException;
use phorce\common\property\PropertySupport;

/**
 * A wrapper around a class that is used to resolve static members.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class ConstantResolver implements PropertySupport
{
    /**
     * @var object ReflectionClass
     * @access private
     */
    private $_class;

    /**
     * Constructs a static member resolver for the specified class.
     * @param string $class Class name
     */
    public function __construct($class)
    {
        $this->_class = new ReflectionClass($class);
    }

    /**
     * Returns the value of the named static member.  Throws an exception if the
     * property does not exist.
     * @param string $name Static member name
     * @return mixed Static member value
     */
    public function &__get($name)
    {
        if (! isset($this->$name))
            throw new PropertyAccessException($this, $name, "No such static member");
        $temp =& $this->_class->getStaticPropertyValue($name);
        return $temp;
    }

    /**
     * Sets the value of the named static member.  Throws an exception if the
     * property does not exist.
     * @param string $name Static member name
     * @param mixed $value Value
     */
    public function __set($name, $value)
    {
        if (! isset($this->$name))
            throw new PropertyAccessException($this, $name, "No such static member");
        return $this->_class->setStaticPropertyValue($name, $value);
    }

    /**
     * Returns true if the public static member is present on the class.
     * @param string $name Static member name
     * @return boolean True if present
     */
    public function __isset($name)
    {
        if ($this->_class->hasProperty($name))
        {
            $prop = $this->_class->getProperty($name);
            return $prop->isPublic() && $prop->isStatic();
        }
        return false;
    }

    /**
     * Cannot unset a static member - throws an exception.
     * @param string $name Static member name
     */
    public function __unset($name)
    {
        throw new PropertyAccessException($this, $name, "Cannot unset a static member");
    }
}

?>
