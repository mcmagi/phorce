<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\common\web\ApplicationContext;

/**
 * Includes a local file and outputs its contents to the page.  The file will be
 * located in the local web application and executed via a
 * <code>include()</code> call (or similar construct as specified by the
 * <code>once</code> and <code>required</code> attributes).  If the path is
 * absolute (preceeding "/") then it will be considered relative to the Phorce
 * application root.  If the path is relative, then it will be considered
 * relative to the directory of the currently executing script.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxIncludeTag extends PhpxIncludeTagBase
{
    /**
     * Whether this resource is to be required.  Required resources are
     * included using the <code>require()</code> construct and cause a fatal
     * error if they do not exist.
     * @var boolean
     * @access private
     */
    private $_require = false;


    /**
     * Returns whether this resource is to be required.
     * @return boolean
     */
    public function getRequire()
    {
        return $this->_require;
    }

    /**
     * Sets whether this resource is to be required.
     * @param string $v
     */
    public function setRequire($v)
    {
        $this->_require = $v;
    }

    /**
     * Performs the inclusion of the constructed URL.
     * @access protected
     */
    protected function _doInclude()
    {
        $ctx = ApplicationContext::getInstance();
        $rd = $ctx->getRequestDispatcher($this->_url->getURL());

        if ($this->_require)
            $rd->requirePath();
        else
            $rd->includePath();
    }
}

?>
