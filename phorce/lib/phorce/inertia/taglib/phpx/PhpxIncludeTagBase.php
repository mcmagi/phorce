<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\inertia\tagext\TagHandler;
use \Net_URL2;

/**
 * A base class common to all tags which perform includes.  Included file
 * contents are commonly outputed to the web browser.  The included file should
 * be specified with the <code>page</code> attribute.  Extending classes must
 * also provide an implementation of <code>_doInclude()</code>, which is
 * expected to perform the include and output its contents.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 * @abstract
 */
abstract class PhpxIncludeTagBase extends TagHandler implements PhpxParamSupport
{
    /**
     * An absolute or relative path to include.  Absolute paths, prefixed with
     * a "/", are relative to the Phorce application root.  Relative paths are
     * relative to the currently executing script.
     * @var string
     * @access private
     */
    private $_page;

    /**
     * The URL to include, constructed during tag execution.
     * @var object Net_URL2
     * @access protected
     */
    protected $_url;


    /**
     * Returns the relative path.
     * @return string Path
     */
    public function getPage()
    {
        return $this->_page;
    }

    /**
     * Sets the relative path.
     * @param string $v Path
     */
    public function setPage($v)
    {
        $this->_page = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        $this->_url = new Net_URL2("");

        // if absolute, make relative to phorce application
        // if relative, make relative to script location
        if (substr($this->_page, 0, 1) == "/")
            $path = $this->_page;
        else
            $path = \phorce\makepath(dirname($this->pageContext->currentPage), $this->_page);

        $this->_url->setPath($path);

        return TagHandler::EVAL_BODY_INCLUDE;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        $this->_doInclude();

        return TagHandler::EVAL_PAGE;
    }

    /**
     * @see PhpxParamSupport::addParameter()
     */
    public function addParameter($name, $value)
    {
        $this->_url->setQueryVariable($name, $value);
    }

    /**
     * Performs the inclusion of the constructed URL.
     * @access protected
     * @abstract
     */
    protected abstract function _doInclude();
}

?>
