<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\inertia\tagext\TagHandler;

/**
 * Specifies a name/value parameter to a parent PHP tag.  For example, when
 * nested under a <code>&lt;phpx:include&gt;</code> tag, it signifies a
 * <code>name=value</code> parameter to append to the query string to be
 * included.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxParamTag extends TagHandler
{
    /**
     * The parameter name.
     */
    private $_name;

    /**
     * The parameter value.
     */
    private $_value;


    /**
     * Returns the parameter name.
     * @return string Parameter name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Sets the parameter name.
     * @param string $v Parameter name
     */
    public function setName($v)
    {
        $this->_name = $v;
    }

    /**
     * Returns the parameter value.
     * @return string Parameter value
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Sets the parameter value.
     * @param string $v Parameter value
     */
    public function setValue($v)
    {
        $this->_value = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        $parent = $this->findAncestorWithClass("PhpxParamSupport");
        if (! is_null($parent))
            $parent->addParameter($this->_name, $this->_value);
        else
        {
            trigger_error(
                "'param' tag does not have a parent tag that implements PhpxParamSupport",
                E_USER_WARNING);
        }

        return TagHandler::SKIP_BODY;
    }
}

?>
