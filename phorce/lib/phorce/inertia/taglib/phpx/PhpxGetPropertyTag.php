<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;

/**
 * Retrieves the specified proeprty from the specified object in page scope.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxGetPropertyTag extends TagHandler
{
    /**
     * Name of the object in page scope.
     * @var string
     * @access private
     */
    private $_name;

    /**
     * Property to get on the named object.
     * @var string
     * @access private
     */
    private $_property;


    /**
     * Returns the name of the object in page scope.
     * @return string Object name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Sets the name of the object in page scope.
     * @param string $v Object name
     */
    public function setName($v)
    {
        $this->_name = $v;
    }

    /**
     * Returns the property to get on the named object.
     * @return string Property
     */
    public function getProperty()
    {
        return $this->_property;
    }

    /**
     * Sets the property to get on the named object.
     * @param string $v Property
     */
    public function setProperty($v)
    {
        $this->_property = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // print the property from a page-scoped object
        echo $this->_getPropertyValue($this->_name, $this->_property,
            PageContext::SCOPE_PAGE);

        return TagHandler::SKIP_BODY;
    }
}

?>
