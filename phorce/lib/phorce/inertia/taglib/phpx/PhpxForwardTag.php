<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use phorce\common\web\ApplicationContext;
use phorce\inertia\tagext\TagHandler;

/**
 * Forwards control to the specified PHP file.  This tag behaves similar to the
 * <code>&lt;phpx:include</code> tag, only execution of the current page ceases
 * after the include is complete.  The file will be located in the local web
 * application and executed via a <code>include()</code> call.  If the path is
 * absolute (preceeding "/") then it will be conisdered relative to the Phorce
 * application root.  If the path is relative, then it will be considered
 * relative to the directory of the currently executing script.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class PhpxForwardTag extends PhpxIncludeTagBase
{
    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        parent::doEndTag();

        // skip the rest of this page after including content
        return TagHandler::SKIP_PAGE;
    }

    /**
     * Performs the inclusion of the constructed URL.
     * @access protected
     */
    protected function _doInclude()
    {
        $ctx = ApplicationContext::getInstance();
        $rd = $ctx->getRequestDispatcher($this->_url->getURL());
        $rd->forwardPath();
    }
}

?>
