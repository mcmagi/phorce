<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

use \ReflectionClass;
use phorce\common\property\PropertyAccessException;
use phorce\common\property\PropertySupport;
use phorce\inertia\InertiaException;

/**
 * A wrapper around a class that is used to resolve enum values.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class EnumResolver implements PropertySupport
{
    /**
     * @var string Class name
     * @access private
     */
    private $_class;

    /**
     * Constructs a enum resolver for the specified class.
     * @param string $class Class name
     */
    public function __construct($class)
    {
        $this->_class = $class;

        // test that class exists and it's an enum
        $rc = new ReflectionClass($class);
        if (! $rc->isSubclassOf('phorce\common\enum\Enum'))
            throw new InertiaException("Class '$class' is not an Enum");
    }

    /**
     * Returns the value of the named enum.  Throws an exception if the
     * property does not exist.
     * @param string $name Constant name
     * @return object Enum
     */
    public function &__get($name)
    {
        $class = $this->_class;
        $temp = $class::$name();
        return $temp;
    }

    /**
     * Cannot set a enum - throws an exception.
     * @param string $name Constant name
     * @param mixed $value Value
     */
    public function __set($name, $value)
    {
        throw new PropertyAccessException($this, $name, "Cannot set an enum");
    }

    /**
     * Returns true if the enum is present on the class.
     * @param string $name Constant name
     * @return boolean True if present
     */
    public function __isset($name)
    {
        $class = $this->_class;
        foreach ($class::values() as $e)
        {
            if ($e->__toString() === $name)
                return true;
        }
        return false;
    }

    /**
     * Cannot unset a enum - throws an exception.
     * @param string $name Constant name
     */
    public function __unset($name)
    {
        throw new PropertyAccessException($this, $name, "Cannot unset an enum");
    }
}

?>
