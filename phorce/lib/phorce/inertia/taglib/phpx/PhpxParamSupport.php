<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\phpx;

/**
 * A tag class which implements this interface will be a parent of and therefore
 * must accept <code>phpx:param</code> tags (i.e. the PhpxParamTag class).  The
 * implementing class must also provide a addParameter() method whereby the
 * name/value pair specified by the <code>phpx:param</code> tag will be
 * communicated to its parent.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
interface PhpxParamSupport
{
    /**
     * Accepts a parameter from a nested PhpxParamTag.
     * @param string $name Name of parameter
     * @param string $value Value of parameter
     */
    public function addParameter($name, $value);
}

?>
