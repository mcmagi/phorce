<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\core;

use phorce\inertia\tagext\TagHandler;

class CoreIfTag extends TagHandler
{
    private $_test;
    private $_var;
    private $_scope;

    public function getTest()
    {
        return $this->_test;
    }

    public function setTest($v)
    {
        $this->_test = $v;
    }

    public function getVar()
    {
        return $this->_var;
    }

    public function setVar($v)
    {
        $this->_var = $v;
    }

    public function getScope()
    {
        return $this->_scope;
    }

    public function setScope($v)
    {
        $this->_scope = $v;
    }

    public function doStartTag()
    {
        if (strlen($this->_var) > 0)
        {
            // save attribute in specified scope
            $this->_pageContext->setAttribute(
                $this->_var, $this->_test, $this->_scope);
        }

        return $this->_test ? TagHandler::EVAL_BODY_INCLUDE :
            TagHandler::SKIP_BODY;
    }
}

?>
