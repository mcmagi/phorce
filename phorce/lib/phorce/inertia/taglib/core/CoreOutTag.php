<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\core;

use phorce\inertia\tagext\TagHandler;
use phorce\inertia\taglib\functions\Functions;

class CoreOutTag extends TagHandler
{
    private $_value;
    private $_default;
    private $_escapeXml = true;

    public function getValue()
    {
        return $this->_value;
    }

    public function setValue($v)
    {
        $this->_value = $v;
    }

    public function getDefault()
    {
        return $this->_default;
    }

    public function setDefault($v)
    {
        $this->_default = $v;
    }

    public function getEscapeXml()
    {
        return $this->_escapeXml;
    }

    public function setEscapeXml($v)
    {
        $this->_escapeXml = ($v === "true" || $v === true);
    }

    public function doStartTag()
    {
        return TagHandler::EVAL_BODY_INCLUDE;
    }

    public function doEndTag()
    {
        // get tag content
        $content = ob_get_clean();

        // use default if value is null, and content if default is null
        $value = ! is_null($this->_value) ? $this->_value :
            ! is_null($this->_default) ? $this->_default :
            $content;

        // escape xml
        if ($this->_escapeXml)
            $value = Functions::escapeXml($value);

        // and output it
        echo $value;

        return TagHandler::EVAL_BODY_INCLUDE;
    }
}

?>
