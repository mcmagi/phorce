<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\core;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;
use phorce\inertia\taglib\functions\Functions;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class CoreSetTag extends TagHandler
{
    private $p_var;
    private $p_value;
    private $p_target;
    private $p_property;
    private $p_scope = PageContext::SCOPE_PAGE;

    public function doStartTag()
    {
        return TagHandler::EVAL_BODY_INCLUDE;
    }

    public function doEndTag()
    {
        // get tag content
        $content = ob_get_clean();

        if (strlen($this->value))
            $content = $this->value;

        if (strlen($this->var))
            $this->_setPropertyValue($content, $this->var, null, $this->scope);
        else
            $this->_setPropertyValue($content, $this->target, $this->property, $this->scope);

        return TagHandler::EVAL_BODY_INCLUDE;
    }
}

?>
