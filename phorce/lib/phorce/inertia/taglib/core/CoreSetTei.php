<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\taglib\core;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagExtraInfo;
use phorce\inertia\tagext\VariableInfo;
use phorce\inertia\tagparser\TagData;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class CoreSetTei extends TagExtraInfo
{
	/**
	 * Returns an array of variable info objects this tag must handle at
	 * translation time.
     * @param object TagData $tagData Info about start tag
	 * @return Array of Variable Infos
	 */
	public function getVariableInfo(TagData $tagData)
	{
		$infos = parent::getVariableInfo($tagData);

		$attrs =& $tagData->getAttributeMap();

		if (array_key_exists("var", $attrs) && (! array_key_exists("scope", $attrs) || $attrs["scope"] === PageContext::SCOPE_PAGE))
			$infos[] = new VariableInfo($attrs["var"], VariableInfo::SCOPE_AT_END);
		return $infos;
	}
}

?>
