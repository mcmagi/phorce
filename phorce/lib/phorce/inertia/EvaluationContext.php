<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia;

/**
 * An interface which abstracts a symbol lookup context for evaluation.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 */
interface EvaluationContext
{
    /**
     * Looks up the value of a symbol from the symbol table.
     * @param string $symbol Symbol name
     * @return mixed Symbol value
     */
    public function lookup($symbol);

    /**
     * Assigns a value to a symbol in the symbol table.
     * @param string $symbol Symbol name
     * @param mixed $value Symbol value
     */
    public function assign($symbol, $value);
}

?>
