<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el\expression;

use phorce\inertia\EvaluationContext;
use phorce\inertia\Expression;

class SymbolExpression implements Expression
{
    private $_name;

    public function __construct($name = null)
    {
        if (! is_null($name))
            $this->setSymbol($name);
    }

    public function setSymbol($name)
    {
        $this->_name = $name;
    }

    public function getSymbol()
    {
        return $this->_name;
    }

    public function evaluate(EvaluationContext $ctx)
    {
        return $ctx->lookup($this->_name);
    }
}

?>
