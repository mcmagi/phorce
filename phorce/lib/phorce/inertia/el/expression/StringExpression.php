<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el\expression;

use phorce\inertia\EvaluationContext;
use phorce\inertia\Expression;

class StringExpression implements Expression
{
    private $_str;

    public function __construct($str = null)
    {
        if (! is_null($str))
            $this->setString($str);
    }

    public function setString($str)
    {
        // strip out leading/trailing quotes
        $this->_str = substr($str, 1, strlen($str)-2);
    }

    public function evaluate(EvaluationContext $ctx)
    {
        return $this->_str;
    }
}

?>
