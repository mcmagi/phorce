<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el\expression;

use phorce\inertia\EvaluationContext;
use phorce\inertia\Expression;

class FunctionExpression implements Expression
{
    private $_prefix;
    private $_name;
    private $_arguments = array();

    public function __construct($prefix = null, $name = null,
        array $arguments = null)
    {
        if (! is_null($prefix))
            $this->setPrefix($prefix);
        if (! is_null($name))
            $this->setSymbol($name);
        if (! is_null($arguments))
            $this->_arguments = $arguments;
    }

    public function addOperand(Expression $operand)
    {
        $this->_arguments[] = $operand;
    }

    public function setPrefix($prefix)
    {
        $this->_prefix = $prefix;
    }

    public function getPrefix($prefix)
    {
        return $this->_prefix;
    }

    public function setSymbol($name)
    {
        $this->_name = $name;
    }

    public function getSymbol()
    {
        return $this->_name;
    }

    public function evaluate(EvaluationContext $ctx)
    {
        // evaluate function arguments
        $args = array();
        foreach ($this->_arguments as $expr)
            $args[] = $expr->evaluate($ctx);

        // invoke function
        return $ctx->invoke($this->_prefix, $this->_name, $args);
    }
}

?>
