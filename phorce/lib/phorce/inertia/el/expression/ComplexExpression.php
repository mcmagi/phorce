<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el\expression;

use phorce\inertia\EvaluationContext;
use phorce\inertia\Expression;
use phorce\inertia\Operator;

class ComplexExpression implements Expression
{
    private $_operands = array();
    private $_operator;

    public function __construct(Operator $op, Expression $arg1 = null,
        Expression $arg2 = null, Expression $arg3 = null)
    {
        $this->_operator = $op;

        if (! is_null($arg1))
            $this->_operands[] = $arg1;
        if (! is_null($arg2))
            $this->_operands[] = $arg2;
        if (! is_null($arg3))
            $this->_operands[] = $arg3;
    }

    public function addOperand(Expression $operand)
    {
        $this->_operands[] = $operand;
    }

    public function setOperator(Operator $op)
    {
        $this->_operator = $op;
    }

    public function evaluate(EvaluationContext $ctx)
    {
        return $this->_operator->operate($this->_operands, $ctx);
    }
}

?>
