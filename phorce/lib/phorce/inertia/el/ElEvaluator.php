<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\phpx\TaglibContext;

/**
 * Evaluates an Expression Language (EL) expression.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class ElEvaluator
{
    /**
     * Execution context of the current PHPX page.
     * @var object PageContext
     * @access private
     */
    private $_pageContext;

    /**
     * Taglib context for prefixed functions.
     * @var object TaglibContext
     * @access private
     */
    private $_taglibContext;

    /**
     * Creates an Evaluator with the specified contextual information.
     * @param object PageContext Execution context of the current PHPX page
     * @param object TaglibContext Taglib context for functions
     */
    public function __construct(PageContext $pageContext,
        TaglibContext $taglibContext)
    {
        $this->_pageContext = $pageContext;
        $this->_taglibContext = $taglibContext;
    }

    /**
     * Evaluates the EL expression.
     * @param string $expr EL Expression
     * @result mixed Expression result
     */
	public function evaluate($expr)
	{
        $ctx = new ElContext($this->_pageContext, $this->_taglibContext);

        // create lexer & parser
        $l = new ElLexer($expr);
        $p = new ElParser($ctx);

        // lex & parse the expression
        while ($l->yylex())
            $p->doParse($l->getParserToken(), $l->getValue());

        // signify end of input
        $p->doParse(0, 0);

        return $ctx->getResult();
	}
}

?>
