<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el;

use \Exception;

/**
 * This class is used to break an EL expression down into lexical symbols.  It
 * is autogenerated from the ElLexer.y grammer file.  Therefore, do not modify
 * the generated class directly!
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class ElLexer
{
    const STATE_INITIAL = 1;

    private $_N = 0;
    private $_data;
    private $_token;
    private $_value;
    private $_line = 1;
    private $_state = self::STATE_INITIAL;
    private $_ytoken;

    public function __construct($expr)
    {
        $this->_data = $expr;
    }

    public function getToken()
    {
        return $this->_token;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function getLine()
    {
        return $this->_line;
    }

    public function getParserToken()
    {
        return $this->_ytoken;
    }


    private $_yy_state = 1;
    private $_yy_stack = array();

    function yylex()
    {
        return $this->{'yylex' . $this->_yy_state}();
    }

    function yypushstate($state)
    {
        array_push($this->_yy_stack, $this->_yy_state);
        $this->_yy_state = $state;
    }

    function yypopstate()
    {
        $this->_yy_state = array_pop($this->_yy_stack);
    }

    function yybegin($state)
    {
        $this->_yy_state = $state;
    }




    function yylex1()
    {
        $tokenMap = array (
              1 => 0,
              2 => 0,
              3 => 0,
              4 => 0,
              5 => 0,
              6 => 0,
              7 => 0,
              8 => 0,
              9 => 0,
              10 => 0,
              11 => 0,
              12 => 0,
              13 => 0,
              14 => 0,
              15 => 0,
              16 => 0,
              17 => 0,
              18 => 0,
              19 => 0,
              20 => 0,
              21 => 0,
              22 => 0,
              23 => 1,
              25 => 0,
              26 => 1,
              28 => 1,
              30 => 0,
              31 => 0,
              32 => 0,
              33 => 0,
              34 => 0,
            );
        if ($this->_N >= strlen($this->_data)) {
            return false; // end of input
        }
        $yy_global_pattern = "/^([ \t\r\n]+)|^(\\+)|^(-)|^(\\*)|^(\/|div(?![A-Za-z0-9_]))|^(%|mod(?![A-Za-z0-9_]))|^(\\|\\||or(?![A-Za-z0-9_]))|^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])/";

        do {
            if (preg_match($yy_global_pattern, substr($this->_data, $this->_N), $yymatches)) {
                $yysubmatches = $yymatches;
                $yymatches = array_filter($yymatches, 'strlen'); // remove empty sub-patterns
                if (!count($yymatches)) {
                    throw new Exception('Error: lexing failed because a rule matched' .
                        'an empty string.  Input "' . substr($this->_data,
                        $this->_N, 5) . '... state INITIAL');
                }
                next($yymatches); // skip global match
                $this->_token = key($yymatches); // token number
                if ($tokenMap[$this->_token]) {
                    // extract sub-patterns for passing to lex function
                    $yysubmatches = array_slice($yysubmatches, $this->_token + 1,
                        $tokenMap[$this->_token]);
                } else {
                    $yysubmatches = array();
                }
                $this->_value = current($yymatches); // token value
                $r = $this->{'yy_r1_' . $this->_token}($yysubmatches);
                if ($r === null) {
                    $this->_N += strlen($this->_value);
                    $this->_line += substr_count($this->_value, "\n");
                    // accept this token
                    return true;
                } elseif ($r === true) {
                    // we have changed state
                    // process this token in the new state
                    return $this->yylex();
                } elseif ($r === false) {
                    $this->_N += strlen($this->_value);
                    $this->_line += substr_count($this->_value, "\n");
                    if ($this->_N >= strlen($this->_data)) {
                        return false; // end of input
                    }
                    // skip this token
                    continue;
                } else {                    $yy_yymore_patterns = array(
        1 => array(0, "^(\\+)|^(-)|^(\\*)|^(\/|div(?![A-Za-z0-9_]))|^(%|mod(?![A-Za-z0-9_]))|^(\\|\\||or(?![A-Za-z0-9_]))|^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        2 => array(0, "^(-)|^(\\*)|^(\/|div(?![A-Za-z0-9_]))|^(%|mod(?![A-Za-z0-9_]))|^(\\|\\||or(?![A-Za-z0-9_]))|^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        3 => array(0, "^(\\*)|^(\/|div(?![A-Za-z0-9_]))|^(%|mod(?![A-Za-z0-9_]))|^(\\|\\||or(?![A-Za-z0-9_]))|^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        4 => array(0, "^(\/|div(?![A-Za-z0-9_]))|^(%|mod(?![A-Za-z0-9_]))|^(\\|\\||or(?![A-Za-z0-9_]))|^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        5 => array(0, "^(%|mod(?![A-Za-z0-9_]))|^(\\|\\||or(?![A-Za-z0-9_]))|^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        6 => array(0, "^(\\|\\||or(?![A-Za-z0-9_]))|^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        7 => array(0, "^(&&|and(?![A-Za-z0-9_]))|^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        8 => array(0, "^(!|not(?![A-Za-z0-9_]))|^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        9 => array(0, "^(\\.)|^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        10 => array(0, "^(,)|^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        11 => array(0, "^(empty(?![A-Za-z0-9_]))|^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        12 => array(0, "^(instanceof(?![A-Za-z0-9_]))|^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        13 => array(0, "^(==|eq(?![A-Za-z0-9_]))|^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        14 => array(0, "^(!=|ne(?![A-Za-z0-9_]))|^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        15 => array(0, "^(<|lt(?![A-Za-z0-9_]))|^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        16 => array(0, "^(>|gt(?![A-Za-z0-9_]))|^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        17 => array(0, "^(<=|le(?![A-Za-z0-9_]))|^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        18 => array(0, "^(>=|ge(?![A-Za-z0-9_]))|^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        19 => array(0, "^(\\?)|^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        20 => array(0, "^(:)|^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        21 => array(0, "^('[^']*')|^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        22 => array(0, "^(\"([^\"])*\")|^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        23 => array(1, "^(null(?![A-Za-z0-9_]))|^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        25 => array(1, "^((true|false)(?![A-Za-z0-9_]))|^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        26 => array(2, "^(([0-9]*\\.)?[0-9]+)|^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        28 => array(3, "^([A-Za-z_][A-Za-z0-9_]*)|^(\\()|^(\\))|^(\\[)|^(\\])"),
        30 => array(3, "^(\\()|^(\\))|^(\\[)|^(\\])"),
        31 => array(3, "^(\\))|^(\\[)|^(\\])"),
        32 => array(3, "^(\\[)|^(\\])"),
        33 => array(3, "^(\\])"),
        34 => array(3, ""),
    );

                    // yymore is needed
                    do {
                        if (!strlen($yy_yymore_patterns[$this->_token][1])) {
                            throw new Exception('cannot do yymore for the last token');
                        }
                        $yysubmatches = array();
                        if (preg_match('/' . $yy_yymore_patterns[$this->_token][1] . '/',
                              substr($this->_data, $this->_N), $yymatches)) {
                            $yysubmatches = $yymatches;
                            $yymatches = array_filter($yymatches, 'strlen'); // remove empty sub-patterns
                            next($yymatches); // skip global match
                            $this->_token += key($yymatches) + $yy_yymore_patterns[$this->_token][0]; // token number
                            $this->_value = current($yymatches); // token value
                            $this->_line = substr_count($this->_value, "\n");
                            if ($tokenMap[$this->_token]) {
                                // extract sub-patterns for passing to lex function
                                $yysubmatches = array_slice($yysubmatches, $this->_token + 1,
                                    $tokenMap[$this->_token]);
                            } else {
                                $yysubmatches = array();
                            }
                        }
                    	$r = $this->{'yy_r1_' . $this->_token}($yysubmatches);
                    } while ($r !== null && !is_bool($r));
			        if ($r === true) {
			            // we have changed state
			            // process this token in the new state
			            return $this->yylex();
                    } elseif ($r === false) {
                        $this->_N += strlen($this->_value);
                        $this->_line += substr_count($this->_value, "\n");
                        if ($this->_N >= strlen($this->_data)) {
                            return false; // end of input
                        }
                        // skip this token
                        continue;
			        } else {
	                    // accept
	                    $this->_N += strlen($this->_value);
	                    $this->_line += substr_count($this->_value, "\n");
	                    return true;
			        }
                }
            } else {
                throw new Exception('Unexpected input at line' . $this->_line .
                    ': ' . $this->_data[$this->_N]);
            }
            break;
        } while (true);

    } // end function


    const INITIAL = 1;
    function yy_r1_1($yy_subpatterns)
    {
 return false;     }
    function yy_r1_2($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_PLUS;     }
    function yy_r1_3($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_MINUS;     }
    function yy_r1_4($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_MUL;     }
    function yy_r1_5($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_DIV;     }
    function yy_r1_6($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_MOD;     }
    function yy_r1_7($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_OR;     }
    function yy_r1_8($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_AND;     }
    function yy_r1_9($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_NOT;     }
    function yy_r1_10($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_DOT;     }
    function yy_r1_11($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_COMMA;     }
    function yy_r1_12($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_EMPTY;     }
    function yy_r1_13($yy_subpatterns)
    {
 $this->_ytoken = null;     }
    function yy_r1_14($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_EQ;     }
    function yy_r1_15($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_NE;     }
    function yy_r1_16($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_LT;     }
    function yy_r1_17($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_GT;     }
    function yy_r1_18($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_LE;     }
    function yy_r1_19($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_GE;     }
    function yy_r1_20($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_IF_THEN;     }
    function yy_r1_21($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_COLON;     }
    function yy_r1_22($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_SQ_STRING;     }
    function yy_r1_23($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_DQ_STRING;     }
    function yy_r1_25($yy_subpatterns)
    {
 $this->_ytoken = ELParser::EL_NULL;     }
    function yy_r1_26($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_BOOLEAN;     }
    function yy_r1_28($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_NUMBER;     }
    function yy_r1_30($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_SYMBOL;     }
    function yy_r1_31($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_L_PAREN;     }
    function yy_r1_32($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_R_PAREN;     }
    function yy_r1_33($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_L_BRACKET;     }
    function yy_r1_34($yy_subpatterns)
    {
 $this->_ytoken = ElParser::EL_R_BRACKET;     }


}

?>
