<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el\operator;

use \Countable;
use phorce\inertia\EvaluationContext;
use phorce\inertia\Operator;

class EmptyOperator implements Operator
{
    public function operate(array $operands, EvaluationContext $ctx)
    {
        $value = $operands[0]->evaluate($ctx);
        if (is_array($value) || $value instanceof Countable)
            return count($value) == 0;      // treat as array
        elseif (is_string($value))
            return strlen($value) == 0;     // treat as string
        return is_null($value);             // all else
    }
}

?>
