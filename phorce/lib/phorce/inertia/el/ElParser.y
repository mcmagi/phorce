%declare_class "class ElParser"
%include_class {
    private $_ctx;
    public function __construct($ctx) {
        $this->_ctx = $ctx;
    }
}

main ::= expr(A). { /* print_r(A); */ $this->_ctx->setResult(A->evaluate($this->_ctx)); }

expr(A) ::= EL_NULL. { A = new NullExpression(); }
expr(A) ::= EL_BOOLEAN(B). { A = new BooleanExpression(B); }
expr(A) ::= EL_NUMBER(B). { A = new NumericExpression(B); }
expr(A) ::= EL_SQ_STRING(B). { A = new StringExpression(B); }
expr(A) ::= EL_DQ_STRING(B). { A = new StringExpression(B); }
expr(A) ::= EL_L_PAREN expr(B) EL_R_PAREN. { A = B; }
expr(A) ::= EL_NOT expr(B). { A = new ComplexExpression(new NotOperator(), B); }
expr(A) ::= EL_EMPTY expr(B). { A = new ComplexExpression(new EmptyOperator(), B); }
expr(A) ::= EL_MINUS expr(B). [EL_NOT] { A = new ComplexExpression(new NegativeOperator(), B); }
expr(A) ::= expr(B) EL_PLUS expr(C). { A = new ComplexExpression(new AddOperator(), B, C); }
expr(A) ::= expr(B) EL_MINUS expr(C). { A = new ComplexExpression(new SubtractOperator(), B, C); }
expr(A) ::= expr(B) EL_MUL expr(C). { A = new ComplexExpression(new MultiplyOperator(), B, C); }
expr(A) ::= expr(B) EL_DIV expr(C). { A = new ComplexExpression(new DivideOperator(), B, C); }
expr(A) ::= expr(B) EL_MOD expr(C). { A = new ComplexExpression(new ModuloOperator(), B, C); }
expr(A) ::= expr(B) EL_EQ expr(C). { A = new ComplexExpression(new EqualOperator(), B, C); }
expr(A) ::= expr(B) EL_NE expr(C). { A = new ComplexExpression(new NotEqualOperator(), B, C); }
expr(A) ::= expr(B) EL_LT expr(C). { A = new ComplexExpression(new LessThanOperator(), B, C); }
expr(A) ::= expr(B) EL_GT expr(C). { A = new ComplexExpression(new LessThanOrEqualOperator(), B, C); }
expr(A) ::= expr(B) EL_LE expr(C). { A = new ComplexExpression(new GreaterThanOperator(), B, C); }
expr(A) ::= expr(B) EL_GE expr(C). { A = new ComplexExpression(new GreaterThanOrEqualOperator(), B, C); }
expr(A) ::= expr(B) EL_IF_THEN expr(C) EL_COLON expr(D). [EL_IF_THEN] { A = new ComplexExpression(new ConditionalOperator(), B, C, D); }
expr(A) ::= EL_SYMBOL(B). { A = new SymbolExpression(B); }
expr(A) ::= expr(B) EL_DOT EL_SYMBOL(C). { A = new ComplexExpression(new PropertyOperator(), B, new SymbolExpression(C)); }
expr(A) ::= EL_SYMBOL(B) EL_L_BRACKET expr(C) EL_R_BRACKET. { A = new ComplexExpression(new PropertyOperator(), B, C); }
expr(A) ::= EL_SYMBOL(B) EL_COLON EL_SYMBOL(C) EL_L_PAREN arglist(D) EL_R_PAREN. { A = new FunctionExpression(B, C, D); }
arglist(A) ::= expr(B). { A = array(B); }
arglist(A) ::= arglist(B) EL_COMMA expr(C). { A = B; array_push(A, C); }

%left EL_COMMA.
%left EL_IF_THEN.
%left EL_OR.
%left EL_AND.
%nonassoc EL_EQ EL_NE.
%nonassoc EL_LT EL_GT EL_LE EL_GE.
%left EL_PLUS EL_MINUS.
%left EL_MUL EL_DIV EL_MOD.
%right EL_NOT EL_EMPTY.
%left EL_L_PAREN EL_R_PAREN.
%left EL_DOT EL_L_BRACKET EL_R_BRACKET.
%nonassoc EL_COLON.
