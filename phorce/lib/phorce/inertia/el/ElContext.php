<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\el;

use phorce\inertia\EvaluationContext;
use phorce\inertia\phpx\PageContext;
use phorce\inertia\phpx\TaglibContext;

/**
 * This class encapsulates runtime contextual information (such as symbols and
 * functions) used to evaluate EL expressions.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Inertia
 */
class ElContext implements EvaluationContext
{
    /**
     * The page context for symbol evaluation.
     * @var object PageContext
     * @access private
     */
    private $_pageContext;

    /**
     * The tag library context for function evaluation.
     * @var object TaglibContext
     * @access private
     */
    private $_taglibContext;

    /**
     * Result of the last EL evaluation.
     * @var mixed
     * @acces private
     */
    private $_result;

    /**
     * Constructs an EL context with the specified PHPX contextual information.
     * @param object PageContext $pageContext
     * @param object TaglibContext $taglibContext
     */
    public function __construct(PageContext $pageContext,
        TaglibContext $taglibContext)
    {
        $this->_pageContext = $pageContext;
        $this->_taglibContext = $taglibContext;
    }

    /**
     * Looks up the value of a symbol from the symbol table.
     * @param string $symbol Symbol name
     * @return mixed Symbol value
     *
     * @todo There are several special symbols in EL which are not handled at
     * present.
     */
    public function lookup($symbol)
    {
        // TODO: special symbols
        //  - pageContext
        //  - pageScope
        //  - requestScope
        //  - sessionScope
        //  - applicationScope
        //  - param
        //  - paramValues
        //  - header
        //  - headerValues
        //  - cookie
        //  - initParam
        return $this->_pageContext->findAttribute($symbol);
    }

    /**
     * Assigns a value to a symbol in the symbol table.
     * @param string $symbol Symbol name
     * @param mixed $value Symbol value
     */
    public function assign($symbol, $value)
    {
        // no assignment in EL
    }

    /**
     * Invokes an EL function.
     * @param string $prefix Function prefix
     * @param string $symbol Function name
     * @param array $arguments Function arguments
     * @return mixed Function result
     *
     * @todo This method is not in the <code>EvaluationContext</code> interface
     * and likely cannot be with its current method signature.  Therefore, it
     * may need some rethinking.
     */
    public function invoke($prefix, $symbol, array $arguments)
    {
        $taglib = $this->_taglibContext->getTagLibrary($prefix);
        $fcnlib = $taglib->getFunctionInstance($symbol);

        $class = new ReflectionClass(get_class($fcnlib));
        $method = $class->getMethod($symbol);
        return $method->invokeArgs($fcnlib, $arguments);
    }

    /**
     * Sets the last evaluation result.
     * @param mixed $v Evaluation result
     */
    public function setResult($v)
    {
        $this->_result = $v;
    }

    /**
     * Returns the last evaluation result.
     * @return mixed Evaluation result
     */
    public function getResult()
    {
        return $this->_result;
    }
}

?>
