<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\request;

use phorce\common\request\IncludeRequestDispatcher;
use phorce\common\request\RequestDispatcher;
use phorce\inertia\InertiaException;
use phorce\inertia\tagparser\CustomTagParser;

/**
 * This is a request dispatcher implementation used to handle inclusion requests
 * for Inertia PHPX files.  For each PHPX file requested, a PHP file gets
 * generated in the Phorce working directory.  If the generated PHP file does
 * not exist on the filesystem or is older than its corresponding PHPX file, a
 * new PHP file will get generated in the working directory.  Lastly, the file
 * gets included.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage Request
 */
class InertiaRequestDispatcher implements RequestDispatcher
{
    /**
     * The path to the PHPX file to include.
     * @var string
     * @access private
     */
    private $_path;

    /**
     * Creates a dispatcher for the specified request path.
     * @param string $path Request path
     */
    public function __construct($path)
    {
        $this->_path = $path;
    }

    /**
     * Calls <code>include</code> on the PHP file generated from the request
     * path.
     */
    public function includePath()
    {
        $d = new IncludeRequestDispatcher($this->_handleRequest());
        $d->includePath();
    }

    /**
     * Calls <code>require</code> on the PHP file generated from the request
     * path.
     */
    public function requirePath()
    {
        $d = new IncludeRequestDispatcher($this->_handleRequest());
        $d->requirePath();
    }

    /**
     * Forwards to the incoming request via an include.  This function will
     * not return.
     */
    public function forwardPath()
    {
        $d = new IncludeRequestDispatcher($this->_handleRequest());
        $d->forwardPath();
    }

    /**
     * Sends a redirect to the browser on the requested path and stops
     * processing.  This function will not return.
     */
    public function redirectPath()
    {
        $d = new IncludeRequestDispatcher($this->_path);
        $d->redirectPath();
    }

    /**
     * Handles the request for a PHPX file.
     * @return string The PHP file generated from the PHPX file.
     * @access private
     */
    private function _handleRequest()
    {
        $phpxfile = PHORCE_WEBROOT . DIRECTORY_SEPARATOR . $this->_path;
        $phpfile = PHORCE_WORK . DIRECTORY_SEPARATOR . $this->_path . ".php";

        if (! file_exists($phpxfile))
            throw new InertiaException("Cannot find file " . $this->_path);

        // get file modified times
        $phpxtime = filemtime($phpxfile);
        $phptime = file_exists($phpfile) ? filemtime($phpfile) : 0;

        // if PHP file needs generation, generate it!
        if ($phpxtime > $phptime)
            $this->_generatePhpFile($phpxfile, $phpfile);

        // return path to php file relative to webroot
        return substr($phpfile, strlen(PHORCE_WEBROOT));
    }

    /**
     * Spawns a CustomTagHandler to generate a PHP file from a PHPX file.
     * @param string $infile Input PHPX file.
     * @param string $outfile Output PHP file.
     * @access private
     */
    private function _generatePhpFile($infile, $outfile)
    {
        $dir = dirname($outfile);

        if (! file_exists($dir))
            mkdir($dir, 0700, true);
        else if (! is_dir($dir))
            throw new InertiaException("Cannot generate PHP file - output directory '$dir' exists as file");

        // generate it
        $parser = new CustomTagParser($infile, $outfile);
        $parser->translateDocument();
    }
}

?>
