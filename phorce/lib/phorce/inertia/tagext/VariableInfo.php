<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\tagext;

/**
 * Retains information on exposing scoped variables as page variables and
 * vice-versa.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagExt
 */
class VariableInfo
{
    /**
     * A variable in page scope needed in tag scope.
     */
    const SCOPE_PASS_TO_TAG = 0;

    /**
     * A variable defined at the start of the tag and ends at the end of the
     * page.
     */
    const SCOPE_AT_BEGIN = 1;

    /**
     * A variable defined at the end of the tag and ends at the end of the page.
     */
    const SCOPE_AT_END = 2;

    /**
     * A variable defined at the end of the tag and ends at the end of the tag.
     */
    const SCOPE_NESTED = 3;


    /**
     * The variable name.
     * @var string
     * @access private
     */
	private $_varName;

    /**
     * The SCOPE_xxx constant which defines the behavior of this variable.
     * @var integer
     * @access private
     */
	private $_scope;


    /**
     * Constructor.
     * @param string $varName Name of the variable
     * @param integer $scope The SCOPE_xxx constant
     */
	public function __construct($varName, $scope)
	{
		$this->_varName = $varName;
		$this->_scope = $scope;
	}

    /**
     * Returns the variable name.
     * @return string
     */
	public function getVarName()
	{
		return $this->_varName;
	}

    /**
     * Returns the variable scope behavior.
     * @return integer
     */
	public function getScope()
	{
		return $this->_scope;
	}

    /**
     * Returns true if the variable is defined before the beginning of the
     * element.
     */
	public function isAtBegin()
	{
		return $this->_scope == self::SCOPE_AT_BEGIN;
	}

    /**
     * Returns true if the variable is defined after the end of the element.
     */
	public function isAtEnd()
	{
		return $this->_scope == self::SCOPE_AT_END;
	}

    /**
     * Returns true if the variable is only defined within the element.
     */
	public function isNested()
	{
		return $this->_scope == self::SCOPE_NESTED;
	}

    /**
     * Returns true if the variable is passed from the page to the tag.
     */
	public function isPassToTag()
	{
		return $this->_scope == self::SCOPE_PASS_TO_TAG;
	}
}

?>
