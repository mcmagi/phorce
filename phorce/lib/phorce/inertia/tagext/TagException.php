<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\tagext;

use phorce\PhorceException;

/**
 * Represents an exception thrown by a TagHandler.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagExt
 */
class TagException extends PhorceException
{
    private $_tag;

    /**
     * Constructs the TagException.
     * @param string $message Optional message
     */
    public function __construct(TagHandler $tag, $message = "")
    {
        parent::__construct($message);

        $this->_tag = $tag;
    }

    public function getTag()
    {
        return $this->_tag;
    }
}

?>
