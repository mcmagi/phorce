<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia\tagext;

use phorce\common\property\PropertyObject;
use phorce\common\property\PropertyUtils;
use phorce\inertia\phpx\PageContext;

/**
 * Base class for all custom tags.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Inertia
 * @subpackage TagExt
 * @abstract
 */
abstract class TagHandler extends PropertyObject
{
    /**
     * A tag return value which directs the page to evaluate the content of a
     * tag.  Can be returned by doStartTag().
     */
    const EVAL_BODY_INCLUDE = 0;

    /**
     * A tag return value which directs the page to evaluate the remainder of
     * the page.  Can be returned by doEndTag().
     */
    const EVAL_PAGE = 1;

    /**
     * A tag return value which directs the page to skip the contents of a tag.
     * Can be returned by doStartTag() or doAfterBody().
     */
    const SKIP_BODY = 2;

    /**
     * A tag return value which directs the page to skip the remainder of the
     * page.  Can be returned by doEndTag().
     */
    const SKIP_PAGE = 3;

    /**
     * A tag return value which directs the page to evaluate the content of a
     * tag again.  Can be returned by doAfterBody().
     */
    const EVAL_BODY_AGAIN = 4;

    /**
     * A tag return value which directs the page to evaluate the content of a
     * tag, but suppress outputting its contents until the end tag is reached.
     * Not yet implemented.  Can be returned by doStartTag().
     * @todo
     */
    const EVAL_BODY_BUFFERED = 5;


    /**
     * The parent TagHandler or null if we are the root.
     * @var object TagHandler
     * @access private
     */
	private $_parent;

    /**
     * The body of the tag as a string.
     * @var string
     * @access private
     */
	private $_body;

    /**
     * The context for the current page, retaining variables accessable in page,
     * request, session, or application scopes.
     * @var object PageContext
     * @access protected
     */
	protected $_pageContext;


	// TODO:
	// write a static method to escape special characters
	// in parameters passed to the tag (like value="$1.00")

	/**
	 * Invoked when a start tag is encoutered during run-time.  Can return
	 * SKIP_BODY, EVAL_BODY_INCLUDE, or EVAL_BODY_BUFFERED.  By
	 * default, the body of the tag is outputted and therefore returns
	 * EVAL_BODY_INCLUDE.
	 * @return integer SKIP_BODY, EVAL_BODY_INCLUDE, or EVAL_BODY_BUFFERED
	 */
	public function doStartTag()
	{
		return self::EVAL_BODY_INCLUDE;
	}

	/**
	 * Invoked after setBodyContent and before the body is evaluated if
	 * doStartTag() returned EVAL_BODY_BUFFERED.  There is no return value.
	 */
	public function doInitBody()
	{
	}

	/**
	 * Invoked after the tag body is evaluated to determine if the body must
	 * be re-evaluated.  Can return SKIP_BODY or EVAL_BODY_AGAIN.  By
	 * default, we do not-reevaulte the body, therefore returns SKIP_BODY.
	 * @return integer SKIP_BODY or EVAL_BODY_AGAIN
	 */
	public function doAfterBody()
	{
		// by default, do not re-evaluate the body
		return self::SKIP_BODY;
	}

	/**
	 * Invoked when an end tag is encountered during run-time.
	 * Can return SKIP_PAGE or EVAL_PAGE.  By default, we continue
	 * processing the rest of the page, therefore it returns EVAL_PAGE.
	 * @return integer SKIP_PAGE or EVAL_PAGE
	 */
	public function doEndTag()
	{
		return self::EVAL_PAGE;
	}

	/**
	 * Sets the TagHandler this tag is nested within.
	 * @param object TagHandler $parent Parent TagHandler
	 */
	public function setParent($parent)
	{
		$this->_parent = $parent;
	}

	/**
	 * Gets the TagHandler this tag is nested within.
	 * @return object TagHandler Parent TagHandler
	 */
	function getParent()
	{
		return $this->_parent;
	}

	/**
	 * Sets the inner html between the start and end tags.
	 * @param string $body Inner HTML
	 */
	function setBodyContent($body)
	{
		$this->_body =& $body;
	}

	/**
	 * Returns the inner html between the start and end tags.
	 * @return string Inner HTML
	 */
	public function &getBodyContent()
	{
		return $this->_body;
	}

    /**
     * Appends content to the page body.
     * @param string $body Page content
     */
	public function addBodyContent($body)
	{
		$this->_body .= $body;
	}

    /**
     * Sets the page context for this tag.
     * @param object PageContext $ctx
     */
	public function setPageContext(PageContext $ctx)
	{
		$this->_pageContext = $ctx;
	}

    /**
     * Returns the page context for this tag.
     * @return object PageContext
     */
	public function getPageContext()
	{
		return $this->_pageContext;
	}

	/**
	 * Recursively finds a parent tag that is or extends a class
	 * with the supplied class name.
	 * @param string $className Class of parent TagHandler
	 * @return object TagHandler Parent TagHandler
	 */
	public function findAncestorWithClass($className)
	{
		if (is_null($this->_parent))
			$obj = null;
		else if ($this->_parent instanceof $className)
			$obj = $this->_parent;
		else
			$obj = $this->_parent->findAncestorWithClass($className);
        return $obj;
	}

	/**
	 * Returns the object referenced by the specified name and property.
	 * @param string $name Name of object in some scope
	 * @param string $property A property on that object (optional)
	 * @param integer $scope Scope in which to find object (optional)
	 * @return mixed Referenced property value
     * @access protected
	 */
	protected function &_getPropertyValue($name, $property = null, $scope = null)
	{
		// get named object in some scope
        if (is_null($scope))
		    $obj =& $this->_pageContext->findAttribute($name);
        else
		    $obj =& $this->_pageContext->getAttribute($name, $scope);

		// get property from named object, if specified
		if (! is_null($property))
			$obj =& PropertyUtils::getProperty($obj, $property);

		// return property value
		return $obj;
	}

	/**
	 * Sets the object referenced by the specified name and property in some
	 * scope.
	 * @param mixed $value Value of object
	 * @param string $name Name of object in some scope
	 * @param string $property A property on that object (optional)
	 * @param integer $scope Scope in which to find object (optional)
     * @access protected
	 */
	protected function _setPropertyValue(&$value, $name, $property = null, $scope = null)
	{
		if (! is_null($property))
		{
			// set property on named object
			$obj =& $this->_pageContext->getAttribute($name, $scope);
            if (! is_null($obj))
			    PropertyUtils::setProperty($obj, $property, $value);
		}
		else
			// replace named object
			$this->_pageContext->setAttribute($name, $value, $scope);
	}
}

?>
