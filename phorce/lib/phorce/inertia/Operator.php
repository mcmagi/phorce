<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\inertia;

interface Operator
{
    /**
     * Performs an operation on the specified operands using an evaluation
     * context.
     * @param array $operands Array of Expression objects as operands
     * @param object EvaluationContext $ctx Symbol lookup table
     */
    public function operate(array $operands, EvaluationContext $ctx);
}

?>
