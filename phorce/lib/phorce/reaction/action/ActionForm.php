<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

use phorce\common\property\PropertyObject;

/**
 * This class represents a user-interactive Form object.  A form contains
 * properties for building a page and retains all user-submitted values.  It
 * serves as a buffer between the action and the page.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class ActionForm extends PropertyObject
{
    /**
     * Validates that the form criteria is correct.  If there are validation
     * errors, this method must return an ActionErrors object.  The default
     * implementation does no validation.
     * @param object ActionMapping $mapping ActionMapping for the event
     * @return object ActionErrors If any error messages
     */
    public function validate(ActionMapping $mapping)
    {
        return null;
    }

    /**
     * Resets the form after the action is run and before the page is displayed.
     * Used primarily to clear checkboxes.
     * @param object ActionMapping $mapping ActionMapping for the event
     */
    public function reset(ActionMapping $mapping)
    {
    }
}
?>
