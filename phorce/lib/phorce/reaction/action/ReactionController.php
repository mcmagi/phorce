<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

use phorce\common\cache\PhorceConfigCache;
use phorce\common\cache\FusionDAOConfigFileLoader;
use phorce\reaction\Globals;
use phorce\reaction\tiles\TilesRequestProcessor;

/**
 * Represents the main Controller process of the MVC framework.  Once
 * instantiated, a call to process() will begin processing the event.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class ReactionController
{
    const REACTION_CONFIG = "reaction-config.xml";

    /**
     * The ReactionConfig loaded from the config file.
     * @var object ReactionConfig
     * @access private
     */
    private $_config;

    /**
     * Internal message resources.
     * @var object MessageResources
     * @access private
     */
    private $_internal;

    /**
     * Constructor.  Loads controller.ini.
     * @param string $cfgFile
     */
    private function __construct($cfgFile)
    {
        // load controller config
        $this->_config = PhorceConfigCache::retrieveConfig(
            "reaction-config", new FusionDAOConfigFileLoader(
                $cfgFile, 'phorce\reaction\config\ReactionConfigDAO'));

        //$dao = new ReactionConfigDAO($cfgFile);
        //$this->_config = $dao->load();

        // load message resources
        $this->initModuleMessageResources();

        // load internal message resources
        //$this->_internal = MessageResources::getMessageResources();
    }

    /**
     * Returns a single controller instance.  A created Controller is stored
     * in the request under Globals::CONTROLLER_KEY.  If one already exists, it
     * will be reused.
     * @param string $cfgFile Location of configuration file
     * @return object Controller
     * @static
     */
    public static function getInstance()
    {
        if (! isset($_REQUEST[Globals::CONTROLLER_KEY]))
        {
            $cfg = \phorce\makepath(PHORCE_CONFIG, self::REACTION_CONFIG);
            $_REQUEST[Globals::CONTROLLER_KEY] = new ReactionController($cfg);
        }

        return $_REQUEST[Globals::CONTROLLER_KEY];
    }

    /**
     * Returns the Reaction Controller configuration.
     * @return object ReactionConfig
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * Returns the action config with the specified path.
     * @param string $path Path to action
     * @return object ActionConfig
     */
    public function findActionConfig($path)
    {
        foreach ($this->_config->actionMappings->actions as $action)
        {
            if ($action->path == $path)
                return $action;
        }
        return null;
    }

    /**
     * Returns the action config with the specified path.
     * @param string $name Name of formBean
     * @return object FormBeanConfig
     */
    public function findFormBeanConfig($name)
    {
        foreach ($this->_config->formBeans->formBeans as $formBean)
        {
            if ($formBean->name == $name)
                return $formBean;
        }
        return null;
    }

    /**
     * Returns the global forward config with the specified name.
     * @param string $name Name of forward
     * @return object ActionForward
     */
    public function findGlobalForward($name)
    {
        foreach ($this->_config->globalForwards->forwards as $f)
        {
            if ($f->name == $name)
                return $f;
        }
        return null;
    }

    /**
     * Returns the message resources configured for this controller.
     * @return object MessageResources Message resources bundle
     * @todo No internal messages yet
     */
    public function getInternal()
    {
        return $this->_internal;
    }

    /**
     * Handles an event.  Results are posted to the Form object, the form is
     * validated, the action is executed, then the ActionForward is loaded.
     * @param string $action Action name in reaction config
     */
    public function process($action)
    {
        // this should be configurable
        $processor = new TilesRequestProcessor($this, $action);
        $processor->process();
    }

    /**
     * Initializes the message resources configuration.
     */
    public function initModuleMessageResources()
    {
        // loop through all resource configs
        foreach ($this->_config->messageResourcess as $msgResCfg)
        {
            // create configured factory object
            $factoryClass = $msgResCfg->factory;
            $factory = new $factoryClass();
            $factory->returnNull = $msgResCfg->null;

            // create resources and store in request
            $_REQUEST[$msgResCfg->key] = $factory->createResources(
                $msgResCfg->parameter);
        }
    }

    /*private static function _repairPostArrays($data)
    {
        // combine rawpost and $_POST ($data) to rebuild broken arrays in $_POST
        $rawpost = "&".file_get_contents("php://input");
        while(list($key,$value)= each($data))
        {
            $pos = preg_match_all("/&" . $key . "=([^&]*)/i", $rawpost, $regs,
                PREG_PATTERN_ORDER);
            if (! is_array($value) && $pos > 1)
            {
                $qform[$key] = array();
                for($i = 0; $i < $pos; $i++)
                    $qform[$key][$i] = urldecode($regs[1][$i]);
            }
            else
                $qform[$key] = $value;
        }

        return $qform;
    }*/
}
?>
