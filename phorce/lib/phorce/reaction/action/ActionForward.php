<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

use phorce\reaction\config\ForwardConfig;

/**
 * This class represents a forwarding to a target file, typically loaded from
 * the controller configuration.  An ActionForward is selected when an Action
 * completes.  It is used to determine which PHP file in the view component to
 * direct control to.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class ActionForward extends ForwardConfig
{
    /**
     * Creates an action forward with the specified values.
     * @param string $name Name of forward
     * @param string $path Path to forward file
     * @param bool $redirect Whether this is a redirecting forward
     */
    public function __construct($name = null, $path = null, $redirect = false)
    {
        $this->name = $name;
        $this->path = $path;
        $this->redirect = $redirect;
    }
}
