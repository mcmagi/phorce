<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

use phorce\common\property\PropertyObject;

/**
 * This class represents a message generated during an action.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class ActionMessage extends PropertyObject
{
    /**
     * The message resource key for this message.
     * @var string
     * @access private
     */
    private $_key;

    /**
     * Whether the key is to be looked up in the message resources (true)
     * or a literal string value (false).
     * @var bool
     * @access private
     */
    private $_resource = true;

    /**
     * The collection of substitution values.
     * @var array
     * @access private
     */
    private $_values;


    /**
     * Constructs an action message with the specified resource key and
     * substitution values.
     * @param string $key Message resource key
     * @param bool|array|string $v0 Has three possible meanings: (optional)
     *            -As a flag to indicate this is a resource key or literal value
     *            -As an array of substitution values
     *            -As Substitution value 0 (subsequent values would follow)
     * @param string $v1 Substitution value 1 (optional)
     * @param string $v2 Substitution value 2 (optional)
     * @param string $v3 Substitution value 3 (optional)
     */
    public function __construct($key, $v0 = null, $v1 = null, $v2 = null,
        $v3 = null)
    {
        $this->_key = $key;

        if (is_bool($v0))
            $this->_resource = $v0;
        elseif (is_array($v0))
            $this->_values = $v0;
        else
        {
            $this->_values = array();

            // add each supplied value to values array if not null
            if (! is_null($v0))
                $this->_values[] = $v0;
            if (! is_null($v1))
                $this->_values[] = $v1;
            if (! is_null($v2))
                $this->_values[] = $v2;
            if (! is_null($v3))
                $this->_values[] = $v3;
        }
    }

    /**
     * Returns the resource key for this message.
     * @return string Key
     */
    public function getKey()
    {
        return $this->_key;
    }

    /**
     * Returns substitution values as an array.
     * @return array Values array
     */
    public function getValues()
    {
        return $this->_values;
    }

    /**
     * Returns whether or not this is a bundle key (true) or literal value
     * (false).  A literal value is not looked up in the message resources.
     * @return bool Boolean
     */
    public function isResource()
    {
        return $this->_resource;
    }

    /**
     * Returns the ActionMessage in the form key[v1, v2, ...].
     * @return string String
     */
    public function __toString()
    {
        $valueStr = "";

        if ($this->_resource)
        {
            $valueStr = "[";
            foreach ($this->_values as $v)
            {
                if (strlen($valueStr) > 1)
                    $valueStr .= ", ";

                $valueStr .= $v;
            }
            $valueStr .= "]";
        }

        //if (count($this->_values) > 0)
            //$valueStr .= "]";

        return $this->_key . $valueStr;
    }
}

?>
