<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

use phorce\reaction\config\ActionConfig;

/**
 * This class offers a convenient way to access event configuration.  When an
 * action completes, findForward() is typically called to find the next page
 * to display.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class ActionMapping extends ActionConfig
{
    /**
     * Creates an ActionMapping instance.
     */
    public function __construct()
    {
        // call super
        parent::__construct();
    }

    /**
     * Returns an ActionForward with the specified name by looking in the local
     * forwards then in the global forwards.
     * @param string $name Name of forward
     * @return object ActionForward
     */
    public function findForward($name)
    {
        // find local forward config
        foreach ($this->forwards as $f)
        {
            if ($f->name == $name)
                return $f;
        }

        // find global forward config
        $controller = ReactionController::getInstance();
        return $controller->findGlobalForward($name);
    }

    /**
     * Returns a list of all locally-defined ActionForward objects in this
     * mapping.  Alias to getForwards().
     * @return array All locally-defined ActionForwards
     */
    public function findForwards()
    {
        return $this->forwards;
    }

    /**
     * Returns the page that control is forwarded to on form validation error.
     * @return object ActionForward
     */
    public function getInputForward()
    {
        return strlen($this->input) ? new ActionForward(null, $this->input, false) : null;
    }
}

?>
