<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

use phorce\NotFoundException;
use phorce\common\property\PropertyUtils;
use phorce\common\web\ApplicationContext;
use phorce\common\web\RequestWrapper;
use phorce\reaction\Globals;
use phorce\reaction\ReactionException;
use phorce\reaction\config\ForwardConfig;

/**
 * This class performs the Reaction processing logic.  This class may be
 * extended to provide additional request-time functionality.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class RequestProcessor
{
    /**
     * The controller that spawned this RequestProcessor.
     * @var object ReactionController
     * @access private
     */
    private $_controller;

    /**
     * The requested action path.
     * @var string
     * @access private
     */
    private $_action;

    /**
     * Contructs a RequestProcessor to handle the specified action.
     * @param object ReactionController The calling controller
     * @param string $action The action to process
     */
    public function __construct(ReactionController $c, $action)
    {
        $this->_controller = $c;
        $this->_action = $action;
    }

    /**
     * Performs the reaction processing logic.
     */
    public function process()
    {
        $req = new RequestWrapper($_REQUEST);

        // find action mapping
        $mapping = $this->processMapping();

        $req[Globals::MAPPING_KEY] = $mapping;

        // find or create action form
        $form = $this->processActionForm($mapping);

        $continue = true;
        if (! is_null($form))
        {
            // populate form
            $this->processPopulate($form, $mapping);

            // validate if not a cancellation
            if (! isset($req[Globals::CANCEL_KEY]) && $mapping->validate)
                $continue = $this->processValidate($form, $mapping);
        }

        // if validation was successful...
        if ($continue)
            $continue = $this->processInclude($mapping);

        // if there was no include...
        if ($continue)
            $continue = $this->processForward($mapping);

        // if there was no forward...
        if ($continue)
        {
            // create the action
            $action = $this->processActionCreate($mapping);
            if (is_null($action))
                throw new ReactionException("Cannot create action '{$mapping->type}' specified in action '{$this->_action}'");

            // execute the action
            $forward = $this->processActionPerform($action, $form, $mapping);
            if (is_null($forward) || ! $forward instanceof ActionForward)
                throw new ReactionException("No forward returned from action '{$mapping->path}'");

            // forward control to the next page
            $this->processForwardConfig($forward);
        }
    }

    /**
     * Returns the ActionMapping configured for the requested path.
     * @return object ActionMapping
     * @access protected
     */
    protected function processMapping()
    {
        $mapping = $this->_controller->findActionConfig($this->_action);

        // throw NotFoundException to indicate a 404 error
        if (is_null($mapping))
            //throw new ReactionException("No such action '{$this->_action}' exists in reaction config");
            throw new NotFoundException($this->_action);

        // exactly one of these must be specified
        $validateArr = array(
            $mapping->include, $mapping->forward, $mapping->type);

        $count = 0;
        foreach ($validateArr as $v)
        {
            if (! is_null($v))
                $count++;
        }

        if ($count == 0)
            throw new ReactionException(
                "Must specify one of 'include', 'forward', or 'type' for action '{$this->_action}'");

        if ($count > 1)
            throw new ReactionException(
                "Cannot specify more than one of 'include', 'forward', or 'type' for action '{$this->_action}'");

        return $mapping;
    }

    /**
     * Returns the ActionForm instance for the specified ActionMapping config.
     * @param object ActionMapping $mapping
     * @return object ActionForm
     * @access protected
     */
    protected function processActionForm(ActionMapping $mapping)
    {
        $form = null;
        $req = new RequestWrapper($_REQUEST);

        // return empty action form if no name is defined
        if (is_null($mapping->name))
            return new ActionForm();

        // get configured form bean for action mapping
        $fbc = $this->_controller->findFormBeanConfig($mapping->name);

        if (is_null($fbc))
            throw new ReactionException("Form '{$mapping->name}' referenced in action '{$mapping->path}' is not defined in reaction config.");

        // locate or create form in request/session
        if ($mapping->scope == "request" || ! strlen($mapping->scope))
        {
            if (! isset($req[$fbc->name]))
                $req[$fbc->name] = new $fbc->type;

            $form = $req[$fbc->name];
        }
        elseif ($mapping->scope == "session")
        {
            if (! array_key_exists($fbc->name, $_SESSION))
                $_SESSION[$fbc->name] = new $fbc->type;

            $form = $_SESSION[$fbc->name];
        }
        else
            throw new ReactionException("Unknown scope '{$mapping->scope}' for action '{$mapping->path}'");

        // return form instance
        return $form;
    }

    /**
     * Populates the ActionForm with request values.
     * @param object ActionForm $form
     * @param object ActionMapping $mapping
     * @access protected
     */
    protected function processPopulate(ActionForm $form, ActionMapping $mapping)
    {
        // do reset before populating
        $form->reset($mapping);

        // set form properties
        $this->_doPopulate($form, new RequestWrapper($_FILES));
        $this->_doPopulate($form, new RequestWrapper($_GET));
        $this->_doPopulate($form, new RequestWrapper($_POST));
    }

    /**
     * Populates the form with the values from a single array.  If no setter is
     * found on the form for the property, it is skipped.
     * @param object ActionForm $form
     * @param array $array Array of property values to save to form
     */
    private function _doPopulate(ActionForm $form, &$array)
    {
        foreach ($array as $name => $value)
        {
            $props =& PropertyUtils::splitPropertyNames($name);
            if (isset($form->{$props[0]}))
                PropertyUtils::setProperty($form, $name, $value);
        }
    }

    /**
     * Performs validation of the ActionForm and execute input forward on error.
     * @param object ActionForm $form
     * @param object ActionMapping $mapping
     * @return boolean True if form validated; false otherwise
     * @access protected
     */
    protected function processValidate(ActionForm $form, ActionMapping $mapping)
    {
        $req = new RequestWrapper($_REQUEST);

        // get any action errors
        $errors = $form->validate($mapping);
        if ($errors instanceof ActionErrors && ! $errors->isEmpty())
        {
            $req[Globals::ERROR_KEY] = $errors;

            $forward = $mapping->inputForward;
            if (is_null($forward))
                throw new ReactionException("Form has validation errors but no input forward found");

            $this->processForwardConfig($forward);

            return false;
        }

        return true;
    }

    /**
     * Creates an Action instance configured in the ActionMapping.
     * @param object ActionMapping $mapping
     * @return object Action The newly created Action
     * @access protected
     */
    protected function processActionCreate(ActionMapping $mapping)
    {
        $action = new $mapping->type;
        if (! $action instanceof Action)
            throw new ReactionException("Type '{$mapping->type}' in action '{$mapping->path}' is not an Action");
        $action->setController($this->_controller);
        return $action;
    }

    /**
     * Performs the specified action, returning the ActionForward.
     * @param object Action $action Action to perform
     * @param object ActionForm $form Passed to Action::execute()
     * @param object ActionMapping $mapping Passed to Action::execute()
     * @return object ActionForward Where we should forward to
     * @access protected
     */
    protected function processActionPerform(Action $action, ActionForm $form,
        ActionMapping $mapping)
    {
        return $action->execute($mapping, $form);
    }

    /**
     * Performs the forwarding to the specified forward.
     * @param object ForwardConfig $forward
     * @access protected
     */
    protected function processForwardConfig(ForwardConfig $forward)
    {
        if ($forward->redirect)
            $this->doRedirect($forward->path);
        else
            $this->doForward($forward->path);
    }

    /**
     * Process a forward configured for this mapping.
     * @param object ActionMapping $mapping
     * @return boolean False if included; true otherwise
     * @access protected
     */
    protected function processInclude(ActionMapping $mapping)
    {
        if (! is_null($mapping->include))
        {
            $this->doInclude($mapping->include);
            return false;
        }
        return true;
    }

    /**
     * Process a forward configured for this mapping.
     * @param object ActionMapping $mapping
     * @return boolean False if forwarded; true otherwise
     * @access protected
     */
    protected function processForward(ActionMapping $mapping)
    {
        if (! is_null($mapping->forward))
        {
            $this->doForward($mapping->forward);
            return false;
        }

        return true;
    }

    /**
     * Includes the specified URI.
     * @param string $uri
     * @access protected
     */
    protected function doInclude($uri)
    {
        $ctx = ApplicationContext::getInstance();
        $dispatcher = $ctx->getRequestDispatcher($uri);
        $dispatcher->includePath();
    }

    /**
     * Forwards to the specified URI.  Will not return.
     * @param string $uri
     * @access protected
     */
    protected function doForward($uri)
    {
        $ctx = ApplicationContext::getInstance();
        $dispatcher = $ctx->getRequestDispatcher($uri);
        $dispatcher->forwardPath();
    }

    /**
     * Redirects to the specified URI.  Will not return.
     * @param string $uri
     * @access protected
     */
    protected function doRedirect($uri)
    {
        $ctx = ApplicationContext::getInstance();
        $dispatcher = $ctx->getRequestDispatcher($uri);
        $dispatcher->redirectPath();
    }
}

?>
