<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

/**
 * This class represents a collection of ActionMessage objects that will be
 * treated as errors.  Errors, once saved to the request, are stored under the
 * ACTION_ERROR_KEY key of the $_REQUEST superglobal and are accessable through
 * the &lt;html:errors&gt; tag.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class ActionErrors extends ActionMessages
{
    /**
     * Creates an action errors collection.  Constructor.  Creates a copy of
     * the specified ActionErrors object or an empty collection if none
     * specified.
     * @param object ActionErrors $errors ActionErrors to copy from (optional)
     */
    public function __construct(ActionErrors $errors = null)
    {
        parent::__construct($errors);
    }
}

?>
