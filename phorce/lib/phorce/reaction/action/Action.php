<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

use phorce\reaction\Globals;

/**
 * This abstract class represents an action to be performed for an event.  When
 * a user triggers an event, the controller maps the event to an action and
 * invokes the abstract execute() method.
 * @package Reaction
 * @subpackage Action
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @abstract
 */
abstract class Action
{
    /**
     * The Controller which invoked this Action.
     * @var object Controller
     * @access protected
     */
    protected $_controller;

    /**
     * Executes the action this class represents.
     * @abstract
     * @param object ActionMapping $mapping Mapping for this action
     * @param object ActionForm $form Form associated with this event
     * @return object ActionForward
     */
    public abstract function execute(ActionMapping $mapping, ActionForm $form);

    /**
     * Returns the controller which invoked this action.
     * @return object Controller
     */
    public function getController()
    {
        return $this->_controller;
    }

    /**
     * Sets the Controller object which invoked this Action.  This is set by
     * the controller itself.
     * @param object Controller $controller The invoking controller
     */
    public function setController(ReactionController $c)
    {
        $this->_controller = $c;
    }

    /**
     * Adds action errors to the request.
     * @access protected
     * @param object ActionErrors $errors
     */
    protected function addErrors(ActionErrors $errors)
    {
        $this->getErrors()->add($errors);
    }

    /**
     * Saves action errors in the request.  This replaces any existing messages.
     * @access protected
     * @param object ActionErrors $errors
     */
    protected function saveErrors(ActionErrors $errors)
    {
        $_REQUEST[Globals::ERROR_KEY] = $errors;
    }

    /**
     * Gets action errors from the request.  If the request attribute is empty,
     * creates a new ActionErrors object.
     * @access protected
     * @return object ActionErrors
     */
    protected function getErrors()
    {
        if (array_key_exists(Globals::ERROR_KEY, $_REQUEST))
            $errors = $_REQUEST[Globals::ERROR_KEY];

        if (! isset($errors) || is_null($errors))
        {
            $errors = new ActionErrors();
            $this->saveErrors($errors);
        }

        return $errors;
    }

    /**
     * Saves action messages in the request.  This replaces any existing
     * messages.
     * @param object ActionMessages $msgs
     * @access protected
     */
    protected function addMessages(ActionMessages $msgs)
    {
        $this->getMessages()->add($msgs);
    }

    /**
     * Saves action messages in the request.  This replaces any existing
     * messages.
     * @param object ActionMessages $msgs
     * @access protected
     */
    protected function saveMessages(ActionMessages $msgs)
    {
        $_REQUEST[Globals::MESSAGE_KEY] = $msgs;
    }

    /**
     * Gets action messages from the request.  If the request attribute is
     * empty, creates a new ActionMessages object.
     * @access protected
     * @return object ActionMessages
     */
    protected function getMessages()
    {
        if (array_key_exists(Globals::MESSAGE_KEY, $_REQUEST))
            $messages = $_REQUEST[Globals::MESSAGE_KEY];

        if (! isset($messages) || is_null($messages))
        {
            $messages = new ActionMessages();
            $this->saveMessages($messages);
        }

        return $messages;
    }

    /**
     * Returns the message resources with the specified bundle key from the
     * request.  If no bundle key is specified, the default bundle key will
     * be used.
     * @access protected
     * @param string $bundleKey Key of the resource bundle in the request
     * @return object MessageResources
     */
    protected function getResources($bundleKey = Globals::MESSAGES_KEY)
    {
        return $_REQUEST[$bundleKey];
    }

    /**
     * Returns true if the cancel button was pressed on a form.
     * @return boolean True if cancelled
     */
    protected function isCancelled()
    {
        return array_key_exists(Globals::CANCEL_KEY, $_REQUEST);
    }
}

?>
