<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\action;

/**
 * This class represents a collection of ActionMessage objects generated during
 * an action.  Messages, once saved to the request, are stored under the
 * Globals::MESSAGE_KEY key of the $_REQUEST superglobal and are accessable
 * through the &lt;html:messages&gt; tag.  All messages added to this collection
 * are associated with a "property" which contextualizes the message.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Action
 */
class ActionMessages
{
    /**
     * An associative array of messages in this collection by property.  Keying
     * this array by property will return an array of messages associated with
     * that property.
     * @var array
     * @access private
     */
    private $_messages = array();

    /**
     * Whether the get() method has been called.
     * @var bool
     * @access private
     */
    private $_accessed = false;


    /**
     * Creates an action messages collection.  Constructor.  Creates a copy of
     * the specified ActionMessages object or an empty collection if none
     * specified.
     * @param object ActionMessages $messages ActionMessages to copy from
     */
    public function __construct(ActionMessages $messages = null)
    {
        // add any provided messages
        if (! is_null($messages))
            $this->add($messages);
    }

    /**
     * Returns whether or not this collection is empty.
     * @return bool True if empty
     */
    public function isEmpty()
    {
        return $this->size() == 0;
    }

    /**
     * Returns the number of messages associated with the specified property or
     * in the entire collection.
     * @param string $property The ActionMessage property (optional)
     * @return integer Size of collection or property
     */
    public function size($property = null)
    {
        $ctr = 0;
        if (is_null($property))
        {
            foreach ($this->properties() as $property)
                $ctr += count($this->_messages[$property]);
        }
        else
        {
            if (array_key_exists($property, $this->_messages))
                $ctr = count($this->_messages[$property]);
        }

        return $ctr;
    }

    /**
     * Returns whether the get() method has been called on this collection.
     * @return bool True if get() has been called
     */
    public function isAccessed()
    {
        return $this->_accessed;
    }

    /**
     * Returns an array of ActionMessage objects associated with the specified
     * property or all ActionMessage objects in the collection.
     * @param string $property The ActionMessage property (optional)
     * @return array Array of ActionMessage objects
     */
    public function get($property = null)
    {
        // we have been accessed at least once
        $this->_accessed = true;

        $arr = array();

        if (is_null($property))
        {
            // build array of all messages
            foreach ($this->properties() as $property)
            {
                $subarr = $this->_messages[$property];
                foreach (array_keys($subarr) as $i)
                    $arr[] = $subarr[$i];
            }
        }
        else
        {
            if (array_key_exists($property, $this->_messages))
                $arr = $this->_messages[$property];
        }

        return $arr;
    }

    /**
     * Adds a new message to this collection.
     * @param string|object ActionMessage $property
     * @param ActionMessage $message
     */
    public function add($property, $message = false)
    {
        if ($message === false)
        {
            // one-argument mode: first parameter is messages
            $messages = $property;

            foreach ($messages->properties() as $property)
            {
                $subarr = $messages->_messages[$property];
                foreach (array_keys($subarr) as $i)
                    $this->add($property, $subarr[$i]);
            }
        }
        else
        {
            // two-argument mode: property and message

            // if empty message list, create array
            if (! array_key_exists($property, $this->_messages))
                $this->_messages[$property] = array();

            // add message to list
            $this->_messages[$property][] = $message;
        }
    }

    /**
     * Returns an array of all properties in this collection.
     * @return array Array of properties
     */
    public function properties()
    {
        return array_keys($this->_messages);
    }

    /**
     * Clears all contents of this collection.
     */
    public function clear()
    {
        $this->_messages = array();
    }

    /**
     * Returns a string representation of all messages in this collection.
     * Each message is delimited by a newline.  The text for each message is
     * determined by a call to ActionMessage::__toString().
     * @see ActionMessage::__toString()
     * @return string String representation
     */
    public function __toString()
    {
        $str = "";

        // loop through properties
        foreach ($this->properties() as $property)
        {
            // loop through messages under this property
            $subarr = $this->_messages[$property];
            foreach (array_keys($subarr) as $i)
            {
                if (strlen($str) > 0)
                    $str .= "\n";

                // add message to output (separated by newline)
                $str .= $subarr[$i]->__toString();
            }
        }

        return $str;
    }
}

?>
