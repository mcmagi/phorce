<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\nested;

use phorce\inertia\tagext\TagHandler;

/**
 * A NestedNestTag that serves as the root of the form fields without the need
 * for (or in replacement of) an HtmlFormTag.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Nested-TagLib
 */
class NestedRootTag extends NestedTagBase implements NestedRootTagSupport
{
    /**
     * @see TagHandler::doStartTag()
     */
	public function doStartTag()
	{
		return TagHandler::EVAL_BODY_INCLUDE;
	}

    /**
     * @see TagHandler::doEndTag()
     */
	public function doEndTag()
	{
		return TagHandler::EVAL_PAGE;
	}
}

?>
