<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\nested;

use phorce\common\property\PropertyUtils;
use phorce\inertia\tagext\TagHandler;

/**
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Nested-TagLib
 */
abstract class NestedTagBase extends TagHandler
{
	private $_name;
	private $_property;
	private $_scope;

	public function getName()
	{
		return $this->_name;
	}

	public function setName($v)
	{
		$this->_name = $v;
	}

	public function getProperty()
	{
		return $this->_property;
	}

	public function setProperty($v)
	{
		$this->_property = $v;
	}

	public function getScope()
	{
		return $this->_scope;
	}

	public function setScope($v)
	{
		$this->_scope = $v;
	}

	/**
	 * Returns the last ancestor class which implements
     * {@link NestedParentTagSupport} that has a non-null name property.
	 * @return object NestedParentTagSupport
     * @access protected
	 */
	protected function _getNestedParentTag()
	{
		// start searching for a nest tag at this tag
		$tagHandler = $this;

		// if this name not specified, use last NestedParentTagSupport with a name
		while (! is_null($tagHandler) && is_null($tagHandler->name))
			$tagHandler = $tagHandler->findAncestorWithClass('phorce\reaction\taglib\nested\NestedParentTagSupport');

		return $tagHandler;
	}

	/**
	 * Returns the name associated with the last ancestor class which implements
     * {@link NestedParentTagSupport} with a non-null name property.
     *
     * <p>For example with code like:</p>
     *
	 * <code>
	 *    &lt;html:form name="myForm"&gt;
	 *      &lt;logic:iterate property="employees" id="e"&gt;
	 *        &lt;nested:nest name="e" property="identification"&gt;
	 *          &lt;logic:equal property="hasSocialSecurity" /&gt;
	 *            &lt;bean:write property="ssn" /&gt;
	 *          &lt;/logic:equal&gt;
	 *        &lt;/nested:nest&gt;
	 *      &lt;/logic:iterate&gt;
	 *    &lt;/html:form&gt;
	 * </code>
     *
	 * <p>The nest name of the bean:write tag would be "e".</p>
     *
	 * @return string Name of last named NestedParentTagSupport
     * @access protected
	 */
	protected function _getNestName()
	{
		$tagHandler = $this->_getNestedParentTag();
		return is_null($tagHandler) ? null : $tagHandler->name;
	}

	/**
	 * Returns the scope associated with the last ancestor class which
     * implements {@link NestedParentTagSupport} with a non-null name property.
	 * @return string Scope of last named NestedParentTagSupport
     * @access protected
	 */
	protected function _getNestScope()
	{
		$tagHandler = $this->_getNestedParentTag();
		return is_null($tagHandler) ? null : $tagHandler->scope;
	}

	/**
	 * Returns property path relative to last ancestor class which implementsd
     * {@link NestedParentTagSupport} with a non-null name property.
     *
     * <p>For example with code like:</p>
     *
	 * <code>
	 *    &lt;html:form name="myForm"&gt;
	 *      &lt;logic:iterate property="employees" id="e"&gt;
	 *        &lt;nested:nest name="e" property="identification"&gt;
	 *          &lt;logic:equal property="hasSocialSecurity" /&gt;
	 *            &lt;bean:write property="ssn" /&gt;
	 *          &lt;/logic:equal&gt;
	 *        &lt;/nested:nest&gt;
	 *      &lt;/logic:iterate&gt;
	 *    &lt;/html:form&gt;
	 * </code>
     *
	 * <p>The nest property path of the bean:write tag would be
	 * "identification.ssn".</p>
     *
	 * @return string Property path relative to named NestedParentTagSupport
     * @access protected
	 */
	protected function _getNestProperty($property = null)
	{
		$formFields = array();
		$tagHandler = $this;

		if (! is_null($property))
			$formFields[] = $property;
		elseif (! is_null($tagHandler->property))
			$formFields[] = $tagHandler->property;

		// loop through parents until we hit a nested parent tag with a name
		while (! is_null($tagHandler) && is_null($tagHandler->name))
		{
			$tagHandler = $tagHandler->findAncestorWithClass('phorce\reaction\taglib\nested\NestedParentTagSupport');

			// property nests under nest tags
			if ($tagHandler instanceof NestedNestTagSupport && ! is_null($tagHandler->nestedProperty))
				$formFields[] = $tagHandler->nestedProperty;
		}

		//echo get_class($this) . " relative property: " .
		//	implode(array_reverse($formFields), ".") . "<br>\n";
		return implode(array_reverse($formFields), ".");
	}

	/**
	 * Returns the name of the last ancestor tag which implements
     * {@link NestedRootTagSupport}.
     *
	 * <p>For example with code like:</p>
     *
	 * <code>
	 *    &lt;html:form name="myForm"&gt;
	 *      &lt;logic:iterate property="employees" id="e"&gt;
	 *        &lt;nested:nest name="e" property="identification"&gt;
	 *          &lt;logic:equal property="hasSocialSecurity" /&gt;
	 *            &lt;bean:write property="ssn" /&gt;
	 *          &lt;/logic:equal&gt;
	 *        &lt;/nested:nest&gt;
	 *      &lt;/logic:iterate&gt;
	 *    &lt;/html:form&gt;
	 * </code>
     *
	 * <p>The root name of the <code>bean:write</code> tag would be
     * "myForm".</p>
     *
	 * @return string Name of last NestedRootTagSupport
     * @access protected
	 */
	protected function _getRootName()
	{
		$tagHandler = $this;

		// get root tag (if we are not one already)
		if (! $tagHandler instanceof NestedRootTagSupport)
			$tagHandler = $tagHandler->findAncestorWithClass('phorce\reaction\taglib\nested\NestedRootTagSupport');

		// property nests under nest tags
		if ($tagHandler instanceof NestedRootTagSupport)
			$name = $tagHandler->name;

		return $name;
	}

	/**
	 * Returns the nested property path relative the last ancestor tag which
     * implements {@link NestedRootTagSupport}.  In other words, the absolute
     * property path.
     *
     * <p>For example with code like:</p>
     *
	 * <code>
	 *    &lt;html:form name="myForm"&gt;
	 *      &lt;logic:iterate property="employees" id="e"&gt;
	 *        &lt;nested:nest name="e" property="identification"&gt;
	 *          &lt;logic:equal property="hasSocialSecurity" /&gt;
	 *            &lt;bean:write property="ssn" /&gt;
	 *          &lt;/logic:equal&gt;
	 *        &lt;/nested:nest&gt;
	 *      &lt;/logic:iterate&gt;
	 *    &lt;/html:form&gt;
	 * </code>
     *
	 * <p>The root property path of the bean:write tag would be
	 * "employees[n].identification.ssn", where n is the current iteration of
	 * the logic:iterate tag.</p>
     *
	 * @return string Property path relative to NestedRootTagSupport
     * @access protected
	 */
	protected function _getRootProperty()
	{
		$formFields = array();
		$tagHandler = $this;
		//$foundId = false;

		// FIXME: works for now, but may not be what we want
		if (! is_null($this->_name))
			return $this->property;

		if (! is_null($tagHandler->property))
			$formFields[] = $tagHandler->property;

		// loop through nested parents until we hit a root tag
		while (! is_null($tagHandler) && ! $tagHandler instanceof NestedRootTagSupport)
		{
			$tagHandler = $tagHandler->findAncestorWithClass('phorce\reaction\taglib\nested\NestedParentTagSupport');

			// property nests under nest tags
			if ($tagHandler instanceof NestedNestTagSupport)
			{
				//if (is_null($this->_name) || $this->_name === $tagHandler->_id)
				//	$foundId = true;

				if (/*$foundId && */ ! is_null($tagHandler->nestedProperty))
					$formFields[] = $tagHandler->nestedProperty;
			}
		}

		return implode(array_reverse($formFields), ".");
	}

	/**
	 * Returns the property value of this tag's name and property attributes
	 * using nested tag rules.  It uses _getNestName() and _getNestProperty()
	 * to determine the last named object and property path and uses
	 * PropertyUtils to retrieve the Property.  If a scope is specified, it
	 * will search only that scope for the named object.
	 * @param string $name Name of the object
	 * @param string $property Property of the object
	 * @param string $scope Scope of the object
	 * @return mixed Property value
     * @access protected
	 */
	protected function &_getNestedPropertyValue($name = null, $property = null,
		$scope = null)
	{
		if (is_null($name))
		{
			$name = $this->_getNestName();
			$property = $this->_getNestProperty($property);
			$scope = $this->_getNestScope();
		}

		if (is_null($property))
			$property = $this->_getNestProperty();

		// search for object depending on scope
		if (! is_null($scope))
			$obj =& $this->_pageContext->getAttribute($name, $scope);
		else
			$obj =& $this->_pageContext->findAttribute($name);

		// return its property
		return PropertyUtils::getProperty($obj, $property);
	}

	/**
	 * Sets the property value of this tag's name and property attributes
	 * using nested tag rules.  It uses _getNestName() and _getNestProperty()
	 * to determine the last named object and property path and uses
	 * PropertyUtils to set the Property.  If a scope is specified, it
	 * will search only that scope for the named object.
	 * @param mixed $value Property value
	 * @param string $name Name of the object
	 * @param string $property Property of the object
	 * @param string $scope Scope of the object
     * @access protected
	 */
	protected function _setNestedPropertyValue(&$value, $name = null, $property = null,
		$scope = null)
	{
		if (is_null($name))
		{
			$name = $this->_getNestName();
			$property = $this->_getNestProperty($property);
			$scope = $this->_getNestScope();
		}

		if (is_null($property))
			$property = $this->_getNestProperty();

		if (! strlen($property))
		{
			// no property: replace nested object in specified scope

			// but first try to find the scope if not specified
			if (is_null($scope))
				$scope = $this->_pageContext->getAttributesScope($name);

			// set object in scope
			$this->_pageContext->setAttribute($name, $value, $scope);
		}
		else
		{
			// property specified, search for object depending on scope
			if (! is_null($scope))
				$obj =& $this->_pageContext->getAttribute($name, $scope);
			else
				$obj =& $this->_pageContext->findAttribute($name);

			// set its property
			PropertyUtils::setProperty($obj, $property, $value);
		}
	}
}

?>
