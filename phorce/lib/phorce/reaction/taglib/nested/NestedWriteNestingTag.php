<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\nested;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Nested-TagLib
 */
class NestedWriteNestingTag extends NestedTagBase
{
	private $_id;
	private $_filter;


	public function getId()
	{
		return $this->_id;
	}

	public function setId($v)
	{
		$this->_id = $v;
	}

	public function getFilter()
	{
		return $this->_filter;
	}

	public function setFilter($v)
	{
		$this->_filter = $v;
	}

    /**
     * @see TagHandler::doStartTag()
     */
	public function doStartTag()
	{
		return TagHandler::SKIP_BODY;
	}

    /**
     * @see TagHandler::doEndTag()
     */
	public function doEndTag()
	{
		$path = $this->_getRootName();

		if ($this->_filter === "true")
			$path = urlencode($path);

		if (! is_null($this->_id))
			// expose as page variable
			$this->_pageContext->setAttribute($this->_id, $path,
				PageContext::SCOPE_PAGE);
		else
			// just print it
			echo $path;

		return TagHandler::EVAL_PAGE;
	}
}

?>
