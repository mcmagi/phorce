<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\nested;

/**
 * This is an interface that tags may implement to be treated as a "nest" tag.
 * Nest tags form a new nesting level under a root tag.  This means that
 * properties referenced by tags under this tag will be relative to this tag,
 * so long as no name attribute is provided on said sub-tags.
 *
 * <p>For example, with the following code:</p>
 *
 * <code>
 *   &lt;nested:root name="myObject"&gt;
 *     &lt;nested:nest property="myProp"&gt;
 *        &lt;bean:write property="value" /&gt;
 *     &lt;/nested:nest&gt;
 *   &lt;/nested:root&gt;
 * </code>
 *
 * <p>The property referneced by the <code>bean:write</code> tag, "value", is a
 * sub-property of the object returned from <code>myObject->myProp</code>.  This
 * is functionally equivalent to the following:</p>
 *
 * <code>
 *   &lt;nested:root name="myObject"&gt;
 *      &lt;bean:write property="myProp.value" /&gt;
 *   &lt;/nested:root&gt;
 * </code>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Nested-TagLib
 */
interface NestedNestTagSupport extends NestedParentTagSupport
{
    /**
     * Returns the property of an object within nesting scope whose return value
     * is to serve as the basis of the next nest level.
     * @return string Property
     */
    public function getProperty();

    /**
     * Returns the property of the object represented by this tag relative to
     * the root tag.  In essence, this is the part of the property string that
     * this nest level represents.
     * @return string Nested property
     */
    public function getNestedProperty();
}

?>
