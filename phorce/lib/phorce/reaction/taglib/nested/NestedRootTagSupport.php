<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\nested;

/**
 * This is an interface that tags may implement to be treated as a "root" tag.
 * Root tags serve as the top of a nesting heirarchy, forming the basis upon
 * which all absolute property strings are generated.
 *
 * <p>Note that since this class extends {@link NestedParentTagSupport}, a body
 * for the function getName() must be specified in the implementing class.  In
 * this interface, the object name returned from getName() will serve as the
 * root of the nesting heirarchy.</p>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Nested-TagLib
 */
interface NestedRootTagSupport extends NestedParentTagSupport
{
}

?>
