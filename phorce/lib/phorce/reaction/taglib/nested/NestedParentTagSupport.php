<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\nested;

/**
 * A tag which implements this interface serves as a "parent tag" for the
 * purpose of the nesting heirarchy.  A parent tag will change the nesting level
 * in some way by providing the name of a new "parent object".
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Nested-TagLib
 */
interface NestedParentTagSupport
{
    /**
     * Returns the name of an object (in some scope) that is to serve as the
     * parent object.
     * @return string Object name
     */
    public function getName();
}

?>
