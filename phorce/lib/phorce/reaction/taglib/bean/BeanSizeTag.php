<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
class BeanSizeTag extends BeanTagBase
{
	private $_id;
	private $_collection;

	public function getId()
	{
		return $this->_id;
	}

	public function setId($v)
	{
		$this->_id = $v;
	}

	public function getCollection()
	{
		return $this->_collection;
	}

	public function setCollection($v)
	{
		$this->_collection = $v;
	}

	/**
	 * @see TagHandler::doStartTag()
	 */
	public function doStartTag()
	{
		if (! is_null($this->_id))
			ob_start();

		$propValue = null;

		if (is_null($this->_collection))
			// get the property value to count
			$propValue =& $this->_getNestedPropertyValue();
		else
			// the specified collection IS the object to count
			$propValue =& $this->_collection;

		// print the count
		echo count($propValue);

		if (! is_null($this->_id))
		{
			// get output buffer and turn off buffering
			$size = (integer) ob_get_clean();

			// expose as variable in page scope
			$this->_pageContext->setAttribute($this->_id, $size,
				PageContext::SCOPE_PAGE);
		}

		return TagHandler::SKIP_BODY;
	}

	/**
	 * @see TagHandler::doEndTag()
	 */
	public function doEndTag()
	{
		return TagHandler::EVAL_PAGE;
	}
}

?>
