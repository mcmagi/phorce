<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

use phorce\inertia\tagext\TagHandler;
use phorce\reaction\taglib\ReactionTagException;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
class BeanDefineTag extends BeanTagBase
{
    private static $_validTypes = array(
        "boolean", "bool",
        "integer", "int",
        "float", "double",
        "string", "array",
        "object", "null"
        );

	private $_id;
	private $_value;
	private $_type;
	private $_toScope;

	public function getId()
	{
		return $this->_id;
	}

	public function setId($v)
	{
		$this->_id = $v;
	}

	public function getValue()
	{
		return $this->_value;
	}

	public function setValue($v)
	{
		$this->_value = $v;
	}

	public function getType()
	{
		return $this->_type;
	}

	public function setType($v)
	{
		$this->_type = $v;
	}

	public function getToScope()
	{
		return $this->_toScope;
	}

	public function setToScope($v)
	{
		$this->_toScope = $v;
	}

	/**
	 * @see TagHandler::doStartTag()
	 */
	public function doStartTag()
	{
		// capture contents of tag
		ob_start();

		return TagHandler::EVAL_BODY_INCLUDE;
	}

	/**
	 * @see TagHandler::doEndTag()
	 */
	public function doEndTag()
	{
		$value = null;

		// get output buffer and turn off buffering
		$tagContent = ob_get_clean();

		if (strlen($tagContent))
			// define as tag content if we have it
			$value = $tagContent;
		elseif (! is_null($this->_value))
			// define as specified value
			$value = $this->_value;
		else
		{
			// find nested property value if name or property specified
			if (! is_null($this->name) || ! is_null($this->property))
				$value =& $this->_getNestedPropertyValue();

			// if null and we know its class, instantiate a new value
			if (is_null($value) && ! is_null($this->_type) && class_exists($this->_type))
			{
				eval('$value = new '.$this->_type.'();');

				if (! is_null($this->name) || ! is_null($this->property))
					$this->_setNestedPropertyValue($value);
			}
		}

		// type cast value if internal type is supplied
        if (! is_null($this->_type) && ! class_exists($this->_type)
                && ! interface_exists($this->_type))
        {
            if (! array_key_exists($this->_type, self::$_validTypes))
                throw new ReactionTagException($this,
                    "Cannot cast value to unknown type '{$this->_type}'");
            settype($value, $this->_type);
        }

		// save in specified context
		$this->_pageContext->setAttribute($this->_id, $value, $this->_toScope);

		// XXX: we may have to fix referencing above (see below)

		/*if (is_object($value) || is_array($value))
			$toScopeArr[$this->_id] = &$value;
		else
			$toScopeArr[$this->_id] = $value;*/

		return TagHandler::EVAL_PAGE;
	}
}

?>
