<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;

/**
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
abstract class BeanRequestTagBase extends TagHandler
{
	private $_id;
	private $_multiple;
	private $_name;
	private $_value;

	public function getId()
	{
		return $this->_id;
	}

	public function setId($v)
	{
		$this->_id = $v;
	}

	public function getMultiple()
	{
		return $this->_multiple;
	}

	public function setMultiple($v)
	{
		$this->_multiple = $v;
	}

	public function getName()
	{
		return $this->_name;
	}

	public function setName($v)
	{
		$this->_name = $v;
	}

	public function getValue()
	{
		return $this->_value;
	}

	public function setValue($v)
	{
		$this->_value = $v;
	}

	/**
	 * @see TagHandler::doStartTag()
	 */
	public function doStartTag()
	{
		$value = $this->_getRequestValue();

		// make array if multiple specified
		if (! is_null($this->_multiple))
			$value = array($value);

		$this->_pageContext->setAttribute($this->_id, $value,
			PageContext::SCOPE_PAGE);

		return TagHandler::SKIP_BODY;
	}

	/**
	 * @see TagHandler::doEndTag()
	 */
	public function doEndTag()
	{
		return TagHandler::EVAL_PAGE;
	}

	/**
	 * (Abstract) Returns the referenced value from the request.
	 * @return Request value
     * @access protected
	 */
	protected abstract function _getRequestValue();
}

?>
