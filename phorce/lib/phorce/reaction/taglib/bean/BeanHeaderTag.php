<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
class BeanHeaderTag extends BeanRequestTagBase
{
	/**
	 * Returns the referenced value from the apache request headers.
	 * @return Header value
     * @access protected
	 */
	protected function _getRequestValue()
	{
		$headers = apache_request_headers();
		if (array_key_exists($this->name, $headers))
			$value = $headers[$this->name];
		else
			$value = $this->value;
		return $value;
	}
}

?>
