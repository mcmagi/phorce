<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
class BeanCookieTag extends BeanRequestTagBase
{
	/**
	 * Returns the referenced value from the $_COOKIE superglobal.
	 * @return Cookie value
     * @access protected
	 */
	protected function _getRequestValue()
	{
		if (array_key_exists($this->name, $_COOKIE))
			$value = $_COOKIE[$this->name];
		else
			$value = $this->value;
		return $value;
	}
}

?>
