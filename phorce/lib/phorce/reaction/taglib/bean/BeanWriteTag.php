<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

use phorce\inertia\tagext\TagHandler;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
class BeanWriteTag extends BeanTagBase
{
	private $_ignore;
	private $_filter;

	public function getIgnore()
	{
		return $this->_ignore;
	}

	public function setIgnore($v)
	{
		$this->_ignore = $v;
	}

	public function getFilter()
	{
		return $this->_filter;
	}

	public function setFilter($v)
	{
		$this->_filter = $v;
	}

    /**
     * @see TagHandler::doStartTag()
     */
	public function doStartTag()
	{
		// find nested property value
		$value =& $this->_getNestedPropertyValue();

		if (is_null($value) && $this->_ignore === "true")
		{
			// no such object in scope, but we'll ignore it as requested
		}
		else
		{
			// escape html entities if filter is enabled
			if ($this->_filter === "true")
				$value = htmlentities($value);

			// print it
			echo $value;
		}

		return TagHandler::SKIP_BODY;
	}

    /**
     * @see TagHandler::doEndTag()
     */
	public function doEndTag()
	{
		return TagHandler::EVAL_PAGE;
	}
}

?>
