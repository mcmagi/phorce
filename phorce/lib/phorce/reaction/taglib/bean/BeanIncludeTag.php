<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

use phorce\common\web\ApplicationContext;
use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;
use phorce\inertia\taglib\phpx\PhpxIncludeTagBase;
use phorce\reaction\action\ReactionController;
use phorce\reaction\taglib\ReactionTagException;
use \Net_URL2;

/**
 * <p>Includes a resource and outputs it to the page or stores it as a page
 * variable.  Resources may be one of four types: a local page, an external
 * URL, an action forward, or an event.  Which gets included depends on which
 * of the following mutually-exclusive attributes are provided.</p>
 *
 * <ul>
 *  <li><em>page</em> - Will include and execute a file in the local web
 *  application, just as the <code>&lt;phpx:include&gt;</code> tag.</li>
 *  <li><em>href</em> - Will include a file a a URL exactly as specified.  If
 *  the specified URL does not have a host and only consists of a path
 *  component, it will be considered relative to the local webserver instance.
 *  This should be used for including external resources.</li>
 *  <li><em>forward</em> - Resolves the specified action forward to a page and
 *  executes it in the local web application.</li>
 *  <li><em>action</em> - Invokes the specified event against the
 *  Controller.</li>
 * </ul>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
class BeanIncludeTag extends PhpxIncludeTagBase
{
    private $_id;

    private $_action;

    /**
     * A URL to include.  The URL may be simply a local path or a full URL
     * complete with scheme, hostname, and path.
     * @var string
     * @access private
     */
    private $_href;

    private $_forward;


    public function getId()
    {
        return $this->_id;
    }

    public function setId($v)
    {
        $this->_id = $v;
    }

    public function getAction()
    {
        return $this->_action;
    }

    public function setAction($v)
    {
        $this->_action = $v;
    }

    public function getHref()
    {
        return $this->_href;
    }

    public function setHref($v)
    {
        $this->_href = $v;
    }

    public function getForward()
    {
        return $this->_forward;
    }

    public function setForward($v)
    {
        $this->_forward = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        if (! is_null($this->getPage()))
        {
            // normal URL construction
            parent::doStartTag();
        }
        elseif (! is_null($this->_href))
        {
            $this->_url = new Net_URL2($this->_href);

            if (! strlen($this->_url->getScheme()))
            {
                // build complete HTTP(S) URL if not provided
                $this->_url->setScheme(
                    (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'])
                    ? 'https' : 'http');
                $this->_url->setHost($_SERVER['SERVER_NAME']);
                $this->_url->setPort($_SERVER['SERVER_PORT']);
            }
        }
        elseif (! is_null($this->_forward))
        {
            $c = ReactionController::getInstance();
            $f = $c->findGlobalForward($this->_forward);

            if (is_null($f))
                throw new ReactionTagException($this, "Cannot find global forward {$this->_forward} in reaction config");

            // save path to URL
            $this->_url = new Net_URL2("");
            $this->_url->path = $f->path;
        }

        return TagHandler::EVAL_BODY_BUFFERED;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        // if they requested the data to be saved as a variable,
        // start output buffering
        if (! is_null($this->_id))
            ob_start();

        // do the include
        parent::doEndTag();

        // if they requested the data to be saved as a variable...
        if (! is_null($this->_id))
        {
            // get output buffer and turn off buffering
            $content = ob_get_clean();

            // expose output buffer as page variable
            $this->_pageContext->setAttribute($this->_id, $content,
                PageContext::SCOPE_PAGE);
        }

        return TagHandler::EVAL_PAGE;
    }

    /**
     * Performs the inclusion of the constructed URL.
     * @access protected
     */
    protected function doInclude()
    {
        if (! is_null($this->_action))
        {
            // handle as an event
            $c = ReactionController::getInstance();
            $c->process($this->_action);

            // FIXME: do we need to load any forms???
        }
        else
        {
            // handle as an include
            $ctx = ApplicationContext::getInstance();
            $rd = $ctx->getRequestDispatcher($this->_url);
            $rd->includePath();
        }
    }
}

?>
