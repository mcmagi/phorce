<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\bean;

use phorce\inertia\tagext\TagHandler;
use phorce\reaction\action\Globals;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Bean-TagLib
 */
class BeanMessageTag extends BeanTagBase
{
	private $_key;
	private $_bundle;
	private $_arg0;
	private $_arg1;
	private $_arg2;
	private $_arg3;
	private $_arg4;

	public function getKey()
	{
		return $this->_key;
	}

	public function setKey($v)
	{
		$this->_key = $v;
	}

	public function getArg0()
	{
		return $this->_arg0;
	}

	public function setArg0($v)
	{
		$this->_arg0 = $v;
	}

	public function getArg1()
	{
		return $this->_arg1;
	}

	public function setArg1($v)
	{
		$this->_arg1 = $v;
	}

	public function getArg2()
	{
		return $this->_arg2;
	}

	public function setArg2($v)
	{
		$this->_arg2 = $v;
	}

	public function getArg3()
	{
		return $this->_arg3;
	}

	public function setArg3($v)
	{
		$this->_arg3 = $v;
	}

	public function getArg4()
	{
		return $this->_arg4;
	}

	public function setArg4($v)
	{
		$this->_arg4 = $v;
	}

	/**
	 * @see TagHandler::doStartTag()
	 */
	public function doStartTag()
	{
		if (! is_null($this->_key))
			$key = $this->_key;
		else
			$key =& $this->_getNestedPropertyValue();

		// build arg array
		$args = array();
		if (! is_null($this->_arg0))
			$args[] = $this->_arg0;
		if (! is_null($this->_arg1))
			$args[] = $this->_arg1;
		if (! is_null($this->_arg2))
			$args[] = $this->_arg2;
		if (! is_null($this->_arg3))
			$args[] = $this->_arg3;
		if (! is_null($this->_arg4))
			$args[] = $this->_arg4;

		// lookup the message resources by bundle key
		if (is_null($this->_bundle))
			$bundle = Globals::MESSAGES_KEY;
		else
			$bundle = $this->_bundle;
		$resources =& $_REQUEST[$bundle];

		// output the message
		echo $resources->getMessage($key, $args);

		return TagHandler::SKIP_BODY;
	}

	/**
	 * @see TagHandler::doEndTag()
	 */
	public function doEndTag()
	{
		return TagHandler::EVAL_PAGE;
	}
}

?>
