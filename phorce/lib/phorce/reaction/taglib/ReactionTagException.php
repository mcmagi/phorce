<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib;

use phorce\inertia\tagext\TagException;
use phorce\inertia\tagext\TagHandler;

/**
 * Represents an exception within the Reaction tag library.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage TagLib
 */
class ReactionTagException extends TagException
{
    public function __construct(TagHandler $tag, $message)
    {
        parent::__construct($tag, $message);
    }
}

?>
