<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\common\web\ApplicationContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\action\ReactionController;
use phorce\reaction\taglib\ReactionTagException;
use phorce\reaction\taglib\nested\NestedRootTagSupport;
use \Net_URL2;

/**
 * A special kind of RootTag that can be used to submit forms.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlFormTag extends HtmlStyleTagBase implements NestedRootTagSupport
{
    private $_action;
    private $_href;
    private $_method;
    private $_target;
    private $_enctype;
    private $_onReset;
    private $_onSubmit;
    private $_disabled = false;

    /**
     * The XHTML representation of this FORM element.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;


    public function getAction()
    {
        return $this->_action;
    }

    public function setAction($v)
    {
        $this->_action = $v;
    }

    public function getHref()
    {
        return $this->_href;
    }

    public function setHref($v)
    {
        $this->_href = $v;
    }

    public function getMethod()
    {
        return $this->_method;
    }

    public function setMethod($v)
    {
        $this->_method = $v;
    }

    public function getTarget()
    {
        return $this->_target;
    }

    public function setTarget($v)
    {
        $this->_target = $v;
    }

    public function getEnctype()
    {
        return $this->_enctype;
    }

    public function setEnctype($v)
    {
        $this->_enctype = $v;
    }

    public function getOnReset()
    {
        return $this->_onReset;
    }

    public function setOnReset($v)
    {
        $this->_onReset = $v;
    }

    public function getOnSubmit()
    {
        return $this->_onSubmit;
    }

    public function setOnSubmit($v)
    {
        $this->_onSubmit = $v;
    }

    public function getDisabled()
    {
        return $this->_disabled;
    }

    public function setDisabled($v)
    {
        $this->_disabled = ($v === "true");
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // get action mapping for specified event
        $c = ReactionController::getInstance();
        $mapping = $c->findActionConfig($this->_action);

        if (is_null($mapping))
            throw new ReactionTagException($this, "Cannot find action with path '{$this->_action}' in reaction config");

        // we must set the name since NestedRootTags need names
        $this->name = $mapping->name;

        // TODO: expose as a page variable so form fields can access it?

        $this->_doc = new DOMDocument();
        $form = $this->_doc->createElement("form");
        $this->_doc->appendChild($form);

        $form->setAttribute("name", $this->name);
        $form->setAttribute("method", $this->_method);

        $ctx = ApplicationContext::getInstance();
        $form->setAttribute("action", $ctx->getContextRoot() . $mapping->path . ".do");

        if (! is_null($this->_target))
            $form->setAttribute("target", $this->_target);
        if (! is_null($this->_enctype))
            $form->setAttribute("enctype", $this->_enctype);

        // JS attributes
        if (! is_null($this->_onReset))
            $form->setAttribute("onReset", $this->_onReset);
        if (! is_null($this->_onSubmit))
            $form->setAttribute("onSubmit", $this->_onSubmit);

        // CSS attributes
        if (! is_null($this->style))
            $form->setAttribute("style", $this->style);
        if (! is_null($this->styleClass))
            $form->setAttribute("class", $this->styleClass);
        if (! is_null($this->styleId))
            $form->setAttribute("id", $this->styleId);

        echo HtmlUtils::getStartTag($form);

        return TagHandler::EVAL_BODY_INCLUDE;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        echo HtmlUtils::getEndTag($this->_doc->documentElement);

        return TagHandler::EVAL_PAGE;
    }
}

?>
