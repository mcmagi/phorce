<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>hidden</code> on an HTML form.  This
 * tag follows nested scope rules for determining property values.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlHiddenTag extends HtmlInputTagBase
{
    /**
     * Whether to write the value of this hidden field when rendering the input
     * element.  Defaults to false.
     * @var boolean
     * @access private
     */
    protected $_write = false;


    /**
     * Returns the type attribute of this tag, "hidden".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_HIDDEN;
    }

    /**
     * Returns whether to write the value of this hidden field.
     * @return boolean
     */
    public function getWrite()
    {
        return $this->_write;
    }

    /**
     * Sets whether to write the value of this hidden field.
     * @param string $v
     */
    public function setWrite($v)
    {
        $this->_write = ($v === "true");
    }

    /**
     * Adds attributes specific to this input type.  This implemenation does
     * nothing since there are no type-specific attributes to populate.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
    }

    /**
     * Populates the value attribute in the XHTML output.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        $propValue =& $this->_getNestedPropertyValue();

        if (is_null($this->value))
            $e->setAttribute("value", $propValue);
        else
            $e->setAttribute("value", $this->value);
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        // print value if write is enabled
        if ($this->_write)
            echo $this->_doc->documentElement->getAttribute("value");

        return parent::doEndTag();
    }
}

?>
