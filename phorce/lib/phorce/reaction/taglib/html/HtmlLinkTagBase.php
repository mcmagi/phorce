<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use phorce\reaction\action\ReactionController;
use \Net_URL2;

/**
 * Serves as a base class for HTML tags that must generate a URL to invoke
 * another Phorce action.
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
abstract class HtmlLinkTagBase extends HtmlActionTagBase
{
    private $_anchor;
    private $_forward;
    private $_href;
    private $_transaction = false;


    public function getAnchor()
    {
        return $this->_anchor;
    }

    public function setAnchor($v)
    {
        $this->_anchor = $v;
    }

    public function getForward()
    {
        return $this->_forward;
    }

    public function setForward($v)
    {
        $this->_forward = $v;
    }

    public function getHref()
    {
        return $this->_href;
    }

    public function setHref($v)
    {
        $this->_href = $v;
    }

    public function getTransaction()
    {
        return $this->_transaction;
    }

    public function setTransaction($v)
    {
        $this->_transaction = ($v === "true");
    }


    protected function _getForwardURL()
    {
        // lookup global forward
        $c = ReactionController::getInstance();
        $f = $c->findGlobalForward($this->_forward);

        // save path to URL
        $url = new Net_URL2();
        $url->path = $f->path;

        return $url;
    }

    protected function _getURL()
    {
        // create the URL based on which destination attribute is specified
        if (! is_null($this->_forward))
            $url = $this->_getForwardURL();
        elseif (! is_null($this->getAction()))
            $url = $this->_getActionURL();
        elseif (! is_null($this->_href))
            $url = new Net_URL2($this->_href));
        elseif (! is_null($this->getPage()) || ! is_null($this->pageKey))
            $url = $this->_getPageURL();

        // set the anchor
        if (! is_null($this->_anchor))
            $url->anchor = $this->_anchor;

        return $url;
    }
}

?>
