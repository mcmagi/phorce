<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \ArrayObject;
use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\Globals;

/**
 * This tag is used to retrieve a set of error messages, look them up in the
 * message resources, and expose each one as a page variable.  The set of
 * messages reference by the <code>name</code> attribute may be an
 * <code>ActionMessages</code> object, an array of strings, or a string.  This
 * tag offers additional flexability over the <code>&lt;html:errors&gt;</code>
 * tag since this tag will execute the contents of this element for each message
 * and expose the resolved message text as a page variable specified by the
 * <code>id</code> attribute.  Thus, the formatting of each message may be
 * controlled entirely within the PHPX file.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlMessagesTag extends HtmlMessagesTagBase
{
    /**
     * Specifies an identifier for the page variable that will be exposed upon
     * each iteration of this tag.  It will contain the text of the message
     * string.
     * @var string
     * @access private
     */
    private $_id;

    /**
     * If set to true, this tag will attempt to retrieve messages from the
     * <code>Globals::MESSAGE_KEY</code> rather than the
     * <code>Globals::ERROR_KEY</code> by default.  Any value in the name
     * attribute will be ignored.
     * @var boolean
     * @access private
     */
    private $_messages = false;

    /**
     * An iterator through the array of messages.
     * @var object ArrayIterator
     * @access private
     */
    private $_iterator;


    /**
     * Returns the identifier for the page variable that will be exposed upon
     * each iteration of this tag.
     * @return string Identifier
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Sets the identifier for the page variable that will be exposed upon
     * each iteration of this tag.
     * @param string $v Identifier
     */
    public function setId($v)
    {
        $this->_id = $v;
    }

    /**
     * Returns whether this tag should retrieve messages rather than errors.
     * @return boolean
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * Sets whether this tag should retrieve messages rather than errors.
     * @param string $v
     */
    public function setMessages($v)
    {
        $this->_messages = ($v === "true");
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // retrieve from messages key instead?
        if ($this->_messages)
            $this->name = Globals::MESSAGE_KEY;

        // retrieve messages as an array
        $messages =& $this->_getMessages();

        if (count($messages))
        {
            // load the message resources
            $this->_loadResources();

            // print message header
            if (! is_null($this->header))
                $this->_printResource($this->header);

            // create new iterator
            $msgArrObj = new ArrayObject($messages);
            $this->_iterator = $msgArrObj->getIterator();
        }

        if (! is_null($this->_iterator) && $this->_iterator->valid())
        {
            $this->_doAssignMessage();
            $rc = TagHandler::EVAL_BODY_INCLUDE;
        }
        else
            $rc = TagHandler::SKIP_BODY;

        return $rc;
    }

    /**
     * @see TagHandler::doAfterBody()
     */
    public function doAfterBody()
    {
        if (! is_null($this->_iterator) && $this->_iterator->valid())
        {
            $this->_doAssignMessage();
            $rc = TagHandler::EVAL_BODY_AGAIN;
        }
        else
            $rc = TagHandler::SKIP_BODY;

        return $rc;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        if (! is_null($this->_iterator))
        {
            // print message footer
            if (! is_null($this->footer))
                $this->_printResource($this->footer);
        }

        return TagHandler::EVAL_PAGE;
    }

    /**
     * Gets the next message from the iterator and assigns it to a page
     * variable.  The page variable name is specified by the <code>id</code>
     * property.
     * @access protected
     */
    protected function _doAssignMessage()
    {
        // get message
        $msgObj = $this->_iterator->current();

        // look up its corresponding text
        $message = $this->_lookupMessage($msgObj);

        // store message as a page variable
        $this->_pageContext->setAttribute($this->_id, $message,
            PageContext::SCOPE_PAGE);

        // get next element
        $this->_iterator->next();
    }
}

?>
