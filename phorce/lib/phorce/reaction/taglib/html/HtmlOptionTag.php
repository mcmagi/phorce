<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\taglib\ReactionTagException;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlOptionTag extends TagHandler
{
    private $_value;
    private $_bundle;
    private $_key;
    private $_filter = false;
    private $_disabled = false;
    private $_style;
    private $_styleClass;
    private $_styleId;

    /**
     * The XHTML representation of this OPTION element.
     * @var object XMLElement
     * @access private
     */
    private $_element;

    public function getValue()
    {
        return $this->_value;
    }

    public function setValue($v)
    {
        $this->_value = $v;
    }

    public function getBundle()
    {
        return $this->_bundle;
    }

    public function setBundle($v)
    {
        $this->_bundle = $v;
    }

    public function getKey()
    {
        return $this->_key;
    }

    public function setKey($v)
    {
        $this->_key = $v;
    }

    public function getFilter()
    {
        return $this->_filter;
    }

    public function setFilter($v)
    {
        $this->_filter = ($v === "true");
    }

    public function getDisabled()
    {
        return $this->_disabled;
    }

    public function setDisabled($v)
    {
        $this->_disabled = ($v === "true");
    }

    public function getStyle()
    {
        return $this->_style;
    }

    public function setStyle($v)
    {
        $this->_style = $v;
    }

    public function getStyleClass()
    {
        return $this->_styleClass;
    }

    public function setStyleClass($v)
    {
        $this->_styleClass = $v;
    }

    public function getStyleId()
    {
        return $this->_styleId;
    }

    public function setStyleId($v)
    {
        $this->_styleId = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        return TagHandler::EVAL_BODY_BUFFERED;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doEndTag()
    {
        // get parent select tag
        $selectTag = $this->findAncestorWithClass('phorce\reaction\taglib\html\HtmlSelectTag');

        // create option element
        $doc = new DOMDocument();
        $option = $doc->createElement("option");
        $doc->appendChild($option);

        $option->setAttribute("value", $this->_value);

        if ($this->_disabled)
            $option->setAttribute("disabled", "disabled");

        // CSS style attributes
        if (! is_null($this->_style))
            $option->setAttribute("style", $this->_style);
        if (! is_null($this->_styleClass))
            $option->setAttribute("class", $this->_styleClass);
        if (! is_null($this->_styleId))
            $option->setAttribute("id", $this->_styleId);

        // is this option selected
        if ($selectTag->isSelected($this->_value))
            $option->setAttribute("selected", "selected");

        // get content between the tags
        $content = $this->getBodyContent();

        if (! is_null($this->_key))
        {
            // print error if there is no bundle
            if (! array_key_exists($this->_bundle, $_REQUEST))
            {
                throw new ReactionTagException($this,
                    "No message resources configured for bundle '{$this->_bundle}'");
            }

            // get message resources for the specified bundle
            $this->_resources = $_REQUEST[$this->_bundle];

            $content = $this->_resources->getMessage($this->_key);
        }
        elseif (! strlen($content))
        {
            // nothing between the tags, so use value attribute
            $content = $this->_value;
        }

        // add label
        $text = $doc->createTextNode($content);
        $option->appendChild($text);

        // print option element
        echo $doc->saveHTML();

        return TagHandler::EVAL_PAGE;
    }
}

?>
