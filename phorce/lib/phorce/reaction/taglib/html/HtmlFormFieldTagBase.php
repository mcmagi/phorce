<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\Globals;
use phorce\reaction\action\ActionErrors;
use phorce\reaction\taglib\ReactionTagException;

/**
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
abstract class HtmlFormFieldTagBase extends HtmlStyleTagBase
{
    /**
     * An initialization value for this field.
     * @var string
     * @access private
     */
    private $_value;

    /**
     * The message resource bundle to use for attributes which require message
     * resources.
     * @var string
     * @access private
     */
    private $_bundle = Globals::MESSAGES_KEY;

    /**
     * A CSS style to apply to the rendered element via a <code>style</code>
     * attribute if there is an error associated with this field.
     * @var string
     * @access private
     */
    private $_errorStyle;

    /**
     * A CSS style to apply to the rendered element via a <code>class</code>
     * attribute if there is an error associated with this field.
     * @var string
     * @access private
     */
    private $_errorStyleClass;

    /**
     * A CSS style to apply to the rendered element via an <code>id</code>
     * attribute if there is an error associated with this field.
     * @var string
     * @access private
     */
    private $_errorStyleId;

    /**
     * The key to use to check for the presence of errors in any scope.  If not
     * specified the default <code>Globals\ERROR_KEY</code> will be used.
     * @var string
     * @access private
     */
    private $_errorKey = Globals::ERROR_KEY;

    /**
     * Text to be populated in the rendered element's <code>alt</code>
     * attribute.
     * @var string
     * @access private
     */
    private $_alt;

    /**
     * The message resource key of text to be populated in the rendered
     * element's <code>alt</code> attribute.
     * @var string
     * @access private
     */
    private $_altKey;

    /**
     * Text to be populated in the rendered element's <code>title</code>
     * attribute.
     * @var string
     * @access private
     */
    private $_title;

    /**
     * The message resource key of text to be populated in the rendered
     * element's <code>title</code> attribute.
     * @var string
     * @access private
     */
    private $_titleKey;

    /**
     * Value for the rendered element's <code>tabindex</code> attribute.  This
     * sets the order of this field when using the tab key to navigate the form.
     * @var integer
     * @access private
     */
    private $_tabindex;

    /**
     * If set to true, disables this form control.
     * @var boolean
     * @access private
     */
    private $_disabled = false;

    /**
     * JavaScript code to execute when the value of this form field is changed.
     * @var string
     * @access private
     */
    private $_onChange;

    /**
     * JavaScript code to execute when this form field goes out of focus.
     * @var string
     * @access private
     */
    private $_onBlur;

    /**
     * JavaScript code to execute when this form field comes in focus.
     * @var string
     * @access private
     */
    private $_onFocus;

    /**
     * JavaScript code to execute when a key is pressed down within this form
     * field.
     * @var string
     * @access private
     */
    private $_onKeyDown;

    /**
     * JavaScript code to execute when a key is released within this form field.
     * @var string
     * @access private
     */
    private $_onKeyUp;

    /**
     * JavaScript code to execute when a key is pressed down and released within
     * this form field.
     * @var string
     * @access private
     */
    private $_onKeyPress;

    /**
     * JavaScript code to execute when this form field is clicked on by the
     * mouse.
     * @var string
     * @access private
     */
    private $_onClick;

    /**
     * JavaScript code to execute when this form field is double-clicked on by
     * the mouse.
     * @var string
     * @access private
     */
    private $_onDblClick;

    /**
     * JavaScript code to execute when this form field is clicked on by a mouse
     * button, but before the button is released.
     * @var string
     * @access private
     */
    private $_onMouseDown;

    /**
     * JavaScript code to execute when a mouse button is released after clicking
     * on this form field.
     * @var string
     * @access private
     */
    private $_onMouseUp;

    /**
     * JavaScript code to execute when a mouse pointer is moved into this form
     * field.
     * @var string
     * @access private
     */
    private $_onMouseOver;

    /**
     * JavaScript code to execute when a mouse button is moved away from this
     * form field
     * @var string
     * @access private
     */
    private $_onMouseOut;

    /**
     * JavaScript code to execute while the mouse pointer is being moved over
     * ths form field.
     * @var string
     * @access private
     */
    private $_onMouseMove;

    /**
     * The message resource bundle.
     * @var object MessageResources
     * @access private
     */
    private $_resources;


    public function getValue()
    {
        return $this->_value;
    }

    public function setValue($v)
    {
        $this->_value = $v;
    }

    public function getErrorStyle()
    {
        return $this->_errorStyle;
    }

    public function setErrorStyle($v)
    {
        $this->_errorStyle = $v;
    }

    public function getErrorStyleClass()
    {
        return $this->_errorStyleClass;
    }

    public function setErrorStyleClass($v)
    {
        $this->_errorStyleClass = $v;
    }

    public function getErrorStyleId()
    {
        return $this->_errorStyleId;
    }

    public function setErrorStyleId($v)
    {
        $this->_errorStyleId = $v;
    }

    public function getErrorKey()
    {
        return $this->_errorKey;
    }

    public function setErrorKey($v)
    {
        $this->_errorKey = $v;
    }

    public function getOnBlur()
    {
        return $this->_onBlur;
    }

    public function setOnBlur($v)
    {
        $this->_onBlur = $v;
    }

    public function getOnChange()
    {
        return $this->_onChange;
    }

    public function setOnChange($v)
    {
        $this->_onChange = $v;
    }

    public function getOnClick()
    {
        return $this->_onClick;
    }

    public function setOnClick($v)
    {
        $this->_onClick = $v;
    }

    public function getOnDblClick()
    {
        return $this->_onDblClick;
    }

    public function setOnDblClick($v)
    {
        $this->_onDblClick = $v;
    }

    public function getOnFocus()
    {
        return $this->_onFocus;
    }

    public function setOnFocus($v)
    {
        $this->_onFocus = $v;
    }

    public function getOnKeyDown()
    {
        return $this->_onKeyDown;
    }

    public function setOnKeyDown($v)
    {
        $this->_onKeyDown = $v;
    }

    public function getOnKeyPress()
    {
        return $this->_onKeyPress;
    }

    public function setOnKeyPress($v)
    {
        $this->_onKeyPress = $v;
    }

    public function getOnKeyUp()
    {
        return $this->_onKeyUp;
    }

    public function setOnKeyUp($v)
    {
        $this->_onKeyUp = $v;
    }

    public function getOnMouseDown()
    {
        return $this->_onMouseDown;
    }

    public function setOnMouseDown($v)
    {
        $this->_onMouseDown = $v;
    }

    public function getOnMouseMove()
    {
        return $this->_onMouseMove;
    }

    public function setOnMouseMove($v)
    {
        $this->_onMouseMove = $v;
    }

    public function getOnMouseOut()
    {
        return $this->_onMouseOut;
    }

    public function setOnMouseOut($v)
    {
        $this->_onMouseOut = $v;
    }

    public function getOnMouseOver()
    {
        return $this->_onMouseOver;
    }

    public function setOnMouseOver($v)
    {
        $this->_onMouseOver = $v;
    }

    public function getOnMouseUp()
    {
        return $this->_onMouseUp;
    }

    public function setOnMouseUp($v)
    {
        $this->_onMouseUp = $v;
    }

    public function getBundle()
    {
        return $this->_bundle;
    }

    public function setBundle($v)
    {
        $this->_bundle = $v;
    }

    public function getAlt()
    {
        return $this->_alt;
    }

    public function setAlt($v)
    {
        $this->_alt = $v;
    }

    public function getAltKey()
    {
        return $this->_altKey;
    }

    public function setAltKey($v)
    {
        $this->_altKey = $v;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function setTitle($v)
    {
        $this->_title = $v;
    }

    public function getTitleKey()
    {
        return $this->_titleKey;
    }

    public function setTitleKey($v)
    {
        $this->_titleKey = $v;
    }

    public function getDisabled()
    {
        return $this->_disabled;
    }

    public function setDisabled($v)
    {
        $this->_disabled = ($v === "true");
    }

    public function getTabindex()
    {
        return $this->_tabindex;
    }

    public function setTabindex($v)
    {
        $this->_tabindex = $v;
    }

    /**
     * Loads the message resources for this tag's configured bundle.  If they
     * have already been loaded once then they will not be loaded again.
     * @return object MessageResources
     */
    protected function _getResources()
    {
        if (is_null($this->_resources))
        {
            // print error if there is no bundle
            if (! array_key_exists($this->_bundle, $_REQUEST))
            {
                throw new ReactionTagException($this,
                    "No message resources configured for bundle '{$this->_bundle}'");
            }

            // get message resources for the specified bundle
            $this->_resources = $_REQUEST[$this->_bundle];
        }

        return $this->_resources;
    }

    /**
     * Adds general attributes common to all form fields.
     * @param object DOMElement $e
     */
    protected function _populateFormFieldAttributes(DOMElement $e)
    {
        //$propValue =& $this->_getNestedPropertyValue();

        // generate name of field from absolute property reference
        $formFieldName = $this->formFieldId;
        if (strlen($formFieldName))
            $e->setAttribute("name", $formFieldName);

        // check if we have errors before styling the output
        $msgObj = $this->_getPropertyValue($this->_errorKey);

        $hasError = false;
        if ($msgObj instanceof ActionErrors)
            $hasError = ! $msgObj->isEmpty();
        else
            $hasError = count($msgObj) > 0;

        // style attributes
        if ($hasError && ! is_null($this->errorStyleId))
            $e->setAttribute("id", $this->errorStyleId);
        elseif (! is_null($this->styleId))
            $e->setAttribute("id", $this->styleId);

        if ($hasError && ! is_null($this->errorStyleClass))
            $e->setAttribute("class", $this->errorStyleClass);
        elseif (! is_null($this->styleClass))
            $e->setAttribute("class", $this->styleClass);

        if ($hasError && ! is_null($this->errorStyle))
            $e->setAttribute("style", $this->errorStyle);
        elseif (! is_null($this->style))
            $e->setAttribute("style", $this->style);

        // javascript attributes
        if (! is_null($this->_onBlur))
            $e->setAttribute("onBlur", $this->_onBlur);
        if (! is_null($this->_onChange))
            $e->setAttribute("onChange", $this->_onChange);
        if (! is_null($this->_onClick))
            $e->setAttribute("onClick", $this->_onClick);
        if (! is_null($this->_onDblClick))
            $e->setAttribute("onDblClick", $this->_onDblClick);
        if (! is_null($this->_onFocus))
            $e->setAttribute("onFocus", $this->_onFocus);
        if (! is_null($this->_onKeyDown))
            $e->setAttribute("onKeyDown", $this->_onKeyDown);
        if (! is_null($this->_onKeyPress))
            $e->setAttribute("onKeyPress", $this->_onKeyPress);
        if (! is_null($this->_onKeyUp))
            $e->setAttribute("onKeyUp", $this->_onKeyUp);
        if (! is_null($this->_onMouseDown))
            $e->setAttribute("onMouseDown", $this->_onMouseDown);
        if (! is_null($this->_onMouseMove))
            $e->setAttribute("onMouseMove", $this->_onMouseMove);
        if (! is_null($this->_onMouseOut))
            $e->setAttribute("onMouseOut", $this->_onMouseOut);
        if (! is_null($this->_onMouseOver))
            $e->setAttribute("onMouseOver", $this->_onMouseOver);
        if (! is_null($this->_onMouseUp))
            $e->setAttribute("onMouseUp", $this->_onMouseUp);

        // title and alt attributes w/ possible message lookup
        if (! is_null($this->_title))
            $e->setAttribute("title", $this->_title);
        elseif (! is_null($this->_titleKey))
            $e->setAttribute("title", $this->_getResources()->getMessage($this->_titleKey));
        if (! is_null($this->_alt))
            $e->setAttribute("alt", $this->_alt);
        elseif (! is_null($this->_altKey))
            $e->setAttribute("alt", $this->_getResources()->getMessage($this->_altKey));

        // set as disabled if either this field or entire form is disabled
        $formTag = $this->findAncestorWithClass('phorce\reaction\taglib\html\HtmlFormTag');
        if ($this->_disabled || (! is_null($formTag) && $formTag->disabled))
            $e->setAttribute("disabled", "disabled");

        // set tab index
        if (! is_null($this->_tabindex))
            $e->setAttribute("tabindex", $this->_tabindex);
    }

    /**
     * Returns the derived name (or id) of the form field.
     * @return string Name of form field
     */
    public function getFormFieldId()
    {
        return $this->_getRootProperty();
    }
}

?>
