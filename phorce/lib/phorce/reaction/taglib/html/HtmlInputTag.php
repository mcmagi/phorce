<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use \DOMElement;

/**
 * Provides an alternative means to render an <code>input</code> element.  Just
 * as a normal HTML <code>input</code> element, the behavior of this tag is
 * altered by the <code>type</code> attribute.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlInputTag extends HtmlInputTagBase
{
    /**
     * The type of input field this tag will manifest as.  Accepts all valid
     * HTML values for this attribute.
     * @var string
     * @access private
     */
    private $_type;

    /**
     * For input elements with a text field, the size of the text field.
     * @var integer
     * @access private
     */
    private $_size;

    /**
     * For input elements with a text field, the maximum allowed input
     * characters of the text field.
     * @var integer
     * @access private
     */
    private $_maxlength;

    /**
     * For input elements with a text field, whether this tag is read-only.
     * @var boolean
     * @access private
     */
    private $_readonly;

    /**
     * For <code>file</code> input elements, the acceptable mimetypes.
     * @var string
     * @access private
     */
    private $_accept;

    /**
     * For <code>image</code> input elements, the path to the image file.
     * @var string
     * @access private
     */
    private $_src;

    /**
     * For <code>image</code> input elements, the horizontal alignment of the
     * image input tag.
     * @var string
     * @access private
     */
    private $_align;

    /**
     * For <code>image</code> input elements, the border size of the image input
     * tag in pixels.
     * @var integer
     * @access private
     */
    private $_border;

    /**
     *
     * @var object HtmlInputTagBase
     * @access private
     */
    private $_typeTag;


    /**
     * Returns the type of input field this tag will manifest as.
     * @return string Input type
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Sets the type of input field this tag will manifest as.
     * @param string $v Input type
     */
    public function setType($v)
    {
        $this->_type = $v;
    }

    /**
     * Returns the size of the text field.
     * @return integer Size
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * Sets the size of the text field.
     * @param integer $v Size
     */
    public function setSize($v)
    {
        $this->_size = $v;
    }

    /**
     * Returns the maximum allowed input characters of the text field.
     * @return integer Max length
     */
    public function getMaxlength()
    {
        return $this->_maxlength;
    }

    /**
     * Sets the maximum allowed input characters of the text field.
     * @param integer $v Max length
     */
    public function setMaxlength($v)
    {
        $this->_maxlength = $v;
    }

    /**
     * Returns the path to the image file.
     * @return string Path
     */
    public function getSrc()
    {
        return $this->_src;
    }

    /**
     * Sets the path to the image file.
     * @param string $v Path
     */
    public function setSrc($v)
    {
        $this->_src = $v;
    }

    /**
     * Returns the horizontal alignment of the image input tag.
     * @return string Alignment
     */
    public function getAlign()
    {
        return $this->_align;
    }

    /**
     * Sets the horizontal alignment of the image input tag.
     * @param string $v Alignment
     */
    public function setAlign($v)
    {
        $this->_align = $v;
    }

    /**
     * Returns the border size of the image input tag in pixels.
     * @return integer Border size
     */
    public function getBorder()
    {
        return $this->_border;
    }

    /**
     * Sets the border size of the image input tag in pixels.
     * @param integer $v Border size
     */
    public function setBorder($v)
    {
        $this->_border = $v;
    }

    /**
     * Returns whether the text field is read-only.
     * @return boolean True if read-only
     */
    public function getReadonly()
    {
        return $this->_readonly;
    }

    /**
     * Sets whether the text field is read-only.
     * @param boolean $v True if read-only
     */
    public function setReadonly($v)
    {
        $this->_readonly = $v;
    }

    /**
     * Returns the acceptable mime types.
     * @return string Mime types
     */
    public function getAccept()
    {
        return $this->_accept;
    }

    /**
     * Sets the acceptable mime types.
     * @param string $v Mime types
     */
    public function setAccept($v)
    {
        $this->_accept = $v;
    }

    public function doStartTag()
    {
        // dummy element so we can call the abstract methods
        $doc = new DOMDocument();
        $dummy = $doc->createElement("input");

        // create inner type-specific input tag and set its attributes
        $this->_populateTypeAttributes($dummy);

        // set the value attribute
        $this->_populateValueAttribute($dummy);

        // common nested tag attributes
        $this->_typeTag->setName($this->getName());
        $this->_typeTag->setProperty($this->getProperty());
        $this->_typeTag->setScope($this->getScope());

        // common CSS form field attributes
        $this->_typeTag->setStyle($this->getStyle());
        $this->_typeTag->setStyleClass($this->getStyleClass());
        $this->_typeTag->setStyleId($this->getStyleId());
        $this->_typeTag->setErrorStyle($this->getErrorStyle());
        $this->_typeTag->setErrorStyleClass($this->getErrorStyleClass());
        $this->_typeTag->setErrorStyleId($this->getErrorStyleId());
        $this->_typeTag->setErrorKey($this->getErrorKey());

        // common javascript form field attributes
        $this->_typeTag->setOnBlur($this->getOnBlur());
        $this->_typeTag->setOnChange($this->getOnChange());
        $this->_typeTag->setOnClick($this->getOnClick());
        $this->_typeTag->setOnDblClick($this->getOnDblClick());
        $this->_typeTag->setOnFocus($this->getOnFocus());
        $this->_typeTag->setOnKeyDown($this->getOnKeyDown());
        $this->_typeTag->setOnKeyPress($this->getOnKeyPress());
        $this->_typeTag->setOnKeyUp($this->getOnKeyUp());
        $this->_typeTag->setOnMouseDown($this->getOnMouseDown());
        $this->_typeTag->setOnMouseMove($this->getOnMouseMove());
        $this->_typeTag->setOnMouseOut($this->getOnMouseOut());
        $this->_typeTag->setOnMouseOver($this->getOnMouseOver());
        $this->_typeTag->setOnMouseUp($this->getOnMouseUp());
        $this->_typeTag->setOnMouseUp($this->getOnMouseUp());

        // other common form field attributes
        $this->_typeTag->setAlt($this->getAlt());
        $this->_typeTag->setAltKey($this->getAltKey());
        $this->_typeTag->setTitle($this->getTitle());
        $this->_typeTag->setTitleKey($this->getTitleKey());
        $this->_typeTag->setTitle($this->getTitle());
        $this->_typeTag->setTabindex($this->getTabindex());
        $this->_typeTag->setDisabled("".$this->getDisabled());
        $this->_typeTag->setBundle($this->getBundle());

        // common input tag attributes
        $this->_typeTag->setAccesskey($this->getAccesskey());

        // also set the page context
        $this->_typeTag->setPageContext($this->_pageContext);

        // execute the tag
        return $this->_typeTag->doStartTag();
    }

    protected function _populateTypeAttributes(DOMElement $e)
    {
        // create inner type-specific input tag and set its attributes
        switch (strtolower($this->_type))
        {
            case self::INPUT_TYPE_SUBMIT:
                $this->_typeTag = new HtmlSubmitTag();
                break;
            case self::INPUT_TYPE_IMAGE:
                $this->_typeTag = new HtmlImageTag();
                $this->_typeTag->setSrc($this->getSrc());
                $this->_typeTag->setAlign($this->getAlign());
                $this->_typeTag->setBorder($this->getBorder());
                break;
            case self::INPUT_TYPE_BUTTON:
                $this->_typeTag = new HtmlButtonTag();
                break;
            case self::INPUT_TYPE_CHECKBOX:
                $this->_typeTag = new HtmlCheckboxTag();
                break;
            case self::INPUT_TYPE_RADIO:
                $this->_typeTag = new HtmlRadioTag();
                $this->_typeTag->setIdName($this->getIdName());
                break;
            case self::INPUT_TYPE_HIDDEN:
                $this->_typeTag = new HtmlHiddenTag();
                //$this->_typeTag->setWrite("".$this->getWrite());
                break;
            case self::INPUT_TYPE_TEXT:
                $this->_typeTag = new HtmlTextTag();
                $this->_typeTag->setSize($this->getSize());
                $this->_typeTag->setMaxlength($this->getMaxlength());
                $this->_typeTag->setReadonly("".$this->getReadonly());
                break;
            case self::INPUT_TYPE_FILE:
                $this->_typeTag = new HtmlFileTag();
                $this->_typeTag->setSize($this->getSize());
                $this->_typeTag->setMaxlength($this->getMaxlength());
                $this->_typeTag->setAccept($this->getAccept());
                break;
            case self::INPUT_TYPE_PASSWORD:
                $this->_typeTag = new HtmlPasswordTag();
                $this->_typeTag->setReadonly("".$this->getReadonly());
                //$this->_typeTag->setRedisplay("".$this->getRedisplay());
                $this->_typeTag->setSize($this->getSize());
                $this->_typeTag->setMaxlength($this->getMaxlength());
                break;
        }
    }

    protected function _populateValueAttribute(DOMElement $e)
    {
        // value attribute
        $this->_typeTag->setValue($this->getValue());
    }
}

?>
