<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\inertia\tagext\TagHandler;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlHtmlTag extends TagHandler
{
    private $_include;
    private $_session = false;
    private $_module;
    private $_name;
    private $_id;
    private $_cacheExpire;
    private $_cacheLimiter;
    private $_savePath;

    private $_sessionAlreadyStarted = false;

    /**
     * The XHTML representation of this HTML element.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;


    public function getInclude()
    {
        return $this->_include;
    }

    public function setInclude($v)
    {
        $this->_include = $v;
    }

    public function getSession()
    {
        return $this->_session;
    }

    public function setSession($v)
    {
        $this->_session = (boolean) $v;
    }

    public function getModule()
    {
        return $this->_module;
    }

    public function setModule($v)
    {
        $this->_module = $v;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setName($v)
    {
        $this->_name = $v;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setId($v)
    {
        $this->_id = $v;
    }

    public function getCacheExpire()
    {
        return $this->_cacheExpire;
    }

    public function setCacheExpire($v)
    {
        $this->_cacheExpire = $v;
    }

    public function getCacheLimiter()
    {
        return $this->_cacheLimiter;
    }

    public function setCacheLimiter($v)
    {
        $this->_cacheLimiter = $v;
    }

    public function getSavePath()
    {
        return $this->_savePath;
    }

    public function setSavePath($v)
    {
        $this->_savePath = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        $this->_doc = new DOMDocument();
        $html = $this->_doc->createElement("html");
        $this->_doc->appendChild($html);

        // load any includes before session is started
        if (! is_null($this->_include))
        {
            // includes can be comma-separated
            $includeList = preg_split("/,/", $this->_include);

            foreach($includeList as $include)
            {
                include_once($include);
            }
        }

        if (isset($_SESSION))
            // session was already started elsewhere
            $this->_sessionAlreadyStarted = true;
        elseif ($this->_session)
        {
            // configure session if necessary before it is started

            if (! is_null($this->_name))
                session_name($this->_name);

            if (! is_null($this->_id))
                session_id($this->_id);

            if (! is_null($this->_cacheExpire))
                session_cache_expire($this->_cacheExpire);

            if (! is_null($this->_cacheLimiter))
                session_cache_expire($this->_cacheLimiter);

            if (! is_null($this->_savePath))
                session_save_path($this->_savePath);

            if (! is_null($this->_module))
                session_module_name($this->_module);

            // start the session
            session_start();
        }

        echo HtmlUtils::getStartTag($html);

        return TagHandler::EVAL_BODY_INCLUDE;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        // only close session if this tag started it
        if ($this->_session && ! $this->_sessionAlreadyStarted)
            session_write_close();

        echo HtmlUtils::getEndTag($this->_doc->documentElement);

        return TagHandler::EVAL_PAGE;
    }
}

?>
