<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>file</code> on an HTML form.  This
 * tag follows nested scope rules for determining property values.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlFileTag extends HtmlTextTagBase
{
    /**
     * The acceptable mime types.
     * @var string
     * @access private
     */
    private $_accept;


    /**
     * Returns the type attribute of this tag, "file".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_FILE;
    }

    /**
     * Returns the acceptable mime types.
     * @return string Mime types
     */
    public function getAccept()
    {
        return $this->_accept;
    }

    /**
     * Sets the acceptable mime types.
     * @param string $v Mime types
     */
    public function setAccept($v)
    {
        $this->_accept = $v;
    }

    /**
     * Adds attributes specific to this input type.  Adds the accept attribute.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
        parent::_populateTypeAttributes($e);

        if (! is_null($this->_accept))
            $e->setAttribute("accept", $this->_accept);
    }

    /**
     * Populates the value attribute in the XHTML output.  The value will only
     * be pre-populated if the value attribute was passed this tag.  No value
     * will be retreived from a nested property since the file tag does not
     * store a textual value.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        if (! is_null($this->value))
            parent::_populateValueAttribute($e);
    }
}

?>
