<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\inertia\tagext\TagHandler;

/**
 * Represents a <code>select</code> element on an HTML form.  This tag follows
 * nested scope rules for determining property values.  Options ought to be
 * nested inside this element to provide a selection list.  If the
 * <code>multiple</code> attribute s true, then this will render a multi-select
 * box.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlSelectTag extends HtmlFormFieldTagBase
{
    /**
     * If set to true, renders a multi-select box which will allow the user to
     * select multiple values rather than a single-value drop-down box.
     * @var boolean
     * @access private
     */
    private $_multiple;

    /**
     * The width of the select box.
     * @var integer
     * @access private
     */
    private $_size;

    /**
     * The XHTML representation of this SELECT element.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;


    /**
     * Returns whether to render a multi-select box.
     * @return boolean True if multi-select
     */
    public function getMultiple()
    {
        return $this->_multiple;
    }

    /**
     * Sets whether to render a multi-select box.
     * @param string $v True if multi-select
     */
    public function setMultiple($v)
    {
        $this->_multiple = ($v === "true");
    }

    /**
     * Returns the width of the select box.
     * @return integer Width
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * Sets the width of the select box.
     * @param integer $v Width
     */
    public function setSize($v)
    {
        $this->_size = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // create select element
        $this->_doc = new DOMDocument();
        $select = $this->_doc->createElement("select");
        $this->_doc->appendChild($select);

        $this->_populateFormFieldAttributes($select);

        if (strlen($this->_multiple))
            $select->setAttribute("multiple", "multiple");
        if (! is_null($this->_size))
            $select->setAttribute("size", $this->_size);

        echo HtmlUtils::getStartTag($select);

        return TagHandler::EVAL_BODY_INCLUDE;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        echo HtmlUtils::getEndTag($this->_doc->documentElement);

        return TagHandler::EVAL_PAGE;
    }

    /**
     * Returns whether the specified value is currently selected by this tag.
     * @param string $value Value to check
     * @return boolean True if selected
     */
    public function isSelected($value)
    {
        $propValue =& $this->_getNestedPropertyValue();

        if ($this->_multiple && is_array($propValue))
        {
            // return true if one of the values in the array matches
            foreach (array_keys($propValue) as $key)
            {
                if ($propValue[$key] === $value)
                    return true;
            }
            return false;
        }
        else
            // return true if the value matches
            return $value === $propValue;
    }
}

?>
