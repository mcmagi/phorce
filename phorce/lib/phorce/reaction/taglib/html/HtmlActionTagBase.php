<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use phorce\reaction\Globals;
use phorce\reaction\action\ReactionController;
use phorce\reaction\taglib\ReactionTagException;
use \Net_URL2;

/**
 * Serves as a base class for HTML tags that must generate a URL to invoke
 * another Phorce action.
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
abstract class HtmlActionTagBase extends HtmlStyleTagBase
{
    private $_action;
    private $_module;
    private $_page;
    private $_pageKey;
    private $_paramId;
    private $_paramName;
    private $_paramProperty;
    private $_paramScope;
    private $_bundle = Globals::MESSAGES_KEY;

    /**
     * The message resource bundle.
     * @var object MessageResources
     * @access private
     */
    private $_resources;


    public function getAction()
    {
        return $this->_action;
    }

    public function setAction($v)
    {
        $this->_action = $v;
    }

    public function getModule()
    {
        return $this->_module;
    }

    public function setModule($v)
    {
        $this->_module = $v;
    }

    public function getPage()
    {
        return $this->_page;
    }

    public function setPage($v)
    {
        $this->_page = $v;
    }

    public function getPageKey()
    {
        return $this->_pageKey;
    }

    public function setPageKey($v)
    {
        $this->_pageKey = $v;
    }

    public function getParamId()
    {
        return $this->_paramId;
    }

    public function setParamId($v)
    {
        $this->_paramId = $v;
    }

    public function getParamName()
    {
        return $this->_paramName;
    }

    public function setParamName($v)
    {
        $this->_paramName = $v;
    }

    public function getParamProperty()
    {
        return $this->_paramProperty;
    }

    public function setParamProperty($v)
    {
        $this->_paramProperty = $v;
    }

    public function getParamScope()
    {
        return $this->_paramScope;
    }

    public function setParamScope($v)
    {
        $this->_paramScope = $v;
    }

    public function getBundle()
    {
        return $this->_bundle;
    }

    public function setBundle($v)
    {
        $this->_bundle = $v;
    }

    /**
     * Loads the message resources for this tag's configured bundle.  If they
     * have already been loaded once then they will not be loaded again.
     * @return object MessageResources
     */
    protected function _getResources()
    {
        if (is_null($this->_resources))
        {
            // print error if there is no bundle
            if (! array_key_exists($this->_bundle, $_REQUEST))
            {
                throw new ReactionTagException($this,
                    "No message resources configured for bundle '{$this->_bundle}'");
            }

            // get message resources for the specified bundle
            $this->_resources = $_REQUEST[$this->_bundle];
        }

        return $this->_resources;
    }

    protected function _getActionURL()
    {
        $url = new Net_URL2();
        $url->path = $this->_action . ".do";
        return $url;
    }

    protected function _getPageURL()
    {
        $url = new Net_URL2();
        $url->path = $this->_page;
        return $url;
    }
}

?>
