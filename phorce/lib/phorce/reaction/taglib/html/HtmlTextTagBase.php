<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
abstract class HtmlTextTagBase extends HtmlInputTagBase
{
    /**
     * The size of the text field.
     * @var integer
     * @access private
     */
    private $_size;

    /**
     * The maximum allowed input characters of the text field.
     * @var integer
     * @access private
     */
    private $_maxlength;


    /**
     * Returns the size of the text field.
     * @return integer Size
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * Sets the size of the text field.
     * @param integer $v Size
     */
    public function setSize($v)
    {
        $this->_size = $v;
    }

    /**
     * Returns the maximum allowed input characters of the text field.
     * @return integer Max length
     */
    public function getMaxlength()
    {
        return $this->_maxlength;
    }

    /**
     * Sets the maximum allowed input characters of the text field.
     * @param integer $v Max length
     */
    public function setMaxlength($v)
    {
        $this->_maxlength = $v;
    }

    /**
     * Adds attributes specific to this input type.  This implementation populates
     * the size and maxlength attributes.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
        if (! is_null($this->_size))
            $e->setAttribute("size", $this->_size);
        if (! is_null($this->_maxlength))
            $e->setAttribute("maxlength", $this->_maxlength);
    }

    /**
     * Populates the value attribute in the XHTML output.  This method will use
     * the value attribute passed to the tag if populated.  Otherwise, it will
     * retrieve and use the property value following standard nesting rules.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        $propValue =& $this->_getNestedPropertyValue();

        if (is_null($this->value))
            $e->setAttribute("value", $propValue);
        else
            $e->setAttribute("value", $this->value);
    }
}

?>
