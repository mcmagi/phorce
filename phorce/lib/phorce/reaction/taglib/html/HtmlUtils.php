<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMAttr;
use \DOMElement;

/**
 * Tools used by the Reaction HTML Tag Library.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 * @abstract
 */
abstract class HtmlUtils
{
    /**
     * Returns the rendered start tag of the specified DOMElement.
     * @param object DOMElement HTML element
     * @return string Start tag
     */
    public static function getStartTag(DOMElement $e)
    {
        $attrs = array();
        for ($i = 0; $i < $e->attributes->length; $i++)
        {
            $node = $e->attributes->item($i);
            if ($node instanceof DOMAttr)
                $attrs[] = "{$node->name}=\"" . htmlspecialchars($node->value) . "\"";
        }

        return "<{$e->tagName} " . implode(" ", $attrs) . ">";
    }

    /**
     * Returns the rendered end tag of the specified DOMElement.
     * @param object DOMElement HTML element
     * @return string End tag
     */
    public static function getEndTag(DOMElement $e)
    {
        return "</{$e->tagName}>";
    }
}

?>
