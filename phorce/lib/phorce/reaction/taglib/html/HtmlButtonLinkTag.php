<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\common\web\ApplicationContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\taglib\ReactionTagException;
use phorce\reaction\taglib\nested\NestedTagBase;
use \Net_URL2;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlButtonLinkTag extends NestedTagBase
{
    private $_anchor;
    private $_href;
    private $_page;
    private $_parameters;
    private $_paramId;
    private $_paramName;
    private $_paramProperty;
    private $_paramScope;
    private $_target;
    private $_style;
    private $_styleClass;
    private $_styleId;
    private $_title;
    private $_value;
    private $_action;

    /**
     * The DOM INPUT element.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;

    public function getParameters()
    {
        return $this->_parameters;
    }

    public function setParameters($v)
    {
        $this->_parameters = $v;
    }

    public function getParamId()
    {
        return $this->_paramId;
    }

    public function setParamId($v)
    {
        $this->_paramId = $v;
    }

    public function getParamName()
    {
        return $this->_paramName;
    }

    public function setParamName($v)
    {
        $this->_paramName = $v;
    }

    public function getParamProperty()
    {
        return $this->_paramProperty;
    }

    public function setParamProperty($v)
    {
        $this->_paramProperty = $v;
    }

    public function getParamScope()
    {
        return $this->_paramScope;
    }

    public function setParamScope($v)
    {
        $this->_paramScope = $v;
    }

    public function getTarget()
    {
        return $this->_target;
    }

    public function setTarget($v)
    {
        $this->_target = $v;
    }

    public function getAnchor()
    {
        return $this->_anchor;
    }

    public function setAnchor($v)
    {
        $this->_anchor = $v;
    }

    public function getHref()
    {
        return $this->_href;
    }

    public function setHref($v)
    {
        $this->_href = $v;
    }

    public function getPage()
    {
        return $this->_page;
    }

    public function setPage($v)
    {
        $this->_page = $v;
    }

    public function getStyle()
    {
        return $this->_style;
    }

    public function setStyle($v)
    {
        $this->_style = $v;
    }

    public function getStyleClass()
    {
        return $this->_styleClass;
    }

    public function setStyleClass($v)
    {
        $this->_styleClass = $v;
    }

    public function getStyleId()
    {
        return $this->_styleId;
    }

    public function setStyleId($v)
    {
        $this->_styleId = $v;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function setTitle($v)
    {
        $this->_title = $v;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function setValue($v)
    {
        $this->_value = $v;
    }

    public function getAction()
    {
        return $this->_action;
    }

    public function setAction($v)
    {
        $this->_action = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        $this->_doc = new DOMDocument();
        $input = $doc->createElement("input");
        $this->_doc->appendChild($input);

        $input->setAttribute("type", "button");

        $link = new Net_URL2();
        if (! is_null($this->_action))
        {
            $c = ReactionController::getInstance();
            $mapping = $c->findActionConfig($this->_action);
            if (is_null($mapping))
                throw new ReactionTagException($this, "Cannot find action with path '{$this->_action}' in reaction config");

            $ctx = ApplicationContext::getInstance();
            $link->setPath($ctx->getContextRoot() . $mapping->path . ".do");
        }
        elseif (! is_null($this->_href))
            $link = new Net_URL2($this->_href);
        elseif (! is_null($this->_page))
            $link->setPath($this->_page);

        if (! is_null($this->_parameters))
            $link->setQuery($this->_parameters);

        if (! is_null($this->_paramId))
        {
            // get param name/property
            $paramValue = $this->_getNestedPropertyValue($this->_paramName,
                $this->_paramProperty, $this->_paramScope);
            $link->setQueryVariable($this->_paramId, $paramValue);
        }

        if (! is_null($this->name) || ! is_null($this->property))
        {
            $value =& $this->_getNestedPropertyValue();
            if (! is_array($value) || $value instanceof Traversable)
                throw new ReactionTagException($this, "name/property must return associative array");

            // build query string from array
            foreach ($value as $n => $v)
                $link->setQueryVariable($n, $v);
        }

        if (! is_null($this->_anchor))
            $link->setFragment($this->_anchor);

        if (! is_null($this->_value))
            $input->setAttribute("value", $this->_value);

        $input->setAttribute("onClick", "window.location.href = '".$link->__toString()."';");
        if (! is_null($this->_target))
            $input->setAttribute("target", $this->_target);
        if (! is_null($this->_title))
            $input->setAttribute("title", $this->_title);
        if (! is_null($this->_styleId))
        {
            $input->setAttribute("id", $this->_styleId);
            $input->setAttribute("name", $this->_styleId);
        }
        if (! is_null($this->_styleClass))
            $input->setAttribute("class", $this->_styleClass);
        if (! is_null($this->_style))
            $input->setAttribute("style", $this->_style);

        return TagHandler::EVAL_BODY_BUFFERED;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        // add body content as child of input tag
        $text = $this->_doc->createTextNode($this->bodyContent);
        $this->_doc->documentElement->appendChild($text);

        // output html
        echo $this->_doc->saveHTML();

        return TagHandler::EVAL_PAGE;
    }
}

?>
