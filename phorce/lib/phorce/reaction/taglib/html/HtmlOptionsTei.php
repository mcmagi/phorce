<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use phorce\inertia\tagparser\TagData;
use phorce\inertia\tagext\VariableInfo;
use phorce\reaction\taglib\nested\NestedTei;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlOptionsTei extends NestedTei
{
    /**
     * Returns an array of variable info objects this tag must handle at
     * translation time.
     * @param object TagData $tagData Info about start tag
     * @return Array of Variable Infos
     */
    public function getVariableInfo(TagData $tagData)
    {
        // get attributes from tagdata
        $attrs =& $tagData->getAttributeMap();

        $infos = parent::getVariableInfo($tagData);
        if (array_key_exists("collection", $attrs))
            $infos[] = new VariableInfo($attrs["collection"], VariableInfo::SCOPE_PASS_TO_TAG);
        return $infos;
    }
}

?>
