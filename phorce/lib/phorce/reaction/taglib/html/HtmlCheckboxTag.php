<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>checkbox</code> on an HTML form.
 * This tag follows nested scope rules for determining property values.  If the
 * property value of this form field is populated (and equals the specified
 * <code>value</code> attribute), then it will be marked as checked when the
 * field is rendered.
 *
 * <p>Checkbox property values must be re-initialized to empty values via the
 * <code>reset()</code> method on the <code>ActionForm</code>.</p>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlCheckboxTag extends HtmlInputTagBase
{
    /**
     * Returns the type attribute of this tag, "checkbox".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_CHECKBOX;
    }

    /**
     * Adds attributes specific to this input type.  This implemenation does
     * nothing since there are no type-specific attributes to populate.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
    }

    /**
     * Populates the value attribute in the XHTML output.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        $propValue =& $this->_getNestedPropertyValue();

        // value defaults to "on" unless otherwise specified
        if (is_null($this->getValue()))
            $value = "on";
        else
            $value = $this->getValue();

        $e->setAttribute("value", $value);

        // check this checkbox if property value is true
        if ($propValue === $value)
            $e->setAttribute("checked", "checked");
    }
}

?>
