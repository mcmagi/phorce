<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\inertia\tagext\TagHandler;

/**
 * Represents a <code>textarea</code> element on an HTML form.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlTextareaTag extends HtmlFormFieldTagBase
{
    /**
     * The number of character rows in this field.
     * @var integer
     * @access private
     */
    private $_rows;

    /**
     * The number of character columns in this field.
     * @var integer
     * @access private
     */
    private $_cols;

    /**
     * Whether this text field is read-only.
     */
    private $_readonly = false;

    /**
     * The XHTML representation of this TEXTAREA element.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;


    /**
     * Returns the number of character rows in this field.
     * @return integer Rows
     */
    public function getRows()
    {
        return $this->_rows;
    }

    /**
     * Sets the number of character rows in this field.
     * @param integer $v Rows
     */
    public function setRows($v)
    {
        $this->_rows = $v;
    }

    /**
     * Returns the number of character columns in this field.
     * @return integer Columns
     */
    public function getCols()
    {
        return $this->_cols;
    }

    /**
     * Sets the number of character columns in this field.
     * @param integer $v Columns
     */
    public function setCols($v)
    {
        $this->_cols = $v;
    }

    /**
     * Returns whether this text field is read-only.
     * @return boolean Read-only
     */
    public function getReadonly()
    {
        return $this->_readonly;
    }

    /**
     * Sets whether this text field is read-only.
     * @return string Read-only
     */
    public function setReadonly($v)
    {
        $this->_readonly = ($v === "true");
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        $this->_doc = new DOMDocument();
        $textarea = $this->_doc->createElement("textarea");
        $this->_doc->appendChild($textarea);

        $this->_populateFormFieldAttributes($textarea);

        if (! is_null($this->_rows))
            $textarea->setAttribute("rows", $this->_rows);
        if (! is_null($this->_cols))
            $textarea->setAttribute("cols", $this->_cols);
        if ($this->_readonly)
            $textarea->setAttribute("readonly", "readonly");

        $propValue =& $this->_getNestedPropertyValue();

        if (! is_null($this->getValue()))
            $content = $this->getValue();
        else
            $content = $propValue;

        $text = $this->_doc->createTextNode($content);
        $textarea->appendChild($text);

        echo $this->_doc->saveHTML();

        return TagHandler::EVAL_BODY_INCLUDE;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }
}

?>
