<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>reset</code> on an HTML form.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlResetTag extends HtmlButtonTagBase
{
    /**
     * Returns the type attribute of this tag, "reset".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_RESET;
    }

    /**
     * Adds attributes specific to this input type.  If no property is specified
     * on this tag then no name attribute will be generated on the input tag.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
        // populate form field name only if property specified
        if (is_null($this->property))
            $e->setAttribute("name", null);
    }

    /**
     * The default value for this button is "Reset".
     * @return string "Reset"
     * @access protected
     */
    protected function _getDefaultValue()
    {
        return "Reset";
    }
}

?>
