<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;
namespace phorce\reaction\Globals;

/**
 * Represents an input field of type <code>submit</code> on an HTML form, but
 * which sets the <code>ACTION_CANCEL_KEY</code> in the request.  This indicates
 * to the controller that <code>validate()</code> on the <code>ActionForm</code>
 * should not be called.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlCancelTag extends HtmlButtonTagBase
{
    /**
     * Returns the type attribute of this tag, "submit".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_SUBMIT;
    }

    /**
     * Adds attributes specific to this input type.  Overrides the default name
     * with the Globals::CANCEL_KEY constant if no property is specified.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
        // if no property specified, use action cancelled key
        if (is_null($this->getProperty()))
            $e->setAttribute("name", Globals::CANCEL_KEY);
    }

    /**
     * The default value for this button is "Cancel".
     * @return string "Cancel"
     * @access protected
     */
    protected function _getDefaultValue()
    {
        return "Cancel";
    }
}

?>
