<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>radio</code> on an HTML form.  This
 * tag follows nested scope rules for determining property values.  If the value
 * attribute of this radio button matches the property value, then it will be
 * marked as selected when the field is rendered.
 *
 * <p>If the <code>idName</code> attribute is specified, then the
 * <code>value</code> attribute will be considered a property name and retrieved
 * from the object specified by <code>idName</code>.</p>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlRadioTag extends HtmlInputTagBase
{
    /**
     * The name of an object from which to retrieve the radio button's value.
     * If specified, then the <code>value</code> attribute will be used as a
     * property to this object.
     */
    protected $_idName;


    /**
     * Returns the type of this input field, "radio".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_RADIO;
    }

    /**
     * Returns the name of an object from which to retrieve the radio button's
     * value.
     * @return string Object name
     */
    public function getIdName()
    {
        return $this->_idName;
    }

    /**
     * Sets the name of an object from which to retrieve the radio button's
     * value.
     * @param string $v Object name
     */
    public function setIdName($v)
    {
        $this->_idName = $v;
    }

    /**
     * Adds attributes specific to this input type.  This implemenation does
     * nothing since there are no type-specific attributes to populate.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
    }

    /**
     * Populates the value attribute in the XHTML output.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        // get selected value of object
        $propValue =& $this->_getNestedPropertyValue();

        // if idName is set, retrieve value as property 'idName->value'
        // otherwise, just use the specified value straight up
        if (! is_null($this->_idName))
            $value = $this->_getPropertyValue($this->_idName, $this->getValue());
        else
            $value = $this->getValue();

        $e->setAttribute("value", $value);

        // check this radio button if property value matches value
        if ($propValue == $this->getValue())
            $e->setAttribute("checked", "checked");
    }
}

?>
