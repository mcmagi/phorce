<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use phorce\inertia\tagext\TagHandler;

/**
 * This tag is used to retrieve a set of error messages, look them up in the
 * message resources, and display them.  The set of messages referenced by the
 * <code>name</code> attribute may be an <code>ActionMessages</code> object,
 * an array of strings, or a string.  The format of the messages may be
 * controlled by the <code>header</code>, <code>footer</code>,
 * <code>prefix</code>, and <code>suffix</code> attributes.  Ths will require
 * storing HTML in the message resources file.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlErrorsTag extends HtmlMessagesTagBase
{
    /**
     * A message key to be looked up in the message resources which will get
     * outputted before each message.
     * @var string
     * @access private
     */
    private $_prefix = "errors.prefix";

    /**
     * A message key to be looked up in the message resources which will get
     * outputted before each message.
     * @var string
     * @access private
     */
    private $_suffix = "errors.suffix";


    /**
     * Constructs an HtmlErrorsTag.  The values of the header and footer
     * attributes are initialized here.
     */
    public function __construct()
    {
        $this->setHeader("errors.header");
        $this->setFooter("errors.footer");
    }

    /**
     * Returns a message key which will get outputted before each message.
     * @return string Message key
     */
    public function getPrefix()
    {
        return $this->_prefix;
    }

    /**
     * Sets a message key which will get outputted before each message.
     * @param string $v Message key
     */
    public function setPrefix($v)
    {
        $this->_prefix = $v;
    }

    /**
     * Returns a message key which will get outputted after each message.
     * @return string Message key
     */
    public function getSuffix()
    {
        return $this->_suffix;
    }

    /**
     * Sets a message key which will get outputted after each message.
     * @param string $v Message key
     */
    public function setSuffix($v)
    {
        $this->_suffix = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // get messages from request for the given key
        $messages =& $this->_getMessages();

        //echo "message count: " . count($messages);

        // only process message object if we have one
        if (count($messages))
        {
            // load message resources
            $this->_loadResources();

            // print message header
            if (! is_null($this->header))
                $this->_printResource($this->header);

            // loop through all messages
            foreach ($messages as $msg)
                $this->_handleMessage($msg);

            // print message footer
            if (! is_null($this->footer))
                $this->_printResource($this->footer);
        }

        return TagHandler::SKIP_BODY;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }

    /**
     * Handles/prints a message object.  Prefix and suffix messages are also
     * displayed if they exist.
     * @param string|object ActionMessage $msg Message object
     * @access protected
     */
    protected function _handleMessage($msg)
    {
        // print message prefix
        if (! is_null($this->prefix))
            $this->_printResource($this->prefix);

        // print message
        echo $this->_lookupMessage($msg);

        // print message suffix
        if (! is_null($this->suffix))
            $this->_printResource($this->suffix);
    }
}

?>
