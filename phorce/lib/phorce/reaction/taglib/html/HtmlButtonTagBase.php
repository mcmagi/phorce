<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
abstract class HtmlButtonTagBase extends HtmlInputTagBase
{
    /**
     * Populates the value attribute in the XHTML output.  If no value is
     * specified, it will check to see if there is a default value by calling
     * {@link #_getDefaultValue()}.  If the default value is null for this tag
     * then no value will be populated.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        if (! is_null($this->getValue()))
            $e->setAttribute("value", $this->getValue());
        elseif (! is_null($this->_getDefaultValue()))
            $e->setAttribute("value", $this->_getDefaultValue());
    }

    /**
     * Returns the default value for this tag.
     * @return string|null The default value
     * @access protected
     * @abstract
     */
    protected abstract function _getDefaultValue();
}

?>
