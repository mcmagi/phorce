<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use phorce\reaction\taglib\nested\NestedTagBase;

/**
 * Serves as a base class for HTML tags with style attributes and nested scope.
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
abstract class HtmlStyleTagBase extends NestedTagBase
{
    /**
     * An optional CSS style to apply to the rendered element via a
     * <code>style</code> attribute.
     * @var string
     * @access private
     */
    private $_style;

    /**
     * An optional CSS style to apply to the rendered element via a
     * <code>class</code> attribute.
     * @var string
     * @access private
     */
    private $_styleClass;

    /**
     * An optional CSS style to apply to the rendered element via an
     * <code>id</code> attribute.
     * @var string
     * @access private
     */
    private $_styleId;


    /**
     * Returns the CSS <code>style</code> attribute.
     * @return string CSS Style
     */
    public function getStyle()
    {
        return $this->_style;
    }

    /**
     * Sets the CSS <code>style</code> attribute.
     * @param string $v CSS Style
     */
    public function setStyle($v)
    {
        $this->_style = $v;
    }

    /**
     * Returns the CSS <code>class</code> attribute.
     * @return string CSS Class
     */
    public function getStyleClass()
    {
        return $this->_styleClass;
    }

    /**
     * Sets the CSS <code>class</code> attribute.
     * @param string $v CSS Class
     */
    public function setStyleClass($v)
    {
        $this->_styleClass = $v;
    }

    /**
     * Returns the CSS <code>id</code> attribute.
     * @return string CSS Id
     */
    public function getStyleId()
    {
        return $this->_styleId;
    }

    /**
     * Sets the CSS <code>id</code> attribute.
     * @param string $v CSS Id
     */
    public function setStyleId($v)
    {
        $this->_styleId = $v;
    }
}

?>
