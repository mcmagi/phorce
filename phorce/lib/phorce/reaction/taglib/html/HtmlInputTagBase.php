<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use \DOMElement;
use phorce\inertia\tagext\TagHandler;

/**
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
abstract class HtmlInputTagBase extends HtmlFormFieldTagBase
{
    /**
     * Type attribute of a text input field.
     */
    const INPUT_TYPE_TEXT = "text";

    /**
     * Type attribute of a hidden input field.
     */
    const INPUT_TYPE_HIDDEN = "hidden";

    /**
     * Type attribute of a file input field.
     */
    const INPUT_TYPE_FILE = "file";

    /**
     * Type attribute of a checkbox input field.
     */
    const INPUT_TYPE_CHECKBOX = "checkbox";

    /**
     * Type attribute of a radio button input field.
     */
    const INPUT_TYPE_RADIO = "radio";

    /**
     * Type attribute of a password input field.
     */
    const INPUT_TYPE_PASSWORD = "password";

    /**
     * Type attribute of a submit button input field.
     */
    const INPUT_TYPE_SUBMIT = "submit";

    /**
     * Type attribute of an image button input field.
     */
    const INPUT_TYPE_IMAGE = "image";

    /**
     * Type attribute of a button field.
     */
    const INPUT_TYPE_BUTTON = "button";

    /**
     * Type attribute of a reset button input field.
     */
    const INPUT_TYPE_RESET = "reset";


    /**
     * The hotkey used to access this input field.
     * @var string
     * @access private
     */
    private $_accesskey;

    /**
     * The XHTML representation of this INPUT element.
     * @var object DOMDocumnt
     * @access protected
     */
    protected $_doc;


    /**
     * Returns the hotkey used to access this input field.
     * @return string Hotkey
     */
    public function getAccesskey()
    {
        return $this->_accesskey;
    }

    /**
     * Sets the hotkey used to access this input field.
     * @param string $v Hotkey
     */
    public function setAccesskey($v)
    {
        $this->_accesskey = $v;
    }

    /**
     * Returns the type attribute for this input field.
     * @return string Type attribute
     * @abstract
     */
    public abstract function getType();

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        return TagHandler::SKIP_BODY;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        $this->_doc = new DOMDocument();
        $input = $this->_doc->createElement("input");
        $this->_doc->appendChild($input);

        $input->setAttribute("type", $this->type);

        // access key attribute
        if (! is_null($this->_accesskey))
            $input->setAttribute("accesskey", $this->_accesskey);

        // common form field attributes
        $this->_populateFormFieldAttributes($input);

        // type-specific attributes
        $this->_populateTypeAttributes($input);

        // value attribute
        $this->_populateValueAttribute($input);

        echo $this->_doc->saveHTML();

        return TagHandler::EVAL_PAGE;
    }

    /**
     * Adds attributes specific to this input type.
     * @param object DOMElement $e XHTML output
     * @abstract
     * @access protected
     */
    protected abstract function _populateTypeAttributes(DOMElement $e);

    /**
     * Populates the value attribute in the XHTML output.
     * @param object DOMElement $e XHTML output
     * @abstract
     * @access protected
     */
    protected abstract function _populateValueAttribute(DOMElement $e);
}

?>
