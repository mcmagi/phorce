<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Provides an alternative way to represent an input field of type
 * <code>checkbox</code> on an HTML form.  In this implementation, multiboxes
 * are generally rendered within a <code>logic:iterate</code> element.  A
 * checkbox will be rendered for each element of the array.  Furthermore, the
 * underlying property value is expected to be an array.
 *
 * <p>If no <code>value</code> attribute is specified, then the contents of this
 * tag will be used as the checkbox value.</p>
 *
 * <p>This tag follows nested scope rules for determining property values.  If
 * the checkbox value is present in the property value array, then it will be
 * marked as checked when the field is rendered.</p>
 *
 * <p>Multibox property values must be re-initialized to empty arrays via the
 * <code>reset()</code> method on the <code>ActionForm</code>.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlMultiboxTag extends HtmlInputTagBase
{
    /**
     * An associative array of array index counters for each unique multibox
     * property.
     * @var array
     * @access private
     * @static
     */
    private static $_idxCtrs = array();

    /**
     * The array index for this tag.
     * @var integer
     * @access private
     */
    private $_arrayIndex;


    /**
     * Returns the type attribute of this tag, "checkbox".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_CHECKBOX;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        return TagHandler::EVAL_BODY_BUFFERED;
    }

    /**
     * Adds attributes specific to this input type.  This implemenation does
     * nothing since there are no type-specific attributes to populate.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
    }

    /**
     * Populates the value attribute in the XHTML output.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        $propValue =& $this->_getNestedPropertyValue();

        // value defaults to "on" unless otherwise specified
        if (is_null($this->value))
            $value = $this->bodyContent;
        else
            $value = $this->value;

        $e->setAttribute("value", $value);

        // check this checkbox if property value is in the array
        if (is_array($propValue) && array_search($value, $propValue) !== false)
            $e->setAttribute("checked", "checked");
    }

    /**
     * Returns the portion of the nested property that this form field
     * represents.  For Multibox tags, the property will be followed by an array
     * index.  This is how we manifest the Multibox as an array to the Phorce
     * application.
     * @return string Form field id
     */
    public function getFormFieldId()
    {
        return parent::getFormFieldId() . "(" .  $this->_getArrayIndex() . ")";
    }

    /**
     * Returns the form field array index that will become part of the form
     * field id for this tag's root property.  Eag set of tags with the same
     * root property gets its own counter.  This is how we manifest the multibox
     * as an array to the Phorce application.
     * @return integer Array index
     * @access private
     */
    private function _getArrayIndex()
    {
        if (is_null($this->_arrayIndex))
        {
            $this->_arrayIndex = self::_getNextArrayIndex(
                $this->_getRootProperty());
        }

        return $this->_arrayIndex;
    }

    /**
     * Returns the next array index that will become part of the form field id
     * for the specified property.  Each unique property gets its own counter.
     * This is how we manifest the multibox as an array to the Phorce
     * application.
     * @param string $property Root property of field
     * @return integer Array index
     * @static
     * @access private
     */
    private static function _getNextArrayIndex($property)
    {
        // initialize counter for property
        if (! array_key_exists($property, self::$_idxCtrs))
            self::$_idxCtrs[$property] = 0;

        // return counter and increment
        return self::$_idxCtrs[$property]++;
    }
}

?>
