<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>text</code> on an HTML form.  This
 * tag follows nested scope rules for determining property values.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlTextTag extends HtmlTextTagBase
{
    /**
     * Whether the text field is read-only.
     * @var boolean
     * @access private
     */
    private $_readonly = false;


    /**
     * Returns the type attribute of this tag, "text".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_TEXT;
    }

    /**
     * Returns whether the text field is read-only.
     * @return boolean True if read-only
     */
    public function getReadonly()
    {
        return $this->_readonly;
    }

    /**
     * Sets whether the text field is read-only.
     * @param string $v "true" if read-only
     */
    public function setReadonly($v)
    {
        $this->_readonly = ($v === "true");
    }

    /**
     * Adds attributes specific to this input type.  Adds the readonly
     * attribute.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
        parent::_populateTypeAttributes($e);

        if ($this->_readonly)
            $e->setAttribute("readonly", "readonly");
    }
}

?>
