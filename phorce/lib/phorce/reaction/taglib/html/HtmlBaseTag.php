<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\common\web\ApplicationContext;
use phorce\inertia\tagext\TagHandler;
use \Net_URL2;

/**
 * Represents an HTML <code>base</code> element.  All relative URLs will be
 * considered relative to this base URL.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlBaseTag extends TagHandler
{
    /**
     * Ref constant used if base is relative to page.  This is the default.
     */
    const REF_PAGE = 'page';

    /**
     * Ref constant used if base is relative to site.
     */
    const REF_SITE = 'site';


    /**
     * The server name of the base URL.  If not specified, uses
     * <code>$_SERVER['SERVER_NAME']</code>.
     * @var string
     * @access private
     */
    private $_server;

    /**
     * The target window for URLs.  Renders a <code>target</code> attribute on
     * the underlying <code>base</code> tag.
     * @var string
     * @access private
     */
    private $_target;

    /**
     * Whether this URL is relative to the current page (value
     * <code>page</code>) or relative to the application context (value
     * <code>site</code>).
     * @var string
     * @access private
     */
    private $_ref = self::REF_PAGE;


    /**
     * Returns the server name of the base URL.
     * @return string Server name
     */
    public function getServer()
    {
        return $this->_server;
    }

    /**
     * Sets the server name of the base URL.
     * @param string $v Server name
     */
    public function setServer($v)
    {
        $this->_server = $v;
    }

    /**
     * Returns the target window for URLs.
     * @return string Target window
     */
    public function getTarget()
    {
        return $this->_target;
    }

    /**
     * Sets the target window for URLs.
     * @param string $v Target window
     */
    public function setTarget($v)
    {
        $this->_target = $v;
    }

    /**
     * Returns whether this URL is relative to the current page or site.
     * @return string Page or Site
     */
    public function getRef()
    {
        return $this->_ref;
    }

    /**
     * Sets whether this URL is relative to the current page or site.
     * @param string $v Page or Site
     */
    public function setRef($v)
    {
        $this->_ref = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // get full script and path info
        if ($this->_ref == self::REF_SITE)
        {
            $ctx = ApplicationContext::getInstance();
            $path = $ctx->getContextRoot();
        }
        elseif ($this->_ref == self::REF_PAGE)
        {
            $path = $_SERVER['PHP_SELF'];
        }

        // determine server name
        if (! is_null($this->_server))
            $server = $this->_server;
        else
            $server = $_SERVER['SERVER_NAME'];

        // build URL
        $url = new Net_URL2();
        $url->scheme =
                    (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'])
                    ? 'https' : 'http';
        $url->scheme = $server;
        $url->path = $path;

        // build <base> tag
        $doc = new DOMDocument();
        $base = $doc->createElement("base");
        $doc->appendChild($base);

        // add href attribute
        $base->setAttribute("href", $url->__toString());

        if (! is_null($this->_target))
            $base->setAttribute("target", $this->_target);

        // output <base> tag
        echo $doc->saveHTML();

        return TagHandler::SKIP_BODY;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }
}

?>
