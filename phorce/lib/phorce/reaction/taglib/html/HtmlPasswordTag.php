<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>password</code> on an HTML form.
 * This tag follows nested scope rules for determining property values.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlPasswordTag extends HtmlTextTagBase
{
    /**
     * Whether the text field is read-only.
     * @var boolean
     * @access private
     */
    private $_readonly = false;

    /**
     * Whether the password is to be redispalyed (as stars) if the underlying
     * underlying property value is populated.  Default is true.
     * @var boolean
     * @access private
     */
    private $_redisplay = true;


    /**
     * Returns the type attribute of this tag, "password".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_PASSWORD;
    }

    /**
     * Returns whether the text field is read-only.
     * @return boolean True if read-only
     */
    public function getReadonly()
    {
        return $this->_readonly;
    }

    /**
     * Sets whether the text field is read-only.
     * @param string $v "true" if read-only
     */
    public function setReadonly($v)
    {
        $this->_readonly = ($v === "true");
    }

    /**
     * Returns whether the password is to be redispalyed.
     * @return string "true" if redisplay
     */
    public function getRedisplay()
    {
        return $this->_redisplay;
    }

    /**
     * Sets whether the password is to be redispalyed.
     * @param string $v "true" if redisplay
     */
    public function setRedisplay($v)
    {
        $this->_redisplay = ($v === "false");
    }

    /**
     * Adds attributes specific to this input type.  Adds the readonly
     * attribute.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
        parent::_populateTypeAttributes($e);

        if ($this->_readonly)
            $e->setAttribute("readonly", "readonly");
    }

    /**
     * Populates the value attribute in the XHTML output.  But Only populates
     * the value if the redisplay attribute passed to this tag is true.  This
     * will use the value attribute passed to the tag if populated.  Otherwise,
     * it will retrieve and use the property value following standard nesting
     * rules.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateValueAttribute(DOMElement $e)
    {
        if ($this->_redisplay)
            parent::_populateValueAttribute($e);
    }
}

?>
