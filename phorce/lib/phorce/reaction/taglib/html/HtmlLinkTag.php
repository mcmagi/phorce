<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\common\web\ApplicationContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\action\ReactionController;
use phorce\reaction\taglib\ReactionTagException;
use phorce\reaction\taglib\nested\NestedTagBase;

use \Net_URL2;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlLinkTag extends NestedTagBase
{
    private $_anchor;
    private $_href;
    private $_page;
    private $_parameters;
    private $_paramId;
    private $_paramName;
    private $_paramProperty;
    private $_paramScope;
    private $_paramValue;
    private $_target;
    private $_style;
    private $_styleClass;
    private $_styleId;
    private $_title;
    private $_action;
    private $_onclick;

    /**
     * The DOM document containing the 'A' HTML element.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;

    public function getParameters()
    {
        return $this->_parameters;
    }

    public function setParameters($v)
    {
        $this->_parameters = $v;
    }

    public function getParamId()
    {
        return $this->_paramId;
    }

    public function setParamId($v)
    {
        $this->_paramId = $v;
    }

    public function getParamName()
    {
        return $this->_paramName;
    }

    public function setParamName($v)
    {
        $this->_paramName = $v;
    }

    public function getParamProperty()
    {
        return $this->_paramProperty;
    }

    public function setParamProperty($v)
    {
        $this->_paramProperty = $v;
    }

    public function getParamScope()
    {
        return $this->_paramScope;
    }

    public function setParamScope($v)
    {
        $this->_paramScope = $v;
    }

    public function getParamValue()
    {
        return $this->_paramValue;
    }

    public function setParamValue($v)
    {
        $this->_paramValue = $v;
    }

    public function getTarget()
    {
        return $this->_target;
    }

    public function setTarget($v)
    {
        $this->_target = $v;
    }

    public function getAnchor()
    {
        return $this->_anchor;
    }

    public function setAnchor($v)
    {
        $this->_anchor = $v;
    }

    public function getHref()
    {
        return $this->_href;
    }

    public function setHref($v)
    {
        $this->_href = $v;
    }

    public function getPage()
    {
        return $this->_page;
    }

    public function setPage($v)
    {
        $this->_page = $v;
    }

    public function getStyle()
    {
        return $this->_style;
    }

    public function setStyle($v)
    {
        $this->_style = $v;
    }

    public function getStyleClass()
    {
        return $this->_styleClass;
    }

    public function setStyleClass($v)
    {
        $this->_styleClass = $v;
    }

    public function getStyleId()
    {
        return $this->_styleId;
    }

    public function setStyleId($v)
    {
        $this->_styleId = $v;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function setTitle($v)
    {
        $this->_title = $v;
    }

    public function getAction()
    {
        return $this->_action;
    }

    public function setAction($v)
    {
        $this->_action = $v;
    }

    public function getOnclick()
    {
        return $this->_onclick;
    }

    public function setOnclick($v)
    {
        $this->_onclick = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        $this->_doc = new DOMDocument();
        $a = $this->_doc->createElement("a");
        $this->_doc->appendChild($a);

        $link = new Net_URL2("");
        if (! is_null($this->_action))
        {
            $c = ReactionController::getInstance();
            $mapping = $c->findActionConfig($this->_action);
            if (is_null($mapping))
                throw new ReactionTagException($this, "Cannot find action with path '{$this->_action}' in reaction config");

            $ctx = ApplicationContext::getInstance();
            $link->setPath($ctx->getContextRoot() . $mapping->path . ".do");
        }
        elseif (! is_null($this->_href))
            $link = new Net_URL2($this->_href);
        elseif (! is_null($this->_page))
            $link->setPath($this->_page);

        if (! is_null($this->_parameters))
            $link->setQuery($this->_parameters);

        if (! is_null($this->_paramId))
        {
            if (! is_null($this->_paramValue))
                $paramValue = $this->_paramValue;
            else
                // get param name/property
                $paramValue = $this->_getNestedPropertyValue($this->_paramName,
                    $this->_paramProperty, $this->_paramScope);

            $link->setQueryVariable($this->_paramId, $paramValue);
        }

        if (! is_null($this->name) || ! is_null($this->property))
        {
            $value =& $this->_getNestedPropertyValue();
            if (! is_array($value) && ! $value instanceof Traversable)
                throw new ReactionTagException($this, "name/property must return associative array");

            // build query string from array
            foreach ($value as $n => $v)
                $link->setQueryVariable($n, $v);
        }

        if (! is_null($this->_anchor))
            $link->setFragment($this->_anchor);

        $a->setAttribute("href", $link->getURL());
        if (! is_null($this->_target))
            $a->setAttribute("target", $this->_target);
        if (! is_null($this->_title))
            $a->setAttribute("title", $this->_title);
        if (! is_null($this->_styleId))
        {
            $a->setAttribute("id", $this->_styleId);
            $a->setAttribute("name", $this->_styleId);
        }
        if (! is_null($this->_styleClass))
            $a->setAttribute("class", $this->_styleClass);
        if (! is_null($this->_style))
            $a->setAttribute("style", $this->_style);
        if (! is_null($this->_onclick))
            $a->setAttribute("onclick", $this->_onclick);

        echo HtmlUtils::getStartTag($a);

        return TagHandler::EVAL_BODY_INCLUDE;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        echo HtmlUtils::getEndTag($this->_doc->documentElement);

        return TagHandler::EVAL_PAGE;
    }
}

?>
