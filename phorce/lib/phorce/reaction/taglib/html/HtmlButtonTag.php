<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;

/**
 * Represents an input field of type <code>button</code> on an HTML form.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlButtonTag extends HtmlButtonTagBase
{
    /**
     * Returns the type attribute of this tag, "button".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_BUTTON;
    }

    /**
     * Adds attributes specific to this input type.  This implemenation does
     * nothing since there are no type-specific attributes to populate.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
    }

    /**
     * The default value for this button is "Click".
     * @return string "Click"
     * @access protected
     */
    protected function _getDefaultValue()
    {
        return "Click";
    }
}

?>
