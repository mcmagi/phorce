<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\Globals;
use phorce\reaction\taglib\ReactionTagException;
use phorce\reaction\action\ActionMessage;
use phorce\reaction\action\ActionMessages;

/**
 * A base class used for the retrieval, lookup, and display of message error
 * messages against the message resources.  The set of messages referenced by
 * the <code>name</code> attribute may be an <code>ActionMessages</code> object,
 * an array of strings, or a string.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 * @abstract
 */
abstract class HtmlMessagesTagBase extends TagHandler
{
    /**
     * The message bundle from which the message strings are retrieved.
     * @var string
     * @access private
     */
    private $_bundle = Globals::MESSAGES_KEY;

    /**
     * The name of the object in request scope from which the messages will be
     * retrieved.
     * @var string
     * @access private
     */
    private $_name = Globals::ERROR_KEY;

    /**
     * If the <code>name</code> attribute references an
     * <code>ActionMessages</code> objet, this specifies the property under
     * which those messages were stored.  If not specified, tee tag will
     * retrieve from all properties.
     * However, if the object referenced is not an <code>ActionMessages</code>
     * object, then the property will be retreived against the named object
     * using standard property rules.  A string or array of strings will be the
     * expected returned type.
     * @var string
     * @access private
     */
    private $_property;

    /**
     * A message key to be looked up in the message resources which will get
     * outputted once before the first message.
     * @var string
     * @access private
     */
    private $_header;

    /**
     * A message key to be looked up in the message resources which will get
     * outputted once after the last message.
     * @var string
     * @access private
     */
    private $_footer;

    /**
     * The message resource bundle.
     * @var object MessageResources
     * @access private
     */
    private $_resources;


    /**
     * Returns the message bundle from which the message strings are retrieved.
     * @return string Message bundle
     */
    public function getBundle()
    {
        return $this->_bundle;
    }

    /**
     * Sets the message bundle from which the message strings are retrieved.
     * @param string $v Message bundle
     */
    public function setBundle($v)
    {
        $this->_bundle = $v;
    }

    /**
     * Returns the name of the object in request scope from which the messages
     * will be retrieved.
     * @return string Name of object
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Sets the name of the object in request scope from which the messages
     * will be retrieved.
     * @param string $v Name of object
     */
    public function setName($v)
    {
        $this->_name = $v;
    }

    /**
     * Returns the property under the named object where messages are stored.
     * @return string Property
     */
    public function getProperty()
    {
        return $this->_property;
    }

    /**
     * Sets the property under the named object where messages are stored.
     * @param string $v Property
     */
    public function setProperty($v)
    {
        $this->_property = $v;
    }

    /**
     * Returns a message key which will get outputted once before the first
     * message.
     * @return string Message key
     */
    public function getHeader()
    {
        return $this->_header;
    }

    /**
     * Sets a message key which will get outputted once before the first
     * message.
     * @param string $v Message key
     */
    public function setHeader($v)
    {
        $this->_header = $v;
    }

    /**
     * Returns a message key which will get outputted once after the last
     * message.
     * @return string Message key
     */
    public function getFooter()
    {
        return $this->_footer;
    }

    /**
     * Sets a message key which will get outputted once after the last message.
     * @param string $v Message key
     */
    public function setFooter($v)
    {
        $this->_footer = $v;
    }

    /**
     * Loads and stores the resource bundle in the <code>_resources</code>
     * protected variable.
     * @access protected
     */
    protected function _loadResources()
    {
        // print error if there is no bundle
        if (! array_key_exists($this->_bundle, $_REQUEST))
            throw new ReactionTagException($this, "No message resources configured for bundle '{$this->_bundle}'");

        // get message resources for the specified bundle
        $this->_resources = $_REQUEST[$this->_bundle];
    }

    /**
     * Looks up any messages present in the request per this tag's name/property
     * configuration and returns them as an array.  The array may consist of
     * either ActionMessage objects (which will need to be looked up against
     * the message resources) or strings.
     * @return array Array of strings or ActionMessage objects
     * @access protected
     */
    protected function &_getMessages()
    {
        $msgArr = array();

        // get messages from request
        if (array_key_exists($this->_name, $_REQUEST))
            $messages = $_REQUEST[$this->_name];

        // if we have messages, determine what kind
        if (isset($messages) && ! is_null($messages))
        {
            if ($messages instanceof ActionMessages)
            {
                // ActionMessages: retrieve array of messages
                if (is_null($this->_property))
                    $msgArr =& $messages->get();
                else
                    $msgArr =& $messages->get($this->_property);
            }
            else
            {
                // not action messages, get named object in request
                $messages =& $this->_getPropertyValue($this->_name,
                    $this->_property, PageContext::SCOPE_REQUEST);

                if (is_array($messages))
                {
                    // array of strings (presumably)
                    $msgArr =& $messages;
                }
                elseif (is_string($messages))
                {
                    // make an array of one string
                    $msgArr = array($messages);
                }
            }
        }

        return $msgArr;
    }

    /**
     * Given a message object, looks up the corresponding message text.  If the
     * object is an ActionMessage object, it will look up the message against
     * the MessageResources.  If the object is a string, it will use the literal
     * value of the string as the message.  If neither, it will trigger an
     * error.
     * @param string|object ActionMessage $msgObj Message object
     * @return string Message text
     * @access protected
     */
    protected function _lookupMessage($msgObj)
    {
        if ($msgObj instanceof ActionMessage)
        {
            // translate message object to message
            if ($msgObj->isResource())
            {
                // lookup in message resources
                $message = $this->_resources->getMessage($msgObj->getKey(),
                    $msgObj->getValues());
            }
            else
            {
                // if not resource, use literal value
                $message = $msgObj->getKey();
            }
        }
        elseif (is_string($msgObj))
        {
            // if it's a string, just use it as the message
            $message = $msgObj;
        }
        else
        {
            // otherwise, error
            throw new ReactionTagException($this, "Property '{$this->property}' on object '{$this->name}'"
                . " is neither an ActionMessage nor a string");
        }

        return $message;
    }

    /**
     * Prints the specified resource string if it is present.  If it is not
     * present, nothing gets printed and no error is triggered.
     * @param string $resname Resource name
     * @access protected
     */
    protected function _printResource($resname)
    {
        if (! is_null($resname) && $this->_resources->isPresent($resname))
            echo $this->_resources->getMessage($resname);
    }
}

?>
