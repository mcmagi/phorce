<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMElement;
use \Net_URL2;

/**
 * Represents an input field of type <code>image</code> on an HTML form.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlImageTag extends HtmlSubmitTag
{
    /**
     * The horizontal alignment of the image input tag.
     * @var string
     * @access private
     */
    private $_align;

    /**
     * The border size of the image input tag in pixels.
     * @var integer
     * @access private
     */
    private $_border;

    /**
     * The module-relative path to the image file.
     * @var string
     * @access private
     * @todo
     */
    private $_page;

    /**
     * The message key of the module-relative path to the image file.
     * @var string
     * @access private
     * @todo
     */
    private $_pageKey;

    /**
     * The path to the image file.
     * @var string
     * @access private
     */
    private $_src;

    /**
     * The message key of the path to the image file.
     * @var string
     * @access private
     */
    private $_srcKey;


    /**
     * Returns the type attribute of this tag, "image".
     * @return string Type
     */
    public function getType()
    {
        return self::INPUT_TYPE_IMAGE;
    }

    /**
     * Returns the horizontal alignment of the image input tag.
     * @return string Alignment
     */
    public function getAlign()
    {
        return $this->_align;
    }

    /**
     * Sets the horizontal alignment of the image input tag.
     * @param string $v Alignment
     */
    public function setAlign($v)
    {
        $this->_align = $v;
    }

    /**
     * Returns the border size of the image input tag in pixels.
     * @return integer Border size
     */
    public function getBorder()
    {
        return $this->_border;
    }

    /**
     * Sets the border size of the image input tag in pixels.
     * @param integer $v Border size
     */
    public function setBorder($v)
    {
        $this->_border = $v;
    }

    /**
     * Returns the module-relative path to the image file.
     * @return string Path
     */
    public function getPage()
    {
        return $this->_page;
    }

    /**
     * Sets the module-relative path to the image file.
     * @param string $v Path
     */
    public function setPage($v)
    {
        $this->_page = $v;
    }

    /**
     * Returns the message key of the module-relative path to the image file.
     * @return string Message key
     */
    public function getPageKey()
    {
        return $this->_pageKey;
    }

    /**
     * Sets the message key of the module-relative path to the image file.
     * @param string $v Message key
     */
    public function setPageKey($v)
    {
        $this->_pageKey = $v;
    }

    /**
     * Returns the path to the image file.
     * @return string Path
     */
    public function getSrc()
    {
        return $this->_src;
    }

    /**
     * Returns the path to the image file.
     * @param string $v Path
     */
    public function setSrc($v)
    {
        $this->_src = $v;
    }

    /**
     * Returns the message key of the path to the image file.
     * @return string Message key
     */
    public function getSrcKey()
    {
        return $this->_srcKey;
    }

    /**
     * Sets the message key of the path to the image file.
     * @param string $v Message key
     */
    public function setSrcKey($v)
    {
        $this->_srcKey = $v;
    }

    /**
     * Adds attributes specific to this input type.
     * @param object DOMElement $e XHTML output
     * @access protected
     */
    protected function _populateTypeAttributes(DOMElement $e)
    {
        // populate submit type attributes
        parent::_populateTypeAttributes($e);

        if (! is_null($this->_align))
            $e->setAttribute("align", $this->_align);
        if (! is_null($this->_border))
            $e->setAttribute("border", $this->_border);

        // find location of image
        if (! is_null($this->_srcKey))
            $url = new Net_URL2($this->_getResources()->getMessage($this->_srcKey));
        elseif (! is_null($this->_src))
            $url = new Net_URL2($this->_src);
        else
        {
            $url = new Net_URL2();

            if (! is_null($this->_pageKey))
                $url->path = $this->_getResources()->getMessage($this->_pageKey);
            elseif (! is_null($this->_page))
                $url->path = $this->_page;
        }

        $e->setAttribute("src", $url->__toString());
    }
}

?>
