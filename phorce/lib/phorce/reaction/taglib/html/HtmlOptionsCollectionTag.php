<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use \Traversable;
use phorce\common\property\PropertyUtils;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\taglib\ReactionTagException;
use phorce\reaction\taglib\nested\NestedTagBase;

/**
 * <p>This tag provides an alternative means to iterate through an array to
 * generate a list of HTML <code>option</code> elements.  Its use is more
 * consistent with other uses of the <code>name</code> and <code>property</code>
 * attributes than the <code>html:options</code> tag.</p>
 *
 * <p>In this tag, <code>name</code> and <code>property</code> reference an
 * array of objects, using nested property rules.  Label and value can be
 * derived from each object by specifying a property name to the
 * <code>label</code> and <code>value</code> attributes, respectively.</p>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlOptionsCollectionTag extends NestedTagBase
{
    /**
     * The property of each object in the array to be used as an option's value.
     * If not specified, defaults to "value".
     * @var string
     * @access private
     */
    private $_value = "value";

    /**
     * The property of each object in the array to be used as an option's label.
     * If not specified, defaults to "label".
     * @var string
     * @access private
     */
    private $_label = "label";

    /**
     * Whether to escape specal HTML characters in the output.
     * @var boolean
     * @access private
     */
    private $_filter = true;

    /**
     * An optional CSS style to apply to the rendered element via a
     * <code>style</code> attribute.
     * @var string
     * @access private
     */
    private $_style;

    /**
     * An optional CSS style to apply to the rendered element via a
     * <code>class</code> attribute.
     * @var string
     * @access private
     */
    private $_styleClass;


    /**
     * Returns the property of each object in the array to be used as an
     * option's value.
     * @return string Value property
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Sets the property of each object in the array to be used as an
     * option's value.
     * @param string $v Value property
     */
    public function setValue($v)
    {
        $this->_value = $v;
    }

    /**
     * Returns the property of each object in the array to be used as an
     * option's label.
     * @return string Label property
     */
    public function getLabel()
    {
        return $this->_label;
    }

    /**
     * Sets the property of each object in the array to be used as an
     * option's label.
     * @param string $v Label property
     */
    public function setLabel($v)
    {
        $this->_label = $v;
    }

    /**
     * Returns whether to escape special HTML characters in the output.
     * @return boolean Filter
     */
    public function getFilter()
    {
        return $this->_filter;
    }

    /**
     * Sets whether to escape special HTML characters in the output.
     * @param boolean $v Filter
     */
    public function setFilter($v)
    {
        $this->_filter = ($v !== "false");
    }

    /**
     * Returns the CSS <code>style</code> attribute.
     * @return string CSS Style
     */
    public function getStyle()
    {
        return $this->_style;
    }

    /**
     * Sets the CSS <code>style</code> attribute.
     * @param string $v CSS Style
     */
    public function setStyle($v)
    {
        $this->_style = $v;
    }

    /**
     * Returns the CSS <code>class</code> attribute.
     * @return string CSS Class
     */
    public function getStyleClass()
    {
        return $this->_styleClass;
    }

    /**
     * Sets the CSS <code>class</code> attribute.
     * @param string $v CSS Class
     */
    public function setStyleClass($v)
    {
        $this->_styleClass = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // get select tag
        $selectTag = $this->findAncestorWithClass('phorce\reaction\taglib\html\HtmlSelectTag');

        // get list to iterate through from nested name/property
        $propValue =& $this->_getNestedPropertyValue();

        if (! is_array($propValue) && ! $propValue instanceof Traversable)
        {
            throw new ReactionTagException($this,
                'html:optionsCollection property value is not an array');
        }

        // loop through objects in array
        foreach ($propValue as $key => $obj)
        {
            // get label/value properties from object
            $value = PropertyUtils::getProperty($obj, $this->_value);
            $label = PropertyUtils::getProperty($obj, $this->_label);

            // create option element
            $doc = new DOMDocument();
            $option = $doc->createElement("option");
            $doc->appendChild($option);

            $option->setAttribute("value", $value);

            // CSS style attributes
            if (! is_null($this->_style))
                $option->setAttribute("style", $this->_style);
            if (! is_null($this->_styleClass))
                $option->setAttribute("class", $this->_styleClass);

            // add label
            $text = $doc->createTextNode($label);
            $option->appendChild($text);

            // is this option selected
            if ($selectTag->isSelected($value))
                $option->setAttribute("selected", "selected");

            // print option element
            echo $doc->saveHTML();
        }

        return TagHandler::SKIP_BODY;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }
}

?>
