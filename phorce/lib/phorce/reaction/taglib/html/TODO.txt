Input Tags
----------
(all but cancel, reset, multibox are indexed)
* <html:button> - accesskey, (fieldattrs), property, value
* <html:cancel> - accesskey, (fieldattrs), property, value
* <html:reset> - accesskey, (fieldattrs), property, value
* <html:submit> - accesskey, (fieldattrs), property, value
* <html:image> - accesskey, align, border, locale, (fieldattrs), page, pageKey, property, src, srcKey, value
* <html:checkbox> - accesskey, (fieldattrs), (errattrs), name, property, value
* <html:multibox> - accesskey, (fieldattrs), (errattrs), name, property, value
* <html:radio> - accesskey, (fieldattrs), (errattrs), idName, name, property, value
* <html:password> - accesskey, (fieldattrs), (errattrs), name, property, readonly, size, value
* <html:text> - accesskey, (fieldattrs), (errattrs), maxlength, name, property, readonly, size, value
* <html:hidden> - accesskey, (fieldattrs), name, property, value, write
* <html:file> - accept, accesskey, (fieldattrs), (errattrs), maxlength, name, property, size, value

HtmlFormFieldTagBase (fieldattrs, errattrs)
 |
 +- HtmlSelectTag (multiple, size)
 |
 +- HtmlTextareaTag (rows, cols, readonly)
 |
 +- HtmlInputTagBase (accesskey)
     |
     +- HtmlInputTag
     |
     +- HtmlButtonTagBase
     |   |
     |   +- HtmlSubmitTag
     |   |   |
     |   |   +- HtmlImageTag (align, border, locale, page, pageKey, src, srcKey)
     |   |
     |   +- HtmlResetTag
     |   |
     |   +- HtmlButtonTag
     |   |
     |   +- HtmlCancelTag
     |
     +- HtmlCheckboxTag
     |
     +- HtmlMultiboxTag
     |
     +- HtmlRadioTag (idName)
     |
     +- HtmlHiddenTag (write)
     |
     +- HtmlTextTagBase (maxlength, size)
         |
         +- HtmlTextTag (readonly)
         |
         +- HtmlPasswordTag (redisplay, readonly)
         |
         +- HtmlFileTag (accept)

Misc Input Tags
---------------
(all are indexed)
* <html:select> - (fieldattrs), (errattrs), multiple, name, property, size, value
* <html:textarea> - (fieldattrs), cols, (errattrs), name, property, readonly, rows, value

Option Tags
-----------
* <html:option> - bundle, disabled, key, locale, (cssattrs), value
* <html:options> - collection, filter, labelName, labelProperty, name, property, style, styleClass
* <html:optionsCollection> - filter, label, name, property, style, styleClass, value

Link Tags
---------
* <html:link> - accesskey, action, anchor, bundle, forward, href, indexed, indexId, linkName, module, name, (jsattrs), page, paramId, paramName, paramProperty, paramScope, property, scope, (cssattrs), tabindex, target, title, titleKey, transaction, useLocalEncoding
  <html:img> - action, align, (fieldattrs), border, height, hspace, imageName, ismap, locale, module, name, page, pageKey, paramId, paramName, paramProperty, paramScope, property, scope, src, srcKey, useLocalEncoding, usermap, vspace, width
  <html:frame> - action, anchor, bundle, forward, frameborder, frameName, href, longdesc, marginheight, marginwidth, module, name, noresize, page, paramId, paramName, paramProperty, paramScope, property, scope, scrolling, (cssattrs), title, titleKey, transaction
  <html:rewrite> - action, anchor, forward, href, module, name, page, paramId, paramName, paramProperty, paramScope, transaction, useLocalEncoding
  <html:param> - name, value

Other HTML Tags
---------------
* <html:base> - ref, server, target
* <html:form> - acceptCharset, action, disabled, enctype, focus, focusIndex, method, onreset, onsubmit, readonly, scriptLanguage, (cssattrs), target
* <html:html> - lang, xhtml
  <html:javascript> - bundle, cdata, dynamicJavascript, formName, htmlComment, method, page, scriptLanguage, src, staticJavascript
  <html:xhtml> - 

HtmlTagBase (@cssattrs)
 - HtmlActionTagBase (action, module, page, paramId, paramName, paramProperty, paramScope)
   - HtmlImgTag (align, @fieldattrs, @jsattrs(-change,focus,blur), border, height, hspace, imageName, ismap, pageKey, src, srcKey, useLocalEncoding, usermap, vspace, width)
   - HtmlLinkTagBase (anchor, forward, href, transaction)
     - HtmlLinkTag (accesskey, bundle, indexId, linkName, @jsattrs, tabindex, target, title, titleKey, useLocalEncoding)
     - HtmlFrameTag (bundle, frameborder, frameName, longdesc, marginheight, marginwidth, noresize, scrolling, title, titleKey)
     - HtmlRewriteTag (useLocalEncoding)
 - HtmlFormTag (acceptCharset, action, disabled, enctype, focus, focusIndex, method, onreset, onsubmit, readonly, scriptLanguage, target)
 - HtmlFormFieldTagBase (value, @errattrs, @fieldattrs)

Message tags
------------
* <html:errors> - bundle, footer, header, locale, name, prefix, property, suffix
* <html:messages> - bundle, footer, header, id, locale, message, name, property


NOTES:
 - jsattrs are: onblur, onchange, onclick, ondblclick, onfocus, onkeydown,
   onkeypress, onkeyup, onmousedown, onmousemove, onmouseout, onmouseover,
   onmouseup
 - cssattrs are: style, styleClass, styleId
 - errattrs are: errorKey, errorStyle, errorStyleClass, errorStyleId
 - fieldattrs is: alt, altKey, bundle, disabled, (jsattrs), (cssattrs),
   tabindex, title, titleKey


OTHER:
*- need common base for HtmlErrorsTag and HtmlMessagesTag
*- make HtmlInputTag wrapper around other tag types
*- add fieldattrs alt, altKey, bundle, disabled, indexed, tabindex, title,
   titlekey
 - Build html/action/link base classes
*- Do html:multibox tag
 - Do html:param tag (based on php:param?)

