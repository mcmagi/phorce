<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\html;

use \DOMDocument;
use phorce\common\property\PropertyUtils;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\taglib\nested\NestedTagBase;

/**
 * <p>This tag is used to iterate through an array to generate a list of HTML
 * <code>option</code> elements.  It may be used in two modes:</p>
 *
 * <p>Without the <code>collection</code> attribute specified, the
 * <code>name</code> attribute must either refernce an array of values from
 * which to generate the options, or an array objects whose value can be derived
 * using the <code>property</code> attribute.  In the same fashion,
 * <code>labelName</code> and <code>labelProperty</code> can be used to provide
 * a list of labels for each value.  Note the label/value pairs must line up
 * exactly in both arrays.  If <code>labelName</code> and
 * <code>labelProperty</code> are not specified, then the value will be used as
 * the label.</p>
 *
 * <p>If the <code>collection</code> attribute is specified, then it will
 * represent an array of objects from which to derive BOTH label and value.
 * Note that this does not represent the name of the array, but the actual
 * array which means the array must be provided using the runtime expression
 * tags, &lt;?= ?&gt;.  Use the <code>property</code> and
 * <code>labelProperty</code> attributes to derive value and label,
 * respectively, from each object in the collection.  If
 * <code>labelProperty</code> is not specified, then the value will be used as
 * the label.  If <code>labelName</code> is not specified, then each object in
 * the collection will be used as both label and value.</p>
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage HTML-TagLib
 */
class HtmlOptionsTag extends NestedTagBase
{
    /**
     * The name of the object from which this option's label collection is
     * derived.  If not specified, follows nesting rules to find a "root" tag.
     * @var string
     * @access private
     */
    private $_labelName;

    /**
     * The property of the named object from the <code>labelName</code>
     * attribute (or nested object) which exposes an array of labels to use for
     * each option.  Note the array of values (referenced by the
     * <code>name</code> and <code>property</code> attributes) and this array
     * of labels must line up exactly for each option.  If
     * <code>collection</code> is specified, then this refers to a property of
     * objects in the array.
     * @var string
     * @access private
     */
    private $_labelProperty;

    /**
     * An array of values to use as the list of options.  If specified, then
     * <code>name</code> and/or <code>property</code> cannot be specified.  If
     * this refers to an array of objects, then use <code>property</code> and
     * <code>labelProperty</code> to retrieve the value and label, respectively,
     * from each object.
     * @var array
     * @access private
     */
    private $_collection;

    /**
     * Whether to escape special HTML characters in the output.  Default is
     * true.
     * @var boolean
     * @access private
     */
    private $_filter = true;

    /**
     * An optional CSS style to apply to the rendered element via a
     * <code>style</code> attribute.
     * @var string
     * @access private
     */
    private $_style;

    /**
     * An optional CSS style to apply to the rendered element via a
     * <code>class</code> attribute.
     * @var string
     * @access private
     */
    private $_styleClass;


    /**
     * Returns the array of values to use as the list of options.
     * @return array Values
     */
    public function getCollection()
    {
        return $this->_collection;
    }

    /**
     * Sets the array of values to use as the list of options.
     * @param array $v Values
     */
    public function setCollection($v)
    {
        $this->_collection = $v;
    }

    /**
     * Returns the name of an object from which the label array is derived.
     * @return string Object name
     */
    public function getLabelName()
    {
        return $this->_labelName;
    }

    /**
     * Sets the name of an object from which the label array is derived.
     * @param string $v Object name
     */
    public function setLabelName($v)
    {
        $this->_labelName = $v;
    }

    /**
     * Returns the property to retrieve each label from the
     * <code>labelName<code> object.
     * @return string Property
     */
    public function getLabelProperty()
    {
        return $this->_labelProperty;
    }

    /**
     * Sets the property to retrieve each label from the <code>labelName<code>
     * object.
     * @param string $v Property
     */
    public function setLabelProperty($v)
    {
        $this->_labelProperty = $v;
    }

    /**
     * Returns whether to escape special HTML characters in the output.
     * @return boolean Filter
     */
    public function getFilter()
    {
        return $this->_filter;
    }

    /**
     * Sets whether to escape special HTML characters in the output.
     * @return string $v Filter
     */
    public function setFilter($v)
    {
        $this->_filter = ($v !== "false");
    }

    /**
     * Returns the CSS <code>style</code> attribute.
     * @return string CSS Style
     */
    public function getStyle()
    {
        return $this->_style;
    }

    /**
     * Sets the CSS <code>style</code> attribute.
     * @param string $v CSS Style
     */
    public function setStyle($v)
    {
        $this->_style = $v;
    }

    /**
     * Returns the CSS <code>class</code> attribute.
     * @return string CSS Class
     */
    public function getStyleClass()
    {
        return $this->_styleClass;
    }

    /**
     * Sets the CSS <code>class</code> attribute.
     * @param string $v CSS Class
     */
    public function setStyleClass($v)
    {
        $this->_styleClass = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        // get select tag
        $selectTag = $this->findAncestorWithClass('phorce\reaction\taglib\html\HtmlSelectTag');

        // get list to iterate through
        if (! is_null($this->_collection))
        {
            // iterate through specified collection
            $propValue =& $this->_pageContext->getAttribute($this->_collection);

            // loop through objects in array
            foreach ($propValue as $key => $obj)
            {
                // get label/value properties from object
                $value = PropertyUtils::getProperty($obj, $this->getProperty());
                $label = PropertyUtils::getProperty($obj, $this->_labelProperty);

                $this->_printOptionTag($selectTag, $label, $value);
            }
        }
        else
        {
            // iterate through collection returned from name/property
            $propValue =& $this->_getNestedPropertyValue();

            // get labelName/labelProperty
            if (! is_null($this->_labelName) || ! is_null($this->_labelProperty))
                $labelValue =& $this->_getNestedPropertyValue($this->_labelName,
                                $this->_labelProperty);

            // loop through objects in array
            foreach ($propValue as $key => $value)
            {
                if (isset($labelValue))
                    // use label array element as label
                    $label = $labelValue[$key];
                else
                    // use value as label
                    $label = $value;

                $this->_printOptionTag($selectTag, $label, $value);
            }
        }

        return TagHandler::SKIP_BODY;
    }

    protected function _printOptionTag(HtmlSelectTag $selectTag, $label, $value)
    {
        // create option element
        $doc = new DOMDocument();
        $option = $doc->createElement("option");
        $doc->appendChild($option);

        $option->setAttribute("value", $value);

        // CSS style attributes
        if (! is_null($this->_style))
            $option->setAttribute("style", $this->_style);
        if (! is_null($this->_styleClass))
            $option->setAttribute("class", $this->_styleClass);

        // add label
        $text = $doc->createTextNode($label);
        $option->appendChild($text);

        // is this option selected
        if ($selectTag->isSelected($value))
            $option->setAttribute("selected", "selected");

        // print option element
        echo $doc->saveHTML();
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }
}

?>
