<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

use phorce\common\ApplicationContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\action\ReactionController;
use phorce\reaction\taglib\ReactionTagException;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
class LogicForwardTag extends TagHandler
{
	private $_name;

    /**
     * @var object ActionForward
     * @access private
     */
    private $_forward;


	public function getName()
	{
		return $this->_name;
	}

	public function setName($v)
	{
		$this->_name = $v;
	}

	/**
	 * @see TagHandler::doStartTag()
	 */
	public function doStartTag()
	{
		if (! is_null($this->_name))
		{
			$controller = ReactionController::getInstance();
            $this->_forward = $controller->config->findGlobalForward($this->_name);

            if (is_null($this->_forward))
                throw new ReactionTagException($this, "No global forward with name '{$this->_name}' found in reaction config");

            $ctx = ApplicationContext::getInstance();
            $dispatcher = $ctx->getRequestDispatcher($forward->path);
            if ($this->_forward->redirect)
                $dispatcher->forwardPath();
            else
                $dispatcher->includePath();
		}

		return TagHandler::SKIP_BODY;
	}

	/**
	 * @see TagHandler::doEndTag()
	 */
	public function doEndTag()
	{
        return $this->_forward->redirect ?
            TagHandler::SKIP_PAGE : parent::doEndTag();
	}
}

?>
