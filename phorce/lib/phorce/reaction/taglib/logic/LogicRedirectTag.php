<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

use phorce\common\web\ApplicationContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\action\ReactionController;
use phorce\reaction\taglib\ReactionTagException;
use \Net_URL2;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
class LogicRedirectTag extends TagHandler
{
	private $_href;
	private $_page;
	private $_forward;
	private $_action;
	private $_anchor;
	private $_paramId;
	private $_paramName;
	private $_paramProperty;
	private $_paramScope;
	private $_name;
	private $_property;
	private $_scope;
	private $_transaction;

	private $_url;


	public function getHref()
	{
		return $this->_href;
	}

	public function setHref($v)
	{
		$this->_href = $v;
	}

	public function getPage()
	{
		return $this->_page;
	}

	public function setPage($v)
	{
		$this->_page = $v;
	}

	public function getForward()
	{
		return $this->_forward;
	}

	public function setForward($v)
	{
		$this->_forward = $v;
	}

	public function getAction()
	{
		return $this->_action;
	}

	public function setAction($v)
	{
		$this->_action = $v;
	}

	public function getAnchor()
	{
		return $this->_anchor;
	}

	public function setAnchor($v)
	{
		$this->_anchor = $v;
	}

	public function getParamId()
	{
		return $this->_paramId;
	}

	public function setParamId($v)
	{
		$this->_paramId = $v;
	}

	public function getParamName()
	{
		return $this->_paramName;
	}

	public function setParamName($v)
	{
		$this->_paramName = $v;
	}

	public function getParamProperty()
	{
		return $this->_paramProperty;
	}

	public function setParamProperty($v)
	{
		$this->_paramProperty = $v;
	}

	public function getParamScope()
	{
		return $this->_paramScope;
	}

	public function setParamScope($v)
	{
		$this->_paramScope = $v;
	}

	public function getScope()
	{
		return $this->_scope;
	}

	public function setScope($v)
	{
		$this->_scope = $v;
	}

	public function getTransaction()
	{
		return $this->_transaction;
	}

	public function setTransaction($v)
	{
		$this->_transaction = $v;
	}

    /**
     * @see TagHandler::doStartTag()
     */
	public function doStartTag()
	{
		if (! is_null($this->_href))
			// absolute url is provided
			$this->_url = new Net_URL2($this->_href);
		elseif (! is_null($this->_page))
		{
			// relative url (a page) is provided
			$this->_url = new Net_URL2("");
			$this->_url->setScheme("http");
			$this->_url->setHost($_SERVER['HTTP_HOST']);

			// figure current directory and prepend to requested page
			$path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
			$this->_url->setPath($path . "/" . $this->_page);
		}
		elseif (! is_null($this->_forward))
		{
			// an action forward is provided
			$this->_url = new Net_URL2("");
			$this->_url->setScheme("http");
			$this->_url->setHost($_SERVER['HTTP_HOST']);

			// look up global action forward in config
			$controller = ReactionController::getInstance();
			$forward = $controller->findGlobalForward($this->_forward);
            if (is_null($forward))
                throw new ReactionTagException($this, "Cannot find global forward with name '{$this->_forward}' in reaction config");

			$this->_url->setPath($forward->path);
		}
		elseif (! is_null($this->_action))
		{
			// an action forward is provided
			$this->_url = new Net_URL2("");
			$this->_url->setScheme("http");
			$this->_url->setHost($_SERVER['HTTP_HOST']);

			// look up action mapping in config
			$controller = ReactionController::getInstance();
			$action = $controller->findActionConfig($this->_action);
            if (is_null($action))
                throw new ReactionTagException($this, "Cannot find action with path '{$this->_action}' in reaction config");

            $ctx = ApplicationContext::getInstance();
			$this->_url->setPath($ctx->getContextRoot() . $action->path . ".do");
		}

		if (! is_null($this->_anchor))
			$this->_url->setFragment($this->_anchor);

		// get parameter map
		if (! is_null($this->_name))
		{
			$array =& $this->_getPropertyValue($this->_name, $this->_property,
				$this->_scope);

			// we expect an associative array for query parameters
			if (is_array($array) || $array instanceof Traversable)
			{
				foreach ($array as $k => $v)
					$this->_url->setQueryParameter($k, $v);
			}
			else
			{
                throw new ReactionTagException($this,
                    "Value retrieved from name/property is not an array");
			}
		}

		// get a single parameter
		if (! is_null($this->_paramId))
		{
			$paramValue =& $this->_getPropertyValue($this->_paramName,
				$this->_paramProperty, $this->_paramScope);

			// we expect a parameter that can be converted to a string
			if (! is_object($paramValue) && ! is_array($paramValue))
				$this->_url->setQueryParameter($this->_paramId, $paramValue);
			else
			{
                throw new ReactionTagException($this,
                    "Value retrieved from paramName/paramProperty is an array");
			}
		}

		if ($this->_transaction === "true" && strlen(SID))
		{
			// send SID if transaction is set
			$params = explode("=", SID);
			$this->_url->setQueryParameter($params[0], $params[1]);
		}

		return TagHandler::EVAL_BODY_INCLUDE;
	}

    /**
     * @see TagHandler::doEndTag()
     */
	public function doEndTag()
	{
        $ctx = ApplicationContext::getInstance();
        $dispatcher = $ctx->getRequestDispatcher($this->_url->getURL());
        $dispatcher->redirectPath();

		return TagHandler::SKIP_PAGE;
	}
}

?>
