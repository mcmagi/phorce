<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

use phorce\reaction\taglib\ReactionTagException;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
class LogicMatchTag extends LogicCompareTagBase
{
    /**
     * Value of 'location' property of tag if we want to match at the start of
     * the string.
     */
    const MATCH_LOCATION_START = 'start';

    /**
     * Value of 'location' property of tag if we want to match at the end of
     * the string.
     */
    const MATCH_LOCATION_END = 'end';


	private $_location;

	public function getLocation()
	{
		return $this->_location;
	}

	public function setLocation($v)
	{
		$this->_location = (boolean) $v;
	}

	/**
	 * Compares if one string is a substring of another.  Type casts arguments
	 * into strings before calling _compare().
	 * @param mixed &$v1 Source string (the haystack)
	 * @param mixed &$v2 Search string (the needle)
	 * @return bool Boolean
     * @access protected
	 */
	protected function _doCompare(&$v1, &$v2)
	{
		$this->_compare((string) $v1, (string) $v2);
	}

	/**
	 * Compares if one string is a substring of another.
	 * @param string $v1 Source string (the haystack)
	 * @param string $v2 Search string (the needle)
	 * @return bool Boolean
     * @access protected
	 */
	protected function _compare($v1, $v2)
	{
		// strstr: returns substring from start of v2 in v1
		$substr = strstr($v1, $v2);

		$c = false;
		if (strlen($substr))
		{
			if (is_null($this->_location))
				// no location - any match is good
				$c = true;
			elseif ($this->_location == self::MATCH_LOCATION_START && $substr == $v1)
				// start location - subtring should match full string
				$c = true;
			elseif ($this->_location == self::MATCH_LOCATION_END && $substr == $v2)
				// end location - substring should match compare string
				$c = true;
			else
                throw new ReactionTagException($this, "Invalid location '{$this->_location}'");
		}

		return $c;
	}
}

?>
