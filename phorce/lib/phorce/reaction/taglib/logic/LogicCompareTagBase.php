<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

/**
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
abstract class LogicCompareTagBase extends LogicTagBase
{
	private $_cookie;
	private $_header;
	private $_parameter;
	private $_value;

	public function getCookie()
	{
		return $this->_cookie;
	}

	public function setCookie($v)
	{
		$this->_cookie = $v;
	}

	public function getHeader()
	{
		return $this->_header;
	}

	public function setHeader($v)
	{
		$this->_header = $v;
	}

	public function getParameter()
	{
		return $this->_parameter;
	}

	public function setParameter($v)
	{
		$this->_parameter = $v;
	}

	public function getValue()
	{
		return $this->_value;
	}

	public function setValue($v)
	{
		$this->_value = $v;
	}

	/**
	 * @see LogicTagBase::_doLogic()
	 */
	protected function _doLogic()
	{
		if (! is_null($this->_parameter))
		{
			if (array_key_exists($this->_parameter, $_REQUEST))
				$value = $_REQUEST[$this->_parameter];
		}
		elseif (! is_null($this->_cookie))
		{
			if (array_key_exists($this->_cookie, $_COOKIE))
				$value = $_COOKIE[$this->_cookie];
		}
		elseif (! is_null($this->_header))
		{
			$headers = apache_request_headers();
			if (array_key_exists($this->_cookie, $headers))
				$value = $headers[$this->_cookie];
		}
		else
			$value =& $this->_getNestedPropertyValue();

		// if comparison returns true, evalute body; otherwise skip body
		return $this->_doCompare($value, $this->_value);
	}

	/**
	 * Wrapper around _compare().  Attempts to perform type conversion on
	 * simple types before the call to _compare() by casting $v2 to the same
	 * type as $v1.
	 * @access protected
	 * @param mixed $v1
	 * @param mixed $v2
	 * @return bool Comparison
	 */
	protected function _doCompare(&$v1, &$v2)
	{
		$rc = null;

		// convert value constant to type returned from property
		if (is_integer($v1))
			$rc = $this->_compare($v1, (integer) $v2);
		else if (is_string($v1))
			$rc = $this->_compare($v1, (string) $v2);
		else if (is_bool($v1))
			$rc = $this->_compare($v1, $v2 === "true" ? true : false);
		else if (is_float($v1))
			$rc = $this->_compare($v1, (float) $v2);
		else
			// do no type conversion
			$rc = $this->_compare($v1, $v2);

		return $rc;
	}

	/**
	 * Compares two values using the operation the Compare Tag subclass
	 * represents.  Must be implemeneted by subclasses.  This implementation
	 * is "abstract" and always returns false.
	 * @abstract
	 * @access protected
	 * @param mixed $v1
	 * @param mixed $v2
	 * @return bool Comparison
	 */
	protected abstract function _compare($v1, $v2);
}

?>
