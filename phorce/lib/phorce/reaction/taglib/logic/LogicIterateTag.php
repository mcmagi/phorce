<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

use \ArrayObject;
use \Traversable;
use phorce\inertia\phpx\PageContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\taglib\ReactionTagException;
use phorce\reaction\taglib\nested\NestedNestTagSupport;
use phorce\reaction\taglib\nested\NestedTagBase;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
class LogicIterateTag extends NestedTagBase implements NestedNestTagSupport
{
    /**
     * Contains an iterator through the array.
     * @var object ArrayIterator
     * @access private
     */
    private $_iterator;

    /**
     * Caches the current value being iterated.
     * @var mixed
     * @access private
     */
    private $_currentValue;

    /**
     * Caches the array index (key) of the current value being iterated.
     * @var object ArrayIterator
     * @access private
     */
    private $_currentKey;

    /**
     * Contains the sequential ordering number within the array of the current
     * value.
     * @var integer
     * @access private
     */
    private $_currentCount = -1;

    private $_id;
    private $_indexId;
    private $_collection;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($v)
    {
        $this->_id = $v;
    }

    public function getIndexId()
    {
        return $this->_indexId;
    }

    public function setIndexId($v)
    {
        $this->_indexId = $v;
    }

    public function getCollection()
    {
        return $this->_collection;
    }

    public function setCollection($v)
    {
        $this->_collection = $v;
    }

    /**
     * @see TagHandler::doStartTag()
     */
    public function doStartTag()
    {
        if (! is_null($this->_collection))
        {
            // use specified collection
            if (is_array($this->_collection))
                $obj = new ArrayObject($this->_collection);
            elseif ($this->_collection instanceof Traversable)
                $obj = new ArrayObject($this->_collection);
            else
                throw new ReactionTagException($this, "Specified collection is not an array or a Traversable object");
        }
        else
        {
            // use nested property value
            $array =& $this->_getNestedPropertyValue();

            if (is_array($array))
                $obj = new ArrayObject($array);
            elseif ($array instanceof Traversable)
                $obj = new ArrayObject($array);
            else
            {
                throw new ReactionTagException($this, "Property '" . $this->_getNestProperty() .
                    "' on object '" . $this->_getNestName() . "is not an array or a Traversable object");
            }
        }

        // create new iterator
        $this->_iterator = $obj->getIterator();

        // begin the iteration
        if ($this->_iterator->valid())
        {
            $this->_doIterate();
            $rc = TagHandler::EVAL_BODY_INCLUDE;
        }
        else
            $rc = TagHandler::SKIP_BODY;

        return $rc;
    }

    /**
     * @see TagHandler::doAfterBody()
     */
    public function doAfterBody()
    {
        // iterate
        if ($this->_iterator->valid())
        {
            $this->_doIterate();
            $rc = TagHandler::EVAL_BODY_AGAIN;
        }
        else
            $rc = TagHandler::SKIP_BODY;

        return $rc;
    }

    /**
     * @see TagHandler::doEndTag()
     */
    public function doEndTag()
    {
        return TagHandler::EVAL_PAGE;
    }

    /**
     * Performs a single iteration of a loop.  The iterator must be valid before
     * this is called.
     * @access protected
     */
    protected function _doIterate()
    {
        // cache info from current iteration
        $this->_currentCount++;
        $this->_currentValue = $this->_iterator->current();
        $this->_currentKey = $this->_iterator->key();

        // set id
        if (! is_null($this->_id))
            $this->_pageContext->setAttribute($this->_id,
                $this->_currentValue, PageContext::SCOPE_PAGE);

        // set index id
        if (! is_null($this->_indexId))
            $this->_pageContext->setAttribute($this->_indexId,
                $this->_currentKey, PageContext::SCOPE_PAGE);

        // get next element
        $this->_iterator->next();
    }

    /**
     * Returns the property this object contributes to the property path.
     * The logic:iterate tag returns the property name followed by the index
     * in parentheses.  We would use brackets, but they don't translate properly
     * to PHP when the form is submitted.
     * @return string Nested property
     */
    public function getNestedProperty()
    {
        return $this->getProperty() . "(" . $this->_currentKey . ")";
    }
}
