<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

use phorce\reaction\Globals;
use phorce\reaction\action\ActionMessages;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
class LogicMessagesPresentTag extends LogicTagBase
{
    private $_message;

    public function getMessage()
    {
        return $this->_message;
    }

    public function setMessage($v)
    {
        $this->_message = $v;
    }

    protected function _doLogic()
    {
        $key = null;
        $value = false;

        // determine lookup key (if any)
        if ($this->_message === "true")
            $key = Globals::MESSAGE_KEY;
        elseif (! is_null($this->name))
            $key = $this->name;
        else
            $key = Globals::ERROR_KEY;

        // get messages in any scope
        $msgObj =& $this->_getPropertyValue($key);

        if (! is_null($msgObj))
        {
            if ($msgObj instanceof ActionMessages)
            {
                if (is_null($this->property))
                    $messages =& $msgObj->get();
                else
                    $messages =& $msgObj->get($this->property);

                // must not be null and not be empty to have messages
                $value = (! is_null($messages) && count($messages) > 0);
            }
            else
            {
                // if not action messages, look for string messages
                $messages =& $this->_getPropertyValue($key, $this->property);

                // we found a string or array (of hopefully strings)!
                $value = (is_string($messages) || is_array($messages));
            }
        }
        else
            $value = false;

        return $value;
    }
}

?>
