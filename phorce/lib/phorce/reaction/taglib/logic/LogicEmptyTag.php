<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

use \Countable;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
class LogicEmptyTag extends LogicTagBase
{
	/**
	 * @see LogicTagBase::_doLogic()
	 */
	protected function _doLogic()
	{
		// if comparison returns true, evalute body; otherwise skip body
		return $this->_isEmpty($this->_getNestedPropertyValue());
	}

	private function _isEmpty(&$object)
	{
        if ($object instanceof Countable)
            return count($object) == 0;
		return empty($object);
	}
}

?>
