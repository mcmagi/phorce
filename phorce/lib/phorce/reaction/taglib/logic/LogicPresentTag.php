<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
class LogicPresentTag extends LogicTagBase
{
	private $_cookie;
	private $_header;
	private $_parameter;

	public function getCookie()
	{
		return $this->_cookie;
	}

	public function setCookie($v)
	{
		$this->_cookie = $v;
	}

	public function getHeader()
	{
		return $this->_header;
	}

	public function setHeader($v)
	{
		$this->_header = $v;
	}

	public function getParameter()
	{
		return $this->_parameter;
	}

	public function setParameter($v)
	{
		$this->_parameter = $v;
	}

	public function _doLogic()
	{
		$val = false;

		if (! is_null($this->_cookie))
			// true if cookie exists
			$val = array_key_exists($this->_cookie, $_COOKIE);
		elseif (! is_null($this->_header))
			// true if header exists
			$val = array_key_exists($this->_header, apache_request_headers());
		elseif (! is_null($this->_parameter))
			// true if request parameter exists
			$val = array_key_exists($this->_parameter, $_REQUEST);
		elseif (is_null($this->property))
		{
			// true if named object exists (in specified scope)
			if (is_null($this->scope))
				$val = ! is_null($this->_pageContext->findAttribute($this->name);
			else
				$val = ! is_null($this->_pageContext->getAttribute($this->name, $this->scope);
		}
		else
			// true if property value is not null
			$val = ! is_null($this->_getNestedPropertyValue());

		return $val;
	}
}

?>
