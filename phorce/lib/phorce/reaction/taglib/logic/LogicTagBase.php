<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\taglib\logic;

use phorce\inertia\tagext\TagHandler;
use phorce\reaction\taglib\nested\NestedTagBase;

/**
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Logic-TagLib
 */
abstract class LogicTagBase extends NestedTagBase
{
	public function doStartTag()
	{
		// if logic returns true, evalute body; otherwise skip body
		return $this->_doLogic() ?
			TagHandler::EVAL_BODY_INCLUDE : TagHandler::SKIP_BODY;
	}

	public function doEndTag()
	{
		return TagHandler::EVAL_PAGE;
	}

	/**
	 * Performs the logic specified the subclass represents and returns true
	 * if the body is to be evaluated; false if it is to be skipped.
	 * @return Boolean
     * @access protected
     * @abstract
	 */
	protected abstract function _doLogic();
}

?>
