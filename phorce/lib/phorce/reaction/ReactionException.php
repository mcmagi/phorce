<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction;

use \Exception;
use phorce\PhorceException;

/**
 * This is an exception which represents an unexpected situation within
 * Reaction.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 */
class ReactionException extends PhorceException
{
    /**
     * Constructs the exception.
     * @param string $msg
     * @param object Exception $ex
     */
    public function __construct($msg, Exception $ex = null)
    {
        parent::__construct($msg, 0, $ex);
    }
}

?>
