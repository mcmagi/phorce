<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction;

/**
 * Contains common constants for use by the Phorce Reaction framework.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @abstract
 */
abstract class Globals
{
    /**
     * Where our controller is stored in the request.
     */
    const CONTROLLER_KEY = 'phorce.reaction.controller';

    /**
     * The key of the request to where the ActionMessages object is stored.
     */
    const MESSAGE_KEY = 'phorce.reaction.message';

    /**
     * The key of the request to where the ActionErrors object is stored.
     */
    const ERROR_KEY = 'phorce.reaction.error';

    /**
     * The key of the request to where the default MessageResources are stored.
     */
    const MESSAGES_KEY = 'MESSAGE';

    /**
     * The key of the request to where the action mapping is stored.
     */
    const MAPPING_KEY = 'phorce.reaction.mapping';

    /**
     * The request attribute set if the action is cancelled.
     */
    const CANCEL_KEY = 'phorce.reaction.cancel';
}

?>
