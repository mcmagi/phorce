<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\tiles;

use phorce\common\cache\PhorceConfigCache;
use phorce\common\cache\FusionDAOConfigFileLoader;
use phorce\reaction\ReactionException;

class TilesConfigLoader
{
    /**
     * The name of the configuration file.
     */
    const CONFIG_FILE = "tiles-config.xml";

    /**
     * Singleton instance.
     * @var object TilesConfigLoader
     * @access private
     * @static
     */
    private static $_instance;

    /**
     * Contains all loaded tiles definitions.
     * @var array Array of DefinitionConfig objects
     * @access private
     */
    private $_defs = array();

    /**
     * Returns the singleton instance.
     * @return object TilesConfigLoader
     * @static
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new TilesConfigLoader();

        return self::$_instance;
    }

    /**
     * Constructs a TilesConfigLoader.  All XML data is loaded from the config
     * file.
     * @access private
     */
    private function __construct()
    {
        $configFile = \phorce\makepath(PHORCE_CONFIG, self::CONFIG_FILE);
        $config = PhorceConfigCache::retrieveConfig(
            "tiles-config", new FusionDAOConfigFileLoader(
                $configFile, 'phorce\reaction\tiles\config\TilesConfigDAO'));

        /* $dao = new TilesConfigDAO(PHORCE_CONFIG . "/" . self::CONFIG_FILE);
        $config = $dao->load(); */

        // build list of definitions
        foreach ($config->definitions as $def)
            $this->_defs[$def->name] = $def;
    }

    /**
     * Returns true if the definition exists.
     * @param string $defName Tiles definition name
     * @return boolean
     */
    public function hasDefinition($defName)
    {
        return array_key_exists($defName, $this->_defs);
    }

    /**
     * Looks up the template path for the specified tiles definition name or
     * null if not found.
     * @param string $defName Tiles definition name
     * @return null|string Template path
     */
    public function getTemplatePath($defName)
    {
        $path = null;

        if ($this->hasDefinition($defName))
        {
            $def = $this->_defs[$defName];

            // if there is no path, check parent definition(s)
            while (is_null($def->path))
            {
                $def = $this->_getParentDefinition($def);

                if (is_null($def))
                    throw new ReactionException(
                        "Tiles definition '{$defName}' has no path");
            }

            $path = $def->path;
        }

        return $path;
    }

    /**
     * Looks up the 'put' configuration for the specified tiles definition name
     * and put name or null if not found.
     * @param string $defName Tiles definition name
     * @param string $putName Tiles 'put' name
     * @return null|object PutConfig
     */
    public function getPutConfig($defName, $putName)
    {
        //echo "looking up putConfig for '$defName', '$putName'<br>\n";

        $put = null;

        if ($this->hasDefinition($defName))
        {
            $def = $this->_defs[$defName];

            do
            {
                foreach ($def->puts as $p)
                {
                    if ($p->name == $putName)
                    {
                        $put = $p;
                        break;
                    }
                }

                // if there is no put, check parent definition
                if (is_null($put))
                    $def = $this->_getParentDefinition($def);

                if (is_null($def))
                    throw new ReactionException(
                        "Tiles definition '{$defName}' has no 'put' with name '{$putName}'");
            }
            while (is_null($put));
        }

        return $put;
    }

    /**
     * Returns the base tiles definition of the specified tiles definition.
     * Throws an exception if it cannot be found.
     * @param object DefinitionConfig $def
     * @return object DefinitionConfig
     * @access private
     */
    private function _getParentDefinition($def)
    {
        if (is_null($def->extends))
            return null;
        elseif (! $this->hasDefinition($def->extends))
            throw new ReactionException(
                "Tiles definition '{$def->name}' extends non-existant definition '{$def->extends}'");

        // lookup parent definition
        return $this->_defs[$def->extends];
    }
}

?>
