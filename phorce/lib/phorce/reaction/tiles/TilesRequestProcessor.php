<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\tiles;

use phorce\reaction\action\RequestProcessor;

class TilesRequestProcessor extends RequestProcessor
{
    const TILES_DEF_PARAM = "phorce.reaction.tiles.tilesdef";

    protected function doInclude($uri)
    {
        parent::doInclude($this->_processTiles($uri));
    }

    protected function doForward($uri)
    {
        parent::doForward($this->_processTiles($uri));
    }

    private function _processTiles($def)
    {
        $path = $def;

        $loader = TilesConfigLoader::getInstance();

        // check if there is a tiles definition with the specified name
        if ($loader->hasDefinition($def))
        {
            // TODO: this works for now, but will not work properly
            // with nested tiles
            $_REQUEST[self::TILES_DEF_PARAM] = $def;

            $path = $loader->getTemplatePath($def);
        }

        //echo "TilesRequestProcessor translating forward for '$def' => '$path'\n";

        return $path;
    }
}
