<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\tiles\taglib;

use phorce\common\web\ApplicationContext;
use phorce\inertia\tagext\TagHandler;
use phorce\reaction\ReactionException;
use phorce\reaction\tiles\TilesConfigLoader;
use phorce\reaction\tiles\TilesRequestProcessor;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Tiles-TagLib
 */
class TilesInsertTag extends TagHandler
{
	private $_attribute;

	public function getAttribute()
	{
		return $this->_attribute;
	}

	public function setAttribute($v)
	{
		$this->_attribute = $v;
	}

    /**
     * @see TagHandler::doStartTag()
     */
	public function doStartTag()
	{
        // TODO: may need to change; see TilesRequestProcessor
        if (! array_key_exists(TilesRequestProcessor::TILES_DEF_PARAM, $_REQUEST))
            throw new ReactionException("No tiles definition was processed.  Inset tag cannot include content.");
        $def = $_REQUEST[TilesRequestProcessor::TILES_DEF_PARAM];

        $loader = TilesConfigLoader::getInstance();
        $put = $loader->getPutConfig($def, $this->_attribute);

        if ($put->type == "string")
            echo $put->value;
        elseif ($put->type == "page")
        {
            // load a request dispatcher
            $ctx = ApplicationContext::getInstance();
            $d = $ctx->getRequestDispatcher($put->value);
            $d->includePath();
        }
        else
            throw new ReactionException("Unknown type '{$put->type}' for definition '$def', attribute {$this->attribute}'");

		return TagHandler::SKIP_BODY;
	}

    /**
     * @see TagHandler::doEndTag()
     */
	public function doEndTag()
	{
		return TagHandler::EVAL_PAGE;
	}
}

?>
