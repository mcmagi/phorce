<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\request;

use phorce\common\request\IncludeRequestDispatcher;
use phorce\common\request\RequestDispatcher;
use phorce\common\web\SessionManager;
use phorce\reaction\action\ReactionController;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Request
 */
class ReactionRequestDispatcher implements RequestDispatcher
{
    /**
     * The path to the action to include.
     * @var string
     * @access private
     */
    private $_path;

    /**
     * Creates a dispatcher for the specified request path.
     * @param string $path Request path
     */
    public function __construct($path)
    {
        $this->_path = $path;
    }

    public function includePath()
    {
        try
        {
            $this->_handleRequest();
        }
        catch (Exception $e)
        {
            // ignore error, just emit warning
            trigger_error($e->__toString(), E_USER_WARNING);
        }
    }

    public function requirePath()
    {
        $this->_handleRequest();
    }

    public function forwardPath()
    {
        $this->includePath();
        exit(0);
    }

    public function redirectPath()
    {
        $d = new IncludeRequestDispatcher($this->_path);
        $d->redirectPath();
    }

    /**
     * Returns true if this action was included previously.  Used to determine
     * the inclusion bahavior of <code>includePathOnce</code> and
     * <code>requirePathOnce</code>.
     * @return boolean True if included previously
     */
    private function _wasIncluded()
    {
        return array_key_exists($this->_path, self::$_included);
    }

    /**
     * Handles a request for an action.
     * @access private
     */
    private function _handleRequest()
    {
        // strip extension
        $dotidx = strrpos($this->_path, ".");
        $action = substr($this->_path, 0, $dotidx);

        // TODO: where should we REALLY start and end the session?
        SessionManager::startSession();
        //ob_end_flush(); - turns off any session output buffering (why???)
        $c = ReactionController::getInstance();
        $c->process("/" . $action); // TODO - fix it later
        SessionManager::endSession();
    }
}

?>
