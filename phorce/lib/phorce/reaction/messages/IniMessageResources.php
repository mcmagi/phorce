<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\messages;

/**
 * This is a MessageResources implementation backed with an ini file.  The
 * file is expected to reside at config/bundle.ini, where "bundle" is the name
 * of the bundle.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Messages
 */
class IniMessageResources extends MessageResources
{
	/**
	 * Creates MessageResources from an ini file for the specified bundle
	 * parameter.
	 * @param object MessageResourcesFactory $factory The invoking
     *                                                MessageResourcesFactory
	 * @param string $config The data source name of the bundle
	 */
	public function __construct(MessageResourcesFactory $factory, $config)
	{
		parent::__construct($factory, $config);

		// load the resource bundle
		$resourceFile = PHORCE_CONFIG . DIRECTORY_SEPARATOR . "$config.ini";
		if (file_exists($resourceFile))
			$this->_messageMap = parse_ini_file($resourceFile);
		else
		{
            // FIXME - exception?
			trigger_error("Cannot find resource bundle '$resourceFile'",
				E_USER_WARNING);

			// use an empty message array
			$this->_messageMap = array();
		}
	}
}

?>
