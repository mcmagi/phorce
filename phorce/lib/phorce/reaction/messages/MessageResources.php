<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\messages;

use phorce\common\property\PropertyObject;

/**
 * The MessageResources class provides a means to access the configured message
 * resources for an application.  Messages are looked up with a given key and
 * substituted with arguments as necessary.
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Messages
 */
abstract class MessageResources extends PropertyObject
{
	/**
	 * An associative array of messages, keyed by message keys.
	 * @var array
	 */
	protected $_messageMap = array();

	/**
	 * Whether to return "null" if there is no message key found on a call to
	 * getMessage().
	 */
	private $_returnNull = true;

	/**
	 * The data source name of this bundle.
	 */
	private $_config;

	/**
	 * Creates MessageResources for the specified bundle parameter.
	 * @param object MessageResourcesFactory $factory The invoking MessageResourcesFactory
	 * @param string $config The data source name of the bundle
	 */
	public function __construct(MessageResourcesFactory $factory, $config)
	{
		$this->_config = $config;
	}

	/**
	 * Factory method which returns the default MessageResources implementation
	 * for the specified bundle.
	 * @static
	 * @param string $config The data source name of the bundle
	 * @return object MessageResources Resource object
	 */
	public function getMessageResources($config)
	{
		$factory = MessageResourcesFactory::createFactory($config);
		return $factory->createResources($config);
	}

	/**
	 * Returns a message string keyed by the specified resource key in the
	 * message resources backend.  Argument values are substituted as necessary.
	 * @param string $key Message resource key
	 * @param array|string $arg0 Has two possible meanings: (optional)
	 *                 -As an array of substitution values
	 *                 -As Substitution value 0 (subsequent values would follow)
	 * @param string $arg1 Substitution value 1 (optional)
	 * @param string $arg2 Substitution value 2 (optional)
	 * @param string $arg3 Substitution value 3 (optional)
	 */
	public function getMessage($key, $arg0 = null, $arg1 = null, $arg2 = null,
		$arg3 = null)
	{
		if (is_array($arg0))
			// arg array was supplied
			$args = $arg0;
		else
		{
			// build arg array
			$args = array();
			if (! is_null($arg0))
				$args[] = $arg0;
			if (! is_null($arg1))
				$args[] = $arg1;
			if (! is_null($arg2))
				$args[] = $arg2;
			if (! is_null($arg3))
				$args[] = $arg3;
		}

		$message = null;

		if ($this->isPresent($key))
		{
			$message = $this->_messageMap[$key];

			// replace each argument in message
			foreach ($args as $i => $arg)
				$message = str_replace("{".$i."}", $arg, $message);
		}
		elseif (! $this->_returnNull)
			$message = "???$key???";
        else
            $message = "null";

		return $message;
	}

	/**
	 * Returns true if the specified message key exists.
	 * @param string $key Message key
	 * @return bool Boolean
	 */
	public function isPresent($key)
	{
		return array_key_exists($key, $this->_messageMap);
	}

	/**
	 * Returns whether null is an allowable return value if a specified key is
	 * not present.  If null is not allowed, a message of the form ???key???
	 * is returned.
	 * @return bool Boolean
	 */
	public function getReturnNull()
	{
		return $this->_returnNull;
	}

	/**
	 * Sets whether null is an allowable return value if a specified key is
	 * not present.  If null is not allowed, a message of the form ???key???
	 * is returned.
	 * @param bool $v Boolean
	 */
	public function setReturnNull($v)
	{
		$this->_returnNull = $v;
	}

	/**
	 * Returns the configured data source name (parameter) of this message
     * bundle.
	 * @return string The data source name (parameter) of the bundle
	 */
	public function getConfig()
	{
		return $this->_config;
	}

	/**
	 * Returns the factory instance which created this MessageResources object.
	 * @return object MessageResourcesFactory The factory which created these
     *                                        resources
	 */
	public function getFactory()
	{
		return $this->_factory;
	}
}

?>
