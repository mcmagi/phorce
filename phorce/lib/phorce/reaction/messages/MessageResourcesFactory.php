<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\messages;

use phorce\common\property\PropertyObject;

/**
 * This class is used to create MessageResources.  In general, one must call
 * createFactory(), configure the factory, then call createResources().
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Messages
 */
abstract class MessageResourcesFactory extends PropertyObject
{
	/**
	 * The MessageResourcesConfig which configured our creation.
	 * @var object MessageResourcesConfig
	 * @access private
	 */
	private $_config;

	/**
	 * Whether MessageResources created by this factory should return "null"
	 * if no message key was found.
	 * @var bool
	 * @access private
	 */
	private $_returnNull = true;


	/**
	 * Creates a message resources factory.
	 */
	public function __construct()
	{
	}

	/**
	 * Returns an instance of the default message resources factory.  The
	 * default in this case is an IniMessageResourcesFactory.
	 * @static
	 * @return object MessageResourcesFactory
	 */
	public static function createFactory()
	{
		return new IniMessageResourcesFactory();
	}

	/**
	 * Returns whether MessageResources created by this factory should return
	 * "null" if no message key was found.
	 * @return bool Boolean
	 */
	public function getReturnNull()
	{
		return $this->_returnNull;
	}

	/**
	 * Sets whether MessageResources created by this factory should return
	 * "null" if no message key was found.
	 * @param bool $v Boolean
	 */
	public function setReturnNull($v)
	{
		$this->_returnNull = $v;
	}

	/**
	 * Returns the MessageResourcesConfig which configured our creation.
	 * @return object MessageResourcesConfig
	 */
	public function getConfig()
	{
		return $this->_config;
	}

	/**
	 * Sets the MessageResourcesConfig which configured our creation.
	 * @param object MessageResourcesConfig $c
	 */
	public function setConfig(MessageResourcesConfig $c)
	{
		$this->_config = $c;
	}

	/**
	 * Creates a MessageResources object given the specified bundle parameter.
	 * @abstract
	 * @param string $config The data source name of the bundle
	 * @return object MessageResources
	 */
	public abstract function createResources($config);
}

?>
