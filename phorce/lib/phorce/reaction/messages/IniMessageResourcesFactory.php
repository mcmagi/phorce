<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\messages;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 * @subpackage Messages
 */
class IniMessageResourcesFactory extends MessageResourcesFactory
{
	/**
	 * Creates a MessageResourcesFactory for IniMessageResources.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Returns the MessageResources for a given bundle parameter.
	 * @param string $config The data source name of the bundle
	 * @return object IniMessageResources
	 */
	public function createResources($config)
	{
		$msgres = new IniMessageResources($this, $config);
		$msgres->returnNull = $this->returnNull;
		return $msgres;
	}
}

?>
