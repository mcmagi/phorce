<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\reaction\config;

use \ArrayObject;
use phorce\common\property\PropertyObject;

/**
 * Represents the complexType 'FormBeansConfig' in the XSD 'reaction-config.xsd'.
 * This class was generated by Fusion Generator v0.2.
 * @author Fusion Generator
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 */
class FormBeansConfig extends PropertyObject
{
    private $p_formBeans;
    
    public function __construct()
    {
        $this->p_formBeans = new ArrayObject();
    }
    
    public function setFormBeans(ArrayObject $v)
    {
        $this->p_formBeans = $v;
    }
}

?>
