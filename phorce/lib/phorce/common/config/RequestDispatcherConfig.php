<?php

namespace phorce\common\config;

use phorce\common\property\PropertyObject;

/**
 * Represents the complexType 'RequestDispatcherConfig' in the XSD 'phorce-config.xsd'.
 * This class was generated by Fusion Generator v0.2.
 * @author Fusion Generator
 */
class RequestDispatcherConfig extends PropertyObject
{
    private $p_pattern;
    private $p_class;
    
    public function __construct()
    {
    }
}

?>
