<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\enum;

use phorce\PhorceException;
use phorce\common\property\PropertyObject;

/**
 * This class provides base enumeration functionality for PHP classes.
 *
 * To define an enum, your class must obey the following rules:
 * <ul>
 *  <li>Extend the <code>Enum</code> class.</li>
 *  <li>Define an <code>init()</code> static method which calls
 *      <code>static::create()</code> for each value your enum will define.
 *      This is essentially your static constructor.</li>
 *  <li>Call <code>init()</code> following your class definition, since PHP has
 *      no real static constructor.</li>
 * </ul>
 *
 * For example, to define a Color enum, one may do the following:
 *
 * <code>
 * use phorce\common\enum\Enum;
 *
 * class Color extends Enum {
 *     private $pRO_rgbValue;
 *
 *     public static function init() {
 *         static::create('RED',   '#ff0000');
 *         static::create('GREEN', '#00ff00');
 *         static::create('BLUE',  '#0000ff');
 *     }
 *
 *     protected __construct($rgb) {
 *         $this->pRO_rgbValue = $rgb;
 *     }
 * }
 *
 * Color::init();
 * </code>
 *
 * The enum values are accessed as static methods on the class.  For example:
 * <code>Color::RED()</code> will return an instance of <code>Color</code>.
 * Note that <code>Enum</code> also extends <code>PropertyObject</code> so that
 * <code>Color</code>'s properties may also be accessed with
 * <code>Color::RED()->rgbValue</code>.
 *
 * Some impelementation notes: the use of this Enum implementation is not ideal
 * but better than the default which PHP offers (which is nothing).  Amongst its
 * issues are the lack of dynamic constants via a magic method, thus requiring
 * the use of dynamic static methods (via <code>__callStatic()</code>) to access
 * the Enum values.  Also, the lack of a true static constructor in PHP requires
 * us to call <code>init()</code> below each class that extends Enum.  Future   
 * implementations of Enum may attempt to improve on these issues if PHP adds
 * support for these features.
 *
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
abstract class Enum extends PropertyObject
{
    /**
     * A bi-dimensional array of all Enum objects.
     * @var array
     * @access private
     */
    private static $_enums = array();

    /**
     * The name by which this enumeration is accessed.
     * @var string
     * @access private
     */
    private $_name;

    /**
     * The ordinal of this enumeration.
     * @var integer
     * @access private
     */
    private $_ordinal;

    /**
     * Returns the ordinal.
     * @return integer
     */
    public function ordinal()
    {
        return $this->_ordinal;
    }

    /**
     * Returns the name of this enumeration.
     * @return string
     */
    public function __toString()
    {
        return $this->_name;
    }

    /**
     * Returns all enumerations for the class from which this is called.  For
     * example, calling <code>MyEnum::values()</code> will return all MyEnum
     * enumerations.
     * @return object ArrayObject
     */
    public static function values()
    {
        $class = get_called_class();
        return new ArrayObject($this->_enums[$class]);
    }

    /**
     * Adds a value to the enumeration.  The name of the value must be supplied.
     * Any additional arguments will be passed to the constuctor of the enum.
     * @param string $name Enumeration name
     * @param ... Constructor arguments
     * @access protected
     */
    protected static function create($name)
    {
        $name = strtoupper($name);
        $class = get_called_class();

        if (! isset(self::$_enums[$class]))
            self::$_enums[$class] = array();

        // enum instances must be unique
        if (isset(self::$_enums[$class][$name]))
            throw new PhorceException("Enum {$enum} defined twice");

        // create enum instance
        /* $c = new ReflectionClass($class);
        $enum = call_user_func_array(array($c, "newInstance"), $args);
        $c->newInstanceArgs($args); */

        // build array of variable arguments
        $args = array();
        for ($i = 1; $i < func_num_args(); $i++) {
            $args[] = func_get_arg($i);
        }

        $paramstr = '';
        for ($i = 0; $i < count($args); $i++) {
            $paramstr .= '$args['.$i.'],';
        }
        $paramstr = rtrim($paramstr,',');
        $enum = eval("return new $class($paramstr);");

        $enum->_name = $name;
        $enum->_ordinal = count(self::$_enums[$class]);
        self::$_enums[$class][$name] = $enum;
    }

    /**
     * Accesses the enumeration object.
     * @param string $name Enumeration name
     * @param string $args Ignored
     */
    public static function __callStatic($name, $args)
    {
        $name = strtoupper($name);
        $class = get_called_class();
        if (! isset(self::$_enums[$class][$name]))
            throw new PhorceException("Enum {$class}::{$name} not defined.");
        return self::$_enums[$class][$name];
    }
}

?>
