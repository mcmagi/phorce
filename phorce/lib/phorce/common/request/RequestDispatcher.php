<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\request;

/**
 * A RequestDispatcher is an object which handles an incoming request by
 * including or requiring that content.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
interface RequestDispatcher
{
    /**
     * Includes the incoming request.  A warning will be issued if it cannot
     * include the content.
     */
    public function includePath();

    /**
     * Requres the incoming request.  An error will be issued if it cannot
     * include the content.
     */
    public function requirePath();

    /**
     * Forwards to the incoming request via an include.  This function will
     * not return.
     */
    public function forwardPath();

    /**
     * Forwards to the incoming request by sending a 302 response to the web
     * browser.  This function will not return.
     */
    public function redirectPath();
}

?>
