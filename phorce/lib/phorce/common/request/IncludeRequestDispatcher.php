<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\request;

use phorce\PhorceException;

/**
 * Dispatches requests to a context-relative path by including the content.
 * An inclusion of this nature will execute PHP files and simply output the
 * contents of all other file types.  This follows the behavior of the
 * <code>include</code> internal PHP function.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class IncludeRequestDispatcher implements RequestDispatcher
{
    /**
     * The path to include.
     * @var string
     * @access private
     */
    private $_path;

    /**
     * Creates a dispatcher for the specified request path.
     * @param string $path Request path
     */
    public function __construct($path)
    {
        if (! strlen($path))
            throw new PhorceException("Path is empty");
        $this->_path = $path;
    }

    /**
     * Calls <code>include</code> on the request path.
     */
    public function includePath()
    {
        include \phorce\makepath(PHORCE_WEBROOT, $this->_path);
    }

    /**
     * Calls <code>require</code> on the request path.
     */
    public function requirePath()
    {
        require \phorce\makepath(PHORCE_WEBROOT, $this->_path);
    }

    /**
     * Forwards to the incoming request via an include.  This function will
     * not return.
     */
    public function forwardPath()
    {
        include \phorce\makepath(PHORCE_WEBROOT, $this->_path);
        exit(0);
    }

    /**
     * Sends a redirect to the browser on the requested path and stops
     * processing.  This function will not return.
     */
    public function redirectPath()
    {
        header("Location: {$this->_path}");
        exit(0);
    }
}

?>
