<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\php;

use phorce\common\property\PropertyObject;

/**
 * Represents a 5.x-compliant PHP variable.  Variables can be contained within
 * a class (prefixed by its visibility) or passed as arguments to functions.
 * Variables can also be assigned default values.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhpVariable extends PropertyObject implements PhpCodeGenerator
{
    const DEFAULT_VIS = "var";

    private $_name;
    private $_init;
    private $_comment;
    private $_passRef = false;
    private $_visibility = self::DEFAULT_VIS;
    private $_static = false;
    private $_final = false;
    private $_typeHint;

    public function __construct($name, $init = null)
    {
        $this->_name = $name;
        $this->_init = $init;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setVisibility($v)
    {
        $this->_visibility = $v;
    }

    public function setPassRef($v)
    {
        return $this->_passRef = (boolean) $v;
    }

    public function setTypeHint($t)
    {
        return $this->_typeHint = $t;
    }

    public function setStatic($s)
    {
        $this->_static = (boolean) $s;
    }

    public function setFinal($f)
    {
        $this->_final = (boolean) $f;
    }

    public function __toString()
    {
        $str = '$' . $this->_name;
        if (! is_null($this->_init))
            $str .= ' = ' . $this->_init;

        // pass by reference?
        if ($this->_passRef)
            $str = '&' . $str;

        // type hint?
        if ($this->_typeHint)
            $str = $this->_typeHint . " " . $str;

        return $str;
    }

    public function getPhpCode(PhpOutput $php)
    {
        /*if (! is_null($this->comment))
            $php->addPhpdocComment($this->comment);*/

        // build member variable prefix
        $prefix = "";
        if ($this->_static)
            $prefix .= "static ";
        if ($this->_final)
            $prefix .= "final ";
        if ($this->_visibility)
            $prefix .= $this->_visibility . " ";

        $php->addStatement($prefix . $this->__toString() . ";");
    }
}

?>
