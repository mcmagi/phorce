<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\php;

use \ArrayObject;
use phorce\common\property\PropertyObject;

/**
 * Represents a 5.x-compliant PHP function definition.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhpFunction extends PropertyObject implements PhpCodeGenerator
{
    private $_name;
    private $_params;
    private $_comment;
    private $_statements;
    private $_returnRef = false;
    private $_visibility;
    private $_static = false;
    private $_final = false;
    private $_abstract = false;

    public function __construct($name)
    {
        $this->_name = $name;

        $this->_params = new ArrayObject();
        $this->_statements = new ArrayObject();
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setReturnRef($v)
    {
        $this->_returnRef = (boolean) $v;
    }

    public function setVisibility($v)
    {
        $this->_visibility = $v;
    }

    public function setStatic($s)
    {
        $this->_static = (boolean) $s;
    }

    public function setFinal($f)
    {
        $this->_final = (boolean) $f;
    }

    public function setAbstract($a)
    {
        $this->_final = (boolean) $a;
    }

    public function getParameters()
    {
        return $this->_params;
    }

    public function getStatements()
    {
        return $this->_statements;
    }

    public function getComments()
    {
        return $this->_comments;
    }

    public function getPhpCode(PhpOutput $php)
    {
        // build parameters
        $paramstr = "";
        foreach ($this->_params as $param)
        {
            // add comma if necessary
            if (strlen($paramstr) > 0)
                $paramstr .= ", ";

            $paramstr .= $param->__toString();
        }

        // build method prefix
        $vis = strlen($this->_visibility) ? $this->_visibility . " " : "";
        $prefix = "";
        $suffix = "";
        if ($this->_abstract)
        {
            $prefix .= "abstract ";
            $suffix = ";";
        }
        else
        {
            if ($this->_static)
                $prefix .= "static ";
            if ($this->_final)
                $prefix .= "final ";
        }
        if ($this->_visibility)
            $prefix .= $this->_visibility . " ";

        $php->addStatement($prefix . "function "
            . ($this->_returnRef ? "&" : "")
            . $this->_name . "($paramstr)" . $suffix);

        if (! $this->_abstract)
        {
            $php->addStatement("{");
            $php->incIndent();

            // append all statements
            foreach ($this->_statements as $stmt)
                $php->addStatement($stmt);

            // close method
            $php->decIndent();
            $php->addStatement("}");
        }
    }
}

?>
