<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\php;

use \ArrayAccess;
use \ArrayObject;
use phorce\common\property\PropertyObject;

/**
 * Represents a PHP-style comment.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhpComment extends PropertyObject implements PhpCodeGenerator
{
    const STYLE_MULTILINE = "multiline";
    const STYLE_SINGLELINE = "singleline";
    const STYLE_PHPDOC = "phpdoc";

    private $_style;
    private $_lines;

    public function __construct($line = null)
    {
        $this->_style = "";

        if (is_null($line))
            $this->_lines = new ArrayObject();
        elseif (is_array($line))
            $this->_lines = new ArrayObject($line);
        elseif ($line instanceof ArrayAccess)
            $this->_lines = $line;
        else
            $this->_lines = new ArrayObject($line);
    }

    public function getLines()
    {
        return $this->_lines;
    }

    public function useMultiLineStyle()
    {
        $this->_style = self::STYLE_MULTILINE;
    }

    public function useSingleLineStyle()
    {
        $this->_style = self::STYLE_SINGLELINE;
    }

    public function usePhpDocStyle()
    {
        $this->_style = self::STYLE_PHPDOC;
    }

    public function getPhpCode(PhpOutput $php)
    {
        if ($this->_style == self::STYLE_PHPDOC)
            $php->addStatement("/**");
        elseif ($this->_style == self::STYLE_MULTILINE)
            $php->addStatement("/*");

        foreach ($this->_lines as $line)
        {
            if ($this->_style == self::STYLE_PHPDOC || $this->_style == self::STYLE_MULTILINE)
                $prefix = " * ";
            elseif ($this->_style == self::STYLE_SINGLELINE)
                $prefix = "// ";

            $php->addStatement($prefix . $line);
        }

        if ($this->_style == self::STYLE_PHPDOC || $this->_style == self::STYLE_MULTILINE)
            $php->addStatement(" */");
    }
}

?>
