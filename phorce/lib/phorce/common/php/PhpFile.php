<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\php;

use \ArrayObject;
use phorce\common\property\PropertyObject;

/**
 * Represents a file containing 5.x-compliant PHP code.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhpFile extends PropertyObject implements PhpCodeGenerator
{
    private $_filename;
    private $_path;
    private $_namespace;
    private $_comment;
    private $_uses;
    private $_requires;
    private $_classes;
    private $_functions;
    private $_statements;

    public function __construct($filename)
    {
        $this->_filename = $filename;

        $this->_uses = new ArrayObject();
        $this->_requires = new ArrayObject();
        $this->_classes = new ArrayObject();
        $this->_functions = new ArrayObject();
        $this->_statements = new ArrayObject();
    }

    public function setPath($p)
    {
        $this->_path = $p;
    }

    public function getPath()
    {
        return $this->_path;
    }

    public function getFilename()
    {
        return $this->_filename;
    }

    public function getFilePath()
    {
        return $this->_path . DIRECTORY_SEPARATOR . $this->_filename;
    }

    public function setComment(PhpComment $c)
    {
        $this->_comment = $c;
    }

    public function getComment()
    {
        return $this->_comment;
    }

    public function setNamespace($n)
    {
        $this->_namespace = $n;
    }

    public function getNamespace()
    {
        return $this->_namespace;
    }

    public function getUses()
    {
        return $this->_uses;
    }

    public function getRequires()
    {
        return $this->_requires;
    }

    public function getClasses()
    {
        return $this->_classes;
    }

    public function getFunctions()
    {
        return $this->_functions;
    }

    public function getStatements()
    {
        return $this->_statements;
    }

    public function getPhpCode(PhpOutput $php)
    {
        $php->startPhpCode();

        if (! is_null($this->_namespace))
        {
            $php->addStatement("");
            $php->addStatement("namespace {$this->_namespace};");
        }

        if (count($this->_requires))
            $php->addStatement("");

        // ensure requires are unique (and sorted)
        $requires = array_unique($this->_requires->getArrayCopy());
        sort($requires);
        foreach ($requires as $req)
            $php->addStatement("require_once \"$req\";");

        if (count($this->_uses))
            $php->addStatement("");

        // ensure uses are unique (and sorted)
        $uses = array_unique($this->_uses->getArrayCopy());
        sort($uses);
        foreach ($uses as $use)
            $php->addStatement("use $use;");

        foreach ($this->_classes as $class)
        {
            $php->addStatement("");
            $class->getPhpCode($php);
        }

        foreach ($this->_functions as $k => $fcn)
        {
            if ($k > 0)
                $php->addStatement("");

            $fcn->getPhpCode($php);
        }

        if (count($this->_statements))
            $php->addStatement("");

        foreach ($this->_statements as $stmt)
            $php->addStatement($stmt);

        $php->addStatement("");
        $php->endPhpCode();
    }
}

?>
