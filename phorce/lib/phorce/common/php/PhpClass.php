<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\php;

use \ArrayObject;
use phorce\common\property\PropertyObject;

/**
 * Represents a 5.x-compliant PHP class definition.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhpClass extends PropertyObject implements PhpCodeGenerator
{
    const VIS_PUBLIC = "public";
    const VIS_PROTECTED = "protected";
    const VIS_PRIVATE = "private";

    private $_classname;
    private $_extends;
    private $_comment;
    private $_constants;
    private $_variables;
    private $_methods;
    private $_abstract = false;
    private $_implements;

    public function __construct($name, $extends = null)
    {
        $this->_classname = $name;
        $this->_extends = $extends;

        $this->_constants = new ArrayObject();
        $this->_variables = new ArrayObject();
        $this->_methods = new ArrayObject();
        $this->_implements = new ArrayObject();
    }

    public function getClassname()
    {
        return $this->_classname;
    }

    public function setAbstract($a)
    {
        $this->_abstract = (boolean) $a;
    }

    public function getImplements()
    {
        return $this->_implements;
    }

    public function getVariables()
    {
        return $this->_variables;
    }

    public function getMethods()
    {
        return $this->_methods;
    }

    public function getConstants()
    {
        return $this->_constants;
    }

    public function setComment(PhpComment $c)
    {
        $this->_comment = $c;
    }

    public function getPhpCode(PhpOutput $php)
    {
        $str = "";

        if (! is_null($this->_comment))
            $this->_comment->getPhpCode($php);

        // begin class statement
        if ($this->_abstract)
            $str .= "abstract ";
        $str .= "class " . $this->_classname;

        if (! is_null($this->_extends))
            $str .= " extends " . $this->_extends;

        if (count($this->_implements))
            $str .= " implements " . implode(", ", (array) $this->_implements);

        $php->addStatement($str);
        $php->addStatement("{");
        $php->incIndent();

        foreach ($this->_constants as $const)
            $const->getPhpCode($php);

        if (count($this->_constants))
            $php->addStatement("");

        foreach ($this->_variables as $var)
            $var->getPhpCode($php);

        if (count($this->_variables))
            $php->addStatement("");

        foreach ($this->_methods as $k => $fcn)
        {
            if ($k > 0)
                $php->addStatement("");

            $fcn->getPhpCode($php);
        }

        $php->decIndent();
        $php->addStatement("}");
    }
}

?>
