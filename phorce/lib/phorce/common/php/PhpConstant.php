<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\php;

use phorce\common\property\PropertyObject;

/**
 * Represents a 5.x-compliant PHP constant.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhpConstant extends PropertyObject implements PhpCodeGenerator
{
    private $_name;
    private $_init;

    public function __construct($name, $init)
    {
        $this->_name = $name;
        $this->_init = $init;
    }

    public function getPhpCode(PhpOutput $php)
    {
        $php->addStatement("const " . $this->_name . " = " . $this->_init . ";");
    }
}

?>
