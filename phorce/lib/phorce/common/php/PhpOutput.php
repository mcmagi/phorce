<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\php;

/**
 * Represents an output stream consiting of PHP code.  Provides indentation
 * support.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhpOutput
{
    const INDENT_WS = "    ";
    private $_output = "";
    private $_indent = "";
    private $_isInPhpCode = false;

    /**
     * Adds text to the output stream.
     * @param string $text The text to be added
     */
    public function addText($text)
    {
        $this->_output .= $text;
    }

    /**
     * Adds a statement to the output stream.
     * @param string $statement PHP statement to append to output
     */
    public function addStatement($statement)
    {
        $this->_output .= $this->_getIndentStr() . $statement . PHP_EOL;
    }

    public function startPhpCode()
    {
        if (! $this->_isInPhpCode)
        {
            $this->_output .= '<?php' . PHP_EOL;

            //$this->incIndent();
            $this->_isInPhpCode = true;
        }
    }

    public function endPhpCode()
    {
        if ($this->_isInPhpCode)
        {
            //$this->decIndent();
            $this->_output .= $this->_getIndentStr() . '?>';
            $this->_isInPhpCode = false;
        }
    }

    public function getOutputString()
    {
        return $this->_output;
    }

    public function getIndent()
    {
        return $this->_indent;
    }

    public function setIndent($v)
    {
        $this->_indent = $v;
    }

    public function incIndent($qty = 1)
    {
        $this->_indent += $qty;
    }

    public function decIndent($qty = 1)
    {
        $this->_indent -= $qty;
    }

    private function _getIndentStr()
    {
        if ($this->_indent > 0)
            return str_repeat(self::INDENT_WS, $this->_indent);
        return "";
    }
}

?>
