<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\property;

/**
 * An interface to indicate "property" support for PHP objects.  Though by no
 * means required to implement in order to provide some level of property
 * support, it helps PropertyUtils in understanding what classes have overriden
 * it.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 * @see PropertyObject
 * @see PropertyWrapper
 */
interface PropertySupport
{
    /**
     * Magic function for accessing a property.
     * @param string $name Property name
     * @return mixed Property value
     */
    public function &__get($name);
    
    /**
     * Magic function for mutating a property.
     * @param string $name Property name
     * @param mixed $value Property value
     */
    public function __set($name, $value);

    /**
     * Magic function for determining whether the property has a value.
     * @param string $name Property name
     * @return boolean True if it exists
     */
    public function __isset($name);

    /**
     * Magic function for removing a property's value.
     * @param string $name Property name
     */
    public function __unset($name);
}

?>
