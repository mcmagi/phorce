<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\property;

use phorce\PhorceException;

/**
 * Exception thrown when a property cannot be accessed.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PropertyAccessException extends PhorceException
{
    private $_object;
    private $_property;

    public function __construct(&$object, $property, $msg)
    {
        $this->_object =& $object;
        $this->_property = $property;

        $msg = "({$this->getType()})->{$this->getProperty()}: $msg";
        parent::__construct($msg);
    }

    public function &getObject()
    {
        return $this->_object;
    }

    public function getProperty()
    {
        return $this->_property;
    }

    public function getType()
    {
        $type = gettype($this->_object);
        if (is_object($this->_object))
            $type = get_class($this->_object);

        return $type;
    }
}

?>
