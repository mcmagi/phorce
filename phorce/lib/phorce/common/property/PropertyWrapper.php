<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\property;

use \ArrayAccess;

/**
 * This class is used to get and set properties given an object and a property
 * name.  PropertyWrapper always checks if the object it is currently operating
 * on is an object or array and accesses its properties as such.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PropertyWrapper implements PropertySupport
{
    /**
     * Inner object.
     * @var mixed
     * @access private
     */
    private $_object;

    /**
     * Constructs an PropertyWrapper around the specified object.
     * @param mixed $object The object to access
     */
    public function __construct(&$object)
    {
        $this->_object =& $object;
    }

    /**
     * Sets a single (non-nested) property on the specified object.
     * @param string $name Name of property to set
     * @param mixed $value Value to set property with
     */
    public function __set($name, $value)
    {
        if (is_null($this->_object))
        {
            throw new PropertyAccessException($this->_object, $name,
                "Cannot mutate property '$name' of a null object.");
        }

        if ($value instanceof PropertyWrapper)
            $v = $value->_object;
        else
            $v =& $value;

        // set property if method exists
        $setMethod = "set$name";
        if (is_array($this->_object) || $this->_object instanceof ArrayAccess)
        {
            // object is an array, use as associative array
            $this->_object[$name] =& $v;
        }
        elseif (method_exists($this->_object, $setMethod))
        {
            // set method exists, use variable function syntax
            $this->_object->$setMethod($v);
        }
        else
        {
            // no set method, check for property
            $this->_object->{$name} =& $v;
        }
    }

    /**
     * Gets a single (non-nested) property on the specified object.
     * @param string $name Name of property to get
     * @return mixed Value of property
     */
    public function &__get($name)
    {
        //echo "getting property $name<br>\n";

        if (is_null($this->_object))
        {
            throw new PropertyAccessException($this->_object, $name,
                "Cannot access property '$name' of a null object.");
        }

        $value = null;
        $getMethod = "get$name";
        $isMethod = "is$name";
        if (is_array($this->_object) || $this->_object instanceof ArrayAccess)
        {
            // object is an array, use as hashmap
            if (isset($this->_object[$name]))
                $value =& $this->_object[$name];
            else
                throw new PropertyAccessException($this->_object, $name,
                    "Array index '$name' does not exist");
        }
        elseif (method_exists($this->_object, $getMethod))
        {
            // get method exists, use variable function syntax
            $value =& $this->_object->$getMethod();
        }
        elseif (method_exists($this->_object, $isMethod))
        {
            // is method exists, use variable function syntax
            $value =& $this->_object->$isMethod();
        }
        else
        {
            // no get/is method, check for property
            // FIXME: PHP is broken here - so suppressing error
            @$value =& $this->_object->{$name};
        }

        $temp = new PropertyWrapper($value);
        return $temp;
    }

    /**
     * Returns true if the specified property exists in the wrapped object.
     * @param string $name Property name
     * @return boolean True if exists
     */
    public function __isset($name)
    {
        if (is_null($this->_object))
        {
            throw new PropertyAccessException($this->_object, $name,
                "Cannot check property '$name' of a null object.");
        }
        
        if (is_array($this->_object) || $this->_object instanceof ArrayAccess)
        {
            // object is an array, use as hashmap
            return isset($this->_object[$name]);
        }
        elseif (method_exists($this->_object, $getMethod))
            return true;
        elseif (method_exists($this->_object, $isMethod))
            return true;

        return isset($this->_object->{$name});
    }

    /**
     * Unsets the specified property in the wrapped object.
     * @param string $name Property name
     */
    public function __unset($name)
    {
        if (is_null($this->_object))
        {
            throw new PropertyAccessException($this->_object, $name,
                "Cannot unset property '$name' of a null object.");
        }
        
        if (is_array($this->_object) || $this->_object instanceof ArrayAccess)
        {
            // object is an array, use as hashmap
            if (isset($this->_object[$name]))
                unset($this->_object[$name]);
        }
        else
            unset($this->_object->{$name});
    }

    /**
     * Invokes a function on the inner object.
     * @param string $name Method name
     * @param array Arguments
     * @return mixed Result
     */
    public function __call($name, array $arguments)
    {
        return call_user_func(array($this->_object, $name), $arguments);
    }

    /**
     * Clones the PropertyWrapper with a cloned inner object.
     */
    public function __clone()
    {
        $this->_object = clone $this->_object;
    }

    /**
     * Returns the wrapped object in string form.
     * @return string Object in string form
     */
    public function __toString()
    {
        return "" . $this->_object;
    }

    /**
     * Returns the wrapped object.
     * @return mixed Wrapped object
     */
    public function &_getObject()
    {
        return $this->_object;
    }
}

?>
