<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\property;

use \ReflectionClass;
use \ReflectionObject;

/**
 * This abstract class should be extended to provide "property" support for PHP
 * objects.  Property support allows access to accessor (aka "getter") and
 * mutator (aka "setter") methods by overriding the <code>"-&gt;"</code>
 * operator through the magic <code>__get()</code> and <code>__set()</code>
 * methods.
 *
 * <p>For example, a class with private member variable <code>$_var</code> may
 * have an accessor method <code>getVar()</code> used to access it.  Normally
 * this method is called through the expression <code>$object->getVar()</code>
 * in PHP.  By extending PropertyObject, the caller may also use the notation
 * <code>$object->var</code>.  Alternatively, if the variable is prefixed with
 * a "p_" (as in <code>$p_var</code>), then no accessor would be necessary
 * except when you want to override the default property access behavior.</p>
 *
 * <p>You may also specify if a property is to be read-only with the "pRO_"
 * prefix and write-only with the "pWO_" prefix.</p>
 *
 * @abstract
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
abstract class PropertyObject implements PropertySupport
{
    /**
     * Magic function acts as the accessor for a property.  It will look for a
     * method 'get{$name}' or 'is{$name}' the where $name is the name of the
     * property.  If no such method exists or it is not public and non-static,
     * then it will attempt to resolve the property by looking for a member
     * variable in the form "p_{$name}" or "pRO_{$name}".  If no such member
     * variable exists, an exception will be thrown.
     * @param string $name Property name
     * @return mixed Property value
     */
    public function &__get($name)
    {
        $obj = new ReflectionObject($this);

        // check if accessor method is present
        if ($obj->hasMethod("get{$name}"))
            $fcn = $obj->getMethod("get{$name}");
        elseif ($obj->hasMethod("is{$name}"))
            $fcn = $obj->getMethod("is{$name}");

        // accessor must be public and not static
        if (isset($fcn) && $fcn->isPublic() && ! $fcn->isStatic())
        {
            $temp =& $fcn->invoke($this);
            return $temp;
        }

        // check if member variable is present
        /* if ($obj->hasProperty("p_{$name}"))
            $prop = $obj->getProperty("p_{$name}");
        elseif ($obj->hasProperty("pRO_{$name}"))
            $prop = $obj->getProperty("pRO_{$name}"); */

        // getProperty doesn't work right for properties in super classes,
        // so we have to code a workaround
        $classes = array();
        do
        {
            array_unshift($classes, $obj);
        }
        while ($obj = $obj->getParentClass());

        foreach ($classes as $obj)
        {
            if ($obj->hasProperty("p_{$name}"))
                $prop = $obj->getProperty("p_{$name}");
            elseif ($obj->hasProperty("pRO_{$name}"))
                $prop = $obj->getProperty("pRO_{$name}");

            if (isset($prop))
                break;
        }

        // member variable may be any visibility but not static
        if (isset($prop) && $prop->isDefault() && ! $prop->isStatic())
        {
            $prop->setAccessible(true);
            $temp =& $prop->getValue($this);
            return $temp;
        }

        throw new PropertyAccessException($this, $name,
            "No public accessor found for '$name'");
    }

    /**
     * Magic function acts as the mutator for a property .  It will look for a
     * method 'set$name' where $name is the name of the property.  If no such
     * method exists or it is not public and non-static, then it will attempt to
     * resolve the property by looking for a member variable in the form
     * "p_{$name}" or "pWO_{$name}".  If no such member variable exists, an
     * exception will be thrown.
     * @param string $name Property name
     * @param mixed $value Property value
     */
    public function __set($name, $value)
    {
        $obj = new ReflectionObject($this);

        // check if mutator method is present
        if ($obj->hasMethod("set{$name}"))
            $fcn = $obj->getMethod("set{$name}");

        // accessor must be public and not static
        if (isset($fcn) && $fcn->isPublic() && ! $fcn->isStatic())
            return $fcn->invoke($this, $value);

        // check if member variable is present
        /* if ($obj->hasProperty("p_{$name}"))
            $prop = $obj->getProperty("p_{$name}");
        elseif ($obj->hasProperty("pWO_{$name}"))
            $prop = $obj->getProperty("pWO_{$name}");*/

        // getProperty doesn't work right for properties in super classes,
        // so we have to code a workaround
        $classes = array();
        do
        {
            array_unshift($classes, $obj);
        }
        while ($obj = $obj->getParentClass());

        foreach ($classes as $obj)
        {
            if ($obj->hasProperty("p_{$name}"))
                $prop = $obj->getProperty("p_{$name}");
            elseif ($obj->hasProperty("pWO_{$name}"))
                $prop = $obj->getProperty("pWO_{$name}");

            if (isset($prop))
                break;
        }

        // member variable may be any visibility but not static
        if (isset($prop) && $prop->isDefault() && ! $prop->isStatic())
        {
            $prop->setAccessible(true);
            $prop->setValue($this, $value);
            return;
        }

        throw new PropertyAccessException($this, $name,
            "No public mutator found for '$name'");
    }

    /**
     * Magic function checks if a property is set.  If a getter or setter is
     * found, the property is considered set.
     * @param string $name Property name
     * @return boolean True if set
     */
    public function __isset($name)
    {
        // must use ReflectionClass here to prevent recursion on hasProperty()
        $obj = new ReflectionClass(get_class($this));

        // check if accessor or mutator method is present
        if ($obj->hasMethod("get{$name}"))
            $fcn = $obj->getMethod("get{$name}");
        elseif ($obj->hasMethod("is{$name}"))
            $fcn = $obj->getMethod("is{$name}");
        elseif ($obj->hasMethod("set{$name}"))
            $fcn = $obj->getMethod("set{$name}");

        // method must be public and not static
        if (isset($fcn) && $fcn->isPublic() && ! $fcn->isStatic())
            return true;

        /* // check if member variable is present
        if ($obj->hasProperty("p_{$name}"))
            $prop = $obj->getProperty("p_{$name}");
        elseif ($obj->hasProperty("pRO_{$name}"))
            $prop = $obj->getProperty("pRO_{$name}");
        elseif ($obj->hasProperty("pWO_{$name}"))
            $prop = $obj->getProperty("pWO_{$name}"); */

        // getProperty doesn't work right for properties in super classes,
        // so we have to code a workaround
        $classes = array();
        do
        {
            array_unshift($classes, $obj);
        }
        while ($obj = $obj->getParentClass());

        foreach ($classes as $obj)
        {
            // check if member variable is present
            if ($obj->hasProperty("p_{$name}"))
                $prop = $obj->getProperty("p_{$name}");
            elseif ($obj->hasProperty("pRO_{$name}"))
                $prop = $obj->getProperty("pRO_{$name}");
            elseif ($obj->hasProperty("pWO_{$name}"))
                $prop = $obj->getProperty("pWO_{$name}");

            if (isset($prop))
                break;
        }

        // member variable may be any visibility but not static
        if (isset($prop) && $prop->isDefault() && ! $prop->isStatic())
            return true;

        return false;
    }

    /**
     * Magic function for unsetting a property throws an exception.  It is not
     * possible to unset a property.
     * @param string $name Property name
     */
    public function __unset($name)
    {
        throw new PropertyAccessException($this, $name,
            "Unsetting a property is not supported");
    }
}

?>
