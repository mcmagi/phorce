<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\property;

use \ArrayAccess;

/**
 * This class is used to get and set properties given an object and a property
 * string.  Property strings are of the form "prop1.prop2.prop3".  For each
 * property in the string, properties are iteratively retrieved from each
 * nested object by calling the getter method (e.g. getText() for a "text"
 * property).  If no getter method exists, it looks for a member variable with
 * the same name.  If the current object is an array, it uses the property name
 * as an array index.  A property can also be of the form "prop1[n]", where n
 * is an array index, either numeric (for a classic array) or a string (for an
 * associative array).  This format is provided for user convenience only.
 * PropertyUtils does not enforce that an object must be an array if its
 * property is followed by an array index.  This basically means that both
 * formats are completely interchangable.  In other words, "prop1.prop2.prop3"
 * is equivalent to "prop1[prop2].prop3".  PropertyUtils always checks if the
 * object it is currently operating on is an object or array and accesses its
 * properties as such.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PropertyUtils
{
    /**
     * Sets the properties on the form using the values in the hashmap.
     * @param array $hash Map containing name/value pairs on form
     * @param object $object Object to set properties on
     */
    public static function copyHashToObject(&$hash, &$object)
    {
        foreach (array_keys($hash) as $propName)
        {
            // get property value
            $propValue = $hash[$propName];

            //echo "<b>ARG:</b>" . htmlentities("$propName = $propValue") . "<br>";

            // set property on object
            self::setProperty($object, $propName, $propValue);
        }
    }

    /**
     * Sets a property on the specified object.
     * @param mixed $object Object to invoke setter on
     * @param string $name Name of property to set
     * @param mixed $value Value to set property with
     */
    public static function setProperty(&$object, $name, $value)
    {
        // get list of properties separated by dot
        $propList =& self::splitPropertyNames($name);

        // remove name of final property (the one to be set)
        $setProp = array_pop($propList);

        // get object before final setter property
        $setobject =& self::getProperty($object, implode('.', $propList));

        try
        {
            // set the property
            $obj =& self::getPropertySupport($setobject);
            $obj->{$setProp} = $value;
        }
        catch (PropertyAccessException $e)
        {
            // throw exception
            throw new PropertyAccessException($object, $name,
                $e->getMessage());
        }
    }

    /**
     * Adds to an array property on the specified object.
     * @param mixed $object Object to invoke adder on
     * @param string $name Name of property to add
     * @param mixed $value Value of property
     */
    public static function addProperty(&$object, $name, $value)
    {
        $arr = self::getProperty($object, $name);
        if (is_array($arr) || $arr instanceof ArrayAccess)
            $arr[] = $value;

        // what if it's not an array?  do nothing???
    }

    /**
     * Gets a property on the specified object.
     * @param mixed $object Object to invoke getter on
     * @param string $name Name of property to get
     * @return mixed Value of property
     */
    public static function &getProperty(&$object, $name)
    {
        //echo "getting property '$name' from '" . get_class($object) . "'<br>\n";
        $obj =& self::getPropertySupport($object);

        // get list of properties separated by dot
        $propList =& self::splitPropertyNames($name);

        // descend nested properties
        $numProps = count($propList);
        for ($i = 0; $i < $numProps; $i++)
        {
            if (! strlen($propList[$i]))
                continue;

            try
            {
                $value = $obj->{$propList[$i]};
                $obj =& self::getPropertySupport($value);
            }
            catch (PropertyAccessException $e)
            {
                // which property in list failed?
                $nPropArr = array();
                for ($j = $i; $j < $numProps; $j++)
                    $nPropArr[] = $propList[$j];
                $nProp = implode('.', $nPropArr);

                // throw exception
                throw new PropertyAccessException($object, $name,
                    $e->getMessage());
            }
        }

        //echo "returning property value "': $obj<br>\n";

        if ($obj instanceof PropertyWrapper)
            return $obj->_getObject();
        else
            return $obj;
    }

    /**
     * Splits property names into an array.  The array will include both
     * property names and array indices/keys.  For example, a property of
     * "prop[1].foo[bar].blah" will return an array (prop,1,foo,bar,blah).
     * @param string $name Property name
     * @return array Array of property names
     */
    public static function &splitPropertyNames($name)
    {
        $propList = array();

        // This regular expression breaks properties by the '.', ensuring
        // array indeces are preserved.  This makes sure that a '.' in an
        // array index (for example as a key an associate array) is preserved
        // and not consdiered a property separator.
        //
        // The regular expression is broken down as follows:
        //  [^.\[\(]+     - matches 1 or more non-separator chars
        //  (             - array index group
        //      [\(\[]    - match begin array
        //      [^\)\]]+  - match 1 or more non-array index chars
        //      [\)\]]    - march end array index
        //  )*            - match 0 or more array indeces
        //
        // PREG_PATTERN_ORDER means array index "0" contains full matches
        preg_match_all('/([^._\[\(]+)([\(\[][^\)\]]+[\)\]])*/', $name,
            $propList, PREG_PATTERN_ORDER);

        // now break arrays down
        $properties = array();
        foreach ($propList[0] as $token)
        {
            // split on array indexes, but do not return empty strings
            $results = preg_split('/[\[\]\(\)]/', $token, -1, PREG_SPLIT_NO_EMPTY);

            // merge into properties array
            $properties = array_merge($properties, $results);
        }

        return $properties;
    }

    /**
     * Returns an object which implements the PropertySupport interface.  If
     * the object already implements PropertySupport, returns the passed-in
     * object.  Otherwise, returns a PropertyWrapper around the specified
     * object.
     * @param mixed &$o Input object
     * @return object PropertySupport
     */
    private static function &getPropertySupport(&$o)
    {
        $rc = null;
        if ($o instanceof PropertySupport)
            $rc =& $o;
        else
            $rc = new PropertyWrapper($o);
        return $rc;
    }

    /*private static function _logPropertyError($propList, $idx, $reason)
    {
        $propArr = array();
        for ($i = 0; $i < $idx; $i++)
            $propArr[] = $propList[$i];
        $problemProp = implode(".", $propArr);

        $msg = "Error accessing '$problemProp': " . $reason;

        trigger_error($msg, E_USER_ERROR);
    }*/
}
?>
