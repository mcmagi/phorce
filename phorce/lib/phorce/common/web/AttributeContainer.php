<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\web;

/**
 * Represents a container of web attributes.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Common
 * @subpackage Web
 */
interface AttributeContainer
{
    /**
     * Returns the value of the specified attribute.
     * @param string $key Key
     * @return mixed Reference to value
     */
    public function &getAttribute($key);

    /**
     * Returns the available keys of all attributes.
     * @return array Array of keys
     */
    public function getAttributeNames();

    /**
     * Sets the value of an attribute.
     * @param string $key Key
     * @param mixed $value Reference to value
     */
    public function setAttribute($key, &$value);

    /**
     * Removes an attribute.
     * @param string $key Key
     */
    public function removeAttribute($key);
}

?>
