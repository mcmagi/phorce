<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\web;

/**
 * Provides a means to start and end sessions via a static class.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class SessionManager
{
    /**
     * Starts the session if it is not already started.  The session id is not
     * used if the session was already started.  If the session id is not
     * supplied, it the default session will be used.
     * @static
     * @param string $sid An optional session id to use
     */
    public static function startSession($sid = null)
    {
        if (! self::isSessionStarted())
        {
            if (! is_null($sid))
                session_id($sid);
            session_start();
            self::isSessionStarted(true);
        }
    }

    /**
     * Returns true if the session is started.  If the optional parameter is
     * supplied, changes the return value to the specified value.
     * @static
     * @param boolean $v Already started?
     * @return boolean
     */
    public static function isSessionStarted($v = null)
    {
        static $started = false;

        if (! is_null($v))
            $started = $v;

        return $started;
    }

    /**
     * Ends the session.
     * @static
     */
    public static function endSession()
    {
        if (self::isSessionStarted())
        {
            session_write_close();
            self::isSessionStarted(false);
        }
    }
}

?>
