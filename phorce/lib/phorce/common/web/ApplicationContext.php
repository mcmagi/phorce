<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\web;

use \Exception;
use phorce\NotFoundException;
use phorce\common\cache\PhorceConfigCache;
use phorce\common\cache\FusionDAOConfigFileLoader;
use phorce\common\config\PhorceConfig;
use phorce\common\config\PhorceConfigDAO;

/**
 * This class passes incoming requests to the appropriate request dispatcher.
 * To use, invoke the <code>getRequestDispatcher()</code> method, supplying the
 * desired virtual path.  It will then load the RequestDispatcher configured for
 * the specified path (identified via regular expression matching).
 *
 * This class also serves as a repository for application-scoped data.  That is,
 * data which is persisted across requests and sessions.  This data is stored
 * as serialized PHP objects in a .dat file.  Application scoped data should
 * typically not be written to except under unique or one-time circumstances.
 * If you are doing otherwise you should consider other alternatives (such as a
 * database).  Note that access to this file is NOT locked and therefore writes
 * are NOT thread-safe.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class ApplicationContext implements AttributeContainer
{
    /**
     * Path to the default request dispatcher class.
     */
    const DEFAULT_HANDLER_CLASS =
        'phorce\common\request\IncludeRequestDispatcher';

    /**
     * Name of the container configuration file.
     */
    const CONFIG_FILE = 'phorce-config.xml';

    /**
     * Name of the default application scope variable persistent file.  This
     * can be overridden in the configuration file.
     */
    const ATTRIBUTE_FILE = 'phorce-attributes.dat';

    /**
     * The application attribute key used to store the context root.
     */
    const CONTEXT_ROOT_KEY = 'phorce.context.root';

    /**
     * The application attribute key used to store the document root.
     */
    const DOCUMENT_ROOT_KEY = 'phorce.document.root';

    /**
     * The request attribute key used to store the requested page.
     */
    const PHORCE_REQUEST_KEY = 'phorce_request';

    /**
     * The request attribute key used to store the exception.
     */
    const PHORCE_EXCEPTION_KEY = 'phorce.exception';

    /**
     * The container context instance.
     * @var object ApplicationContext
     * @access private
     * @static
     */
    private static $_instance;

    /**
     * The container configuration.
     * @var object PhorceConfig
     * @access private
     */
    private $_config;

    /**
     * Application scope attributes.
     * @var array
     * @access private
     */
    private $_attributeMap;

    /**
     * Returns an instance of the container context.
     * @return object ApplicationContext The instance
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance))
            // setup a new phorce context
            self::$_instance = new ApplicationContext();
        return self::$_instance;
    }

    /**
     * Private constructor to guarantee singleton instance.
     */
    private function __construct()
    {
    }

    /**
     * Object destructor.  Saves any changes to the application scope attributes
     * to a file.
     */
    public function __destruct()
    {
        if (! is_null($this->_attributeMap))
            $this->_saveAttributes();
    }

    private function _loadConfig($configFile)
    {
        // load config from cache
        $configFile = \phorce\makepath(PHORCE_CONFIG, self::CONFIG_FILE);
        $this->_config = PhorceConfigCache::retrieveConfig(
            "phorce-config", new FusionDAOConfigFileLoader(
                $configFile, 'phorce\common\config\PhorceConfigDAO'));

        /* $p = new PhorceConfigDAO(
            \phorce\makepath(PHORCE_CONFIG, self::CONFIG_FILE));
        $this->_config = $p->load(); */
    }

    /**
     * Dispatches the request to the requested page or (if not provided) the
     * value stored under the PHORCE_REQUEST_KEY.
     * @param string $path Request path
     */
    public function dispatchRequest($path = null)
    {
        if (is_null($path) && array_key_exists(self::PHORCE_REQUEST_KEY, $_GET))
            $path = $_GET[self::PHORCE_REQUEST_KEY];

        if (! is_null($path))
        {
            try
            {
                $dispatcher = $this->getRequestDispatcher($path);
                $dispatcher->requirePath();
            }
            catch (NotFoundException $ex)
            {
                $_REQUEST[self::PHORCE_EXCEPTION_KEY] = $ex;
                header("Status: 404 Not Found");

                // TODO: lookup 404 page in config
                $dispatcher = $this->getRequestDispatcher("404notfound.html");
                $dispatcher->requirePath();
            }
            catch (Exception $ex)
            {
                $_REQUEST[self::PHORCE_EXCEPTION_KEY] = $ex;
                header("Status: 500 Server Error");

                // TODO: lookup 500 page in config
                $dispatcher = $this->getRequestDispatcher("500servererror.html");
                $dispatcher->requirePath();
            }
        }
    }

    /**
     * Returns the RequestDispatcher that is configured to handle requests for
     * the specified request path.
     * @param string $path Request path
     * @return object RequestDispatcher The request dispatcher for this request
     */
    public function getRequestDispatcher($path)
    {
        $classname = $this->_getRequestDispatcherClass($path);
        return new $classname($path);
    }

    /**
     * Returns the request handler class associated with the specified path.
     * @param string $path Request path
     * @access private
     */
    private function _getRequestDispatcherClass($path)
    {
        if (is_null($this->_config))
            $this->_loadConfig(self::CONFIG_FILE);

        // do regex match against each entry in config
        foreach ($this->_config->requestDispatchers as $rhc)
        {
            // return first entry that matches
            if (preg_match("/" . $rhc->pattern . "/", $path))
                return $rhc->class;
        }

        return self::DEFAULT_HANDLER_CLASS;
    }

    /**
     * Returns the filesystem path for a given virtual path.
     * @param string $vpath Virtual path
     * @return string Filesystem path
     */
    public function getRealPath($vpath)
    {
        return realpath(self::_normalizePath($this->getDocumentRoot() . DIRECTORY_SEPARATOR . $vpath));
    }

    /**
     * Returns the context root of the Phorce web application.  This
     * translates to the document root when resolved by the web server.
     * @return string Context root
     */
    public function getContextRoot()
    {
        if (! $this->getAttribute(self::CONTEXT_ROOT_KEY))
            $this->_determineRoot();
        return $this->getAttribute(self::CONTEXT_ROOT_KEY);
        //return $this->_getVirtualPath(PHORCE_WEBROOT);
    }

    /**
     * Returns the document root of the Phorce web application.  The document
     * root is the path on the filesystem of the web application root.
     * @return string Document root
     */
    public function getDocumentRoot()
    {
        return PHORCE_WEBROOT;
    }

    /**
     * Returns the directory, relative to the context and document roots where
     * the request has been invoked.
     * @return string Execution context dir
     */
    public function getExecutionContextDir()
    {
        $reqdir = dirname($_SERVER['REQUEST_URI']);

        // strip context root off of request dir to get path relative to context root
        return substr($reqdir, strlen($appCtx->getContextRoot()));
    }

    /**
     * Infers the document and context roots and saves them as application
     * attributes.
     */
    private function _determineRoot()
    {
        // Find /phorce/scripts/dispatch.php relative to request URI.
        // If not present, ascend directories until we find it.
        // Once found, the remaining requri is our context root.
        $requri = $_SERVER['REQUEST_URI'];
        do
        {
            $requri = dirname($requri);
            $myuri = $requri . "/phorce/scripts/dispatch.php";

            $obj = apache_lookup_uri($myuri);
        }
        while (! file_exists($obj->filename));

        if ($requri == "/")
            $requri = "";

        $this->setAttribute(self::CONTEXT_ROOT_KEY, $requri);
    }

    /**
     * Given a filesystem path, returns the virtual path relative to the
     * document root.
     * @param string $path Real path
     * @return string virtual path
     * @access private
     */
    private function _getVirtualPath($rpath)
    {
        $rpath = realpath($rpath);
        $docroot = $this->getDocumentRoot();

        // get index of document root in real path
        //  NOTE: this technically should be zero - why wouldn't it be???
        $idx = strpos($rpath, $docroot);
        if ($idx === false)
            return null;

        // remove document root from real path
        return self::_normalizePath(substr($rpath, $idx + strlen($docroot)));
    }

    /**
     * Ensures all directory separators in the specified path are returned as
     * forward slashes ('/').
     * @param string $path Input path
     * @return string Normalized path
     * @access private
     * @static
     */
    private static function _normalizePath($path)
    {
        return str_replace("\\", '/', $path);
    }

    /**
     * Returns the value of the specified application-scoped attribute.
     * @param string $key Key
     * @return mixed Reference to value
     */
    public function &getAttribute($key)
    {
        // load attributes only as needed
        $this->_loadAttributes();

        $value = null;
        if (array_key_exists($key, $this->_attributeMap))
            $value =& $this->_attributeMap[$key];
        return $value;
    }

    /**
     * Returns the available keys of all application-scoped attributes.
     * @return array Array of keys
     */
    public function getAttributeNames()
    {
        // load attributes only as needed
        $this->_loadAttributes();

        return array_keys($this->_attributeMap);
    }

    /**
     * Sets the value of an application-scoped attribute.
     * @param string $key Key
     * @param mixed $value Reference to value
     */
    public function setAttribute($key, &$value)
    {
        // load attributes only as needed
        $this->_loadAttributes();

        $this->_attributeMap[$key] =& $value;
    }

    /**
     * Removes an application-scoped attribute.
     * @param string $key Key
     */
    public function removeAttribute($key)
    {
        // load attributes only as needed
        $this->_loadAttributes();

        unset($this->_attributeMap[$key]);
    }

    /**
     * Restores the application scope attributes from a data file.  The data
     * file contains a serialized version of the $_attributeMap array.  If the
     * data file does not exist, initializes an empty array
     * @access private
     */
    private function _loadAttributes()
    {
        if (is_null($this->_attributeMap))
        {
            $attrFile = $this->_getAttributeFile();

            if (! file_exists($attrFile))
                // initialize empty array
                $this->_attributeMap = array();
            else
                // load array from file
                $this->_attributeMap = unserialize(file_get_contents($attrFile));
        }
    }

    /**
     * Saves the application scope attributes to a data file.  The data file
     * will contain a serialized version of the $_attributeMap array.  This
     * only gets invoked if there are changes to the attributes.
     */
    private function _saveAttributes()
    {
        file_put_contents($this->_getAttributeFile(),
            serialize($this->_attributeMap));
    }

    /**
     * Returns the name of the file containing the application attributes.  If
     * it is not in the configuration file, the default will be used.
     * @return string Filename
     */
    private function _getAttributeFile()
    {
        /* $af = $this->_config->getAttributeFile();
        if (empty($af))
            $af = self::ATTRIBUTE_FILE; */
        return \phorce\makepath(PHORCE_WORK, self::ATTRIBUTE_FILE);
    }

    // TODO:
    //  - getInitParameter
    //  - getInitParameterNames
    //  - getNamedDispatcher
    //  - getApplicationContextName ???
}

?>
