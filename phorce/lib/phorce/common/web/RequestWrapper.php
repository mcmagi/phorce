<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\web;

use \ArrayAccess;
use \ArrayIterator;
use \Countable;
use \IteratorAggregate;

/**
 * Wraps the request, converting underscores in properties names to periods.
 * This attempts to reverse the damage done by PHP's decision to escape all
 * periods as underscores.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Common
 * @subpackage Web
 */
class RequestWrapper implements AttributeContainer, ArrayAccess, Countable,
    IteratorAggregate
{
    /**
     * The request array being referenced.
     * @var array
     * @access private
     */
    private $_array;

    /**
     * Constructs a request wrapper around the specified array.
     * @param array $array
     */
    public function __construct(array &$array)
    {
        $this->_array =& $array;
    }

    /**
     * Returns the value of the specified attribute.
     * @param string $key Key
     * @return mixed Reference to value
     */
    public function &getAttribute($key)
    {
        return $this->_array[$this->convertPropertyName($name)];
    }

    /**
     * Returns the available keys of all attributes.
     * @return array Array of keys
     */
    public function getAttributeNames()
    {
        $keys = array();
        foreach (array_keys($this->_array) as $key)
            $keys[] = $this->convertPropertyName($key);
        return $keys;
    }

    /**
     * Sets the value of an attribute.
     * @param string $key Key
     * @param mixed $value Reference to value
     */
    public function setAttribute($key, &$value)
    {
        $this->_array[$this->convertPropertyName($name)] = $value;
    }

    /**
     * Removes an attribute.
     * @param string $key Key
     */
    public function removeAttribute($key)
    {
        unset($this->_array[$this->convertPropertyName($name)]);
    }

    /**
     * Returns the value of the specified attribute.
     * @param string $key Key
     * @return mixed Reference to value
     */
    public function offsetGet($key)
    {
        return $this->_array[$this->convertPropertyName($key)];
    }

    /**
     * Sets the value of an attribute.
     * @param string $key Key
     * @param mixed $value Reference to value
     */
    public function offsetSet($key, $value)
    {
        $this->_array[$this->convertPropertyName($key)] = $value;
    }

    /**
     * Returns true if the the specified attribute exists.
     * @param string $key Key
     * @return boolean True if exists
     */
    public function offsetExists($key)
    {
        return isset($this->_array[$this->convertPropertyName($key)]);
    }

    /**
     * Removes an attribute.
     * @param string $key Key
     */
    public function offsetUnset($key)
    {
        unset($this->_array[$this->convertPropertyName($key)]);
    }

    /**
     * Returns the number of items the the array.
     * @param string $key Key
     */
    public function count()
    {
        return count($this->_array);
    }

    /**
     * Return an iterator on the converted array.
     */
    public function getIterator()
    {
        $arr = array();
        foreach ($this->_array as $key => $value)
            $arr[$this->convertPropertyName($key)] = $value;
        return new ArrayIterator($arr);
    }

    /**
     * Attempts to reverse-mangle the property name by replacing underscores
     * with periods.
     * @param string $name PHP-Mangled property name
     * @return string Reverse mangled (sort of) property name
     * @access private
     */
    private function convertPropertyName($name)
    {
        return preg_replace("/_/", ".", $name);
    }
}

?>
