<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\cache;

use \DateTime;
use phorce\PhorceException;

/**
 * An abstract implementation of the PhorceConfigLoader that loads configuration
 * from a file.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
abstract class PhorceConfigFileLoader implements PhorceConfigLoader
{
    /**
     * The config filename.
     * @var string
     * @access private
     */
    private $_filePath;

    /**
     * Constructs the loader with the specified config filename.
     * @param string $file The config filename
     * @param string $dir The config directory in which to search
     */
    protected function __construct($file)
    {
        // make sure it exists
        if (! file_exists($file))
            throw new PhorceException("Cannot find configuration file '$file'");

        $this->_filePath = $file;
    }

    /**
     * Returns the configuration file path.
     * @return string File path
     */
    protected function getFilePath()
    {
        return $this->_filePath;
    }

    /**
     * Returns whether the configuration has changed and the cache will
     * need to be refreshed.
     * @param object DateTime $lastLoadTime The last time the config was loaded
     * @return boolean
     */
    public function hasConfigChanged(DateTime $lastLoadTime)
    {
        return $lastLoadTime->getTimestamp() < filemtime($this->_filePath);
    }
}

?>
