<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\cache;

/**
 * An implementation of PhorceConfigLoader that loads configuration from an
 * XML file using Fusion.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class FusionDAOConfigFileLoader extends PhorceConfigFileLoader
{
    /**
     * The DAO class used to load the config.
     * @var string
     * @access private
     */
    private $_daoName;

    /**
     * Whether to schema-validate the config file.
     * @var boolean
     * @access private
     */
    private $_validate;

    /**
     * Constructs a loader for the specified file using the specified DAO.
     * @param string $file XML Configuration file
     * @param string $daoName The FusionDAO class name
     * @param boolean $validate Whether to validate the config file
     */
    public function __construct($file, $daoName, $validate = true)
    {
        parent::__construct($file);
        $this->_daoName = $daoName;
        $this->_validate = $validate;
    }

    /**
     * Loads the configuration from the config file.
     * @return mixed Loaded configuration object
     */
    public function loadConfig()
    {
        // instantiate the dao
        $dao = new $this->_daoName($this->getFilePath(), $this->_validate);

        // and load
        return $dao->load();
    }
}

?>
