<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\cache;

use \DateTime;
use phorce\common\property\PropertyObject;

/**
 * Represents an entry of Phorce configuration information in the config cache.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhorceConfigCacheEntry extends PropertyObject
{
    /**
     * The key the object is stored with.
     * @var string
     * @access private
     */
    private $_key;

    /**
     * Object being stored.
     * @var mixed
     * @access private
     */
    private $_dataObject;

    /**
     * Time of storage.
     * @var Datetime
     * @access private
     */
    private $_storageTime;

    /**
     * Constructs an instance of cached config data.
     * @param string $key The key object is stored with
     * @param mixed $dataObject Object to be stored
     * @param object DateTime $storeTime Time of storage
     */
    public function __construct($key, $dataObject)
    {
        $this->_key = $key;
        $this->_dataObject = $dataObject;
        $this->_storageTime = new DateTime();
    }

    /**
     * Returns the key the object was stored with.
     * @return string Key
     */
    public function getKey()
    {
        return $this->_key;
    }

    /**
     * Returns the stored object.
     * @return mixed Stored object
     */
    public function getDataObject()
    {
        return $this->_dataObject;
    }

    /**
     * Returns the time of storage.
     * @return object DateTime Time of storage
     */
    public function getStorageTime()
    {
        return $this->_storageTime;
    }
}

?>
