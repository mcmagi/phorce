<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\cache;

use \DateTime;

/**
 * An interface used to load a configuration file.  This is used by the
 * PhorceConfigCache when it needs to load configuration data into the cache.
 * It is designed as an interface so there may be an implementation-specific
 * means of loading the configuration.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Common
 * @subpackage Cache
 */
interface PhorceConfigLoader
{
    /**
     * Returns whether the configuration has changed and the cache will
     * need to be refreshed.
     * @param object DateTime $lastLoadTime The last time the config was loaded
     * @return boolean
     */
    public function hasConfigChanged(DateTime $lastLoadTime);

    /**
     * Loads the configuration.
     * @return mixed Loaded configuration object
     */
    public function loadConfig();
}

?>
