<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\common\cache;

use \ArrayObject;
use phorce\PhorceException;
use phorce\common\web\ApplicationContext;

/**
 * Represents a cache of Phorce configuration information.  The cache is stored
 * in application-scoped data.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhorceConfigCache extends ArrayObject
{
    const CONFIG_PARAM = "phorce.config.cache";

    /**
     * Retrieves config data from the config cache with the specified key.
     * @param string $key The key for the configuration data
     * @param object PhorceConfigLoader $loader For config loading callback
     * @return mixed The configuration object
     * @static
     */
    public static function retrieveConfig($key, PhorceConfigLoader $loader)
    {
        $dataobj = null;

        // load config info from cache
        $ctx = ApplicationContext::getInstance();
        $cache = $ctx->getAttribute(self::CONFIG_PARAM);

        if (! $cache instanceof PhorceConfigCache)
            $cache = new PhorceConfigCache();

        if (isset($cache[$key]))
        {
            $entry = $cache[$key];
            // check if file has been updated since last cached
            if (! $loader->hasConfigChanged($entry->storageTime))
                $dataobj = $entry->dataObject;
        }

        if (is_null($dataobj))
        {
            // no data object loaded - load config
            $dataobj = $loader->loadConfig();
            if (is_null($dataobj))
                throw new PhorceException("Null configuration data returned from file '$file'");

            // store cache entry
            $cache[$key] = new PhorceConfigCacheEntry($key, $dataobj);
            $ctx->setAttribute(self::CONFIG_PARAM, $cache);
        }

        return $dataobj;
    }
}

?>
