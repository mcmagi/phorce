<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce;

/**
 * This is an exception which represents a file that is not found.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Reaction
 */
class NotFoundException extends PhorceException
{
    private $_file;

    /**
     * Constructs the exception.
     * @param string $file The missing file
     */
    public function __construct($file)
    {
        parent::__construct("Not found: $file");

        $this->_file = $file;
    }

    public function getFilename()
    {
        return $this->_file;
    }
}

?>
