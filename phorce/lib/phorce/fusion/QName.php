<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion;

use \DOMElement;
use phorce\common\property\PropertyObject;

/**
 * Represents a namespace-qualified name in the schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class QName extends PropertyObject
{
    /**
     * The namespace URI of the schema element.
     * @var string
     * @access private
     */
    private $_namespace;

    /**
     * The name of the schema element.
     * @var string
     * @access private
     */
    private $_name;

    /**
     * Constructs a QName object.
     * @param string $name XML object name
     * @param string $namespace XML namespace
     */
    public function __construct($name = null, $namespace = null)
    {
        $this->_namespace = $namespace;
        $this->_name = $name;
    }

    /**
     * Constructs a QName object using the specified DOM element and
     * prefix-qualified name.
     * the specified name.
     * @param object DOMElement $e A DOM element (for namespace lookups)
     * @param string $name Prefixed-qualified name
     * @return object QName
     * @static
     */
    public static function createFromDOM(DOMElement $e = null, $name = null)
    {
        $qname = new QName();

        // empty qname
        if (is_null($e))
            return;

        // default to element tag name if no name given
        if (is_null($name))
            $name = $e->tagName;

        if (strpos($name, ":") !== false)
        {
            // colonized: ask DOM to lookup URI from prefix
            list($prefix, $qname->_name) = explode(":", $name);
            $qname->_namespace = $e->lookupNamespaceURI($prefix);
            //echo $qname->_namespace . "\n";
        }
        else
        {
            // non-colonized: default to schema target namespace
            $qname->_name = $name;
            $qname->_namespace = $e->lookupNamespaceURI(null);
            //echo $qname->_namespace . "\n";
        }

        return $qname;
    }

    /**
     * Returns the namespace URI of the schema element.
     * @return string Namespace URI
     */
    public function getNamespace()
    {
        return $this->_namespace;
    }

    /**
     * Returns the name of the schema element.
     * @return string Name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Returns a string representation of the qualified name tuple.
     * @return string Qualified name
     */
    public function __toString()
    {
        return "({$this->_namespace},{$this->_name})";
    }
}

?>
