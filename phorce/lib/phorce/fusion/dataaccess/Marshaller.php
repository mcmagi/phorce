<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\dataaccess;

use \DOMDocument;
use \DOMDocumentFragment;
use \DOMElement;
use \DOMNode;
use phorce\common\property\PropertyObject;
use phorce\fusion\QName;
use phorce\fusion\FusionException;
use phorce\fusion\mapper\XmlDocumentWriter;

/**
 * The Fusion Marshaller is used to marshal XML to various types of resources.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class Marshaller extends PropertyObject
{
    /**
     * The associated FusionContext which created this Marshaller.
     * @var object FusionContext
     * @access private
     */
    private $_ctx;

    /**
     * The object being marshalled.
     * @var object
     * @access private
     */
    private $_object;

    /**
     * An optional root element name to marshal to.
     * @var string
     * @access private
     */
    private $_root;

    /**
     * The encoding used in the XML declaration.
     * @var string
     */
    private $_encoding = 'UTF-8';

    /**
     * Whether the marshaller will format XML output.
     * @var boolean
     */
    private $_formatted = false;

    /**
     * The no-namespace schema location.
     * @var string
     */
    private $_noNamespaceSchemaLocation;

    /**
     * Associative array of namespace prefixes keyed by namespace URIs.
     * @var array
     */
    private $_nsPrefixMap = array();

    /**
     * Constructs an Marshaller for the specified FusionContext.  A root element
     * name may be optionally provided which will ensure that XML will be
     * marshalled into that element.
     * @param object FusionContext $ctx Associated FusionContext
     * @param mixed $object Object to marshal
     * @param string $root Root element name to marshal to
     */
    public function __construct(FusionContext $ctx, $object, $root = null)
    {
        $this->_ctx = $ctx;
        $this->_object = $object;
        $this->_root = $root;
    }

    /**
     * Returns the encoding used in the XML declaration.  Default is UTF-8.
     * @return string Encoding
     */
    public function getEncoding()
    {
        return $this->_encoding;
    }

    /**
     * Sets the encoding used in the XML declaration.
     * @return boolean Encoding
     */
    public function setEncoding($v)
    {
        $this->_encoding = $v;
    }

    /**
     * Returns whether the marshaller will format XML output.  Default is false.
     * @return boolean
     */
    public function isFormatted()
    {
        return $this->_formatted;
    }

    /**
     * Sets whether the marshaller will format XML output.
     * @param boolean $v
     */
    public function setFormatted($v)
    {
        $this->_formatted = $v;
    }

    /**
     * Returns the schema location for elements without a namespace.
     * @return string
     */
    public function getNoNamespaceSchemaLocation()
    {
        return $this->_noNamespaceSchemaLocation;
    }

    /**
     * Sets the schema location for elements without a namespace.
     * @param string $v
     */
    public function setNoNamespaceSchemaLocation($v)
    {
        $this->_noNamespaceSchemaLocation = $v;
    }

    /**
     * Assigns a prefix to use for the specified namespace URI.  If not
     * specified, the Marshaller will use the prefix "nsX" where X is a number.
     * @param string $namespace Namespace URI
     * @param string $prefix Prefix
     */
    public function setNamespacePrefix($namespace, $prefix)
    {
        $this->_nsPrefixMap[$namespace] = $prefix;
    }

    /**
     * Returns the prefix to use for the specified namespace URI.
     * @param string $namespace Namespace URI
     * @return string Prefix
     */
    public function getNamespacePrefix($namespace)
    {
        return $this->_nsPrefixMap[$namespace];
    }

    /**
     * Marshals XML to a file.
     * @param string $file Filename
     */
    public function toFile($file)
    {
        // marshal to a DOM document and save to file
        $doc = $this->_marshal()->save($file);
    }

    /**
     * Marshals XML to a string.
     * @return string
     */
    public function toString()
    {
        // marshal to a DOM document and return as string
        return $this->_marshal()->saveXML();
    }

    /**
     * Marshals XML to a DOMNode.  The DOMNode must be a DOMElement,
     * DOMDocument, DOMDocumentFragment.
     * @param object DOMNode $node DOMElement, DOMDocument, DOMDocumentFragment
     */
    public function fromDOM(DOMNode $node)
    {
        $doc = $this->_marshal();

        if ($node instanceof DOMElement || $node instanceof DOMDocument ||
            $node instanceof DOMDocumentFragment)
        {
            // append as child of passed-in document
            $e = $node->ownerDocument->importNode($doc->documentElement, true);
            $node->appendChild($e);
        }
        else
            throw new FusionException("DOMNode is of type '" . get_class($node) . "'.  It must be either a DOMDocument, DOMDocumentFragment, or DOMElement");
    }

    /**
     * Marshals XML to a DOMDocument.
     * @return object DOMDocument
     * @access private
     */
    private function _marshal()
    {
        // create root qname if root name was given
        $qname = null;
        if (! is_null($this->_root))
            $qname = new QName($this->_root, $this->_ctx->namespaceConfig->namespace);

        // perform marshalling
        //echo "marshalling: " . get_class($this->_object) . "\n";
        $writer = new XmlDocumentWriter($this->_nsPrefixMap,
            $this->_noNamespaceSchemaLocation);
        $doc = $writer->mapRootObject($this->_object, $qname);

        // validate against schema
        /* if ($this->_ctx->validate)
        {
            $schema = $this->_ctx->namespaceConfig->resolvedSchemaLocation;
            //echo "validating against schema {$schema}\n";
            $doc->schemaValidate($schema);
        } */
        // FIXME - DOM bug: validation of an unsaved document fails!

        // configure doc before returning
        $doc->encoding = $this->_encoding;
        $doc->formatOutput = $this->_formatted;

        return $doc;
    }
}

?>
