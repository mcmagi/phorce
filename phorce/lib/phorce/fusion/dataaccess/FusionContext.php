<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\dataaccess;

use phorce\common\property\PropertyObject;
use phorce\fusion\registry\SchemaNamespaceConfig;
use phorce\fusion\registry\SchemaRegistry;

/**
 * The FusionContext provides a means to retrieve and persist data objects in a
 * particular XML namespace.  The two primary methods are {@link #load()}, which
 * retrieves a data object hierarchy from an XML document, and {@link $save()}
 * which persists a hierarchy to an XML document.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class FusionContext extends PropertyObject
{
    /**
     * The configuration for the XML namespace this FusionContext will bind
     * objects for.
     * @var object SchemaNamespaceConfig
     * @access private
     */
    private $_namespaceConfig;

    /**
     * Whether or not to validate the file.
     * @var boolean
     * @access private
     */
    private $_validate;

    /**
     * Constructs the FusionContex for the specified namespace and XML schema.
     * @param object SchemaNamespaceConfig $nscfg XML namespace configuration
     * @param boolean $validate Whether to validate the XML content
     */
    private function __construct(SchemaNamespaceConfig $nscfg, $validate = true)
    {
        $this->_namespaceConfig = $nscfg;
        $this->_validate = $validate;
    }

    /**
     * Creates a FusionContext to handle objects in the specified namespace.
     * @param string $namespace Namespace URI
     * @param boolean $validate Whether to validate the XML content
     * @return object FusionContext
     */
    public function createForNamespace($namespace, $validate = true)
    {
        $registry = SchemaRegistry::getInstance();
        $nscfg = $registry->lookupNamespace($namespace);
        return new FusionContext($nscfg, $validate);
    }

    /**
     * Creates a FusionContext to handle objects for the specified schema.
     * @param string $schemaLocation Location of schema file
     * @param boolean $validate Whether to validate the XML content
     * @return object FusionContext
     */
    public function createForSchema($schemaLocation, $validate = true)
    {
        $registry = SchemaRegistry::getInstance();
        $schema = $registry->findSchemaByLocation($schemaLocation);
        return self::createForNamespace($schema->targetNamespace, $validate);
    }

    /**
     * Returns the XML namespace configuration fusion context will bind objects
     * for.
     * @return string XML namespace
     */
    public function getNamespaceConfig()
    {
        return $this->_namespaceConfig;
    }

    /**
     * Returns whether or not this context will schema-validate the XML content.
     * @return boolean
     */
    public function isValidate()
    {
        return $this->_validate;
    }

    /**
     * Returns an Unmarshaller which will read, unmarsal, and bind data from an
     * XML data source.  If a class name is specified, the Unmarshaller will
     * attempt to unmarshal to the specified class.  If no class name is
     * specified, the Unmarshaller will determine the class to unmarshal to
     * based on the namespace of the root element in the document.  If it cannot
     * be determined, an exception will be thrown.
     * @param string $class Optional class name to unmarshal to
     * @return object Unmarshaller
     */
    public function load($class = null)
    {
        return new Unmarshaller($this, $class);
    }

    /**
     * Returns a Marhaller which will bind data from the specified object,
     * marshal, and output to an XML data source.  If a root element name is
     * specified, the Marshaller will use the specified name for the root
     * element.  If no root element name is specified, the Marshaller will
     * use the root element name specified in the class binding.  If none is
     * found, an exception will be thrown.
     * @param mixed $object The object to marshal to XML
     * @param string $root Root element name
     * @return object Marshaller
     */
    public function save($object, $root = null)
    {
        return new Marshaller($this, $object, $root);
    }
}

?>
