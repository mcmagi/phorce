<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\dataaccess;

use \DOMDocument;
use \DOMElement;
use \DOMNode;
use phorce\fusion\FusionException;
use phorce\fusion\QName;
use phorce\fusion\mapper\XmlDocumentReader;
use phorce\fusion\registry\XmlBindingRegistry;

/**
 * The Fusion Unmarshaller is used to unmarshal XML from various types of
 * sources.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class Unmarshaller
{
    /**
     * The associated FusionContext which created this Unmarshaller.
     * @var object FusionContext
     * @access private
     */
    private $_ctx;

    /**
     * An optional class name to unmarshal to.
     * @var string
     * @access private
     */
    private $_class;

    /**
     * Constructs an Unmarshaller for the specified FusionContext.  A class name
     * may be optionally provided which will ensure that XML will be
     * unmarshalled into that class.
     * @param object FusionContext $ctx Associated FusionContext
     * @param string $class Class name to unmarshal to
     */
    public function __construct(FusionContext $ctx, $class = null)
    {
        $this->_ctx = $ctx;
        $this->_class = $class;
    }

    /**
     * Unmarshals XML from a file.
     * @param string $file Filename
     * @return object
     */
    public function fromFile($file)
    {
        // read XML file with DOM
        //echo "unmarshalling file {$file}\n";
        $doc = DOMDocument::load($file);

        return $this->_unmarshal($doc);
    }

    /**
     * Unmarshals XML from a string.
     * @param string $str XML string
     * @return object
     */
    public function fromString($str)
    {
        // read XML string with DOM
        $doc = DOMDocument::loadXML($str);

        return $this->_unmarshal($doc);
    }

    /**
     * Unmarshals XML from a DOMNode.  The DOMNode must be a DOMElement or
     * DOMDocument.
     * @param object DOMNode $node DOMElement or DOMDocument
     * @return object
     */
    public function fromDOM(DOMNode $node)
    {
        if ($node instanceof DOMElement)
        {
            // create document just for the section to be unmarshalled
            $doc = new DOMDocument();
            $doc->documentElement = $doc->importNode($e, true);
        }
        elseif ($node instanceof DOMDocument)
            $doc = $node;
        else
            throw new FusionException("DOMNode is of type '" . get_class($node) . "'.  It must be either a DOMDocument or DOMElement");

        return $this->_unmarshal($doc);
    }

    /**
     * Unmarshals XML from the specified DOMDocument.
     * @param object DOMDocument $doc
     * @return object
     * @access private
     */
    private function _unmarshal(DOMDocument $doc)
    {
        // validate against schema
        if ($this->_ctx->validate)
        {
            $schema = $this->_ctx->namespaceConfig->resolvedSchemaLocation;
            //echo "validating against schema {$schema}\n";
            $doc->schemaValidate($schema);
        }

        // get binding factory
        $bfactory = XmlBindingRegistry::getInstance();

        if (is_null($this->_class))
        {
            // class not given explicitly to unmarshaller
            // so we will need to derive class from qname of root element
            $qname = QName::createFromDOM($doc->documentElement);

            // locate class bindings by root qname
            $cbin = $bfactory->findByRoot($qname);
            if (is_null($cbin))
                throw new FusionException("No binding found for root element {$qname}");
        }
        else
        {
            // class given explicity to unmarshaller
            // so locate class bindings by class name
            $cbin = $bfactory->findByClass($this->_class);
            if (is_null($cbin))
                throw new FusionException("No binding found for class '{$this->_class}'");
        }

        // read the xml document and map to an object
        //echo "unmarshalling: " . $cbin->name . "\n";
        $reader = new XmlDocumentReader($doc);
        return $reader->mapDocument($cbin);
    }
}

?>
