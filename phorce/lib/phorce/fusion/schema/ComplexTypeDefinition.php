<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \ArrayObject;
use \DOMElement;
use phorce\fusion\Constants;

/**
 * A complex type definition represents a ComplexType element in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class ComplexTypeDefinition extends DataTypeDefinitionBase
    implements AttributeContainer, ModelGroupContainer
{
    const ELEM_COMPLEX_CONTENT = "complexContent";
    const ELEM_SIMPLE_CONTENT = "simpleContent";
    const ELEM_RESTRICTION = "restriction";
    const ELEM_EXTENSION = "extension";
    const ATTR_BASE = "base";
    const ATTR_MIXED = "mixed";

    /**
     * The contained model group definition.
     * @var object ModelGroupDefinition
     * @access private
     */
    private $_modelGroup;

    /**
     * The contained group definition.
     * @var object GroupDefinition
     * @access private
     */
    private $_group;

    /**
     * Whether this type contains complex content.  Default is true.
     * @var boolean
     * @access private
     */
    private $_complexContent = true;

    /**
     * Whether this type contains mixed content.
     * @var boolean
     * @access private
     */
    private $_mixed = false;

    /**
     * Array of AttributeDefinition objects
     * @var object ArrayObject
     * @access private
     */
    private $_attributes;

    /**
     * Array of AttributeGroupDefinition objects
     * @var object ArrayObject
     * @access private
     */
    private $_attributeGroups;

    /**
     * Any attribute definition.
     * @var object AnyAttributeDefinition
     * @access private
     */
    private $_anyAttribute;

    /**
     * Creates a complex type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        // determine if mixed
        if ($e->hasAttribute(self::ATTR_MIXED))
            $this->_mixed = $e->getAttribute(self::ATTR_MIXED) === "true";

        $this->_attributes = new ArrayObject();
        $this->_attributeGroups = new ArrayObject();

        // get elements and attributes (if no complex/simple content)
        $this->_extractModelGroupInfo($e);
        $this->_extractAttributeInfo($e);

        // get base type (and elements/attributes if complex/simple content)
        $this->_extractBaseTypeInfo($e);
    }

    /**
     * Extracts all elements from the specified DOMElement.  The elements are
     * saved in the $_elements array.  The DOMElement must represent a grouping
     * of elements (e.g. complexType, sequence, etc).
     * @param object DOMElement $elem
     * @access private
     */
    private function _extractModelGroupInfo(DOMElement $elem)
    {
        foreach ($this->getChildDOMElements($elem) as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case SchemaDefinition::ELEM_GROUP;
                $this->_group = new GroupDefinition($this, $e);
                break;
            case ModelGroupDefinition::ELEM_SEQUENCE:
            case ModelGroupDefinition::ELEM_CHOICE:
            case ModelGroupDefinition::ELEM_ALL:
                //echo "{$this->name}: model group: {$qname->name}\n";
                $this->_modelGroup = new ModelGroupDefinition($this, $e);
            }
        }
    }

    /**
     * Extracts the base type from the specified DOMElement.  The base type is
     * saved in the $_base member variable.  The DOMElement must represent a
     * container of content.  (e.g. complexType, simpleContent, complexContent)
     * @param object DOMElement $elem
     * @access private
     */
    private function _extractBaseTypeInfo(DOMElement $elem)
    {
        foreach ($this->getChildDOMElements($elem) as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case self::ELEM_SIMPLE_CONTENT:
                $this->_complexContent = false;
                // continue
            case self::ELEM_COMPLEX_CONTENT:
                $this->_extractBaseTypeInfo($e);

                // complexContent can override mixed setting
                if ($e->hasAttribute(self::ATTR_MIXED))
                    $this->_mixed = $e->getAttribute(self::ATTR_MIXED) === "true";

                break;
            case self::ELEM_RESTRICTION:
            case self::ELEM_EXTENSION:
                $this->_base = $e->getAttribute(self::ATTR_BASE);

                // extension/restriction contains complex type information
                if ($this->_complexContent)
                {
                    $this->_extractModelGroupInfo($e);
                    $this->_extractAttributeInfo($e);
                }
            }
        }
    }

    /**
     * Extracts all attributes from the specified DOMElement.  The attributes
     * are saved in the $_attributes array.  The DOMElement must represent a
     * grouping of attributes (e.g. complexType or attributeGroup).
     * @param object DOMElement $elem
     * @access private
     */
    private function _extractAttributeInfo(DOMElement $elem)
    {
        foreach (self::getChildDOMElements($elem) as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case SchemaDefinition::ELEM_ATTRIBUTE:
                $this->_attributes[] = new AttributeDefinition($this, $e);
                break;
            case SchemaDefinition::ELEM_ATTRIBUTE_GROUP:
                $this->_attributeGroups[] = new AttributeGroupDefinition($this, $e);
                break;
            case self::ELEM_ANY_ATTRIBUTE:
                $this->_anyAttribute = new AnyAttributeDefinition($this, $e);
            }
        }
    }

    /**
     * Returns the model group definition.
     * @return object ModelGroupDefinition
     */
    public function getModelGroup()
    {
        return $this->_modelGroup;
    }

    /**
     * Returns the group definition.
     * @return object GroupDefinition
     */
    public function getGroup()
    {
        return $this->_group;
    }

    /**
     * Returns all attribute objects.
     * @return object ArrayObject Array of AttributeDefinition objects
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * Returns all attribute group objects.
     * @return object ArrayObject Array of AttributeGroupDefinition objects
     */
    public function getAttributeGroups()
    {
        return $this->_attributeGroups;
    }

    /**
     * Returns an AnyAttribute definition.
     * @return object AnyAttribute
     */
    public function getAnyAttribute()
    {
        return $this->_anyAttribute;
    }

    /**
     * Returns true if the type contains mixed content.
     * @return boolean True if mixed
     */
    public function isMixed()
    {
        return $this->_mixed;
    }

    /**
     * Returns true if there is no name for this type.
     * @return boolean True if anonymous
     */
    public function isAnonymous()
    {
        return ! strlen($this->name);
    }

    /**
     * Returns whether this type contains complex content.  Default is true.
     * @return boolean True if complex content
     */
    public function isComplexContent()
    {
        return $this->_complexContent;
    }

    /**
     * Returns whether this type contains simple content.  Default is true.
     * @return boolean True if simple content
     */
    public function isSimpleContent()
    {
        return ! $this->_complexContent;
    }

    /**
     * Gets the class name this type will be referred to in Fusion-generated
     * classes.
     * @return string Class name
     */
    public function getClassName()
    {
        if ($this->anonymous)
        {
            // use parent defs to infer class names for anonymous types
            $elem = $this->_parentDef;
            if ($elem->_parentDef->_parentDef instanceof SchemaDefinition)
                // element defined in schema - use element name for class
                return ucfirst($this->_parentDef->propertyName);
            else
                // element defined in another type - use type_element name
                return $this->_parentDef->parentType->className . "_" .
                    ucfirst($this->_parentDef->propertyName);
        }
        else
        {
            // split by space, dash, or underscore delimited word
            // and capitalize the first letter of each word
            $converted = "";
            foreach (preg_split('/[-_ ]/', $this->name) as $v)
                $converted .= ucfirst($v);
            return $converted;
        }
    }
}

?>
