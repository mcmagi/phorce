<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \ArrayObject;
use \DOMElement;
use \DOMDocument;
use phorce\fusion\Constants;
use phorce\fusion\registry\DefaultSchemaResolver;
use phorce\fusion\registry\SchemaRegistry;

/**
 * A schema definition represents a Schema element in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class SchemaDefinition extends DefinitionBase
{
    const ELEM_COMPLEX_TYPE = 'complexType';
    const ELEM_SIMPLE_TYPE = 'simpleType';
    const ELEM_ATTRIBUTE = 'attribute';
    const ELEM_ELEMENT = 'element';
    const ELEM_ATTRIBUTE_GROUP = 'attributeGroup';
    const ELEM_GROUP = 'group';
    const ELEM_INCLUDE = 'include';
    const ELEM_REDEFINE = 'redefine';
    const ELEM_IMPORT = 'import';
    const ATTR_TARGET_NAMESPACE = 'targetNamespace';
    const ATTR_ELEMENT_FORM_DEFAULT = 'elementFormDefault';
    const ATTR_ATTRIBUTE_FORM_DEFAULT = 'attributeFormDefault';
    const ATTR_SCHEMA_LOCATION = 'schemaLocation';
    const ATTR_NAMESPACE = 'namespace';
    const FORM_QUALIFIED = 'qualified';
    const FORM_UNQUALIFIED = 'unqualified';

    /**
     * The DOM Document object.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;

    /**
     * Array of complex type definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_complexTypes;

    /**
     * Array of simple type definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_simpleTypes;

    /**
     * Associative array of type names to type definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_typeMap;

    /**
     * Array of top-level element definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_elements;

    /**
     * Array of top-level attribute definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_attributes;

    /**
     * Array of attribute group definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_attributeGroups;

    /**
     * Array of group definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_groups;

    /**
     * Array of schema definitions.  Includes this schema and imported schemas.
     * @var object ArrayObject
     * @access private
     */
    private $_imports;

    /**
     * Location where schema was loaded.
     * @var string
     * @access private
     */
    private $_schemaLocation;

    /**
     * Constructs a schema definition for the specified XSD file.
     * @param string $xsd XSD file
     */
    public function __construct($xsd)
    {
        $this->_schemaLocation = $xsd;

        $this->_doc = new DOMDocument();
        $this->_doc->load($this->_schemaLocation);

        parent::__construct($this, $this->_doc->documentElement);

        $this->_complexTypes = new ArrayObject();
        $this->_simpleTypes = new ArrayObject();
        $this->_attributes = new ArrayObject();
        $this->_elements = new ArrayObject();
        $this->_groups = new ArrayObject();
        $this->_attributeGroups = new ArrayObject();
        $this->_imports = new ArrayObject();

        foreach ($this->getDOMElements() as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case self::ELEM_COMPLEX_TYPE:
                $this->_complexTypes[] = new ComplexTypeDefinition($this, $e);
                break;
            case self::ELEM_SIMPLE_TYPE:
                $this->_simpleTypes[] = new SimpleTypeDefinition($this, $e);
                break;
            case self::ELEM_ATTRIBUTE:
                $a = new AttributeDefinition($this, $e);
                $this->_attributes[$a->name] = $a;
                break;
            case self::ELEM_ELEMENT:
                $el = new ElementDefinition($this, $e);
                $this->_elements[$el->name] = $el;
                break;
            case self::ELEM_ATTRIBUTE_GROUP:
                $ag = new AttributeGroupDefinition($this, $e);
                $this->_attributeGroups[$ag->name] = $ag;
                break;
            case self::ELEM_GROUP:
                $g = new GroupDefinition($this, $e);
                $this->_group[$g->name] = $g;
                break;
            case self::ELEM_INCLUDE:
                $this->_includeSchema($e);
                break;
            case self::ELEM_REDEFINE:
                $this->_redefineSchema($e);
                break;
            case self::ELEM_IMPORT:
                $this->_importSchema($e);
                break;
            }
        }

        // build map of types by name
        $this->_typeMap = new ArrayObject();
        foreach ($this->_simpleTypes as $v)
            $this->_typeMap[$v->name] = $v;
        foreach ($this->_complexTypes as $v)
            $this->_typeMap[$v->name] = $v;
    }

    /**
     * Includes the schema in the specified xsd:include DOM Element.
     * @param object DOMElement $e Include DOM element
     * @access private
     */
    private function _includeSchema(DOMElement $e)
    {
        $schema = $this->_loadSchema($e);

        // include all its types and objects in this namespace
        foreach ($schema->_elements as $v)
            $this->_elements[] = $v;
        foreach ($schema->_attributes as $v)
            $this->_attributes[] = $v;
        foreach ($schema->_groups as $v)
            $this->_groups[] = $v;
        foreach ($schema->_attributeGroups as $v)
            $this->_attributeGroups[] = $v;
        foreach ($schema->_complexTypes as $v)
            $this->_complexTypes[] = $v;
        foreach ($schema->_simpleTypes as $v)
            $this->_simpleTypes[] = $v;
        foreach ($schema->_typeMap as $v)
            $this->_typeMap[] = $v;

        // Note that despite how it looks, the included $schema object doesn't
        // get entirely discarded here.  It's still referenced by all of the
        // included objects and types.  This is important since we do not want
        // to include its imports for our schema to see.  Nor do we want its
        // types and objects to be able to see our types and objects.

        // keep associtive array of child schemas
        $this->_includes[$schema->_schemaLocation] = 1;
        foreach ($schema->_includes as $k => $v)
            $this->_includes[$k] = 1;
    }

    /**
     * Imports the schema in the specified xsd:import DOM Element.
     * @param object DOMElement $e Import DOM element
     * @access private
     */
    private function _importSchema(DOMElement $e)
    {
        $schema = $this->_loadSchema($e);

        $namespace = $e->getAttribute(self::ATTR_NAMESPACE);

        if ($schema->targetNamespace != $namespace)
            throw new SchemaException(self::getElementQName($e), "Namespace '{$schema->targetNamespace}' of imported schema '$file' does not match namespace '$namespace' of import element.");

        // add to list of imported schemas
        $this->_addSchema($schema);
    }

    /**
     * Redefines elements from the schema in the specified xsd:redefine DOM
     * Element.
     * @param object DOMElement $e Redefine DOM element
     * @access private
     */
    private function _redefineSchema(DOMElement $e)
    {
        $schema = $this->_loadSchema($e);

        // TODO
    }

    private function _loadSchema(DOMElement $e)
    {
        // load the schema
        $file = $e->getAttribute(self::ATTR_SCHEMA_LOCATION);

        $registry = SchemaRegistry::getInstance();
        return $registry->findSchemaByLocation($file,
            new DefaultSchemaResolver(dirname($this->_schemaLocation)));
    }

    /**
     * Adds a schema definition to the list of schemas, keyed by target
     * namespace.
     * @param object SchemaDefinition $schema
     * @access private
     */
    private function _addSchema(SchemaDefinition $schema)
    {
        $this->_imports[$schema->targetNamespace] = $schema;
    }

    /**
     * Returns an array of Element definitions.
     * @return object ArrayObject Array of Element definitions
     */
    public function getElements()
    {
        return $this->_elements;
    }

    /**
     * Returns an array of Attribute definitions.
     * @return object ArrayObject Array of Attribute definitions
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * Returns an array of AttributeGroup definitions.
     * @return object ArrayObject Array of AttributeGroup definitions
     */
    public function getAttributeGroups()
    {
        return $this->_attributeGroups;
    }

    /**
     * Returns an array of Group definitions.
     * @return object ArrayObject Array of Group definitions
     */
    public function getGroups()
    {
        return $this->_groups;
    }

    /**
     * Returns all complexType objects.
     * @return object ArrayObject Array of ComplexTypeDefinition objects
     */
    public function getComplexTypes()
    {
        return $this->_complexTypes;
    }

    /**
     * Returns all simpleType objects.
     * @return object ArrayObject Array of SimpleTypeDefinition objects
     */
    public function getSimpleTypes()
    {
        return $this->_simpleTypes;
    }

    /**
     * Returns the list of all schemas imported by this schema.
     * @return object ArrayObject Array of SchemaDefinition objects
     */
    public function getImports()
    {
        return $this->_imports;
    }

    /**
     * Returns the target namespace URI of this XSD.
     * @return string Namespace URI
     */
    public function getTargetNamespace()
    {
        return $this->_getAttribute(self::ATTR_TARGET_NAMESPACE);
    }

    /**
     * Returns true if the nested elements should be namespace qualified.
     * Defaults to false.
     * @return boolean True if qualified
     */
    public function isElementQualified()
    {
        return $this->_getAttribute(self::ATTR_ELEMENT_FORM_DEFAULT, self::FORM_UNQUALIFIED) == self::FORM_QUALIFIED;
    }

    /**
     * Returns true if the nested attributes should be namespace qualified.
     * Defaults to false.
     * @return boolean True if qualified
     */
    public function isAttributeQualified()
    {
        return $this->_getAttribute(self::ATTR_ATTRIBUTE_FORM_DEFAULT, self::FORM_UNQUALIFIED) == self::FORM_QUALIFIED;
    }

    /**
     * Returns the schema with the specified namespace.
     * @param string $typeName Name of type
     * @return object SchemaDefinition Schema
     */
    public function findSchema($namespace)
    {
        if ($namespace == $this->targetNamespace)
            return $this;
        elseif ($this->_imports->offsetExists($namespace))
            return $this->_imports[$namespace];

        return null;
    }

    /**
     * Returns the type with the specified name.
     * @param string $typeName Name of type
     * @return object DataTypeDefinitionBase Type
     */
    public function findType($typeName)
    {
        if (! isset($this->_typeMap[$typeName]))
            return null;
        return $this->_typeMap[$typeName];
    }

    /**
     * Returns true if the specified schema was included.
     */
    public function hasInclude($schemaLocation)
    {
        return isset($this->_include[$schemaLocation]);
    }
}

?>
