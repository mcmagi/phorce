<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;

/**
 * An element definition represents an Element element in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class ElementDefinition extends DataObjectDefinitionBase
    implements Particle
{
    const ATTR_NILLABLE = "nillable";

    /**
     * Creates an element definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e Element DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        if ($pdef instanceof SchemaDefinition)
        {
            // elements that cannot be present if schema is our parent
            $this->_validateNonPresence(self::ATTR_MAX_OCCURS, self::REASON_SCHEMA);
            $this->_validateNonPresence(self::ATTR_MIN_OCCURS, self::REASON_SCHEMA);
        }
        elseif ($this->reference)
        {
            // elements that cannot be present if schema is a reference
            $this->_validateNonPresence(self::ATTR_NILLABLE, self::REASON_REF);
        }
    }

    /**
     * Returns the maxOccurs attribute.  Default is 1.
     * @return integer Max occurs
     */
    public function getMaxOccurs()
    {
        return $this->_getAttribute(self::ATTR_MAX_OCCURS, 1);
    }

    /**
     * Returns the minOccurs attribute.  Default is 1.
     * @return integer Min occurs
     */
    public function getMinOccurs()
    {
        return $this->_getAttribute(self::ATTR_MIN_OCCURS, 1);
    }

    /**
     * Returns true if maxOccurs > 1 or unbounded.
     * @return boolean True if multple
     */
    public function isMultiple()
    {
        return $this->maxOccurs > 1 || $this->maxOccurs == self::MAX_OCCURS_UNBOUNDED;
    }

    /**
     * Returns true if maxOccurs == 1.
     * @return boolean True if multple
     */
    public function isSingle()
    {
        return $this->maxOccurs == 1;
    }

    /**
     * Gets the property name this object will be referred to in
     * Fusion-generated classes.  This implementation appends an "s" to make
     * the relationship plural if isMultiple() returns true.
     * @return string Property name
     */
    public function getPropertyName()
    {
        // make plural if multiple
        return $this->isMultiple() ?
            self::_makePlural(parent::getPropertyName()) :
            parent::getPropertyName();
    }

    /**
     * Returns true if the element is nillable.  Default is false.
     * @return True if nillable
     */
    public function isNillable()
    {
        return $this->_getAttribute(self::ATTR_NILLABLE) === "true";
    }

    /**
     * Returns true if minOccurs is at least 1.
     * @return boolean True if required
     */
    public function isRequired()
    {
        return $this->minOccurs >= 1;
    }

    /**
     * Returns true if maxOccurs is 0.  This element should not be rendered.
     * @return boolean True if prohibited
     */
    public function isProhibited()
    {
        return ! $this->multiple && $this->maxOccurs == 0;
    }

    /**
     * Returns true if the form default for this data object is qualified.
     * This is managed via the elementFormDefault attribute of the schema.
     * @return boolen True if qualified by default
     */
    protected function isDefaultQualified()
    {
        return $this->schema->elementQualified;
    }

    /**
     * Attempts to follow some common English rules to make words plural.
     * May not always work but does its best.  ;)
     * @param string $str Word to make plural
     * @return string Plural word
     * @access private
     * @static
     */
    private static function _makePlural($str)
    {
        if (preg_match("/y$/", $str))
            return preg_replace("/y$/", "ies", $str);
        elseif (preg_match("/(ss|ch)$/", $str))
            return "${str}es";
        else
            return "${str}s";
    }
}

?>
