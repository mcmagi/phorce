<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;
use phorce\fusion\QName;

/**
 * A data type definition is the base class for schema objects which represent
 * data types: for example, complex types and simple types.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 * @abstract
 */
abstract class DataTypeDefinitionBase extends DefinitionBase
{
    const ATTR_NAME = "name";

    /**
     * Base type.
     * @var string
     * @access protected
     */
    protected $_base;

    /**
     * Creates a data type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e DOM element
     * @access protected
     */
    protected function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        if (! $pdef instanceof SchemaDefinition)
        {
            // elements that can only be present if schema is our parent
            $this->_validateNonPresence(self::ATTR_NAME, self::REASON_NON_SCHEMA);
        }

        // base type is extracted differently depending on type
    }

    /**
     * Returns the name attribute.
     * @return string Name
     */
    public function getName()
    {
        return $this->_getAttribute(self::ATTR_NAME);
    }

    /**
     * Returns the base type.
     * @return string Base type
     */
    public function getBase()
    {
        return $this->_base;
    }

    /**
     * Returns the reference to the base type.
     * @return object DataTypeReference Base type
     */
    public function getBaseTypeReference()
    {
        if (! is_null($this->_base))
        {
            return new DataTypeReference(
                $this, QName::createFromDOM($this->_element, $this->_base));
        }

        return null;
    }
}

?>
