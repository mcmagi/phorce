<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

/**
 * A particle in the schema is a schema object which is represented in the
 * instance document by one or more element objects.  This includes Elements,
 * Groups, Anys, and ModelGroups.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
interface Particle
{
    const ATTR_MAX_OCCURS = "maxOccurs";
    const ATTR_MIN_OCCURS = "minOccurs";
    const MAX_OCCURS_UNBOUNDED = "unbounded";

    /**
     * Returns the maxOccurs attribute.  Default is 1.
     * @return integer Max occurs
     */
    public function getMaxOccurs();

    /**
     * Returns the minOccurs attribute.  Default is 1.
     * @return integer Min occurs
     */
    public function getMinOccurs();

    /**
     * Returns true if maxOccurs > 1 or unbounded.
     * @return boolean True if multple
     */
    public function isMultiple();

    /**
     * Returns true if maxOccurs == 1.
     * @return boolean True if multple
     */
    public function isSingle();

    /**
     * Returns true if minOccurs is at least 1.
     * @return boolean True if required
     */
    public function isRequired();

    /**
     * Returns true if maxOccurs is 0.  This particle should not be rendered.
     * @return boolean True if prohibited
     */
    public function isProhibited();
}

?>
