<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

/**
 * An attribute container in the schema is a schema object which contains other
 * attribute definitions.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
interface AttributeContainer
{
    const ELEM_ANY_ATTRIBUTE = "anyAttribute";

    /**
     * Returns an array of Attribute definitions.
     * @return object ArrayObject Array of Attribute definitions
     */
    public function getAttributes();

    /**
     * Returns an array of AttributeGroup definitions.
     * @return object ArrayObject Array of AttributeGroup definitions
     */
    public function getAttributeGroups();

    /**
     * Returns an AnyAttribute definition.
     * @return object AnyAttribute
     */
    public function getAnyAttribute();
}

?>
