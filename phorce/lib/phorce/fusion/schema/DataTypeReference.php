<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use phorce\fusion\Constants;
use phorce\fusion\QName;

/**
 * A data type reference is a reference to another data type in an XSD schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DataTypeReference extends ReferenceBase
{
    /**
     * The custom type definition (simple or complex) being referenced.
     * @var object DataTypeDefinitionBase
     * @access private
     */
    private $_nestedType;

    /**
     * Creates a data type reference.
     * @param object DefinitionBase $def The data object making the reference
     * @param object QName|object DataTypeDefinitionBase $type The type to be referenced
     */
    public function __construct(DefinitionBase $def, $type)
    {
        if ($type instanceof DataTypeDefinitionBase)
        {
            $this->_nestedType = $type;
            parent::__construct($def, new QName());
        }
        elseif ($type instanceof QName)
            parent::__construct($def, $type);
    }

    /**
     * Returns the custom type definition (simple or complex) being referenced
     * if defined locally.
     * @return object DataTypeDefinitionBase Type definition
     */
    public function getNestedType()
    {
        return $this->_nestedType;
    }

    /**
     * Returns true if the type is in the XSD namespace.
     * @return boolean True if built-in
     */
    public function isBuiltin()
    {
        return $this->qname->namespace == Constants::XSD_NAMESPACE_URI;
    }

    /**
     * Returns true if the type is in the target namespace.
     * @return boolean True if local
     */
    public function isNested()
    {
        return ! is_null($this->_nestedType);
    }

    /**
     * Resolves and returns the data type being referenced.
     * @return mixed
     */
    public function resolve()
    {
        //print_r($this->qname);
        if ($this->nested)
            return $this->_nestedType;
        elseif ($this->builtin)
            return InternalTypeDefinition::getInternalType($this->qname->name);
        else
        {
            // validate qname
            if (empty($this->qname->namespace))
                throw new SchemaException($this->definition->qname,
                    "Could not identify namespace for type '{$this->qname->name}'");

            $schema = $this->definition->schema->findSchema($this->qname->namespace);

            // check for namespace import
            if (is_null($schema))
                throw new SchemaException($this->definition->qname,
                    "No schema imported with namespace '{$this->qname->namespace}'");

            $type = $schema->findType($this->qname->name);

            // check for type
            if (is_null($type))
                throw new SchemaException($this->definition->qname,
                    "No type '{$this->qname->name}' defined in namespace '{$this->qname->namespace}'");

            return $type;
        }
    }
}

?>
