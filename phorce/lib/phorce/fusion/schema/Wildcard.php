<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

/**
 * Represents a wildcard data object in the XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
interface Wildcard
{
    const ATTR_NAMESPACE = "namespace";
    const NAMESPACE_ANY = "##any";
    const NAMESPACE_OTHER = "##other";
    const NAMESPACE_LOCAL = "##local";
    const NAMESPACE_TARGET = "##targetNamespace";

    const ATTR_PROCESS_CONTENTS = "processContents";
    const PROCESS_CONTENTS_STRICT = "strict";
    const PROCESS_CONTENTS_LAX = "lax";
    const PROCESS_CONTENTS_SKIP = "skip";

    /**
     * Returns the namespace attribute.  Default is ##any.
     * @return string Namespace
     */
    public function getNamespace();

    /**
     * Returns the process contents attribute.  Default is strict.
     * @return string Process contents
     */
    public function getProcessContents();
}

?>
