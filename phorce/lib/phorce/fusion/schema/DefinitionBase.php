<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \ArrayObject;
use \DOMElement;
use \DOMNodeList;
use phorce\common\property\PropertyObject;
use phorce\fusion\QName;

/**
 * This is the abstract base class for all definition classes.  It wraps the
 * underlying DOM Element the definition class abstracts as well as the root
 * schema definition.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 * @abstract
 */
abstract class DefinitionBase extends PropertyObject
{
    const REASON_SCHEMA = "the containing element is the 'schema' element";
    const REASON_NON_SCHEMA = "the containing element is not the 'schema' element";
    const REASON_REF = "the 'ref' attribute is specified";

    /**
     * The parent definition this relates to.
     * @var object DefinitionBase
     * @access private
     */
    protected $_parentDef;

    /**
     * The DOM object.
     * @var object DOMElement
     * @access private
     */
    protected $_element;

    /**
     * Creates a complex type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     * @access protected
     */
    protected function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        $this->_parentDef = $pdef;
        $this->_element = $e;
    }

    /**
     * Returns the qualified name of the schema element.
     * @return object QName Qualified name
     */
    public function getQName()
    {
        return self::getElementQName($this->_element);
    }

    /**
     * Get schema definition by recursively searching parent objects.
     * @return object SchemaDefinition Schema definition
     */
    public function getSchema()
    {
        return $this instanceof SchemaDefinition ? $this : $this->_parentDef->schema;
    }

    /**
     * Returns the attribute value or the specified default if no attribute is
     * present.
     * @param string $attrname Attribute name
     * @param mixed $default Optional default value to return
     */
    protected function _getAttribute($attrname, $default = null)
    {
        return $this->_element->hasAttribute($attrname) ?
            $this->_element->getAttribute($attrname) :
            $default;
    }

    /**
     * Returns the list of DOMElement objects one level under this DOMElement.
     * @return object ArrayObject List of DOMElement objects
     * @access protected
     */
    protected function getDOMElements()
    {
        return self::getChildDOMElements($this->_element);
    }

    /**
     * Returns the list of DOMElement objects one level under the specified DOMElement.
     * @param object DOMElement $elem DOMElement object
     * @return object ArrayObject List of DOMElement objects
     * @access protected
     * @static
     */
    protected static function getChildDOMElements(DOMElement $elem)
    {
        $arr = new ArrayObject();
        foreach (self::toArray($elem->childNodes) as $e)
        {
            if ($e instanceof DOMElement)
                $arr[] = $e;
        }
        return $arr;
    }

    /**
     * Returns the list of all DOMElement objects under this DOMElement with
     * the specified name.  Objects are returned as an ArrayObject rather than
     * as a DOMNodeList so it may be used in a <code>foreach</code>.
     * @param string $tagname
     * @return object ArrayObject List of DOMElement objects
     * @access protected
     */
    protected function getElementsByTagName($tagname)
    {
        return self::toArray($this->_element->getElementsByTagName($tagname));
    }

    /**
     * Converts a DOMNodeList to an array of DOMNode objects.
     * @param object DOMNodeList $nodeList
     * @return object ArrayObject
     * @access protected
     * @static
     */
    protected static function toArray(DOMNodeList $nodeList)
    {
        $arr = new ArrayObject();
        for ($i = 0; $i < $nodeList->length; $i++)
            $arr[] = $nodeList->item($i);
        return $arr;
    }

    /**
     * Returns the QName for the specified DOMElement.
     * @return object QName Qualified name
     * @access protected
     * @static
     */
    protected static function getElementQName(DOMElement $e)
    {
        return QName::createFromDOM($e, $e->tagName);
    }

    /**
     * Checks that the attribute specified is not present in the element,
     * otherwise throws an exception.
     * @param string $attrname Attribute to check
     * @param string $reason Reason for error if present
     * @access private
     */
    protected function _validateNonPresence($attrname, $reason)
    {
        if ($this->_element->hasAttribute($attrname))
        {
            throw new SchemaException($this->qname, "Attribute '{$attrname}' is prohibited if {$reason}");
        }
    }
}

?>
