<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \ArrayObject;
use \DOMElement;
use phorce\fusion\Constants;

/**
 * A model group definition represents an All, Choice, or Sequence element in
 * an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class ModelGroupDefinition extends DefinitionBase
    implements Particle
{
    const ELEM_SEQUENCE = "sequence";
    const ELEM_CHOICE = "choice";
    const ELEM_ALL = "all";
    const ELEM_ANY = "any";

    /**
     * Array of Particle objects.  May include ElementDefinition,
     * GroupDefinition, AnyDefinition, or ModelGroupDefinition objects.
     * @var object ArrayObject
     * @access private
     */
    private $_particles;

    /**
     * Whether this group is sequenced.  Default is false.
     * @var boolean
     * @access private
     */
    private $_sequence = false;

    /**
     * Whether this group must contain no more than one of the returned
     * particles.  Default is false.
     * @var boolean
     * @access private
     */
    private $_choice = false;

    /**
     * Creates a complex type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        if ($pdef instanceof SchemaDefinition)
        {
            // elements that cannot be present if schema is our parent
            $this->_validateNonPresence(self::ATTR_MAX_OCCURS, self::REASON_SCHEMA);
            $this->_validateNonPresence(self::ATTR_MIN_OCCURS, self::REASON_SCHEMA);
        }

        // get elements
        $this->_particles = new ArrayObject();
        $this->_extractModelGroupInfo($e);
    }

    /**
     * Extracts all elements from the specified DOMElement.  The elements are
     * saved in the $_elements array.  The DOMElement must represent a grouping
     * of elements (e.g. group, sequence, etc).
     * @param object DOMElement $elem
     * @access private
     */
    private function _extractModelGroupInfo(DOMElement $elem)
    {
        // flag as sequence or choice
        $qname = $this->qname;
        if ($qname->name == self::ELEM_SEQUENCE)
            $this->_sequence = true;
        elseif ($qname->name == self::ELEM_CHOICE)
            $this->_choice = true;

        foreach ($this->getChildDOMElements($elem) as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            // all element can only contain element elements
            if (! $this->choice && ! $this->sequence)
            {
                if ($qname->name != SchemaDefinition::ELEM_ELEMENT)
                {
                    throw new SchemaException($this->qname,
                        "Must only contain 'element' elements.");
                }
            }

            switch ($qname->name)
            {
            case SchemaDefinition::ELEM_ELEMENT;
                $this->_particles[] = new ElementDefinition($this, $e);
                break;
            case SchemaDefinition::ELEM_GROUP;
                $this->_particles[] = new GroupDefinition($this, $e);
                break;
            case self::ELEM_ANY:
                $this->_particles[] = new AnyDefinition($this, $e);
                break;
            case self::ELEM_SEQUENCE:
            case self::ELEM_CHOICE:
                $this->_particles[] = new ModelGroupDefinition($this, $e);
                break;
            }
        }
    }

    /**
     * Returns the maxOccurs attribute.  Default is 1.
     * @return integer Max occurs
     */
    public function getMaxOccurs()
    {
        return $this->_getAttribute(self::ATTR_MAX_OCCURS, 1);
    }

    /**
     * Returns the minOccurs attribute.  Default is 1.
     * @return integer Min occurs
     */
    public function getMinOccurs()
    {
        return $this->_getAttribute(self::ATTR_MIN_OCCURS, 1);
    }

    /**
     * Returns true if maxOccurs > 1 or unbounded.
     * @return boolean True if multple
     */
    public function isMultiple()
    {
        return $this->maxOccurs > 1 || $this->maxOccurs == self::MAX_OCCURS_UNBOUNDED;
    }

    /**
     * Returns true if maxOccurs == 1.
     * @return boolean True if multple
     */
    public function isSingle()
    {
        return $this->maxOccurs == 1;
    }

    /**
     * Returns true if minOccurs is at least 1.
     * @return boolean True if required
     */
    public function isRequired()
    {
        return $this->minOccurs >= 1;
    }

    /**
     * Returns true if maxOccurs is 0.  This particle should not be rendered.
     * @return boolean True if prohibited
     */
    public function isProhibited()
    {
        return ! $this->multiple && $this->maxOccurs == 0;
    }

    /**
     * Returns all particle objects.  May include ElementDefinition,
     * GroupDefinition, or AnyDefinition objects.  If sequenced, particles will
     * be returned in the order they are to be sequenced.
     * @return object ArrayObject Array of AbstractElement objects
     */
    public function getParticles()
    {
        return $this->_particles;
    }

    /**
     * Returns true if the contents of the group are sequenced.  This is
     * possible if the group contains a 'sequence' element.
     * @return boolean True if sequenced
     */
    public function isSequence()
    {
        return $this->_sequence;
    }

    /**
     * Returns true if the group must contain no more than one of the
     * returned particles.  This is possible if the group contains a 'choice'
     * element.
     * @return boolean True if choice
     */
    public function isChoice()
    {
        return $this->_choice;
    }
}

?>
