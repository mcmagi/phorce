<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use phorce\common\property\PropertyObject;
use phorce\fusion\QName;

/**
 * A ReferenceBase is the base class for a reference to another element in an
 * XSD schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 * @abstract
 */
abstract class ReferenceBase extends PropertyObject
{
    /**
     * The referencing object.
     * @param DefinitionBase
     */
    private $_def;

    /**
     * The qualified name of the type being referenced.
     * @var object QName
     * @access private
     */
    private $_qname;

    /**
     * Creates a data type reference.
     * @param object DefinitionBase $def The data object making the reference
     * @param object QName $qname The object to be referenced
     * @access protected
     */
    protected function __construct(DefinitionBase $def, QName $qname)
    {
        $this->_def = $def;
        $this->_qname = $qname;
    }

    /**
     * Returns the object that is making this reference.
     * @return object DefinitionBase
     */
    public function getDefinition()
    {
        return $this->_def;
    }

    /**
     * Returns the qualified name of the type being referenced.
     * @return object QName
     */
    public function getQName()
    {
        return $this->_qname;
    }

    /**
     * Resolves and returns the object being referenced.
     * @return mixed The referenced object
     * @abstract
     */
    public abstract function resolve();
}

?>
