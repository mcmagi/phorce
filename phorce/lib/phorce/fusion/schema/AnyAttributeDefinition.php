<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;

/**
 * An any attribute definition represents an AnyAttribute element in an XML
 * schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class AnyAttributeDefinition extends DefinitionBase implements Wildcard
{
    /**
     * Creates a complex type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);
    }

    /**
     * Returns the namespace attribute.  Default is ##any.
     * @return string Namespace
     */
    public function getNamespace()
    {
        return $this->_getAttribute(self::ATTR_NAMESPACE, self::NAMESPACE_ANY);
    }

    /**
     * Returns the process contents attribute.  Default is strict.
     * @return string Process contents
     */
    public function getProcessContents()
    {
        return $this->_getAttribute(self::ATTR_PROCESS_CONTENTS,
            self::PROCESS_CONTENTS_STRICT);
    }
}

?>
