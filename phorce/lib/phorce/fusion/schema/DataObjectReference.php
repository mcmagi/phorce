<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use phorce\fusion\QName;

/**
 * A data object reference is a reference to another data object in an XSD
 * schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DataObjectReference extends ReferenceBase
{
    /**
     * Creates a data object reference.
     * @param object DefinitionBase $schema The data object where the type is referenced
     * @param object QName $type The type to be referenced
     */
    public function __construct(DefinitionBase $def, QName $qname)
    {
        parent::__construct($def, $qname);
    }

    /**
     * Resolves and returns the data object being referenced.
     * @return mixed The referenced object
     * @abstract
     */
    public function resolve()
    {
        // validate qname
        if (empty($this->qname->namespace))
            throw new SchemaException($this->definition->qname,
                "Could not identify namespace for ref '{$this->qname->name}'");

        $schema = $this->definition->schema->findSchema($this->qname->namespace);
        if (is_null($schema))
            throw new SchemaException($this->definition->qname,
                "No schema imported with namespace '{$this->qname->namespace}'");

        if ($this->definition instanceof AttributeDefinition)
            $object = $schema->attributes[$this->qname->name];
        elseif ($this->definition instanceof ElementDefinition)
            $object = $schema->elements[$this->qname->name];
        elseif ($this->definition instanceof AttributeGroupDefinition)
            $object = $schema->attributeGroups[$this->qname->name];
        elseif ($this->definition instanceof GroupDefinition)
            $object = $schema->groups[$this->qname->name];

        if (! isset($object) || is_null($object))
            throw new SchemaException($this->definition->qname,
                "Cannot find object '{$this->qname->name}' defined in namespace '{$this->qname->namespace}'");

        return $object;
    }
}
