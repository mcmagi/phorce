<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use phorce\fusion\FusionException;
use phorce\fusion\QName;

/**
 * Represents an exception in processing the schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class SchemaException extends FusionException
{
    /**
     * The qualified name of the affected element.
     * @var object QName
     * @access private
     */
    private $_qname;

    /**
     * Constructs an exception.
     * @param object QName $qname Qualfied name
     * @param string $message The error message
     */
    public function __construct(QName $qname, $message)
    {
        parent::__construct("{$qname}: {$message}");

        $this->_qname = $qname;
    }

    /**
     * Returns the qualified name of the affected element.
     * @return object QName Qualfied name
     */
    public function getDOMElement()
    {
        return $this->_qname;
    }
}

?>
