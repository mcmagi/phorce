<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;
use phorce\fusion\Constants;
use phorce\fusion\QName;

/**
 * A data object definition is the base class for schema objects which hold
 * data.  For example, elements and attributes.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 * @abstract
 */
abstract class DataObjectDefinitionBase extends ReferenceDefinitionBase
{
    const ATTR_TYPE = "type";
    const ATTR_FORM = "form";
    const ATTR_DEFAULT = "default";
    const ATTR_FIXED = "fixed";
    const FORM_QUALIFIED = "qualified";

    /**
     * Any custom type (simple or complex) defined inside the data object.
     * @var object DataTypeDefinitionBase
     * @access private
     */
    private $_innerType;

    /**
     * Creates a data object definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e DOM object
     * @access protected
     */
    protected function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        // get type information
        $this->_extractTypeInfo();

        if ($pdef instanceof SchemaDefinition)
        {
            // elements that cannot be present if schema is our parent
            $this->_validateNonPresence(self::ATTR_REF, self::REASON_SCHEMA);
        }
        elseif ($this->reference)
        {
            // elements that cannot be present if this is a reference
            $this->_validateNonPresence(self::ATTR_TYPE, self::REASON_REF);
            $this->_validateNonPresence(self::ATTR_FORM, self::REASON_REF);
            $this->_validateNonPresence(self::ATTR_DEFAULT, self::REASON_REF);
            $this->_validateNonPresence(self::ATTR_FIXED, self::REASON_REF);

            if (! is_null($this->_innerType))
            {
                throw new SchemaException($this->_qname,
                    "Cannot have nested type when 'ref' attribute is present");
            }
        }
        elseif (! is_null($this->_innerType) && $this->_element->hasAttribute(self::ATTR_TYPE))
        {
            throw new SchemaException($this->qname,
                "Cannot have nested type when 'type' attribute is present");
        }
        elseif (! is_null($this->fixed) && ! is_null($this->default))
        {
            throw new SchemaException($this->qname,
                "The 'default' and 'fixed' attributes cannot both be present");
        }
    }

    /**
     * Inspects child elements for nested type information.
     * @access private
     */
    private function _extractTypeInfo()
    {
        foreach ($this->getDOMElements() as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case SchemaDefinition::ELEM_COMPLEX_TYPE:
                $this->_innerType = new ComplexTypeDefinition($this, $e);
                break;
            case SchemaDefinition::ELEM_SIMPLE_TYPE:
                $this->_innerType = new SimpleTypeDefinition($this, $e);
            }
        }
    }

    /**
     * Returns any default value.
     * @return string Default
     */
    public function getDefault()
    {
        return $this->_getAttribute(self::ATTR_DEFAULT);
    }

    /**
     * Returns any fixed value.
     * @return string Fixed
     */
    public function getFixed()
    {
        return $this->_getAttribute(self::ATTR_FIXED);
    }

    /**
     * Returns the type definition reference.
     * @return object DataTypeReference Reference to type
     */
    public function getTypeReference()
    {
        $type = $this->_getAttribute(self::ATTR_TYPE);
        if (! is_null($type))
        {
            return new DataTypeReference(
                $this, QName::createFromDOM($this->_element, $type));
        }
        else
        {
            return new DataTypeReference(
                $this->schema,
                $this->_innerType);
        }
    }

    /**
     * Returns the type definition of the type this object is defined in.
     * Returns null if this object represents a root element.
     * @return object DataTypeDefinition Parent type
     */
    public function getParentType()
    {
        return $this->_parentDef instanceof DataTypeDefinition ?
            $this->_parentDef->type : null;
    }

    /**
     * Gets the property name this object will be referred to in
     * Fusion-generated classes.
     * @return string Property name
     */
    public function getPropertyName()
    {
        // split by space, dash, or underscore delimited word
        // and capitalize the first letter of each word
        $converted = "";
        foreach (preg_split('/[-_ ]/', $this->name) as $v)
            $converted .= ucfirst($v);

        // oops - we didn't want the first word capitalized
        return lcfirst($converted);
        //return self::_lcfirst($converted);
    }

    /**
     * Returns whether the data object is namespace qualified.  Data objects
     * are namespace qualified if they have a 'form' attribute with value
     * "qualified", their parent is the schema, or they are qualified by
     * default.  (see isDefaultQualified())
     * @return boolean True if qualified
     */
    public function isQualified()
    {
        if ($this->_element->hasAttribute(self::ATTR_FORM))
            return $this->_element->getAttribute(self::ATTR_FORM) == self::FORM_QUALIFIED;
        if ($this->_parentDef instanceof SchemaDefinition)
            return true;
        return $this->isDefaultQualified();
    }

    /**
     * Returns true if the form default for this data object is qualified.
     * This is managed via the attributeFormDefault or elementFormDefault
     * attributes of the schema.
     * @return boolen True if qualified by default
     */
    protected abstract function isDefaultQualified();

    /**
     * Returns the target namespace of the data object.  The target namespace
     * of the data object is the target namespace of the schema if this data
     * object is qualified.  Otherwise, it is null.
     * @return string Target namespace
     */
    public function getTargetNamespace()
    {
        return $this->qualified ? $this->schema->targetNamespace : null;
    }

    /**
     * This method is not in PHP 5 yet...
     * @param string $str A string
     * @return string Result string
     */
    private static function _lcfirst($str)
    {
        if (strlen($str) == 0)
            return $str;
        elseif (strlen($str) == 1)
            return strtolower($str);
        else
            return strtolower(substr($str, 0, 1)) . substr($str, 1);
    }
}

?>
