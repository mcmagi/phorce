<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;

/**
 * An any definition represents an Any element in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class AnyDefinition extends DefinitionBase implements Particle, Wildcard
{
    /**
     * Creates a complex type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);
    }

    /**
     * Returns the maxOccurs attribute.  Default is 1.
     * @return integer Max occurs
     */
    public function getMaxOccurs()
    {
        return $this->_getAttribute(self::ATTR_MAX_OCCURS, 1);
    }

    /**
     * Returns the minOccurs attribute.  Default is 1.
     * @return integer Min occurs
     */
    public function getMinOccurs()
    {
        return $this->_getAttribute(self::ATTR_MIN_OCCURS, 1);
    }

    /**
     * Returns true if maxOccurs > 1 or unbounded.
     * @return boolean True if multple
     */
    public function isMultiple()
    {
        return $this->maxOccurs > 1 || $this->maxOccurs == self::MAX_OCCURS_UNBOUNDED;
    }

    /**
     * Returns true if maxOccurs == 1.
     * @return boolean True if multple
     */
    public function isSingle()
    {
        return $this->maxOccurs == 1;
    }

    /**
     * Returns true if minOccurs is at least 1.
     * @return boolean True if required
     */
    public function isRequired()
    {
        return $this->minOccurs >= 1;
    }

    /**
     * Returns true if maxOccurs is 0.  This element should not be rendered.
     * @return boolean True if prohibited
     */
    public function isProhibited()
    {
        return ! $this->multiple && $this->maxOccurs == 0;
    }

    /**
     * Returns the namespace attribute.  Default is ##any.
     * @return string Namespace
     */
    public function getNamespace()
    {
        return $this->_getAttribute(self::ATTR_NAMESPACE, self::NAMESPACE_ANY);
    }

    /**
     * Returns the process contents attribute.  Default is strict.
     * @return string Process contents
     */
    public function getProcessContents()
    {
        return $this->_getAttribute(self::ATTR_PROCESS_CONTENTS,
            self::PROCESS_CONTENTS_STRICT);
    }
}

?>
