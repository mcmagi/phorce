<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \ArrayObject;
use phorce\fusion\Constants;

/**
 * A group definition represents a Group element in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class GroupDefinition extends ReferenceDefinitionBase
    implements ModelGroupContainer, Particle
{
    /**
     * The contained model group definition. (if not a reference)
     * @var object ModelGroupDefinition
     * @access private
     */
    private $_modelGroup;

    /**
     * Creates a complex type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        if ($pdef instanceof SchemaDefinition)
        {
            // elements that cannot be present if schema is our parent
            $this->_validateNonPresence(self::ATTR_MAX_OCCURS, self::REASON_SCHEMA);
            $this->_validateNonPresence(self::ATTR_MIN_OCCURS, self::REASON_SCHEMA);
        }

        // get elements
        $this->_extractGroupInfo($e);

        if ($this->reference)
        {
            // elements that cannot be present if this is a reference
            if (! is_null($this->modelGroup))
            {
                throw new SchemaException($this->qname,
                    "Cannot have nested elements when 'ref' attribute is present");
            }
        }
    }

    /**
     * Extracts the model group from the specified DOMElement.  The model group
     * is saved in the $_modelGroup variable.  The DOMElement must represent a
     * group definition.
     * @param object DOMElement $elem
     * @access private
     */
    private function _extractGroupInfo(DOMElement $elem)
    {
        foreach ($this->getChildDOMElements($elem) as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case ModelGroupDefinition::ELEM_SEQUENCE:
            case ModelGroupDefinition::ELEM_CHOICE:
            case ModelGroupDefinition::ELEM_ALL:
                $this->_modelGroup = new ModelGroupDefinition($this, $e);
            }
        }
    }

    /**
     * Returns the maxOccurs attribute.  Default is 1.
     * @return integer Max occurs
     */
    public function getMaxOccurs()
    {
        return $this->_getAttribute(self::ATTR_MAX_OCCURS, 1);
    }

    /**
     * Returns the minOccurs attribute.  Default is 1.
     * @return integer Min occurs
     */
    public function getMinOccurs()
    {
        return $this->_getAttribute(self::ATTR_MIN_OCCURS, 1);
    }

    /**
     * Returns true if maxOccurs > 1 or unbounded.
     * @return boolean True if multple
     */
    public function isMultiple()
    {
        return $this->maxOccurs > 1 || $this->maxOccurs == self::MAX_OCCURS_UNBOUNDED;
    }

    /**
     * Returns true if maxOccurs == 1.
     * @return boolean True if multple
     */
    public function isSingle()
    {
        return $this->maxOccurs == 1;
    }

    /**
     * Returns true if minOccurs is at least 1.
     * @return boolean True if required
     */
    public function isRequired()
    {
        return $this->minOccurs >= 1;
    }

    /**
     * Returns true if maxOccurs is 0.  This particle should not be rendered.
     * @return boolean True if prohibited
     */
    public function isProhibited()
    {
        return ! $this->multiple && $this->maxOccurs == 0;
    }

    /**
     * Returns the model group definition.
     * @return object ModelGroupDefinition
     */
    public function getModelGroup()
    {
        return $this->_modelGroup;
    }
}

?>
