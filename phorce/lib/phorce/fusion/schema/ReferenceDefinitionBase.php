<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;
use phorce\fusion\QName;

/**
 * A schema definition that is referencable elsewhere in the schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 * @abstract
 */
abstract class ReferenceDefinitionBase extends DefinitionBase
{
    const ATTR_NAME = "name";
    const ATTR_REF = "ref";

    /**
     * Creates a reference definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e DOM object
     * @access protected
     */
    protected function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        if ($pdef instanceof SchemaDefinition)
        {
            // elements that cannot be present if schema is our parent
            $this->_validateNonPresence(self::ATTR_REF, self::REASON_SCHEMA);
        }
        else if ($this->reference)
        {
            // ref and name are mutually exclusive
            $this->_validateNonPresence(self::ATTR_NAME, self::REASON_REF);
        }
    }

    /**
     * Returns the name attribute.
     * @return string Name
     */
    public function getName()
    {
        return $this->_getAttribute(self::ATTR_NAME);
    }

    /**
     * Returns true if this is a reference to another data object.
     * @return boolean
     */
    public function isReference()
    {
        return $this->_element->hasAttribute(self::ATTR_REF);
    }

    /**
     * Returns a Reference to the object being referenced.  If the object is
     * not a reference this method will return null.
     * @return object DataObjectReference Reference
     */
    public function getObjectReference()
    {
        if ($this->reference)
        {
            return new DataObjectReference(
                $this, QName::createFromDOM(
                    $this->_element, $this->_getAttribute(self::ATTR_REF)));
        }
        return null;
    }
}

?>
