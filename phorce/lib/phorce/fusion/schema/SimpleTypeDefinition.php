<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;
use phorce\fusion\Constants;

/**
 * A simple type definition represents a SimpleType element in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class SimpleTypeDefinition extends DataTypeDefinitionBase
{
    const ELEM_RESTRICTION = "restriction";
    const ELEM_LIST = "list";
    const ELEM_UNION = "union";
    const ATTR_BASE = "base";
    const ATTR_ITEM_TYPE = "itemType";

    /**
     * Creates a simple type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        $this->_extractBaseTypeInfo();
    }

    /**
     * Extracts the base type from this DOMElement.
     * @access private
     */
    private function _extractBaseTypeInfo()
    {
        foreach ($this->getDOMElements() as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case self::ELEM_RESTRICTION:
                $this->_base = $e->getAttribute(self::ATTR_BASE);
                break;
            case self::ELEM_LIST:
                $this->_base = $e->getAttribute(self::ATTR_ITEM_TYPE);
                break;
            case self::ELEM_UNION:
                // handle unions as any simple type
                $xsdPrefix = $e->lookupPrefix(Constants::XSD_NAMESPACE_URI);
                $this->_base = "{$xsdPrefix}:anySimpleType";
            }
        }
    }
}

?>
