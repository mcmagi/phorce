<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \DOMElement;

/**
 * An attribute definition represents an Attribute element in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class AttributeDefinition extends DataObjectDefinitionBase
{
    const ATTR_USE = "use";

    const USE_OPTIONAL = "optional";
    const USE_REQUIRED = "required";
    const USE_PROHIBITED = "prohibited";

    /**
     * Creates an attribute definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e Attribute DOM element
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);
    }

    /**
     * Returns true if use is defined as "required".
     * @return boolean True if required
     */
    public function isRequired()
    {
        return $this->_getAttribute(self::ATTR_USE, self::USE_OPTIONAL) == self::USE_REQUIRED;
    }

    /**
     * Returns true if use is "prohibited".  This element should not be
     * rendered.
     * @return boolean True if prohibited
     */
    public function isProhibited()
    {
        return $this->_getAttribute(self::ATTR_USE, self::USE_OPTIONAL) == self::USE_PROHIBITED;
    }

    /**
     * Returns true if the form default for this data object is qualified.
     * This is managed via the attributeFormDefault attribute of the schema.
     * @return boolen True if qualified by default
     */
    protected function isDefaultQualified()
    {
        return $this->schema->attributeQualified;
    }
}

?>
