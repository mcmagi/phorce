<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use phorce\common\property\PropertyObject;
use phorce\fusion\type\XmlTypeConverterFactory;

/**
 * An internal type definition represents a built-in type in an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class InternalTypeDefinition extends PropertyObject
{
    /**
     * Associative array of internal type definitions by name.
     * @var array
     * @access private
     * @static
     */
    private static $_TYPE_MAP = array();

    /**
     * The name of the internal type.
     * @var string
     * @access private
     */
    private $_name;

    /**
     * PHP type this internal type maps to.
     * @var string
     * @access private
     */
    private $_phpType;

    /**
     * The class to be used if the PHP type is an object.
     * @var string
     * @access private
     */
    private $_class;

    /**
     * Creates a simple type definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e ComplexType DOM element
     */
    private function __construct($name, $phpType, $class = null)
    {
        $this->_name = $name;
        $this->_phpType = $phpType;
        $this->_class = $class;
    }

    /**
     * Returns the name of the internal type.
     * @return string Internal type name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Returns the PHP type the internal type maps to.
     * @return string PHP type
     */
    public function getPhpType()
    {
        return $this->_phpType;
    }

    /**
     * Returns the Class the internal type maps to.  Only valid if phpType is
     * "object".
     * @return string Class
     */
    public function getClass()
    {
        return $this->_class;
    }

    /**
     * Returns the internal type definition for the specified type name.
     * @param string $typeName Non-prefixed internal type name
     * @return object InternalTypeDefinition
     * @static
     */
    public static function getInternalType($typeName)
    {
        // check if this type was already insantiated
        if (array_key_exists($typeName, self::$_TYPE_MAP))
            return self::$_TYPE_MAP[$typeName];

        $tfactory = XmlTypeConverterFactory::getInstance();
        $t = $tfactory->findByXmlType($typeName);

        // make new type definition
        $typeDef = new InternalTypeDefinition(
            $typeName, $t->getType(), $t->getClass());

        // and cache for later
        self::$_TYPE_MAP[$typeName] = $typeDef;

        return $typeDef;
    }
}

?>
