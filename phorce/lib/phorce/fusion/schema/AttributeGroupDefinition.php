<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\schema;

use \ArrayObject;
use \DOMElement;
use phorce\fusion\Constants;

/**
 * An attribute group definition represents an AttributeGroup element in an XML
 * schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class AttributeGroupDefinition extends ReferenceDefinitionBase
    implements AttributeContainer
{
    /**
     * Array of top-level attribute definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_attributes;

    /**
     * Array of attribute group definitions.
     * @var object ArrayObject
     * @access private
     */
    private $_attributeGroups;

    /**
     * Any attribute definition.
     * @var object AnyAttributeDefinition
     * @access private
     */
    private $_anyAttribute;

    /**
     * Creates an attribute group definition.
     * @param object DefinitionBase $pdef Parent definition
     * @param object DOMElement $e DOM object
     */
    public function __construct(DefinitionBase $pdef, DOMElement $e)
    {
        parent::__construct($pdef, $e);

        $this->_attributes = new ArrayObject();
        $this->_attributeGroups = new ArrayObject();

        // get attribute information
        $this->_extractAttributeInfo();
    }

    /**
     * Inspects child elements for nested type information.
     * @access private
     */
    private function _extractAttributeInfo()
    {
        foreach ($this->getDOMElements() as $e)
        {
            $qname = self::getElementQName($e);
            if ($qname->namespace != Constants::XSD_NAMESPACE_URI)
                continue;

            switch ($qname->name)
            {
            case SchemaDefinition::ELEM_ATTRIBUTE:
                $this->_attributes[] = new AttributeDefinition($this, $e);
                break;
            case SchemaDefinition::ELEM_ATTRIBUTE_GROUP:
                $this->_attributeGroups[] = new AttributeGroupDefinition($this, $e);
                break;
            case self::ELEM_ANY_ATTRIBUTE:
                $this->_anyAttribute = new AnyAttributeDefinition($this, $e);
            }
        }
    }

    /**
     * Returns an array of Attribute definitions.
     * @return object ArrayObject Array of Attribute definitions
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * Returns an array of AttributeGroup definitions.
     * @return object ArrayObject Array of AttributeGroup definitions
     */
    public function getAttributeGroups()
    {
        return $this->_attributeGroups;
    }

    /**
     * Returns an AnyAttribute definition.
     * @return object AnyAttribute
     */
    public function getAnyAttribute()
    {
        return $this->_anyAttribute;
    }
}

?>
