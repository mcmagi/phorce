<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\generator;

/**
 * The UniqueValueGenerator class provides a means to ensure that the values
 * (not just the keys) of an associative array remain unique.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class UniqueValueMap
{
    /**
     * Associative array of keys to unique values.
     * @param array
     * @access private
     */
    private $_keyValueMap = array();

    /**
     * An associative array of counters keyed by values.  The counter is the
     * next integer to be used at the end of a value to garuntee uniqueness.
     * @param array
     * @access private
     */
    private $_valueCounterMap = array();

    /**
     * Returns the value for the specified key.  If the key has been used before
     * then it will return the value stored for that key.  If the key has not
     * been used before then it will call {@link #setValue} and return the
     * value.  A null key will always generate a new value.
     * @param string $key Key
     * @param string $value Suggested value
     * @return string Actual value
     */
    public function getValue($key, $value)
    {
        // if key is not used already then set its suggested value
        if (is_null($key) || ! isset($this->_keyValueMap[$key]))
            return $this->setValue($key, $value);

        return $this->_keyValueMap[$key];
    }

    /**
     * Sets the value for the specified key.  If the key has not been used
     * before then it will make sure the suggested value provided is unique.  If
     * it is, then it will be set as the value for the key and returned.  If it
     * is not, a new value will be derived by appending a counter to the end of
     * the value.  A null key will not be stored but will always generate a new
     * value.
     * @param string $key Key
     * @param string $value Suggested value
     * @return string Actual value
     */
    public function setValue($key, $value)
    {
        if (! isset($this->_valueCounterMap[$value]))
        {
            // value is not used already so use it
            $this->_valueCounterMap[$value] = 0;
        }
        else
        {
            // value is used already, so append a counter
            //  (making sure that the value-counter combo is not used too)
            do
            {
                $newValue = $value . $this->_valueCounterMap[$value]++;
            }
            while (isset($this->_valueCounterMap[$newValue]));

            // reserve new value
            $this->_valueCounterMap[$newValue] = 0;
            $value = $newValue;
        }

        if (! is_null($key))
            $this->_keyValueMap[$key] = $value;

        return $value;
    }

    /**
     * Returns all key-value pairs as an associative array.
     * @return array
     */
    public function values()
    {
        return $this->_keyValueMap;
    }
}

?>
