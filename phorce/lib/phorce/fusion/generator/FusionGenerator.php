<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\generator;

use \ArrayObject;
use phorce\common\php\PhpClass;
use phorce\common\php\PhpComment;
use phorce\common\php\PhpConstant;
use phorce\common\php\PhpFile;
use phorce\common\php\PhpFunction;
use phorce\common\php\PhpOutput;
use phorce\common\php\PhpVariable;
use phorce\common\property\PropertyObject;
use phorce\fusion\Constants;
use phorce\fusion\FusionException;
use phorce\fusion\binding\ClassBinding;
use phorce\fusion\binding\FusionBindingDAO;
use phorce\fusion\binding\xml\AttributeBinding;
use phorce\fusion\binding\xml\ElementBinding;
use phorce\fusion\binding\xml\ExtendsBinding;
use phorce\fusion\binding\xml\FusionBindingXmlDAO;
use phorce\fusion\binding\xml\RootBinding;
use phorce\fusion\binding\xml\TypeBinding;
use phorce\fusion\dataaccess\FusionContext;
use phorce\fusion\mapper\XmlPropertyMapper;
use phorce\fusion\registry\DefaultSchemaResolver;
use phorce\fusion\registry\SchemaRegistry;

/**
 * This class is used to generate PHP classes from an XML schema definition
 * (XSD).
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class FusionGenerator extends PropertyObject
{
    const APP_NAME = 'Fusion Generator';
    const APP_VERSION = '0.2';

    const MODE_CLASSES = 'classes';
    const MODE_BINDING = 'binding';

    const PREFIX_BINDING = '';
    const PREFIX_XML = 'x';

    /**
     * The mode of the generator.
     * @var string
     * @access private
     */
    private $_mode;

    /**
     * The XML schema file.
     * @var string
     * @access private
     */
    private $_schema;

    /**
     * The Fusion Binding file.
     * @var string
     * @access private
     */
    private $_binding;

    /**
     * The PHP namespace of generated classes for target XML namespace.
     * @var string
     * @access private
     */
    private $_namespace;

    /**
     * Additional PHP namespaces keyed by imported XML namespaces.
     * @var object ArrayObject
     * @access private
     */
    private $_importNamespaces;

    /**
     * Path to generate files.
     * @var string
     * @access private
     */
    private $_outputDir;

    /**
     * The name of the DAO class to generate.
     * @var string
     * @access private
     */
    private $_daoClass;

    /**
     * Creates a FusionGenerator.
     */
    public function __construct()
    {
        $this->_importNamespaces = new ArrayObject();
    }

    /**
     * Returns the mode of the generator.
     * @return string
     */
    public function getMode()
    {
        return $this->_mode;
    }

    /**
     * Sets the mode of the generator.
     * @param string $v
     */
    public function setMode($v)
    {
        $this->_mode = $v;
    }

    /**
     * Sets the XML schema file.
     * @param string $v
     */
    public function setSchema($v)
    {
        $this->_schema = $v;
    }

    /**
     * Sets the Fusion Binding file.
     * @param string $v
     */
    public function setBinding($v)
    {
        $this->_binding = $v;
    }

    /**
     * Sets the PHP namespace of generated classes for target XML namespace.
     * @param string $v
     */
    public function setNamespace($v)
    {
        $this->_namespace = $v;
    }

    /**
     * Adds the PHP namespace of generated classes for the specified XML
     * namespace.
     * @param string $xml XML namespace
     * @param string $php PHP namespace
     */
    public function addImportNamespace($xml, $php)
    {
        $this->_importNamespaces[$xml] = $php;
    }

    /**
     * Sets the path to generate files.
     * @param string $v
     */
    public function setOutputDir($v)
    {
        $this->_outputDir = $v;
    }

    /**
     * Sets the name of the DAO class.
     * @param string $v
     */
    public function setDaoClass($v)
    {
        $this->_daoClass = $v;
    }

    /**
     * Reads the XSD file and generates all class files.
     */
    public function generate()
    {
        // load and parse schema
        $registry = SchemaRegistry::getInstance();
        $sdef = $registry->findSchemaByLocation(
            basename($this->_schema),
            new DefaultSchemaResolver(dirname($this->_schema)));

        // construct the binding builder, registering namespaces
        $builder = new XmlBindingBuilder($sdef, $this->_namespace);
        foreach ($this->_importNamespaces as $xml => $php)
            $builder->registerImportedNamespace($xml, $php);

        // create binding from schema
        $binding = $builder->create();

        // generate class files for all classes in binding
        foreach ($binding->classes as $cbin)
            $this->_generateClassFile($cbin);

        // generate a DAO class
        $this->_generateDAO($this->_getDaoClassName(), $sdef->targetNamespace);

        // save binding file if specified
        if (! is_null($this->_binding))
        {
            $outfile = $this->_binding;
            echo "writing $outfile", PHP_EOL;

            $ctx = FusionContext::createForNamespace(FusionBindingDAO::XML_NAMESPACE);
            $m = $ctx->save($binding);
            $m->formatted = true;
            $m->setNamespacePrefix(FusionBindingDAO::XML_NAMESPACE, self::PREFIX_BINDING);
            $m->setNamespacePrefix(FusionBindingXmlDAO::XML_NAMESPACE, self::PREFIX_XML);
            $m->toFile($outfile);
            //$dao = new FusionBindingDAO($outfile);
            //$dao->save($binding);
        }
    }

    /**
     * Generates a class file for the given binding.
     * @param object ClassBinding $cbin
     * @access private
     */
    private function _generateClassFile(ClassBinding $cbin)
    {
        $filepath = self::_getFilePath($cbin->name);
        $file = new PhpFile(basename($filepath));

        // get type information
        foreach ($cbin->any as $structInfo)
        {
            if ($structInfo instanceof TypeBinding)
                $tbin = $structInfo;
            elseif ($structInfo instanceof ExtendsBinding)
                $ebin = $structInfo;
            elseif ($structInfo instanceof RootBinding)
                $rbin = $structInfo;

            // TODO: handle groups, sequences, and choices
        }

        // determine what class extends
        if (isset($ebin) && ! $ebin->type->anonymous)
            $extends = $ebin->name; // this is the class name
        else
            $extends = 'phorce\common\property\PropertyObject';

        // parse out classname & namespace
        list($namespace, $simpleClassName) = self::_parseClassName($cbin->name);
        $file->namespace = $namespace;

        // for readability: shorten extends class name and add full class name to uses
        list($ens, $simpleExtends) = self::_parseClassName($extends);
        if (! empty($ens) && $ens != $file->namespace)
            $file->uses[] = $extends;

        $class = new PhpClass($simpleClassName, $simpleExtends);
        $file->classes[] = $class;
        $file->path = dirname($filepath);

        $comment = new PhpComment(array(
            "Represents the complexType '{$tbin->name}' in the XSD '" . basename($this->_schema) . "'.",
            'This class was generated by ' . self::APP_NAME . ' v' . self::APP_VERSION . '.',
            '@author ' . self::APP_NAME
            ));
        $comment->usePhpDocStyle();
        $class->comment = $comment;

        $con = new PhpFunction('__construct');
        $con->visibility = PhpClass::VIS_PUBLIC;
        $class->methods[] = $con;

        // loop through all property bindings
        foreach ($cbin->properties as $pbin)
        {
            if ($pbin->readonly)
                $varname = 'pRO_' . $pbin->name;
            else
                $varname = 'p_' . $pbin->name;
            
            // get data element binding (within any data adapters)
            $mapper = new XmlPropertyMapper($pbin);
            $data = $mapper->getDataElementBinding();

            if ($data instanceof ContentBinding)
            {
                $var = new PhpVariable($varname);

                // add content value as optional constructor argument
                $con->parameters[] = new PhpVariable('v', 'null');
                $con->statements[] = '$this->' . $varname . ' = $v;';
            }
            else
            {
                // if there's a default value and it's a simple type, prepare it
                // FIXME: custom simple types are broken here
                $default = null;
                if (isset($data->default) && ! is_null($data->default) &&
                    $data->type->namespace == Constants::XSD_NAMESPACE_URI)
                {
                    // convert type and adapt value
                    $value = $mapper->convertFromResource($data->default);
                    $value = $mapper->adaptFromResource($value);

                    // determine string to output as default value
                    // depending on converted/adapted type
                    if (is_string($value))
                        $default = self::_escapeString($value);
                    elseif (is_bool($value))
                        $default = $value ? "true" : "false";
                    elseif (is_integer($value))
                        $default = $value;
                    elseif (is_float($value))
                        $default = sprintf("%F", $value);
                    elseif (is_object($value))
                    {
                        // objects have to be serialized/unserialized
                        //  (it's not ideal, but it's either this or enforcing
                        //   single-arg constructors and __toString() which is
                        //   more readable but not always garunteed to work)
                        $con->statements[] = '$this->' . $varname . ' = unserialize(' .
                            '(' . self::_escapeString(serialize($value)) . ');';
                    }
                }

                $var = new PhpVariable($varname, $default);
            }

            $var->visibility = PhpClass::VIS_PRIVATE;
            $class->variables[] = $var;

            // initialize arrays in constructor
            if ($pbin->many)
                $con->statements[] = '$this->' . $varname . ' = new ArrayObject();';

            /* The p_var property syntax removes the need for an explicit
             * accessor... at least for now.  Once they get return type hints
             * into PHP then we'll have a need for this code...

            // determine accessor name
            if ($pbin->type == 'boolean')
                $getname = 'is' . ucfirst($pbin->name);
            else
                $getname = 'get' . ucfirst($pbin->name);

            // declare accessor
            $get = new PhpFunction($getname);
            $get->visibility = PhpClass::VIS_PUBLIC;
            $get->statements[] = 'return $this->' . $varname . ';';
            $class->methods[] = $get;
             */

            // build mutator if not readonly
            if (! $pbin->readonly)
            {
                // determine type hint of mutator argument
                if ($pbin->many)
                {
                    if (array_search('\ArrayObject', $file->uses->getArrayCopy()) === false)
                        $file->uses[] = '\ArrayObject';

                    $typeHint = 'ArrayObject';
                }
                elseif ($pbin->type == 'object' && ! is_null($pbin->class))
                {
                    // 'use' class if not in same namespace
                    // and has not been 'used' before
                    list($pns, $pclass) = self::_parseClassName($pbin->class);
                    if ($pns != $file->namespace && array_search($pbin->class->getArrayCopy(), $file->uses) === false)
                        $file->uses[] = $pbin->class;

                    $typeHint = $pclass;
                }
                //elseif ($pbin->type == 'object')
                    //$typeHint = 'object';

                // only need to declare mutator if we have a type hint
                if (isset($typeHint))
                {
                    $set = new PhpFunction('set' . ucfirst($pbin->name));
                    $set->visibility = PhpClass::VIS_PUBLIC;
                    $setvar = new PhpVariable('v');
                    $setvar->typeHint = $typeHint;
                    $set->parameters[] = $setvar;
                    $set->statements[] = '$this->' . $varname . ' = $v;';
                    $class->methods[] = $set;
                }
            }
        }

        $this->_writePhpFile($file);
    }

    /**
     * Escapes a string value and surrounds it with quotes.
     * @param string $str String
     * @return string Escaped string
     * @access private
     * @static
     */
    private static function _escapeString($str)
    {
        return "'" . preg_replace("/(['\\\\])/", '\\1', $str) . "'";
    }

    /**
     * Generates a DAO class file with the specified name.
     * @param string $className Fully qualified class name
     * @param string $xmlNamespace The namespace of the DAO
     * @access private
     */
    private function _generateDAO($className, $xmlNamespace)
    {
        $filepath = self::_getFilePath($className);
        $file = new PhpFile(basename($filepath));

        list($phpNamespace, $simpleClassName) = self::_parseClassName($className);

        $file->namespace = $phpNamespace;
        $file->uses[] = 'phorce\fusion\dataaccess\FusionContext';
        $file->uses[] = 'phorce\fusion\dataaccess\FusionDAO';

        $class = new PhpClass($simpleClassName);
        $class->implements[] = 'FusionDAO';
        $file->classes[] = $class;
        $file->path = dirname($filepath);

        $comment = new PhpComment(array(
            "Used to load and save objects in the '{$xmlNamespace}' XML namespace.",
            'This class was generated by ' . self::APP_NAME . ' v' . self::APP_VERSION . '.',
            '@author ' . self::APP_NAME
            ));
        $comment->usePhpDocStyle();
        $class->comment = $comment;

        // add namespace constant
        $class->constants[] = new PhpConstant('XML_NAMESPACE', self::_escapeString($xmlNamespace));

        // add member variable for filename
        $var = new PhpVariable('_xmlFile');
        $var->visibility = PhpClass::VIS_PRIVATE;
        $class->variables[] = $var;

        // add member variable for context
        $var = new PhpVariable('_context');
        $var->visibility = PhpClass::VIS_PRIVATE;
        $class->variables[] = $var;

        // add constructor
        $con = new PhpFunction('__construct');
        $con->visibility = PhpClass::VIS_PUBLIC;
        $con->parameters[] = new PhpVariable('xmlFile');
        $con->parameters[] = new PhpVariable('validate', 'true');
        $con->statements[] = '$this->_xmlFile = $xmlFile;';
        $con->statements[] = '$this->_context = FusionContext::createForNamespace(self::XML_NAMESPACE, $validate);';
        $class->methods[] = $con;

        // add getFusionContext() method
        $gfc = new PhpFunction('getFusionContext');
        $gfc->visibility = PhpClass::VIS_PUBLIC;
        $gfc->statements[] = 'return $this->_context;';
        $class->methods[] = $gfc;

        // add load() method
        $load = new PhpFunction('load');
        $load->visibility = PhpClass::VIS_PUBLIC;
        $load->statements[] = 'return $this->_context->load()->fromFile($this->_xmlFile);';
        $class->methods[] = $load;

        // add save() method
        $save = new PhpFunction('save');
        $save->visibility = PhpClass::VIS_PUBLIC;
        $savevar = new PhpVariable('obj');
        //$savevar->typeHint = 'object';
        $save->parameters[] = $savevar;
        $save->statements[] = '$this->_context->save($obj)->toFile($this->_xmlFile);';
        $class->methods[] = $save;

        $this->_writePhpFile($file);
    }

    /**
     * Returns the filename given a type.
     * @param string $className Fully qualified class name
     * @return string Filename
     * @access private
     * @static
     */
    private static function _getFilePath($className)
    {
        return preg_replace("/\\\\/", DIRECTORY_SEPARATOR, $className) . '.php';
    }

    /**
     * Returns the unqualified class name and namespace separately.
     * @param string $className Fully qualified class name
     * @return array Namespace and unqualified classname
     * @access private
     * @static
     */
    private static function _parseClassName($className)
    {
        $parts = explode("\\", $className);
        $class = array_pop($parts);
        $namespace = implode("\\", $parts);
        return array($namespace, $class);
    }

    /**
     * Writes the PhpFile object to a file.
     * @param object PhpFile File to write
     * @access private
     */
    private function _writePhpFile(PhpFile $file)
    {
        // generate code
        $out = new PhpOutput();
        $out->startPhpCode();
        $file->getPhpCode($out);
        $out->endPhpCode();

        // get output file name
        $outfile = \phorce\makepath($this->_outputDir, $file->filePath);

        // make sure directory exists
        $dir = dirname($outfile);
        if (! file_exists($dir))
            mkdir($dir, 0777, true); // Good Thing: 0777 gets modified by umask
        elseif (! is_dir($dir))
            throw new FusionException("Output directory '$dir' exists as file");

        // write file
        echo "writing $outfile", PHP_EOL;
        if (! file_put_contents($outfile, $out->getOutputString() . PHP_EOL))
            throw new FusionException("Failed to create file '$outfile'");
    }

    /**
     * Returns the class name to use for the DAO derived from the XML schema
     * file or binding file name.
     * @return string Class name
     */
    private function _getDaoClassName()
    {
        if (! is_null($this->_daoClass))
            $daoClass = $this->_daoClass;
        else
        {
            // use binding or schema file
            $filename = $this->_binding;
            if (is_null($filename))
                $filename = $this->_schema;

            // strip path and extension
            list($filename, $ext) = explode('.', basename($filename));

            // split by space, dash, or underscore delimited word
            // and capitalize the first letter of each word
            $converted = '';
            foreach (preg_split('/[-_ ]/', $filename) as $v)
                $converted .= ucfirst($v);

            $daoClass = "{$converted}DAO";
        }

        // prepend namespace
        return "{$this->_namespace}\\{$daoClass}";
    }
}

?>
