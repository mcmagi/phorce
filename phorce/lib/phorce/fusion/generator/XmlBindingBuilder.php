<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\generator;

use \ArrayObject;
use phorce\fusion\FusionException;
use phorce\fusion\QName;
use phorce\fusion\binding\ClassBinding;
use phorce\fusion\binding\FusionBinding;
use phorce\fusion\binding\PropertyBinding;
use phorce\fusion\binding\xml\AnyAttributeBinding;
use phorce\fusion\binding\xml\AnyElementBinding;
use phorce\fusion\binding\xml\AttributeBinding;
use phorce\fusion\binding\xml\ContentBinding;
use phorce\fusion\binding\xml\ElementBinding;
use phorce\fusion\binding\xml\ExtendsBinding;
use phorce\fusion\binding\xml\RootBinding;
use phorce\fusion\binding\xml\TypeBinding;
use phorce\fusion\registry\XmlBindingRegistry;
use phorce\fusion\schema\AnyAttributeDefinition;
use phorce\fusion\schema\AnyDefinition;
use phorce\fusion\schema\AttributeContainer;
use phorce\fusion\schema\AttributeDefinition;
use phorce\fusion\schema\ComplexTypeDefinition;
use phorce\fusion\schema\DataTypeReference;
use phorce\fusion\schema\DefinitionBase;
use phorce\fusion\schema\ElementDefinition;
use phorce\fusion\schema\GroupDefinition;
use phorce\fusion\schema\InternalTypeDefinition;
use phorce\fusion\schema\ModelGroupDefinition;
use phorce\fusion\schema\SchemaDefinition;
use phorce\fusion\schema\SimpleTypeDefinition;

/**
 * This class is used to generate a Fusion Binding from an XML Schema Definition
 * file (XSD).
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlBindingBuilder
{
    /**
     * Used to generate unique class names.
     * @var object UniqueValueMap
     * @access private
     */
    private $_classNames;

    /**
     * Used to generate unique property names per class.  An associative array
     * of UniqueValueMaps.
     * @var object UniqueValueMap
     * @access private
     */
    private $_propertyNames = array();

    /**
     * An associative array of ClassBindings keyed by type QName.
     * @var array
     * @access private
     */
    private $_bindingsByType = array();

    /**
     * The XML schema being constructed.
     * @var object SchemaDefinition
     * @access private
     */
    private $_schema;

    /**
     * The PHP namespace of classes mapped from this schema.
     * @var string
     * @access private
     */
    private $_phpNamespace;

    /**
     * An array of PHP namespaces keyed by XML namespace.
     * @var object ArrayObject
     * @access private
     */
    private $_namespaces;

    /**
     * An array of XmlBindingBuilders keyed by PHP namespace.
     * @var object ArrayObject
     * @access private
     */
    private $_builders;

    /**
     * Creates a XmlBindingBuilder object which generates to a PHP namespace.
     * @param object SchemaDefinition $def The XML schema
     * @param string $phpNamespace The PHP namespace to generate to
     */
    public function __construct(SchemaDefinition $def, $phpns)
    {
        $this->_schema = $def;
        $this->_phpNamespace = $phpns;
        $this->_namespaces = new ArrayObject();
        $this->_builders = new ArrayObject();

        $this->_classNames = new UniqueValueMap();
    }

    /**
     * Registers the imported namespace, indicating that the specified XML
     * namespace should generate classes in the specified PHP namespace.
     * @param string $xmlns XML namespace URI
     * @param string $phpns PHP namespace
     */
    public function registerImportedNamespace($xmlns, $phpns)
    {
        $this->_namespaces[$xmlns] = $phpns;
    }

    /**
     * Returns the XmlBindingBuilder for the specified imported schema.
     * @param object SchemaDefinition $schema XML schema
     * @return object XmlBindingBuilder
     */
    private function _getBindingBuilder(SchemaDefinition $schema)
    {
        $xmlns = $schema->targetNamespace;
        $phpns = $this->_namespaces[$xmlns];

        //echo "getting binding builder $xmlns=$phpns\n";

        if (! isset($this->_builders[$phpns]))
        {
            // create binding builder for imported namespace
            $builder = new XmlBindingBuilder($schema, $phpns);
            $this->_builders[$xmlns] = $builder;

            // and we all share the same set of builders
            //  (smells sorta like hack... see below)
            $builder->_builders = $this->_builders;
        }

        return $this->_builders[$xmlns];
    }

    /**
     * Reads the XML schema and generates all class files into a FusionBinding
     * class.  If one is not specified, a new one will be created.
     * @param object FusionBinding $binding
     * @return object FusionBinding
     */
    public function create(FusionBinding $binding = null)
    {
        // FIXME: right now we're using the specification of the "binding"
        // argument to indicate this is a "child" builder.  This reeks of major
        // hack but it's 2am and I don't want to gut the whole class again.
        // Anyway, we really should do this very differently, preferably with
        // some parent class to manage all the builders.
        $child = ! is_null($binding);

        if (is_null($binding))
            $binding = new FusionBinding();

        foreach ($this->_schema->complexTypes as $type)
            $this->_createClassBinding($binding, $type);

        foreach ($this->_schema->elements as $type)
            $this->_createRootBinding($binding, $type);

        if (! $child)
        {
            foreach ($this->_builders as $builder)
                $builder->create($binding);
        }

        return $binding;
    }

    /**
     * Creates a root binding from an element definition in a schema.
     * @param object FusionBinding $binding
     * @param object ElementDefinition $e
     * @return object RootBinding
     */
    private function _createRootBinding(FusionBinding $binding, ElementDefinition $e)
    {
        // create root binding
        $root = new RootBinding();
        $root->name = $e->name;
        $root->namespace = $e->schema->targetNamespace;

        // get type reference
        $typeRef = $e->typeReference;

        if ($typeRef->nestedType)
        {
            // if nested, create new class binding
            $class = $this->_createClassBinding($binding, $typeRef->resolve());
        }
        elseif (isset($this->_bindingsByType["{$typeRef->qname}"]))
        {
            // existing class binding
            $class = $this->_bindingsByType["{$typeRef->qname}"];
        }
        else
        {
            // edits in schema classes should prevent this, but just in case...
            throw new FusionException("Could not resolve type '{$type->qname}' of root element '{$e->name}' to a class binding.");
        }

        // associate root with class
        $class->any[] = $root;

        return $root;
    }

    /**
     * Creates a class binding from a complex type definition in a schema.
     * @param object FusionBinding $binding
     * @param object ComplexTypeDefinition $type
     * @return object ClassBinding
     */
    private function _createClassBinding(FusionBinding $binding, ComplexTypeDefinition $type, ClassBinding $parent = null)
    {
        $class = new ClassBinding();
        $class->name = $this->_getClassName($type);

        // create type binding for name/namespace
        $tbin = new TypeBinding();
        $tbin->name = $type->name;
        $tbin->namespace = $type->schema->targetNamespace;
        $tbin->anonymous = is_null($type->name);
        $class->any[] = $tbin;

        // create parent binding if nested type
        if (! is_null($parent))
        {
            $pbin = new ParentBinding();
            $pbin->name = $parent->name;
            $class->any[] = $pbin;
        }

        $this->_mapAttributes($class, $type);

        if (! is_null($type->group))
            $this->_mapGroup($binding, $class, $type->group);
        elseif (! is_null($type->modelGroup))
            $this->_mapModelGroup($binding, $class, $type->modelGroup);

        $baseTypeRef = $type->baseTypeReference;

        // content or extension?
        if (! is_null($baseTypeRef))
        {
            if ($type->simpleContent && $baseTypeRef->builtin)
            {
                // If simpleContent and base is a builtin type,
                // create content property
                $prop = new PropertyBinding();
                $prop->name = $this->_getPropertyName($class->name);
                $prop->any = new ContentBinding();

                $prop->any->type = new TypeBinding();
                $prop->any->type->name = $baseTypeRef->qname->name;
                $prop->any->type->namespace = $baseTypeRef->qname->namespace;
                $prop->any->type->anonymous = is_null($baseTypeRef->qname->name);

                // resolve to PHP type
                list($prop->type, $prop->class) = $this->_resolvePhpType($typeRef);

                $class->properties[$prop->name] = $prop;
            }
            else
            {
                // If complex content OR if simpleContent and base is a
                // complexType, extend the base

                $extends = new ExtendsBinding();
                $extends->type = new TypeBinding();
                $extends->type->name = $baseTypeRef->qname->name;
                $extends->type->namespace = $baseTypeRef->qname->namespace;
                $extends->type->anonymous = is_null($baseTypeRef->qname->name);
                $class->any[] = $extends;

                // resolve to PHP type
                list($tmp, $extends->name) = $this->_resolvePhpType($baseTypeRef);
            }
        }

        // add to binding
        $binding->classes[$class->name] = $class;

        // add to bindingsByType assoc array keyed by type qname
        if (! $tbin->anonymous)
        {
            $qname = new QName($tbin->name, $tbin->namespace);
            $this->_bindingsByType["$qname"] = $class;
        }

        return $class;
    }

    private function _mapAttributes(ClassBinding $class, AttributeContainer $ac)
    {
        // attribute groups
        foreach ($ac->attributeGroups as $ag)
        {
            if ($ag->reference)
                $ag = $ag->objectReference->resolve();

            $this->_mapAttributes($class, $ag);
        }

        // attributes
        foreach ($ac->attributes as $a)
        {
            // do not make a property if it is never to be rendered
            if ($a->prohibited)
                return;

            if ($a->reference)
                $a = $a->objectReference->resolve();

            $prop = new PropertyBinding();
            $prop->name = $this->_getPropertyName($class->name, $a);
            $prop->any = new AttributeBinding();
            $prop->any->name = $a->name;
            if ($a->qualified)
                $prop->any->namespace = $a->schema->targetNamespace;

            $typeRef = $a->typeReference;

            if (! $typeRef->builtin)
            {
                // if we are referencing a simple type,
                // then it should extend a builtin type
                $type = $typeRef->resolve();
                if ($type instanceof SimpleTypeDefinition)
                {
                    // loop through base types until we find the builtin type
                    $typeRef = $type->baseTypeReference;
                    while (! $typeRef->builtin)
                        $typeRef = $typeRef->resolve()->baseTypeReference;
                }
            }

            $prop->any->type = new TypeBinding();
            $prop->any->type->name = $typeRef->qname->name;
            $prop->any->type->namespace = $typeRef->qname->namespace;
            $prop->any->type->anonymous = is_null($typeRef->qname->name);
            $prop->any->required = $a->required;

            if (! is_null($a->default))
                $prop->any->default = $a->default;
            elseif (! is_null($a->fixed))
            {
                $prop->any->default = $a->default;
                $prop->readonly = true;
            }

            // resolve to PHP type
            list($prop->type, $prop->class) = $this->_resolvePhpType($typeRef);

            $class->properties[$prop->name] = $prop;
        }

        // any attributes
        if (! is_null($ac->anyAttribute))
        {
            $prop = new PropertyBinding();
            $prop->name = $this->_getPropertyName($class->name,
                $ac->anyAttribute);
            $prop->type = "mixed";
            $prop->many = true;

            $prop->any = new AnyAttributeBinding();
            $prop->any->namespace = $ac->anyAttribute->namespace;
            $prop->any->processContents = $ac->anyAttribute->processContents;
        }
    }

    /**
     * Maps the particles in a group to the specified class binding.
     * @param object FusionBinding $binding
     * @param object ClassBinding $class
     * @param object GroupDefinition $g
     */
    private function _mapGroup(FusionBinding $binding, ClassBinding $class, GroupDefinition $g)
    {
        // do not make a property if it is never to be rendered
        if ($g->prohibited)
            return;

        // TODO: handle multiple groups as a new class
        if ($g->multiple)
            throw new FusionException("Multiple groups not supported in this release.");

        // resolve reference
        if ($g->reference)
            $g = $g->objectReference->resolve();

        $this->_mapModelGroup($binding, $class, $g->modelGroup);
    }

    /**
     * Maps the particles in a model group to the specified class binding.
     * @param object FusionBinding $binding
     * @param object ClassBinding $class
     * @param object ModelGroupDefinition $mg
     */
    private function _mapModelGroup(FusionBinding $binding, ClassBinding $class, ModelGroupDefinition $mg)
    {
        // do not make a property if it is never to be rendered
        if ($mg->prohibited)
            return;

        $multiAll = false;
        if ($mg->multiple)
        {
            // TODO: handle multiple sequences/choices as a new class
            if ($mg->sequence || $mg->choice)
                throw new FusionException("Multiple sequences or choices not supported in this release.");
            else
            {
                // We can have only elements in an All
                // If the all is multiple, then force all properties to be multiple
                $multiAll = true;
            }
        }

        //echo "mapping modelgroup: {$mg->qname->name} to {$class->name}\n";

        // particles
        foreach ($mg->particles as $p)
        {
            // do not make a property if it is never to be rendered
            if ($p->prohibited)
                continue;

            //echo "mapping particle: {$p->qname->name} to {$class->name}\n";

            if ($p instanceof GroupDefinition)
            {
                // recurse through group
                $this->_mapGroup($class, $p);
            }
            elseif ($p instanceof ModelGroupDefinition)
            {
                // recurse through model-group
                $this->_mapModelGroup($class, $p);
            }
            else
            {
                $prop = new PropertyBinding();
                $prop->many = $p->multiple || $multiAll;

                // property is required if BOTH element and model group are required
                $required = $p->required && $mg->required;

                if ($p instanceof AnyDefinition)
                {
                    // wildcard
                    $prop->name = $this->_getPropertyName($class->name, $p);
                    $prop->type = "mixed";

                    $prop->any = new AnyElementBinding();
                    $prop->any->namespace = $p->namespace;
                    $prop->any->processContents = $p->processContents;
                    $prop->any->required = $required;
                }
                elseif ($p instanceof ElementDefinition)
                {
                    $prop->any = new ElementBinding();
                    $prop->any->required = $required;

                    // resolve reference
                    if ($p->reference)
                        $p = $p->objectReference->resolve();

                    $prop->name = $this->_getPropertyName($class->name, $p);
                    $prop->any->name = $p->name;
                    if ($p->qualified)
                        $prop->any->namespace = $p->schema->targetNamespace;

                    $typeRef = $p->typeReference;

                    if (! $typeRef->builtin)
                    {
                        // if we are referencing a simple type,
                        // then it should extend a builtin type
                        $type = $typeRef->resolve();
                        if ($type instanceof SimpleTypeDefinition)
                        {
                            // loop through base types until we find the builtin type
                            $typeRef = $type->baseTypeReference;
                            while (! $typeRef->builtin)
                                $typeRef = $typeRef->resolve()->baseTypeReference;
                        }
                    }

                    $prop->any->type = new TypeBinding();
                    $prop->any->type->name = $typeRef->qname->name;
                    $prop->any->type->namespace = $typeRef->qname->namespace;
                    $prop->any->type->anonymous = is_null($typeRef->qname->name);

                    // resolve to PHP type
                    list($prop->type, $prop->class) = $this->_resolvePhpType($typeRef);

                    // define class binding also for nested complex types
                    if ($typeRef->nested && $typeRef->nestedType instanceof ComplexTypeDefinition)
                        $this->_createClassBinding($binding, $typeRef->nestedType, $class);

                    if (! is_null($p->default))
                        $prop->any->default = $p->default;
                    elseif (! is_null($p->fixed))
                    {
                        $prop->any->default = $p->default;
                        $prop->readonly = true;
                    }
                }

                $class->properties[$prop->name] = $prop;
            }
        }
    }

    /**
     * Resolves the specified type reference to a primitive PHP type or class.
     * @param object DataTypeReference $ref
     * @return array Type and class
     * @access private
     */
    private function _resolvePhpType(DataTypeReference $typeRef)
    {
        $typedef = $typeRef->resolve();

        if ($typedef instanceof InternalTypeDefinition)
        {
            // resolve to primitive PHP type or class
            $type = $typedef->phpType;
            $class = $typedef->class;
        }
        elseif ($typedef instanceof SimpleTypeDefinition)
        {
            list($type, $class) = $this->_resolvePhpType(
                $typedef->baseTypeReference);
        }
        elseif ($typedef instanceof ComplexTypeDefinition)
        {
            $type = "object";

            if ($typeRef->qname->namespace == $this->_schema->targetNamespace)
            {
                // type is in target namespace
                $class = $this->_getClassName($typedef);
            }
            else
            {
                // type is in a different (imported) namespace

                if (isset($this->_namespaces[$typeRef->qname->namespace]))
                {
                    // a PHP namespace was explicity declared for it,
                    // so get builder for namespace to generate its classes
                    $builder = $this->_getBindingBuilder($typedef->schema);
                    //echo "type '{$typeRef->qname}' in target namespace '{$typeRef->qname->namespace}' resolves to '{$builder->_phpNamespace}'\n";
                    list($type, $class) = $builder->_resolvePhpType($typeRef);
                }
                else
                {
                    // attempt to locate namespace
                    $bfactory = XmlBindingRegistry::getInstance();
                    $cbin = $bfactory->findByType($typeRef->qname);

                    if (! is_null($cbin))
                    {
                        // type is registered in binding factory
                        //echo "type '{$typeRef->qname}' found as class '{$cbin->name}'\n";
                        $class = $cbin->name;
                    }
                    else
                    {
                        // cannot locate type - throw error
                        throw new FusionException("Type '{$typeRef->qname}' not found.  "
                            . "Either define a binding for this schema in Fusion configuration "
                            . "or specify a PHP namespace where its classes should be generated.");

                        // FIXME:
                        // Previous idea was to generate classes in current PHP
                        // namespace, but that is now abandonded.  Seems better
                        // for now to require the user to explicity define what
                        // they want to do with it, but I'd still like some
                        // default behavior.  JAXB will automagically pick a
                        // namespace based on a root element in the schema.
                    }
                }
            }
        }

        return array($type, $class);
    }

    /**
     * Gets a unique class name for the type.  May append a number to the name.
     * @param object ComplexTypeDefinition $typedef
     * @return string Unique class name
     */
    private function _getClassName(ComplexTypeDefinition $typedef)
    {
        $name = $this->_classNames->getValue(
            $typedef->name, $typedef->className);
        return $this->_phpNamespace . "\\" . $name;
    }

    /**
     * Gets a unique property name for a data object within a class.  May
     * append a number to the name.
     * @param string $className Class Name
     * @param object DataObjectDefinitionBase $objdef
     * @return string Unique property name
     */
    private function _getPropertyName($className, DefinitionBase $objdef = null)
    {
        // if value map does not exist for this class yet, create one
        if (! isset($this->_propertyNames[$className]))
            $this->_propertyNames[$className] = new UniqueValueMap();

        // determine key and suggested value
        if ($objdef instanceof AttributeDefinition)
        {
            $objname = "a:{$objdef->name}";
            $propname = $objdef->propertyName;
        }
        elseif ($objdef instanceof ElementDefinition)
        {
            $objname = "e:{$objdef->name}";
            $propname = $objdef->propertyName;
        }
        elseif ($objdef instanceof AnyAttributeDefinition)
        {
            $objname = 'anyAttribute';
            $propname = 'anyAttribute';
        }
        elseif ($objdef instanceof AnyDefinition)
        {
            $objname = null; // XXX: no way to key it?
            $propname = 'any';
        }
        elseif (is_null($objdef))
        {
            $objname = 'content';
            $propname = 'content';
        }
        else
            throw new FusionException("Cannot get property name for data object " . get_class($objdef));

        // get actual value
        return $this->_propertyNames[$className]->getValue($objname, $propname);
    }
}

?>
