<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\registry;

/**
 * Provides a means to resolve a 'schemaLocation' attribute to locate an XML
 * Schema file.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
interface SchemaResolver
{
    /**
     * Resolves the schema location value into it's actual location.
     * @param string $schemaLocation Schema location from XML document or schema
     * @return string Action location of file
     */
    public function resolve($schemaLocation);
}

?>
