<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\registry;

use \ArrayObject;
use \Exception;
use phorce\common\cache\FusionDAOConfigFileLoader;
use phorce\common\cache\PhorceConfigCache;
use phorce\fusion\FusionException;
use phorce\fusion\QName;
use phorce\fusion\binding\ClassBinding;
use phorce\fusion\binding\xml\ParentBinding;
use phorce\fusion\binding\xml\RootBinding;
use phorce\fusion\binding\xml\TypeBinding;

/**
 * This class is used to register and retrieve ClassBinding objects.  The
 * ClassBinding represents the association between an XML data element and a
 * PHP class.
 *
 * An association can be registered either "explicity" (via a binding file) or
 * "implicity" (via a schema and PHP namespace).  If a binding file is given,
 * either through configuration or registerBindingFile(), then the binding will
 * be loaded and used as specified in the file.  If a schema file and PHP
 * namespace are given, either through configuration or registerSchemaFile(),
 * then the binding configuration will be generated directly from the schema,
 * with classes expected to exist under the PHP namespace.
 *
 * An association may also be registered explicity and programmatically by
 * custom-building a ClassBinding object and invoking registerClassBinding().
 *
 * Bindings are keyed (and therefore retrieved) in one of three ways: by class
 * name, by XML type, or by root element name.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlBindingRegistry extends BindingRegistry
{
    /**
     * The location of the configuration file.
     */
    const CONFIG_FILE = 'fusion-config.xml';
    const CONFIG_DAO_CLASS = 'phorce\fusion\config\FusionConfigDAO';

    const CONFIG_SCHEMA = 'fusion-config.xsd';
    const BINDING_SCHEMA = 'fusion-binding.xsd';
    const XML_BINDING_SCHEMA = 'fusion-binding-xml.xsd';

    const CONFIG_PHPNS = 'phorce\fusion\config';
    const BINDING_PHPNS = 'phorce\fusion\binding';
    const XML_BINDING_PHPNS = 'phorce\fusion\binding\xml';

    /**
     * The string used to represent the XML data resource.
     */
    const RESOURCE = 'xml';

    /**
     * An array of ClassBinding objects keyed by type qname.
     * @var object ArrayObject
     * @access private
     */
    private $_bindingsByType;

    /**
     * An array of ClassBinding objects keyed by root qname.
     * @var object ArrayObject
     * @access private
     */
    private $_bindingsByRoot;

    /**
     * An array of ClassBinding objects keyed by parent class name.
     * @var object ArrayObject
     * @access private
     */
    private $_bindingsByParent;

    /**
     * Returns the singleton XmlBindingRegistry instance.
     * @return object XmlBindingRegistry
     * @static
     */
    public static function getInstance()
    {
        return BindingRegistry::getInstance(self::RESOURCE);
    }

    /**
     * Protected constructor enforces use of factory method and singleton
     * pattern.
     * @access protected
     */
    protected function __construct()
    {
        parent::__construct();

        // initialize assoc arrays
        $this->_bindingsByType = new ArrayObject();
        $this->_bindingsByRoot = new ArrayObject();
    }

    /**
     * Initializes the registry.
     * @access protected
     */
    protected function init()
    {
        parent::init();

        // register Fusion internal bindings
        // These bindings are required in order for Fusion to work.  We MUST
        // register them by schema file and not binding file however, because
        // registerBindingFile won't work until the binding schema is registered
        $this->registerSchemaFile(self::CONFIG_SCHEMA, self::CONFIG_PHPNS);
        $this->registerSchemaFile(self::BINDING_SCHEMA, self::BINDING_PHPNS);
        $this->registerSchemaFile(self::XML_BINDING_SCHEMA, self::XML_BINDING_PHPNS);

        // load fusion configuration
        $configFile = \phorce\makepath(PHORCE_CONFIG, self::CONFIG_FILE);
        $loader = new FusionDAOConfigFileLoader(
            $configFile, self::CONFIG_DAO_CLASS);
        $config = PhorceConfigCache::retrieveConfig('fusion-config', $loader);

        // register each binding in configuration
        foreach ($config->xmlBindings->bindings as $bindconf)
        {
            try
            {
                if (! is_null($bindconf->file))
                    $this->registerBindingFile($bindconf->file);
                elseif (! is_null($bindconf->schema))
                    $this->registerSchemaFile($bindconf->schema, $bindconf->phpNamespace);
            }
            catch (Exception $e)
            {
                // print any errors on initialization as warnings
                if (! is_null($bindconf->file))
                    $msg = "Unable to load binding file {$bindconf->file}";
                elseif (! is_null($bindconf->schema))
                    $msg = "Unable to load bindings from schema {$bindconf->schema}";

                trigger_error("{$msg}: {$e->getMessage()}", E_USER_WARNING);
            }
        }
    }

    /**
     * Registers the specified schema file to a PHP namespace using a default
     * binding.  The classes associated with the schema file are expected to
     * reside in this namespace.  If they do not, this class will throw an
     * exception.
     * @param string $file Schema file
     * @param string $phpns PHP namespace
     */
    public function registerSchemaFile($file, $phpns)
    {
        $loader = new XmlSchemaBindingFileLoader($file, $phpns);
        $binding = PhorceConfigCache::retrieveConfig(
            "fusion-binding-$file", $loader);

        // add them to the config
        foreach ($binding->classes as $class)
            $this->registerClassBinding($class);
    }

    /**
     * Registers the specified ClassBinding to the factory configuration.
     * If the class does not exist, it will throw an exception.
     * @param object ClassBinding $class The binding to add
     */
    public function registerClassBinding(ClassBinding $class)
    {
        //echo "registering class binding {$class->name}<br>\n";

        // key by class name
        parent::registerClassBinding($class);

        // loop through data structure bindings
        foreach ($class->any as $struct)
        {
            if ($struct instanceof TypeBinding)
            {
                // key by type name
                $q = new QName($struct->name, $struct->namespace);
                //echo "registering class binding by type {$q}<br>\n";
                $this->_bindingsByType["$q"] = $class;
            }
            elseif ($struct instanceof RootBinding)
            {
                // key by root element name
                $q = new QName($struct->name, $struct->namespace);
                //echo "registering class binding by root {$q}<br>\n";
                $this->_bindingsByRoot["$q"] = $class;
            }
            elseif ($struct instanceof ParentBinding)
            {
                // key by parent class name
                //echo "registering class binding by parent class {$struct->name}<br>\n";
                $this->_bindingsByParent["$struct->name"] = $class;
            }
        }

        //echo "number of any's: " . count($class->any) . "<br>\n";
        //echo "number of properties: " . count($class->properties) . "<br>\n";
    }

    /**
     * Returns the ClassBinding by parent class name.  Returns null if not
     * found.
     * @param string $class The class name
     * @return object ClassBinding
     */
    public function findByParent($class)
    {
        return isset($this->_bindingsByParent[$class])
            ? $this->_bindingsByParent[$class] : null;
    }

    /**
     * Returns the ClassBinding by root element name.  Returns null if not
     * found.
     * @param object QName $root QName of the root element
     * @return object ClassBinding
     */
    public function findByRoot(QName $root)
    {
        return isset($this->_bindingsByRoot["$root"])
            ? $this->_bindingsByRoot["$root"] : null;
    }

    /**
     * Returns the ClassBinding by class name.  Returns null if not found.
     * @param object QName $type QName of the XML type
     * @return object ClassBinding
     */
    public function findByType(QName $type)
    {
        return isset($this->_bindingsByType["$type"])
            ? $this->_bindingsByType["$type"] : null;
    }
}

?>
