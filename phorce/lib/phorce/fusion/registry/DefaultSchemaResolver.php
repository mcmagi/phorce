<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\registry;

/**
 * A SchemaResolver which resolves relative path names to a configurable
 * directory.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DefaultSchemaResolver implements SchemaResolver
{
    /**
     * The location of the directory to look for schemas.
     * @var string
     * @access private
     */
    private $_dir;

    /**
     * Constructs a schema resolver for the specified directory.
     * @param string $dir
     */
    public function __construct($dir)
    {
        $this->_dir = $dir;
    }

    /**
     * Resolves the schema location value into it's actual location.
     * @param string $schemaLocation Schema location from XML document or schema
     * @return string Actual location of file
     */
    public function resolve($schemaLocation)
    {
        $url = parse_url($schemaLocation);
        if ($this->_isUrl($url))
        {
            // if url, see if we can find it locally first
            $file = \phorce\makepath($this->_dir, basename($url["path"]));
            if (file_exists($file))
                return realpath($file);

            return $schemaLocation;
        }

        // if relative URI, prepend PWD
        if (! preg_match("/^[\\\\\/]/", $schemaLocation))
            $schemaLocation = \phorce\makepath($this->_dir, $schemaLocation);

        return realpath($schemaLocation);
    }

    /**
     * Returns true if the URL has a scheme which is not a DOS drive letter.
     * @param array $url
     * @return True if it looks like a real URL
     */
    private function _isUrl(array $url)
    {
        return isset($url["scheme"]) && ! preg_match("/^[C-Z]$/", $url["scheme"]);
    }
}

?>
