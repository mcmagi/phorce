<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\registry;

use \ArrayObject;
use phorce\common\cache\FusionDAOConfigFileLoader;
use phorce\common\cache\PhorceConfigCache;
use phorce\fusion\FusionException;
use phorce\fusion\binding\ClassBinding;

/**
 * This class is used to register and retrieve ClassBinding objects for a
 * particular data resource.  The ClassBinding represents the association
 * between a data structure within the resource and a data object (or PHP
 * class).
 *
 * This is an abstract base class for resource-specific binding factories.
 *
 * An association may also be registered explicity and programmatically by
 * custom-building a ClassBinding object and invoking registerClassBinding().
 *
 * Bindings are keyed (and therefore retrieved) in by class name.  Subclasses
 * may implement different implementation-specific retrieval methods.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
abstract class BindingRegistry
{
    const BINDING_DAO_CLASS = 'phorce\fusion\binding\FusionBindingDAO';

    /**
     * Contains singleton instances of BindingFactories, keyed by resource.
     * @staticvar array
     * @access private
     */
    private static $_instance = array();

    /**
     * An array of ClassBinding objects keyed by class name.
     * @var object ArrayObject
     * @access private
     */
    private $_bindingsByClass;

    /**
     * Returns one of the singleton BindingRegistry instances.
     * @return object BindingRegistry
     * @static
     */
    public static function getInstance($resource)
    {
        if (! isset(self::$_instance[$resource]))
        {
            self::$_instance[$resource] = new XmlBindingRegistry();
            self::$_instance[$resource]->init();
        }
        return self::$_instance[$resource];
    }

    /**
     * Private constructor enforces use of factory method and singleton pattern.
     * @access protected
     */
    protected function __construct()
    {
        // initialize assoc arrays
        $this->_bindingsByClass = new ArrayObject();
    }

    /**
     * Initializes the registry.
     * @access protected
     */
    protected function init()
    {
        // make sure XML is always initialized first
        // This is to make sure the binding schema gets loaded,
        // which is required for registerBindingFile to work.
        if (! $this instanceof XmlBindingRegistry)
            XmlBindingRegistry::getInstance();
    }

    /**
     * Registers the specified binding file with the factory.  If a class
     * referenced in the binding file does not exist, this method will throw an
     * exception.  The file path will be considered relative to PHORCE_HOME.
     * @param string $file Binding file
     */
    public function registerBindingFile($file)
    {
        $file = \phorce\makepath(PHORCE_HOME, $file);

        $loader = new FusionDAOConfigFileLoader($file,
            'phorce\fusion\binding\FusionBindingDAO', false);
        $binding = PhorceConfigCache::retrieveConfig(
            "fusion-binding-$file", $loader);
        
        // add them to the config
        foreach ($binding->classes as $class)
            $this->registerClassBinding($class);
    }

    /**
     * Registers the specified ClassBinding to the factory configuration.
     * If the class does not exist, it will throw an exception.
     * @param object ClassBinding $class The binding to add
     */
    public function registerClassBinding(ClassBinding $class)
    {
        //echo "registering class binding {$class->name}<br>\n";

        // if has class name, key by class name
        //  (this should be present except for anonymous/on-the-fly bindings)
        //  (which really aren't quite implemented yet...)
        if (! is_null($class->name))
        {
            // make sure class exists before registering it
            if (! class_exists($class->name))
                throw new FusionException("Registering class {$class->name}: not found");

            $this->_bindingsByClass[$class->name] = $class;
        }
    }

    /**
     * Returns the ClassBinding by class name.  Returns null if not found.
     * @param string $class Name of the class
     * @return object ClassBinding
     */
    public function findByClass($class)
    {
        return isset($this->_bindingsByClass[$class])
            ? $this->_bindingsByClass[$class] : null;
    }
}

?>
