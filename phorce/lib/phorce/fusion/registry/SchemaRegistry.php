<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\registry;

use \ArrayObject;
use \RecursiveDirectoryIterator;
use \RecursiveIteratorIterator;
use phorce\fusion\FusionException;
use phorce\fusion\schema\SchemaDefinition;

define('PHORCE_FUSION_SCHEMA_DIR',
    \phorce\makepath(PHORCE_CONFIG, "schema"));

/**
 * This class is used as a registry for all XML Schemas.  It provides
 * functionality to load a new or retrieve an existing SchemaDefinition.  It
 * also allows you to lookup any configuration information for a namespace
 * without returning the entire schema (see {@link SchemaNamespaceConfig}).
 * All schemas under PHORCE_HOME/config/schema are registered by default.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class SchemaRegistry
{
    /**
     * The default directory where the schemas are expected to be located.
     */
    const SCHEMA_DIR = PHORCE_FUSION_SCHEMA_DIR;

    /**
     * The singleton instance of this class.
     * @var object SchemaRegistry
     * @access private
     * @static
     */
    private static $_instance;

    /**
     * An array of SchemaNamespaceConfigs keyed by namespace.
     * @var object ArrayObject
     * @access private
     */
    private $_configs;

    /**
     * An array of SchemaDefinitions keyed by schema location.
     * @var object ArrayObject
     * @access private
     */
    private $_schemas;

    /**
     * Returns the singleton SchemaRegistry instance.
     * @return object SchemaRegistry
     * @static
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance))
        {
            self::$_instance = new SchemaRegistry();
            self::$_instance->_initialize();
        }
        return self::$_instance;
    }

    /**
     * Private constructor enforces use of factory method and singleton pattern.
     * @access private
     */
    private function __construct()
    {
        $this->_schemas = new ArrayObject();
        $this->_configs = new ArrayObject();
    }

    /**
     * Loads all schema files we can find from the SCHEMA_DIR.
     * @access private
     */
    private function _initialize()
    {
        // look up all schema files in config/schema directory
        $it = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator(self::SCHEMA_DIR),
            RecursiveIteratorIterator::SELF_FIRST);
        foreach ($it as $name => $file)
        {
            // register XSD files (with relative path name)
            if ($file->isFile() && preg_match('/\.xsd$/', $name))
            {
                $this->findSchemaByLocation(
                    preg_replace("/^[\\\\\/]/", '',
                        substr($name, strlen(self::SCHEMA_DIR))));
            }
        }
    }

    /**
     * Locates a registered schema by namespace URI.  If not found, returns
     * null.
     * @param string $namespace Namespace URI
     * @return object SchemaDefinition
     */
    public function findSchemaByNamespace($namespace)
    {
        $nscfg = $this->_lookupNamespace($namespace);

        // this should only be false for null
        if (isset($this->_schemas[$nscfg->resolvedSchemaLocation]))
            return $this->_schemas[$nscfg->resolvedSchemaLocation];

        return null;
    }

    /**
     * Locates or registers a schema by its location (URI or path and filename).
     * If not already registered, it will attempt to register the schema at the
     * specified location.
     * @param string $location URI or path/filename to schema
     * @param object SchemaResolver $resolver Resolves filenames
     * @return object SchemaDefinition
     */
    public function findSchemaByLocation($location, SchemaResolver $resolver = null)
    {
        // resolve path to schema file
        if (is_null($resolver))
            $resolver = new DefaultSchemaResolver(self::SCHEMA_DIR);
        $rlocation = $resolver->resolve($location);

        // if schema is not found, register it
        if (! isset($this->_schemas[$rlocation]))
            $this->registerSchema($location, $resolver);

        return $this->_schemas[$rlocation];
    }

    /**
     * Registers a schema in the factory.  A registered schema will be
     * accessible as a FusionContext by target namespace via a call to
     * {@link #getFusionContext()}.  An exception will be thrown if the
     * target namespace specified in the schema is already registered.
     * @param string $schema The location of the XML schema file
     * @param object SchemaResolver $resolver A resolver for schemaLocations
     * @return string The target namespace of the schema
     */
    public function registerSchema($location, SchemaResolver $resolver = null)
    {
        //echo "registering schema $location\n";

        static $i = 0;
        if ($i++ == 20)
            throw new FusionException("infinite recursion detected");

        // resolve path to schema file
        if (is_null($resolver))
            $resolver = new DefaultSchemaResolver(self::SCHEMA_DIR);
        $rlocation = $resolver->resolve($location);

        //echo "resolved $location = $rlocation\n";

        if (! file_exists($rlocation))
            throw new FusionException("Schema '{$rlocation}' does not exist");

        if (! isset($this->_schemas[$rlocation]))
        {
            // load schema file
            $sdef = new SchemaDefinition($rlocation);

            // key it using the resolved location
            $this->_schemas[$rlocation] = $sdef;

            // Schemas can be hierarchical - they can include schema files from
            // the same namespace.  However, we may not always load them in a
            // consistent order.  Therefore, need to do some fuzzy logic here:

            // case 1:
            // * Parent schema is registered first:
            // * Create new namespace config.  There will be no entry for that
            //   namespace before.
            // * When a child is registered, don't replace parent's namespace
            //   config, but add it to its list of includes.

            // case 2:
            // * Child schema is registered first:
            // * Create new namespace config.  There will be no entry for that
            //   namespace before.
            // * If a "peer" (another child) is registered next, just add peer
            //   schema to list of includes. (behaves as in case 1)
            // * When the parent is registered, mark child as include and
            //   replace $nscfg->schema with parent schema.

            $namespace = $sdef->targetNamespace;
            if (! isset($this->_configs[$namespace]))
            {
                $this->_configs[$namespace] =
                    new SchemaNamespaceConfig($location, $rlocation, $namespace);
            }
            else
            {
                $nscfg = $this->_configs[$namespace];

                if ($sdef->hasInclude($nscfg->resolvedSchemaLocation))
                {
                    // $sdef is parent
                    // replace definition with newly identified parent schema
                    $this->_configs[$namespace] =
                        new SchemaNamespaceConfig($location, $rlocation, $namespace);
                }
            }
        }
        else
        {
            $namespace = $this->_schemas[$schema]->targetNamespace;
            trigger_error("Schema '$schema' registered with SchemaRegistry twice.", E_WARNING);
        }

        $i--;

        return $namespace;
    }

    /**
     * Returns the namespace configuration with the specified URI.
     * @param string $namespace namespace URI
     * @return object SchemaNamespaceConfig
     */
    public function lookupNamespace($namespace)
    {
        if (isset($this->_configs[$namespace]))
            return $this->_configs[$namespace];
        return null;
    }
}

?>
