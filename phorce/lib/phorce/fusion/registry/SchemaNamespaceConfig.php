<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\registry;

use phorce\common\property\PropertyObject;

/**
 * Contains namespace configuration for an XML schema.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class SchemaNamespaceConfig extends PropertyObject
{
    /**
     * The location of the XML schema.
     * @var string
     * @access private
     */
    private $_schemaLocation;

    /**
     * The resolved location of the XML schema.
     * @var string
     * @access private
     */
    private $_resolvedSchemaLocation;

    /**
     * The schema namespace.
     * @var string
     * @access private
     */
    private $_namespace;

    /**
     * Constructs a namespace configuration with the specified information.
     * @param string $schema The path to the XML schema
     * @param string $namespace The schema namespace
     */
    public function __construct($schemaLoc, $resolvedSchemaLoc, $namespace)
    {
        $this->_namespace = $namespace;
        $this->_schemaLocation = $schemaLoc;
        $this->_resolvedSchemaLocation = $resolvedSchemaLoc;
    }

    /**
     * Returns the path to the XML schema.
     * @return string
     */
    public function getSchemaLocation()
    {
        return $this->_schemaLocation;
    }

    /**
     * Returns the resolved path to the XML schema.
     * @return string
     */
    public function getResolvedSchemaLocation()
    {
        return $this->_resolvedSchemaLocation;
    }

    /**
     * Returns the schema namespace.
     * @return string
     */
    public function getNamespace()
    {
        return $this->_namespace;
    }
}

?>
