<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\registry;

use phorce\common\cache\PhorceConfigFileLoader;
use phorce\fusion\generator\XmlBindingBuilder;

/**
 * This class is used to load bindings from a schema file when it is needed for
 * the config cache.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlSchemaBindingFileLoader extends PhorceConfigFileLoader
{
    /**
     * The PHP namespace.
     * @var string
     * @access private
     */
    private $_phpns;

    /**
     * The location of the XML schema.
     * @var string
     * @access private
     */
    private $_schemaLocation;

    /**
     * Constructs the binding file loader for an XML Schema.
     * @param string $file Schema file
     * @param string $phpns PHP namespace
     */
    public function __construct($file, $phpns)
    {
        // resolve actual location of schema file for constructor
        $resolver = new DefaultSchemaResolver(SchemaRegistry::SCHEMA_DIR);
        parent::__construct($resolver->resolve($file));

        $this->_phpns = $phpns;
        $this->_schemaLocation = $file;
    }

    /**
     * Loads the configuration from the schema file.
     * @return mixed Loaded configuration object
     */
    public function loadConfig()
    {
        $registry = SchemaRegistry::getInstance();
        $schema = $registry->findSchemaByLocation($this->_schemaLocation);

        // generate the bindings we need from the schema
        $builder = new XmlBindingBuilder($schema, $this->_phpns);
        return $builder->create();
    }
}

?>
