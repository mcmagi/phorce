<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\mapper;

use \ArrayObject;
use \DOMDocument;
use \DOMElement;
use \DOMNode;
use \Traversable;
use phorce\common\property\PropertyUtils;
use phorce\fusion\Constants;
use phorce\fusion\FusionException;
use phorce\fusion\QName;
use phorce\fusion\adapter\DataAdapterRegistry;
use phorce\fusion\binding\ClassBinding;
use phorce\fusion\binding\DataAdpterBinding;
use phorce\fusion\binding\PropertyBinding;
use phorce\fusion\binding\xml\AnyAttributeBinding;
use phorce\fusion\binding\xml\AnyElementBinding;
use phorce\fusion\binding\xml\AttributeBinding;
use phorce\fusion\binding\xml\ContentBinding;
use phorce\fusion\binding\xml\ElementBinding;
use phorce\fusion\binding\xml\RootBinding;
use phorce\fusion\generator\UniqueValueMap;
use phorce\fusion\object\XmlValue;
use phorce\fusion\registry\SchemaRegistry;
use phorce\fusion\registry\XmlBindingRegistry;
use phorce\fusion\type\XmlTypeConverterFactory;

/**
 * Writes an object to an XML document.  The ClassBinding determines the
 * structure of the XML document and is derived from the object's class.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlDocumentWriter
{
    /**
     * Namespace prefix of of XML Schema Instance (XSI) schema.
     */
    const XSI_PREFIX = 'xsi';

    // attributes from XSI schema
    const XSI_SCHEMA_LOCATION = 'schemaLocation';
    const XSI_NO_NAMESPACE_SCHEMA_LOCATION = 'noNamespaceSchemaLocation';
    const XSI_TYPE = 'type';
    const XSI_NIL = 'nil';

    /**
     * The DOM Document.
     * @var object DOMDocument
     * @access private
     */
    private $_document;

    /**
     * Used to manage a unique set of prefixes keyed by namespace.
     * @var object UniqueValueMap
     * @access private
     */
    private $_nsPrefixMap;

    /**
     * The location of the schema for elements with no namespace.
     * @param string
     * @access private
     */
    private $_noNamespaceSchemaLocation;

    /**
     * Creates an XmlDocumentWriter with the specified configuration.
     * @param array $nsPrefixMap
     * @param string $noNamespaceSchemaLocation
     */
    public function __construct(array $nsPrefixMap, $noNamespaceSchemaLocation)
    {
        $this->_document = new DOMDocument();
        $this->_nsPrefixMap = new UniqueValueMap();

        // register all namespace prefixes
        $this->_registerNamespacePrefix(Constants::XSI_NAMESPACE_URI, self::XSI_PREFIX);
        foreach ($nsPrefixMap as $namespace => $prefix)
            $this->_registerNamespacePrefix($namespace, $prefix);

        $this->_noNamespaceSchemaLocation = $noNamespaceSchemaLocation;
    }

    /**
     * Maps the root object in the binding to a DOMDocument.  If the QName is
     * not given, it will be derived from the class binding.
     * @param mixed $obj The root object to map
     * @param object QName $qname The qualified name of the root object
     * @return object DOMDocument Mapped document
     */
    public function mapRootObject($obj, QName $root = null)
    {
        // map root element
        $this->_mapElement($this->_document, $root, $obj);

        // create xmlns:prefix for all namespaces
        $root = $this->_document->documentElement;
        foreach ($this->_nsPrefixMap->values() as $namespace => $prefix)
        {
            $xmlns = empty($prefix) ? 'xmlns' : "xmlns:{$prefix}";
            $root->setAttribute($xmlns, $namespace);
        }
        $root->setAttribute('xmlns:xsi', Constants::XSI_NAMESPACE_URI);

        // add xsi:schemalocation
        $registry = SchemaRegistry::getInstance();
        $locations = array();
        foreach ($this->_nsPrefixMap->values() as $namespace => $prefix)
        {
            $nscfg = $registry->lookupNamespace($namespace);
            if (! is_null($nscfg))
                $locations[] = "$namespace " . $nscfg->schemaLocation;
        }
        if (count($locations) > 0)
        {
            $this->_mapAttribute($root,
                new QName(self::XSI_SCHEMA_LOCATION, Constants::XSI_NAMESPACE_URI),
                implode(' ', $locations));
        }

        // add xsi:noNamespaceSchemaLocation
        if (! empty($this->_noNamespaceSchemaLocation))
        {
            $this->_mapAttribute($root,
                new QName(self::XSI_NO_NAMESPACE_SCHEMA_LOCATION, Constants::XSI_NAMESPACE_URI),
                $this->_noNamespaceSchemaLocation);
        }

        return $this->_document;
    }

    /**
     * Derives the root element name of the given object from the class binding.
     * If the class binding for the object does not have a root binding, then
     * an exception will be thrown.
     * @param mixed $obj The root object
     * @return object QName Root element name
     * @access private
     * @static
     */
    private static function _getRootElementName($obj)
    {
        //echo 'get root element for class: ' . get_class($obj) . "\n";

        $cbin = self::_findClassBinding($obj);

        // locate the root binding for the root element
        foreach ($cbin->any as $struct)
        {
            if ($struct instanceof RootBinding)
                $root = new QName($struct->name, $struct->namespace);
        }

        if (! isset($root))
            throw new FusionException("No root element name for class '" . get_class($obj) . "'");

        //echo "returning: {$qname}\n";

        return $root;
    }

    /**
     * Maps the specified value as a sub-element of the DOM element.  The value
     * may be an object or a non-object.  If QName is given as null, then the
     * element will be looked up as a root element.  The last two arguments are
     * optional as they are not present for the root element.
     * @param object DOMNode $e The parent DOM element
     * @param object QName $q The QName of the child element
     * @param mixed $value The element value
     * @param object PropertyBinding The property binding for the element
     * @access private
     */
    public function _mapElement(DOMNode $n, QName $qname = null, $value,
        PropertyBinding $prop = null)
    {
        // determine root object name if not given
        if (is_null($qname))
            $qname = $this->_getRootElementName($value);

        //echo "mapping element: {$qname}\n";

        // create element node
        //echo "colonized name: {$this->_getColonizedName($qname)}\n";
        $e = $this->_document->createElement(
            $this->_getColonizedName($qname));

        if (! is_object($value))
        {
            // set as content
            $this->_mapContent($e, $value);

            if (is_null($value) && ! is_null($prop) && $prop->any->nillable)
                $this->_mapAttribute($e, new QName(self::XSI_NIL, Constants::XSI_NAMESPACE_URI), true);
        }
        else
        {
            //echo 'binding as class: ' . get_class($value) . "\n";
            // set as complex type
            $cbin = self::_findClassBinding($value);
            $this->_mapClass($e, $cbin, $value);

            // add xsi:type if it is not the type we expect
            //  (ignore xsd:any though)
            if (! is_null($prop) &&
                ! $prop->any instanceof AnyElementBinding &&
                ! is_null($prop->any->type))
            {
                // get the qname of the expected type
                $expqname = new QName($prop->any->type->name, $prop->any->type->namespace);

                // now get the actual instance type
                foreach ($cbin->any as $struct)
                {
                    if ($struct instanceof TypeBinding)
                        $type = $struct;
                }

                if (isset($type) && ! $type->anonymous)
                {
                    $typeqname = new QName($type->name, $type->namespace);

                    if ($typeqname != $expqname)
                    {
                        // types are not equal: create xsi:type attribute
                        $this->_mapAttribute($e,
                            new QName(self::XSI_TYPE, Constants::XSI_NAMESPACE_URI),
                            $this->_getColonizedName($typeqname));
                    }
                }
            }
        }

        // add element to parent node
        $n->appendChild($e);

        return $e;
    }

    /**
     * Maps the specified value as an attribute of the DOM element.
     * @param object DOMElement $e The parent DOM element
     * @param object QName $q The QName of the attribute
     * @param mixed $value The attribute value
     * @access private
     */
    private function _mapAttribute(DOMElement $e, QName $qname, $value)
    {
        $e->setAttribute(
            $this->_getColonizedName($qname),
            $value);
    }

    /**
     * Maps the specified value as text content of the DOM element.  Appends
     * the text to the element before returning it.
     * @param object DOMElement $e The parent DOM element
     * @param mixed $value The text content
     * @return object DOMText Newly created text node
     * @access private
     */
    private function _mapContent(DOMElement $e, $value)
    {
        $t = $this->_document->createTextNode($value);
        $e->appendChild($t);
        return $t;
    }

    /**
     * Maps the properties of the specified object to the DOM Element.
     * @param object DOMElement $e The parent DOM element
     * @param object ClassBinding The class binding
     * @param mixed $obj Object to map
     * @access private
     */
    private function _mapClass(DOMElement $e, ClassBinding $cbin, $obj)
    {
        foreach ($cbin->properties as $prop)
        {
            $mapper = new XmlPropertyMapper($prop);

            // get data element binding (within any data adapters)
            $data = $mapper->getDataElementBinding();

            // get property value
            $value = PropertyUtils::getProperty($obj, $prop->name);

            // skip empty values
            if (is_null($value) || count($value) == 0)
                continue;

            // make an array out of single values (prep for loop)
            if (! is_array($value) && ! $value instanceof Traversable)
                $value = new ArrayObject(array($value));

            // and iterate through all values
            foreach ($value as $v)
            {
                // get qname
                $qname = null;
                if ($v instanceof XmlValue)
                    $qname = $v->qname;
                elseif ($data instanceof AttributeBinding || $data instanceof ElementBinding)
                    $qname = new QName($data->name, $data->namespace);

                // get value
                if ($v instanceof XmlValue)
                    $v = $v->value;
                else
                {
                    // adapt value (if necessary)
                    $v = $mapper->adaptToResource($v);

                    // and do type conversion
                    if (isset($data->type) && $data->type->namespace == Constants::XSD_NAMESPACE_URI)
                        $v = $mapper->convertToResource($v);
                }

                if ($data instanceof AttributeBinding)
                {
                    //echo "setting attribute {$qname} = {$v}\n";
                    $this->_mapAttribute($e, $qname, $v);
                }
                elseif ($data instanceof AnyAttributeBinding)
                {
                    //echo "setting any attribute {$qname} = {$v}\n";
                    $this->_mapAttribute($e, $qname, $v);
                }
                elseif ($data instanceof ElementBinding)
                {
                    //echo "setting element {$qname}\n";
                    if (! is_null($v) || $data->required)
                        $this->_mapElement($e, $qname, $v, $prop);
                }
                elseif ($data instanceof AnyElementBinding)
                {
                    //echo "setting any {$qname} = {$v}\n";
                    if (! is_null($v) || $data->required)
                        $this->_mapElement($e, $qname, $v, $prop);
                }
                elseif ($data instanceof ContentBinding)
                {
                    //echo "setting content = {$v}\n";
                    $this->_mapContent($e, $v);
                }
            }
        }
    }

    /**
     * Resolves the QName into a colonized "prefix:name" format.  If no
     * namespace is given, then it will just return the name without a prefix.
     * @param object QName Namespace-qualified name
     * @return string Colonized name
     */
    private function _getColonizedName(QName $q)
    {
        if (! is_null($q->namespace))
        {
            // determine prefix for namespace
            $prefix = $this->_getNamespacePrefix($q->namespace);
            $cname = empty($prefix) ?
                $q->name : "{$prefix}:{$q->name}";
        }
        else
            $cname = $q->name;

        return $cname;
    }

    /**
     * Returns a unique prefix for the namespace within the document.  If the
     * namespace has no prefix registered for it, then the prefix "ns" will
     * attempt to be registered.  See {@link #_registerNamespacePrefix()}.
     * @param string $namespace Namespace URI
     * @return string Prefix for namespace
     * @access private
     */
    private function _getNamespacePrefix($namespace)
    {
        return $this->_nsPrefixMap->getValue($namespace, "ns");
    }

    /**
     * Registers the requested prefix for the specified namespace.  Note that
     * in the case the requested prefix is already registered, it will append
     * a number to the end of the prefix, ensuring uniqueness.
     * @param string $namespace Namespace URI
     * @param string $prefix Requested prefix
     * @access private
     */
    private function _registerNamespacePrefix($namespace, $prefix)
    {
        return $this->_nsPrefixMap->setValue($namespace, $prefix);
    }

    /**
     * Gets the class binding for the specified object.  Throws an exception if
     * it cannot be found.
     * @param mixed $obj Data object
     * @return object ClassBinding
     * @access private
     * @static
     */
    private static function _findClassBinding($obj)
    {
        //echo 'getting class binding for class: ' . get_class($obj) . "\n";
        $factory = XmlBindingRegistry::getInstance();
        $cbin = $factory->findByClass(get_class($obj));

        if (is_null($cbin))
            throw new FusionException("No binding for class '{$class}'");

        return $cbin;
    }
}
