<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\mapper;

use phorce\fusion\FusionException;
use phorce\fusion\QName;
use phorce\fusion\adapter\DataAdapterRegistry;
use phorce\fusion\binding\DataAdapterBinding;
use phorce\fusion\binding\PropertyBinding;
use phorce\fusion\type\XmlTypeConverterFactory;

/**
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlPropertyMapper
{
    private $_property;

    /**
     * @param object PropertyBinding $prop Property binding
     */
    public function __construct(PropertyBinding $prop)
    {
        $this->_property = $prop;
    }

    public function getPropertyBinding()
    {
        return $this->_property;
    }

    /**
     * Returns the data element binding for the property (within any data
     * adapters).
     * @return object DataElementBinding Nested data element binding
     */
    public function getDataElementBinding()
    {
        // get data element binding (within any data adapters)
        $data = $this->_property->any;
        while ($data instanceof DataAdapterBinding)
            $data = $data->any;
        return $data;
    }

    /**
     * Loops through all DataAdapterBindings between the PropertyBinding and
     * the DataElementBinding, adapting each value along the way.  If there are
     * no DataAdapterBindings nested beneath the PropertyBinding, then no
     * action will be taken.
     * @param mixed $value Value to adapt
     * @return mixed Adapted value
     */
    public function adaptFromResource($value)
    {
        // There may be anywhere from 0 to many DataAdapterBindings
        // between the PropertyBinding and the DataElementBinding.
        // Loop through them and invoke each adapter found in reverse order.
        $adapters = array();
        for ($dab = $this->_property->any; $dab instanceof DataAdapterBinding; $dab = $binding->any)
        {
            $dfactory = DataAdapterRegistry::getInstance();
            $adapters[] = $dfactory->getAdapter($dab);
        }

        // adapt values in reverse order
        for ($i = count($adapters) - 1; $i >= 0; $i--)
            $value = $adapters[$i]->fromResource($dab, $value);

        return $value;
    }

    /**
     * Loops through all DataAdapterBindings between the PropertyBinding and
     * the DataElementBinding, adapting each value along the way.  If there are
     * no DataAdapterBindings nested beneath the PropertyBinding, then no
     * action will be taken.
     * @param mixed $value Value to adapt
     * @return mixed Adapted value
     */
    public function adaptToResource($value)
    {
        // There may be anywhere from 0 to many DataAdapterBindings
        // between the PropertyBinding and the DataElementBinding.
        // Loop through them and invoke each adapter found.
        for ($dab = $this->_property->any; $dab instanceof DataAdapterBinding; $dab = $binding->any)
        {
            // adapt value
            $dfactory = DataAdapterRegistry::getInstance();
            $value = $dfactory->getAdapter($dab)->toResource($dab, $value);
        }
        return $value;
    }

    /**
     * Converts a simple value to the appropriate PHP type.
     * @param mixed $v Value to convert
     * @param string $type XML Type (from xsi:type)
     * @return mixed Converted value
     */
    public function convertFromResource($v, $type = null)
    {
        if (is_null($v))
            return $v;

        if (is_null($type))
            $type = $this->getTypeQName()->name;

        $tconv = XmlTypeConverterFactory::getInstance()->findByXmlType($type);
        return $tconv->fromResource($v);
    }

    /**
     * Converts a PHP value to the appropriate simple type.
     * @param mixed $v Value to convert
     * @return mixed Converted value
     */
    public function convertToResource($v)
    {
        if (is_null($v))
            return $v;

        $type = $this->getTypeQName()->name;

        $tconv = XmlTypeConverterFactory::getInstance()->findByXmlType($type);
        return $tconv->toResource($v);
    }

    /**
     * Returns the XML TypeBinding for this property.
     * @return object TypeBinding
     */
    public function getTypeBinding()
    {
        $deb = $this->getDataElementBinding();

        if (is_null($deb))
            throw new FusionException("No data element binding for property '{$this->_property->name}'");
        elseif (! isset($deb->type))
            throw new FusionException("No XML type information for property '{$this->_property->name}'");

        return $deb->type;
    }

    /**
     * Returns the QName of the type for this property.
     * @return object QName
     */
    public function getTypeQName()
    {
        $type = $this->getTypeBinding();
        return new QName($type->name, $type->namespace);
    }
}

?>
