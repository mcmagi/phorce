<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\mapper;

use \ArrayObject;
use \DOMAttr;
use \DOMDocument;
use \DOMElement;
use \DOMText;
use phorce\common\property\PropertyUtils;
use phorce\fusion\Constants;
use phorce\fusion\FusionException;
use phorce\fusion\QName;
use phorce\fusion\adapter\DataAdapterRegistry;
use phorce\fusion\binding\ClassBinding;
use phorce\fusion\binding\DataAdapterBinding;
use phorce\fusion\binding\PropertyBinding;
use phorce\fusion\binding\xml\AnyAttributeBinding;
use phorce\fusion\binding\xml\AnyElementBinding;
use phorce\fusion\binding\xml\AttributeBinding;
use phorce\fusion\binding\xml\ContentBinding;
use phorce\fusion\binding\xml\ElementBinding;
use phorce\fusion\object\DataObject;
use phorce\fusion\object\XmlAttribute;
use phorce\fusion\object\XmlElement;
use phorce\fusion\object\XmlValue;
use phorce\fusion\registry\XmlBindingRegistry;
use phorce\fusion\type\XmlTypeConverterFactory;

/**
 * This class is used to read from an XML document and produce XML data objects.
 * Data objects come in one of two varieties: class-bound and anonymous.
 * Class-bound data objects are strongly typed.  They are mapped to a PHP class
 * using a ClassBinding.  The ClassBinding in this case is detailed in a
 * fusion-binding XML document.  The anonymous data object however has no class
 * binding detailed in an a fusion-binding XML document.  Despite this, it still
 * has a ClassBinding, but it is generated on-the-fly with properties named by
 * convention.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlDocumentReader
{
    const XSI_TYPE = 'xsi:type';

    /**
     * The document to read from.
     * @var object DOMDocument
     * @access private
     */
    private $_doc;

    /**
     * Constructs a new XmlDocumentReader instance.
     * @param object DOMDocument $doc The document to read from
     */
    public function __construct(DOMDocument $doc)
    {
        $this->_doc = $doc;
    }

    /**
     * Creates a data object with properties defined in the specified class
     * binding and sourced by the contained DOM document.  If the binding has a
     * class name associated with it, an instance of that class is returned.  If
     * no class name is present, a {@link DataObject} is returned.
     * @param object ClassBinding $class
     * @return object Generated object
     */
    public function mapDocument(ClassBinding $class)
    {
        return $this->_mapComplexType($class, $this->_doc->documentElement);
    }

    /**
     * Creates a data object with properties defined in the specified class
     * binding and sourced by the specified DOM element.  If the binding has a
     * class name associated with it, an instance of that class is returned.  If
     * no class name is present, an {@link DataObject} is returned.
     * @param object ClassBinding Binding information for this data type
     * @param object DOMElement The DOM element the data is sourced from
     * @return object Generated object
     * @access private
     */
    private function _mapComplexType(ClassBinding $class, DOMElement $e)
    {
        // create class
        if (is_null($class->name))
            // anonymous object
            $obj = new DataObject($class);
        else
            // class-bound object
            $obj = new $class->name;

        // associative arrays for indexing properties by attribute/element name
        $attrPropBindings = new ArrayObject();
        $elemPropBindings = new ArrayObject();
        $contentPropBinding = null;
        $anyAttrPropBinding = null;
        $anyElemPropBindings = new ArrayObject();

        // index property bindings by attribute/element name
        foreach ($class->properties as $prop)
        {
            // get data element binding (within any data adapters)
            $data = $prop->any;
            while ($data instanceof DataAdapterBinding)
                $data = $data->any;

            if (isset($data->name) && isset($data->namespace))
                $q = new QName($data->name, $data->namespace);

            if ($data instanceof AttributeBinding)
                $attrPropBindings["$q"] = $prop;
            elseif ($data instanceof AnyAttributeBinding)
                $anyAttrPropBinding = $prop;
            elseif ($data instanceof ElementBinding)
                $elemPropBindings["$q"] = $prop;
            elseif ($data instanceof AnyElementBinding)
                $anyElemPropBindings[] = $prop;
            elseif ($data instanceof ContentBinding)
                $contentPropBinding = $prop;
        }

        unset($prop);

        // loop through all attributes
        for ($i = 0; $i < $e->attributes->length; $i++)
        {
            $node = $e->attributes->item($i);
            if ($node instanceof DOMAttr)
            {
                // skip XSI - only needed to avoid unmatched attribute warnings
                if ($node->namespaceURI == Constants::XSI_NAMESPACE_URI)
                    continue;

                $q = new QName($node->name, $node->namespaceURI);

                $anyAttr = false;
                if (isset($attrPropBindings["$q"]))
                {
                    // attribute is always single-valued simple-type
                    $prop = $attrPropBindings["$q"];
                }
                elseif (! is_null($anyAttrPropBinding))
                {
                    // anyAttribute (array of simple-type values)
                    $prop = $anyAttrPropBinding;
                    $anyAttr = true;
                }
                else
                    trigger_error("Cannot find a matching binding for attribute $q", E_USER_WARNING);

                // do not map if we cannot find a property binding
                // or if the property is readonly
                //  (note: the latter means that readonly properties cannot be
                //   adapted - this is really because they lack a mutator)
                if (isset($prop) && ! $prop->readonly)
                {
                    $mapper = new XmlPropertyMapper($prop);

                    // get value
                    if ($anyAttr)
                    {
                        // wrap in XmlAttribute if this is a wildcard
                        //  (no type conversion can be done)
                        $value = new XmlAttribute($node->value, $q);
                    }
                    else
                    {
                        // otherwise, map as simple type
                        $value = $mapper->convertFromResource($node->value);

                    }

                    // and adapt value (if necessary)
                    $value = $mapper->adaptFromResource($value);

                    if ($prop->many)
                    {
                        $arr = PropertyUtils::getProperty($obj, $prop->name);
                        $arr[] = $value;
                    }
                    else
                        PropertyUtils::setProperty($obj, $prop->name, $value);

                    unset($prop);
                }
            }
        }

        // loop through all simple/complex content
        $anyIdx = 0;
        $lastAny = false;
        for ($i = 0; $i < $e->childNodes->length; $i++)
        {
            $node = $e->childNodes->item($i);
            if ($node instanceof DOMElement)
            {
                $q = QName::createFromDOM($node);
                if (isset($elemPropBindings["$q"]))
                {
                    // get property binding for 'any' element
                    $prop = $elemPropBindings["$q"];

                    if ($lastAny)
                    {
                        // if last element mapped to an 'any'
                        // need to increment to next 'any' property
                        $anyIdx++;
                        $lastAny = false;
                    }
                }
                elseif (isset($anyElemPropBindings[$anyIdx]))
                {
                    // get property binding for 'any' element
                    $prop = $anyElemPropBindings[$anyIdx];

                    // flag that we are in an 'any'
                    $lastAny = true;
                }
                else
                    // log a warning - unexpected content will get lost
                    trigger_error("Cannot find a matching binding for element $q", E_USER_WARNING);

                if (isset($prop))
                {
                    // map property value
                    //  (will take care of type conversion/adaptation for us)
                    $value = $this->_mapElement($class, $prop, $node);

                    //echo "mapping element property value: {$node->tagName} => {$prop->name}<br>\n";

                    if ($prop->many)
                    {
                        $arr = PropertyUtils::getProperty($obj, $prop->name);
                        $arr[] = $value;

                        /* - keys will be done differently.  saving the code for later though.
                        if (is_null($prop->key))
                        {
                            // no key to this array
                            $arr[] = $value;
                        }
                        elseif (self::isArray($value))
                        {
                            // we should have a key, but cannot get it as a property of an array
                            throw new FusionException("Cannot derive a key for property '{$prop->name}' from an array.");
                        }
                        else
                        {
                            // we have a key
                            $keyval = PropertyUtils::getProperty($value, $key);

                            // check if it's ok to use...
                            if (self::isArray($keyval))
                                // key itself cannot be an array
                                throw new FusionException("Key for property '{$prop->name}' is an array.");
                            else if (is_object($keyval) && method_exists($keyval, "__toString"))
                                // if an object, must be __toString'able,
                                throw new FusionException("Key for property '{$prop->name}' is an object without a __toString() method.");

                            // otherwise, we have a valid key
                            $arr["{$keyval}"] = $value;
                        } */
                    }
                    else
                        PropertyUtils::setProperty($obj, $prop->name, $value);

                    unset($prop);
                }
            }
            elseif ($node instanceof DOMText)
            {
                if (! is_null($contentPropBinding))
                {
                    $mapper = new XmlPropertyMapper($contentPropBinding);

                    // get value
                    $value = $mapper->convertFromResource($node->wholeText);

                    // and adapt value (if necessary)
                    $value = $mapper->adaptFromResource($value);

                    PropertyUtils::setProperty($obj, $contentPropBinding->name, $value);
                }
            }
        }

        return $obj;
    }

    /**
     * Loops through all DataAdapterBindings between the PropertyBinding and
     * the DataElementBinding, adapting each value along the way.  If there are
     * no DataAdapterBindings nested beneath the PropertyBinding, then no
     * action will be taken.
     * @param object PropertyBinding $prop Property binding
     * @param mixed $value Value to adapt
     * @return mixed Adapted value
     */
    public function adaptValue(PropertyBinding $prop, $value)
    {
        // There may be anywhere from 0 to many DataAdapterBindings
        // between the PropertyBinding and the DataElementBinding.
        // Loop through them and invoke each adapter found in reverse order.
        $adapters = array();
        for ($dab = $prop->any; $dab instanceof DataAdapterBinding; $dab = $binding->any)
        {
            $dfactory = DataAdapterRegistry::getInstance();
            $adapters[] = $dfactory->getAdapter($dab);
        }

        // adapt values in reverse order
        for ($i = count($adapters) - 1; $i >= 0; $i--)
            $value = $adapters[$i]->fromResource($dab, $value);

        return $value;
    }

    /**
     * Converts a DOMElement to either a simple value or class-bound value.
     * @param object ClassBinding $class
     * @param object PropertyBinding $prop
     * @param object DOMElement $e
     * @return mixed
     * @access private
     */
    private function _mapElement(ClassBinding $class, PropertyBinding $prop, DOMElement $e)
    {
        //echo "entering _mapElement({$class->name}->{$prop->name})<br>\n";

        // get binding factory for looking up types
        $bfactory = XmlBindingRegistry::getInstance();

        // create mapper
        $mapper = new XmlPropertyMapper($prop);

        // get data element binding (within any data adapters)
        $data = $mapper->getDataElementBinding();

        // strategy:
        //  - if element has xsi:type - use that type
        //  - otherwise, get type from property binding (if available)
        //  if type is determined:
        //  - if XSD namespace, convert as simple type
        //  - otherwise, look up class binding by type name
        //  if no type could be determined:
        //  - lookup class by root name
        //  - if not found, set value = XmlElement
        //  adapt value

        if ($e->hasAttribute(self::XSI_TYPE))
        {
            // if the element was so kind as to tell us its type...
            $tqname = QName::createFromDOM($e, $e->getAttribute(self::XSI_TYPE));
            //echo "got xsi:type ($tqname)<br>\n";
        }
        elseif (isset($data->type) && ! is_null($data->type))
        {
            // otherwise, get type information from data element binding
            // note that xsd:any's will not have these
            $tqname = $mapper->getTypeQName();
            //echo "got type information ($tqname)<br>\n";
        }

        // was type determined?
        if (isset($tqname))
        {
            if ($tqname->namespace == Constants::XSD_NAMESPACE_URI)
            {
                //echo "treating as simple type: {$tqname->name}<br>\n";
                // treat as simple type
                $value = $mapper->convertFromResource($e->textContent, $tqname->name);
            }
            else
            {
                // treat as complex type
                if (is_null($tqname->name))
                {
                    // anonymous (inner) type - find by ParentBinding
                    $childclass = $bfactory->findByParent($class->name);
                    //echo "complex type (found by parent) with class {$childclass->name}<br>\n";
                    if (is_null($childclass))
                        throw new FusionException("Property {$class->name}->{$prop->name}: No binding found for parent class '{$class->name}'");
                }
                else
                {
                    // named type - find by qname
                    $childclass = $bfactory->findByType($tqname);
                    //echo "complex type (found by type) with class {$childclass->name}<br>\n";
                    if (is_null($childclass))
                        throw new FusionException("Property {$class->name}->{$prop->name}: No binding found for type '{$tqname}'");

                    // FIXME: what if the xsi:type is a custom simple type?
                    // Then we either have to look at the schema here to find it
                    // or change the binding schema to incorporate simple types.
                }

                $value = $this->_mapComplexType($childclass, $e);
            }
        }
        else
        {
            // we get here in an xsd:any with no xsi:type information

            // find by root element name
            $qname = QName::createFromDOM($e);
            $childclass = $bfactory->findByRoot($qname);

            if (! is_null($childclass))
            {
                // class binding found by root element name
                // map to class using class binding definition
                //echo "complex type (found by root) with class {$childclass->name}<br>\n";
                $value = $this->_mapComplexType($childclass, $e);
            }
            else
            {
                // class binding not found - wrap text content in xml element
                //echo "{$prop->name}: class binding not found: use XmlElement<br>\n";
                $value = new XmlElement($e->textContent, $qname);

                // FIXME - what if it's a local element with complex content?
                // I guess there's only so much we can really do for now.
            }
        }

        // adapt value (if necessary)
        $value = $mapper->adaptFromResource($value);

        return $value;
    }

    /**
     * Returns true if the object is an array object or XmlValue containing
     * an array.
     * @param mixed $value
     * @return boolean True if an array
     */
    private static function isArray($value)
    {
        return is_array($value) || $value instanceof ArrayObject ||
            ($value instanceof XmlValue && (is_array($value->value) || $value->value instanceof ArrayObject));
    }
}

?>
