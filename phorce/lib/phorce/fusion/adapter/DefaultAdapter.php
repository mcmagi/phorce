<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\adapter;

use phorce\fusion\FusionException;
use phorce\fusion\binding\Adapter;
use phorce\fusion\binding\DataAdapterBinding;

/**
 * The DefaultAdapter is essentially a wrapper around another DataAdapter.  It
 * is provided as a convenience so that users may create custom DataAdapters
 * without having to define an XML element or register them in the
 * DataAdapterFactory.  Users can simply add the element
 * <code>&lt;adapter class="myAdapterClass"&gt;</code> to their binding file.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DefaultAdapter implements DataAdapter
{
    /**
     * Adapts a value when reading from a resource.
     * @param mixed $value Value on resource
     * @return mixed Value on object
     */
    public function fromResource(DataAdapterBinding $dab, $value)
    {
        return $this->_createAdapter($dab)->fromResource($dab, $value);
    }

    /**
     * Adapts a value when writing to a resource.
     * @param mixed $value Value on object
     * @return mixed Value on resource
     */
    public function toResource(DataAdapterBinding $dab, $value)
    {
        return $this->_createAdapter($dab)->toResource($dab, $value);
    }

    /**
     * Creates the configured data adapter.
     * @param object Adapter $dab Default adapter binding
     * @return object DataAdapter Value on resource
     * @access private
     */
    private function _createAdapter(Adapter $dab)
    {
        // make sure class exists
        if (class_exists(! $dab->class))
            throw new FusionException("Class '{$dab->class}' given in adapter binding does not exist.");

        // create adapter
        $adapter = new $dab->class;

        // make sure it's an adapter
        if (! $adapter instanceof DataAdapter)
            throw new FusionException("Class '{$dab->class}' is not a DataAdapter.");

        return $adapter;
    }
}

?>
