<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\adapter;

use phorce\fusion\binding\DataAdapterBinding;

/**
 * A DataAdapter is a class which performs custom value mappings for property
 * bindings.  DataAdapters must be registered with the DataAdapterFactory before
 * use.  Once registered, a DataAdapter is activated by specifying the adapter
 * in the property binding within the binding file.
 *
 * When data is being read from a resource to be bound to an object, if a
 * DataAdapter was specified for a property, then the {@link #fromResource()}
 * method will be invoked on this adapter.  It will happen after the resource
 * has been read but before the property has been written.
 *
 * When data is being persisted from an object back to a resource, if a
 * DataAdapter was specified for a property, then the {@link #toResource()}
 * method will be invoked on this adapter.  It will happen after the property
 * has been read but before the resource has been written.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
interface DataAdapter
{
    /**
     * Adapts a value when reading from a resource.
     * @param mixed $value Value on resource
     * @return mixed Value on object
     */
    public function fromResource(DataAdapterBinding $dab, $value);

    /**
     * Adapts a value when writing to a resource.
     * @param mixed $value Value on object
     * @return mixed Value on resource
     */
    public function toResource(DataAdapterBinding $dab, $value);
}

?>
