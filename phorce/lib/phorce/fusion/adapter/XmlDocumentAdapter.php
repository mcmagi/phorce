<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\adapter;

use phorce\fusion\binding\DataAdapterBinding;
use phorce\fusion\binding\xml\DocumentAdapter;

/**
 * The XmlDocumentAdapter provides a means to adapt a nested XML document
 * to an object during the resource binding process.  The DataAdapterBinding
 * must be an instance of DocumentAdapter.  A FusionContext is retrieved with
 * the namespace given on the DocumentAdapter.  The passed in value is then
 * either marshalled or unmarshalled as a string value.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlDocumentAdapter implements DataAdapter
{
    /**
     * Adapts a value when reading from a resource.
     * @param mixed $value Value on resource
     * @return mixed Value on object
     */
    public function fromResource(DataAdapterBinding $dab, $value)
    {
        return $this->_getFusionContext($dab)->load()->fromString($value);
    }

    /**
     * Adapts a value when writing to a resource.
     * @param mixed $value Value on object
     * @return mixed Value on resource
     */
    public function toResource(DataAdapterBinding $dab, $value)
    {
        return $this->_getFusionContext($dab)->save($value)->toString();
    }

    /**
     * Returns the FusionContext as configured by the DocumentBinding.
     * @param object DocumentBinding $dbin
     * @return object FusionContext
     * @access private
     */
    private function _getFusionContext(DocumentBinding $dbin)
    {
        return $factory->createForNamespace($dbin->namespace, $dbin->validate);
    }
}

?>
