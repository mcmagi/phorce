<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\adapter;

use phorce\fusion\FusionException;

/**
 * The DataAdapterRegistry provides a means to access data adapters during the
 * object-resource binding process.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DataAdapterRegistry
{
    /**
     * The singleton instance.
     * @var object DataAdapterRegistry
     * @access private
     * @static
     */
    private static $_instance;

    /**
     * Associative array of DataAdapterBinding classes to DataAdapter classes.
     * @var object ArrayObject
     * @access private
     */
    private $_adapters;

    /**
     * Returns the singleton instance.
     * @return object DataAdapterRegistry
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new DataAdapterRegistry();
    }

    /**
     * Private constructor enforces singleton pattern.
     * @access private
     */
    private function __construct()
    {
        $this->_adapters = new ArrayObject();

        // register builtin-adapters
        $this->registerDataAdapter(
            'phorce\fusion\binding\Adapter',
            'phorce\fusion\adapter\DefaultAdapter');
        $this->registerDataAdapter(
            'phorce\fusion\binding\xml\DocumentBinding',
            'phorce\fusion\adapter\XmlDocumentAdapter');

        // TODO - need to allow for registration of custom adapters (somehow)
    }

    /**
     * Looks up and returns the DataAdapter registered for the specified
     * binding.
     * @param object DataAdapterBinding $dab
     * @return object DataAdapter
     */
    public function getAdapter(DataAdapterBinding $dab)
    {
        $bindingClass = get_class($dab);
        if (! isset($this->_adapters[$bindingClass]))
            throw new FusionException("No DataAdapter registered for binding ''");

        $adapterClass = $this->_adapters[$bindingClass];
        $adapter = new $adapterClass;

        if (! $adapter instanceof DataAdapter)
            throw new FusionException("Class '$adapterClass' is not a DataAdapter");
        return $adapter;
    }

    /**
     * Adds an adapter to the registry.  Adapters are keyed by the class used
     * in the binding schema.
     * @param string $bindingClass Binding class
     * @param string $adapterClass DataAdapter class
     */
    public function registerDataAdapter($bindingClass, $adapterClass)
    {
        // verify classes exist
        if (! class_exists($bindingClass))
            throw new FusionException("Binding class '$bindingClass' does not exist.");
        if (! class_exists($adapterClass))
            throw new FusionException("Adapter class '$adapterClass' does not exist.");

        // store in associative array
        $this->_adapters[$bindingClass] = $adapterClass;
    }
}

?>
