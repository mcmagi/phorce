<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\type;

/**
 * The type converter provides a means to convert from a resource-specific data
 * type to a PHP data type and vice-versa.  It is expected that this interface
 * will be implemented for each type of unique conversion routine that is needed
 * for a resource. type represented by the resource.  The
 * PHP data type may map to one or more resource-specific data types.
 *
 * It is worthwhile to note that functionally, the TypeConverter behaves very
 * similarly to a DataAdapter.  But while DataAdapters were designed with
 * flexible extension in mind, TypeConverters may only be extended in a limited
 * fashion.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
interface TypeConverter
{
    /**
     * Returns the PHP type this converter deals with.
     * @return string PHP type
     */
    public function getType();

    /**
     * Returns the PHP class this converter deals with.  Only relevant if PHP
     * type is object.
     * @return string Class name
     */
    public function getClass();

    /**
     * Converts a value when reading from a resource.
     * @param mixed $value Value on resource
     * @return mixed Value in PHP
     */
    public function fromResource($value);

    /**
     * Converts a value when writing to a resource.
     * @param mixed $value Value in PHP
     * @return mixed Value on resource
     */
    public function toResource($value);
}

?>
