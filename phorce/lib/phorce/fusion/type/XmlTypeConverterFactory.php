<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\type;

/**
 * This class is used to create TypeConverters for XML data resources.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlTypeConverterFactory
{
    /**
     * The singleton instance.
     * @staticvar object XmlTypeConverterFactory
     * @access private
     */
    private static $_instance;

    /**
     * Private constructor enforces use of factory method.
     * @access private
     */
    private function __construct()
    {
    }

    /**
     * Returns the singleton instance.
     * @return object XmlTypeConverterFactory
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new XmlTypeConverterFactory();
        return self::$_instance;
    }

    /**
     * Returns the type converter for a given XML type.
     * @param string XML type
     * @return object TypeConverter
     */
    public function findByXmlType($xmlType)
    {
        switch ($xmlType)
        {
        case 'boolean':
            // boolean type
            $type = new BooleanTypeConverter();
            break;
        case 'long':
        case 'integer':
        case 'int':
        case 'short':
        case 'byte':
        case 'nonPositiveInteger':
        case 'positiveInteger':
        case 'nonNegativeInteger':
        case 'negativeInteger':
        case 'unsignedLong':
        case 'unsignedInt':
        case 'unsignedShort':
        case 'unsignedByte':
            // all integer types
            $type = new IntegerTypeConverter();
            break;
        case 'decimal':
            // all floating point types
            $type = new DecimalTypeConverter();
            break;
        case 'double':
        case 'float':
            // all floating point types
            $type = new FloatTypeConverter();
            break;
        case 'duration':
            $type = new DurationTypeConverter();
            break;
        case 'dateTime':
            $type = new DateTimeTypeConverter();
            break;
        case 'date':
            $type = new DateTypeConverter();
            break;
        case 'time':
            $type = new TimeTypeConverter();
            break;
        /* case 'QName':
            $type = new QNameTypeConverter();
            break; */
        default:
            // use string for anything else
            $type = new StringTypeConverter();
        }

        return $type;
    }
}

?>
