<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\type;

use phorce\common\php\PhpType;

/**
 * Converts to and from a PHP integer type.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DecimalTypeConverter implements TypeConverter
{
    /**
     * Returns the PHP type this converter deals with.
     * @return string PHP type
     */
    public function getType()
    {
        return PhpType::TYPE_FLOAT;
    }

    /**
     * Returns the PHP class this converter deals with.  Only relevant if PHP
     * type is object.
     * @return string Class name
     */
    public function getClass()
    {
        return null;
    }

    /**
     * Converts a value when reading from a resource.
     * @param mixed $value Value on resource
     * @return mixed Value in PHP
     */
    public function fromResource($value)
    {
        return (float) $value;
    }

    /**
     * Converts a value when writing to a resource.
     * @param mixed $value Value in PHP
     * @return mixed Value on resource
     */
    public function toResource($value)
    {
        return $this->canonify(sprintf("%F", $value));
    }

    /**
     * Converts PHP form for decimals to XML canonical form.  Removes any
     * trailing 0's, but leaving one if it follows the decimal.
     * @param string $value Exponent form
     * @return string Canonified form
     */
    private function canonify($value)
    {
        return preg_replace('/\.$/', '.0',
            preg_replace('/0*$/', '', $value));
    }
}

?>
