<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\type;

use phorce\common\php\PhpType;

/**
 * Converts to and from a PHP float type to XML float/double types.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class FloatTypeConverter implements TypeConverter
{
    const POS_INFINITY = 'INF';
    const NEG_INFINITY = '-INF';
    const NOT_A_NUMBER = 'NaN';

    /**
     * Returns the PHP type this converter deals with.
     * @return string PHP type
     */
    public function getType()
    {
        return PhpType::TYPE_FLOAT;
    }

    /**
     * Returns the PHP class this converter deals with.  Only relevant if PHP
     * type is object.
     * @return string Class name
     */
    public function getClass()
    {
        return null;
    }

    /**
     * Converts a value when reading from a resource.  If the float value is
     * NaN, INF, or -INF then this string will be returned verbatim.  Otherwise,
     * string-to-float conversion will take place.
     * @param mixed $value Value on resource
     * @return mixed Value in PHP
     */
    public function fromResource($value)
    {
        if ($value == self::NOT_A_NUMBER || $value == self::POS_INFINITY || $value == self::NEG_INFINITY)
            return $value;
        elseif (preg_match('/[Ee]/', $value))
        {
            // see docs for pow() - it may not always return a float
            list($mantissa, $exp) = preg_split('/[Ee]/', $value);
            return $mantissa * pow(10, $exp);
        }
        else
            return (float) $value;
    }

    /**
     * Converts a value when writing to a resource.  Numeric types or numeric
     * strings (identified by is_numeric) will be converted to exponent format.
     * The strings INF and -INF will be returned verbatim.  All other values
     * will return NaN (Not a Number).
     * @param mixed $value Value in PHP
     * @return mixed Value on resource
     */
    public function toResource($value)
    {
        if (is_numeric($value))
        {
            return $this->canonify(sprintf("%e", $value));
        }
        elseif ($value == self::POS_INFINITY || $value == self::NEG_INFINITY)
            return $value;
        else
            return self::NOT_A_NUMBER;
    }

    /**
     * Converts PHP form for exponents to XML canonical form.  Upper-cases the
     * exponent delimiter 'E', removes '+' sign before the exponent, removes
     * trailing 0's, but leaving one if it follows the decimal.
     * @param string $value Exponent form
     * @return string Canonified form
     */
    private function canonify($value)
    {
        return preg_replace('/\.E/', '.0E',
            preg_replace('/0*e\+?/', 'E', $value));
    }
}

?>
