<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\type;

use \DateInterval;
use phorce\common\php\PhpType;

/**
 * Converts to and from a PHP DateInterval object to XML duration types.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DurationTypeConverter implements TypeConverter
{
    /**
     * Returns the PHP type this converter deals with.
     * @return string PHP type
     */
    public function getType()
    {
        return PhpType::TYPE_OBJECT;
    }

    /**
     * Returns the PHP class this converter deals with.  Only relevant if PHP
     * type is object.
     * @return string Class name
     */
    public function getClass()
    {
        return '\DateInterval';
    }

    /**
     * Converts a value when reading from a resource.
     * @param mixed $value Value on resource
     * @return mixed Value in PHP
     */
    public function fromResource($value)
    {
        return new DateInterval($value);
    }

    /**
     * Converts a value when writing to a resource.
     * @param mixed $value Value in PHP
     * @return mixed Value on resource
     */
    public function toResource($value)
    {
        // Usage for DateInterval->format().  Not documented in PHP manual yet,
        // so figured I'd put something here for reference.
        // %Y - two-digit year
        // %y - one-digit year
        // %M - two-digit month
        // %m - one-digit month
        // %D - two-digit day
        // %d - one-digit day
        // %H - two-digit hour
        // %h - one-digit hour
        // %I - two-digit minute
        // %i - one-digit minute
        // %R - direction prefix (+/-)
        // %r - direction prefix (none or -)
        // %S - two-digit second
        // %s - one-digit second
        return $value->format("%rP%yY%mM%dDT%hH%iM%sS");
    }
}

?>
