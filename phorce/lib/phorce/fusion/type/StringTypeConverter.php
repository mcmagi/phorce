<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\type;

use phorce\common\php\PhpType;

/**
 * Converts to and from a PHP string type.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class StringTypeConverter implements TypeConverter
{
    /**
     * Returns the PHP type this converter deals with.
     * @return string PHP type
     */
    public function getType()
    {
        return PhpType::TYPE_STRING;
    }

    /**
     * Returns the PHP class this converter deals with.  Only relevant if PHP
     * type is object.
     * @return string Class name
     */
    public function getClass()
    {
        return null;
    }

    /**
     * Converts a value when reading from a resource.
     * @param mixed $value Value on resource
     * @return mixed Value in PHP
     */
    public function fromResource($value)
    {
        return (string) $value;
    }

    /**
     * Converts a value when writing to a resource.
     * @param mixed $value Value in PHP
     * @return mixed Value on resource
     */
    public function toResource($value)
    {
        return (string) $value;
    }
}

?>
