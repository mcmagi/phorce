<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\type;

/**
 * Converts to and from a PHP DateTime object to XML date types.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DateTypeConverter extends DateTimeTypeConverter
{
    /**
     * Converts a value when writing to a resource.
     * @param mixed $value Value in PHP
     * @return mixed Value on resource
     */
    public function toResource($value)
    {
        list($date, $time) = preg_split('/T/', parent::toResource($value));
        return $date;
    }
}

?>
