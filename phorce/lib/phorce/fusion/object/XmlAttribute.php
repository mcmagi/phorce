<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\object;

use phorce\fusion\QName;

/**
 * This class contains the value stored in an XML element.  This is used
 * primarily for &lt;xsd:anyAttribute&gt; bindings when the type cannot be
 * derived.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class XmlAttribute extends XmlValue
{
    /**
     * Constructs an XML Attribute.
     * @param mixed $value Attribute value
     * @param object QName $qname Attribute qualified name
     */
    public function __construct($value, QName $qname)
    {
        parent::__construct($value, $qname);
    }
}

?>
