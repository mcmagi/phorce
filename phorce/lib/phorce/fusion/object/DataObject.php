<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\object;

use \ArrayAccess;
use \ArrayObject;
use \Countable;
use \IteratorAggregate;
use phorce\common\property\PropertySupport;
use phorce\fusion\FusionException;
use phorce\fusion\binding\ClassBinding;

/**
 * Represents a data object sourced from a resource.  Data may be retrieved
 * from this class using one of several methods. The first is access by
 * property, made available via PropertySupport, whereby a user may use the
 * syntax <code>$obj->property</code> to set or get a property's value.  The
 * second, made available by the ArrayAccess interface, allows access via
 * array syntax: <code>$obj['property']</code>.  Third, a user may iterate
 * through the properties of the data object using a <code>foreach</code> loop
 * via the IteratorAggregate interface.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class DataObject implements IteratorAggregate, ArrayAccess, Countable,
    PropertySupport
{
    /**
     * The class binding this data object represents.
     * @var object ClassBinding
     * @access private
     */
    private $_binding;

    /**
     * An associative array of property values keyed by property names.
     * @var object ArrayObject
     * @access private
     */
    private $_properties;

    /**
     * Constructs a Data Object.
     * @param object ClassBinding Property information
     */
    public function __construct(ClassBinding $binding)
    {
        $this->_binding = $binding;
        $this->_properties = new ArrayObject();

        // initialize properties
        for ($binding->properties as $prop)
        {
            // initialize property value
            if ($prop->array)
                $this->_properties[$prop->name] = new ArrayObject();
            else
                $this->_properties[$prop->name] = null;
        }
    }

    /**
     * Returns the number of properties in this data object.
     * @return integer
     */
    public function count()
    {
        return $this->_properties->count();
    }

    /**
     * Returns an Iterator used to loop through properties in this data object.
     * @return object Iterator
     */
    public function getIterator()
    {
        return $this->_properties->getIterator();
    }

    /**
     * Returns the property with the specified name.
     * @param string $name Property name
     * @return mixed Property value
     */
    public function offsetGet($name)
    {
        return isset($this->$name) ? $this->_properties[$name] : null;
    }

    /**
     * Sets the property with the specified name to the specified value.
     * @param string $name Property name
     * @param mixed $value Property value
     */
    public function offsetSet($name, $value)
    {
        if (! isset($this->$name))
            throw new FusionException("Property '{$name}' does not exist in this DataObject");

        // TODO: cannot set readonly properties

        $this->_properties[$name] = $value;
    }

    /**
     * Returns true if the property exists in this data object.
     * @param string $name Property name
     */
    public function offsetExists($name)
    {
        return isset($this->_properties[$name]);
    }

    /**
     * Properties cannot be unset.  This method throws an exception.
     * @param string $name Property name
     */
    public function offsetUnset($name)
    {
        throw new FusionException("Cannot unset properties of an DataObject");
    }

    /**
     * Returns the property with the specified name.
     * @param string $name Property name
     * @return mixed Property value
     */
    public function __get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     * Sets the property with the specified name to the specified value.
     * @param string $name Property name
     * @param mixed $value Property value
     */
    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
    }

    /**
     * Returns true if the property exists in this data object.
     * @param string $name Property name
     */
    public function __isset($name)
    {
        return $this->offsetExists($name);
    }

    /**
     * Properties cannot be unset.  This method throws an exception.
     * @param string $name Property name
     */
    public function __unset($name)
    {
        $this->offsetUnset($name);
    }
}

?>
