<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion\object;

use phorce\common\property\PropertyObject;
use phorce\fusion\QName;

/**
 * This class contains the value stored in an XML data element.  It may contain
 * the value from either an element OR an attribute.  This is used primarily
 * for &lt;xsd:any&gt; and &lt;xsd:anyAttribute&gt; bindings when the type
 * cannot be derived.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
abstract class XmlValue extends PropertyObject
{
    /**
     * The element qualified name.
     * @var object QName
     * @access private
     */
    private $_qname;

    /**
     * The element value.
     * @var mixed
     * @access private
     */
    private $_value;

    /**
     * Constructs an XML Value.
     * @param mixed $value XML value
     * @param object QName $qname XML object's qualified name
     */
    protected function __construct($value, QName $qname)
    {
        $this->_value = $value;
        $this->_qname = $qname;
    }

    /**
     * Returns the element qualified name.
     * @return object QName
     */
    public function getQName()
    {
        return $this->_qname;
    }

    /**
     * Returns the element value.
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Sets the element value.
     * @param mixed $v
     */
    public function setValue($v)
    {
        $this->_value = $v;
    }
}

?>
