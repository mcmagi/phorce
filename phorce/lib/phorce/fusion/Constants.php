<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce\fusion;

/**
 * Contains common constants for the Fusion package.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Fusion
 */
class Constants
{
    /**
     * Namespace URI of of XML Schema Definition (XSD) schema.
     */
    const XSD_NAMESPACE_URI = 'http://www.w3.org/2001/XMLSchema';

    /**
     * Namespace URI of of XML Schema Instance (XSI) schema.
     */
    const XSI_NAMESPACE_URI = 'http://www.w3.org/2001/XMLSchema-instance';
}

?>
