<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

namespace phorce;

use \Exception;

/**
 * Represents an exception which occurred during processing within the Phorce
 * framework.
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsf.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */
class PhorceException extends Exception
{
    /**
     * Another exception which is associated with this exception.  Typically,
     * this variable contains the exception which caused this exception.
     * @var object Exception
     * @access private
     */
    private $_cause;


    /**
     * Constructs a new error.  The backtrace is determined on construction.
     * @param string $message An optional message text for the exception
     * @param string $code An optional error code for the exception
     * @param object Exception $cause An optional exception associated with this exception
     */
    public function __construct($message = "", $code = 0,
        Exception $cause = null)
    {
        parent::__construct($message, $code);

        $this->_cause = $cause;
    }

    /**
     * Returns the exception which caused this exception, if any.
     * @return object Exception
     */
    public function getCause()
    {
        return $this->_cause;
    }
}

?>
