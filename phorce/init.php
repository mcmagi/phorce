<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

/**
 * Initialization script for the Phorce Web Framework.  Sets up global
 * definitions, autoloaders, and include paths needed for the framework to work
 * properly.
 *
 * This script must be included before any Phorce-specific behavior is used.
 * If all requests are rewritten to the dispatch.php script via .htaccess (the
 * default setup) then it will always be included and need not be included
 * explicitly.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsg.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */

namespace phorce;

/**
 * The root of the Phorce Web Framework.  It is typically a directory named
 * "phorce" and must be one level below the web context root.
 */
define('PHORCE_HOME', __DIR__);

/**
 * The location of Phorce configuration files.
 */
define('PHORCE_CONFIG', makepath(PHORCE_HOME, "config"));

/**
 * The location of libraries upon which the application depends.  This includes,
 * among other things, Phorce itself.
 */
define('PHORCE_LIB', makepath(PHORCE_HOME, "lib"));

/**
 * The location of web application classes.
 */
define('PHORCE_CLASSES', makepath(PHORCE_HOME, "classes"));

/**
 * The location of Phorce working data.  The contents of this directory are
 * considered volatile data by the framework and can therefore be deleted at
 * will.
 */
define('PHORCE_WORK', makepath(PHORCE_HOME, "work"));

/**
 * The web application context root.  It must be one level above the Phorce
 * Home directory.
 */
define('PHORCE_WEBROOT', realpath(makepath(PHORCE_HOME, "..")));

// for now - must provide way to override this
ini_set('display_errors', 'On');
session_cache_limiter('none');
error_reporting(E_ALL);
ini_set('magic_quotes_gpc', 'Off');
ini_set('log_errors_max_len', 2048); // to see more of stack traces

// set up include path
$includes = array();
$includes[] = PHORCE_WEBROOT;
$includes[] = PHORCE_LIB;
$includes[] = PHORCE_CLASSES;
$includes[] .= ini_get('include_path');
ini_set('include_path', implode($includes, PATH_SEPARATOR));

// register autoloaders
spl_autoload_register('phorce\namespace_autoloader');
spl_autoload_register('phorce\pear_autoloader');

/**
 * Attempts to autoload the class as a namespace-packaged class.
 * @param string $classname
 */
function namespace_autoloader($classname)
{
    callback_autoloader($classname, 'phorce\namespace_resolve_filename');
}

/**
 * Attempts to autoload the class as a PEAR-packaged class.
 * @param string $classname
 */
function pear_autoloader($classname)
{
    callback_autoloader($classname, 'phorce\pear_resolve_filename');
}

function namespace_resolve_filename($classname)
{
    return preg_replace("/\\\\/", DIRECTORY_SEPARATOR, $classname) . ".php";
}

function pear_resolve_filename($classname)
{
    return preg_replace("/_/", DIRECTORY_SEPARATOR, $classname) . ".php";
}

/**
 * Autoload the class using the specified callback function.
 * @param string $classname
 * @param string $callback Function name
 */
function callback_autoloader($classname, $callback)
{
    $include_path = get_include_path();
    $include_path_tokens = explode(':', $include_path);
     
    foreach (explode(PATH_SEPARATOR, get_include_path()) as $dir)
    {
        $path = $dir . DIRECTORY_SEPARATOR . $callback($classname);
        //echo "attempting to autoload $classname as $path<br/>\n";
        if (file_exists($path)) {
            //echo "autoloading $classname as $path<br/>\n";
            require_once $path;
            return;
        }
    }
}

function makepath()
{
    $args = array();
    for ($i = 0; $i < func_num_args(); $i++)
        $args[] = func_get_arg($i);
    return implode($args, DIRECTORY_SEPARATOR);
}

?>
