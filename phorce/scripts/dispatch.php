<?php
/*
 * The contents of this file are subject to the terms of the GNU Lesser
 * General Public License Version 3 ("LGPL").  You may not use this
 * file except in compliance with the License.  When distributing the
 * software, include this License Header Notice in each file and
 * include the License file at phorce/LICENSE.txt.
 */

/**
 * Web request dispatcher script for the Phorce Web Framework.  By default, the
 * .htaccess rewrites all requests to call this script, which sets up the Phorce
 * environment (via init.php) before dispatching the request.  Depending on the
 * request path, a different RequestDispatcher may be used to handle the
 * request.  Thus, different paths may be treated differently and in a very
 * customizable fashion.
 *
 * @author Michael C. Maggio <mcmaggio@mcmagi.com>
 * @copyright Copyright (c) 2008-2009, Michael C. Maggio
 * @license http://www.fsg.org/copyleft/lgpl.html GNU Lesser General Public License v3
 * @package Phorce
 * @subpackage Common
 */

require_once "../init.php";

use phorce\common\web\ApplicationContext;

$ctx = ApplicationContext::getInstance();
$ctx->dispatchRequest();

?>